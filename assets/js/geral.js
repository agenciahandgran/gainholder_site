		
$(function(){

		// // SLICKNAV
		// $('.menu-superior').slicknav({
		// 	'label' : 'MENU',
		// 	'prependTo' : '.menu-mobile'
		// });

		// // BANNER APP
		// $.smartbanner({ 
		// 	daysHidden: 5,
		//  	daysReminder: 15, 
		//  	title:'Exemplo',
		//  	price: 'Grátis',
		//  	inGooglePlay: 'no Google Play',
		//  	inAppStore: 'no App Store',
		//  	inWindowsStore: 'no Windows Store',
		//  	button: 'Baixar',
		//  	author: 'Exemplo',
		//  	scale: 'auto',
	 // 	});

	 $(".cep").mask("99999-999");

	 /* BUSCA RÁPIDA DE CEP */
	 $('.cep').keyup(function(event) {

	 	if(event.keyCode == '13') {

	 		event.preventDefault();
	 	}

	 	Delay(

	 		function(){

	 			BuscarCep();
	 		}, 

	 		800
	 		);  

	 });

	});

	function BuscarCep(URL){

		var cep        = $('.cep').val();
		var app_key    = "TZsFj7W7keODIb4GB0T1IxyCTITKqW9V";
		var app_secret = "UBNOH22FaepmcvM9Rw7lrbmgYtYMANtkHN8CQqqUW7hfLmza";

		$.ajax({
			url: "https://webmaniabr.com/api/1/cep/"+cep+"/?app_key="+app_key+"&app_secret="+app_secret,
			type: 'GET',
			data : "",
			success: function (resp) {

				if(typeof(resp) != "undefined" && resp !== null){

					if(resp.Erro){

						alert(resp.Erro);
					}

					cep            = cep.replace('-', '');

					var estado     = resp.uf;
					var cidade     = resp.cidade;
					var bairro     = resp.bairro;
					var logradouro = resp.endereco;
					
					$('.estado option').removeAttr('selected');
					$('.estado option[value="' + estado + '"]').attr('selected', 'selected');
					$('.cidade option').removeAttr('selected');
					$('.cidade option[value="' + cidade + '"]').attr('selected', 'selected');
					$('input.uf').val(estado);
					$('input.cidade').val(cidade);
					$('input.logradouro').val(logradouro);
					$('input.bairro').val(bairro);
					$(".numero").focus();
				}
			}        
		});
	}

	/* PLUGIN DELAY P/ KEYUP */ 
	var Delay = (function(){

		var timer = 0;
		return function(callback, ms){

			clearTimeout(timer);
			timer = setTimeout(callback, ms);
		};
	})();