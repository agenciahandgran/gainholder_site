   $(function(){

       $("#cpf").mask('999.999.999-99');

       var navListItems = $('ul.setup-panel li a'),
       allWells = $('.setup-content');

       allWells.hide();

       navListItems.click(function(e){
        e.preventDefault();
        var $target = $($(this).attr('href')),
        $item = $(this).closest('li');

        if (!$item.hasClass('disabled')) {
            navListItems.closest('li').removeClass('active');
            $item.addClass('active');
            allWells.hide();
            $target.show();
        }
    });

       $('ul.setup-panel li.active a').trigger('click');

        // PASSO 2
        $('#passo-2').on('click', function(e) {

            $('ul.setup-panel li:eq(1)').removeClass('disabled');
            $('ul.setup-panel li a[href="#step-2"]').trigger('click');

            $.ajax({
                url: URL+"/library/Requests.php",
                type: 'GET',
                data : {
                    modulo  : 'assinatura',
                    acao    : 'detalharDadosUsuario'
                },
                dataType: 'json',
                cache: false,
                success: function (resp) {

                    if(typeof(resp) != "undefined" && resp != null){    

                        // VERIFICA QUAL AÇÃO REALIZAR (CADASTRO/ATUALIZAÇÃO)
                        if(resp.cep == null){
                            $("#acao").val("1");
                        }else{
                            $("#acao").val("2");
                        }

                        // DETALHES CLIENTE
                        $("#cpf").val(resp.cpf);
                        $("#cep").val(resp.cep);
                        $("#endereco").val(resp.endereco);
                        $("#numero").val(resp.numero);
                        $("#bairro").val(resp.bairro);
                        $("#cidade").val(resp.cidade);
                        $("#uf").val(resp.uf);
                        $("#complemento").val(resp.complemento);

                    }else{
                        console.log('Ocorreu um erro na requisição');
                    }

                },

                error: function(resp) {
                    console.log('Ocorreu um erro na resposta');
                }
            });
        });  

        // PASSO 3
        $('#passo-3').on('click', function(e) {
            proximaEtapa = true;
            $(".verificador").each(function(){
                if($(this).val() == ""){
                    proximaEtapa = false;
                    return false;
                }
            });

            if(proximaEtapa){

                // DETALHES CLIENTE
                var acao        = $("#acao").val();
                var cpf         = $("#cpf").val();
                var cep         = $("#cep").val();
                var endereco    = $("#endereco").val();
                var numero      = $("#numero").val();
                var bairro      = $("#bairro").val();
                var cidade      = $("#cidade").val();
                var uf          = $("#uf").val();
                var complemento = $("#complemento").val();

                $.ajax({
                    url: URL+"/library/Requests.php",
                    type: 'POST',
                    data : {
                        modulo      : 'assinatura',
                        acao        : 'atualizarDadosUsuario',
                        acaoUsuario : acao,
                        cpf         : cpf,
                        cep         : cep,
                        endereco    : endereco, 
                        numero      : numero,
                        bairro      : bairro,
                        cidade      : cidade,
                        uf          : uf,
                        complemento : complemento                   
                    },
                    dataType: 'json',
                    cache: false,
                    success: function (resp) {

                        if(typeof(resp) != "undefined" && resp != null){    

                            if(resp.status == 1){

                                $('ul.setup-panel li:eq(2)').removeClass('disabled');
                                $('ul.setup-panel li a[href="#step-3"]').trigger('click');
                                $(".mensagem-erro").fadeOut('slow');
                            
                            }else{
                                $(".mensagem-erro").fadeIn('slow');
                            }
                            

                        }else{
                            console.log('Ocorreu um erro na requisição');
                        }

                    },

                    error: function(resp) {
                        console.log('Ocorreu um erro na resposta');
                    }
                });


            }else{
               $(".mensagem-erro").fadeIn('slow');
           }
        });   

        // RECUPERAR BANDEIRA DO CARTÃO
        $("#cartaoNumero").keyup(function(){

            var cartaoBin = $(this).val().substring(0, 6);

            if(cartaoBin.length >= 6){

                PagSeguroDirectPayment.getBrand({

                    cardBin: cartaoBin,

                    success: function(response) {

                        var brand = response.brand.name;
                        
                        $("#cartaoBrand").val(brand);                    
                        
                    },

                    error: function(response) {
                        console.log(response);
                    },

                    complete: function(response) {
                        console.log('Completo erro cartão '+response.error);
                    }

                });
            }
        });

        var showLoading = function(text) {
    
            $.colorbox({
                html: "<h1>Aguarde...</h1>",
                transition: 'none',
                close: false,
                escKey: false,
                overlayClose: false,
                fixed: true
            });
        
        };

        // FINALIZAR PAGAMENTO E GERAR TOKEN PARA CARTÃO DE CRÉDITO
        $("#finalizar-pagamento").click(function(){
            
            finalizarPagamento = true;

            $(".verificadorCartao").each(function(){
                if($(this).val() == ""){
                    finalizarPagamento = false;
                    return false;
                }
            });

            if(finalizarPagamento){
                
                $('.background-loader').fadeIn();

                $(".mensagem-pagamento").fadeOut('slow');

                var planoId = $("#planoId").val();

                PagSeguroDirectPayment.createCardToken({

                    cardNumber: $("#cartaoNumero").val(),
                    brand: $("#cartaoBrand").val(),
                    cvv: $("#cartaoCodigoSeguranca").val(),
                    expirationMonth: $("#cartaoMes").val(),
                    expirationYear: $("#cartaoAno").val(),

                    success: function(response) {

                        // Obtendo token para pagamento com cartão
                        var token = response.card.token;
                        var hash  = PagSeguroDirectPayment.getSenderHash();

                        $.ajax({
                            url: URL+"/library/Requests.php",
                            type: 'GET',
                            data : {
                                modulo  : 'assinatura',
                                acao    : 'realizarPagamento',
                                token   : token,
                                hash    : hash,
                                planoId : planoId
                            },
                            dataType: 'json',
                            cache: false,
                            success: function (resp) {

                                if(typeof(resp) != "undefined" && resp != null){    

                                    if(resp.status == 1){
                                        
                                        $('.background-loader').fadeOut('');
                                        
                                        $(".mensagem-pagamento").addClass('alert-success').fadeIn('slow');
                                        $(".mensagem-pagamento-texto").text('Assinatura efetuada com sucesso!');

                                        setTimeout(function(){
                                            window.location = URL+'/clientes/perfil';
                                        }, 2000);   

                                    }else{
                                        
                                        $('.background-loader').fadeOut('');

                                        $(".mensagem-pagamento").addClass('alert-danger').fadeIn('slow');
                                        $(".mensagem-pagamento-texto").text('Ocorreu um erro durante o pagamento. Verique se os dados estão preenchidos corretamente.');
                                    }

                                }else{
                                    console.log('Ocorreu um erro na requisição');
                                }

                            },

                            error: function(resp) {
                                console.log('Ocorreu um erro na resposta');
                            }
                        }); 

                    },

                    error: function(response) {

                        console.log('Error: '+response.errors);

                    },

                    complete: function(response) {
                        console.log('completou');
                    }

                });

            }else{
                $(".mensagem-pagamento").addClass('alert-danger').fadeIn('slow');
                $(".mensagem-pagamento-texto").text('Preencha todos os campos');
            }

        });        

        $(".modal-pagamento").click(function(e){

            e.preventDefault();

            var btn     = $(this);
            var planoId = $(this).attr('data-id');
            
            $.ajax({
                url: URL+"/library/Requests.php",
                type: 'GET',
                data : {
                    modulo  : 'assinatura',
                    acao    : 'detalharDadosPlano',
                    planoId : planoId
                },
                dataType: 'json',
                cache: false,
                success: function (resp) {

                    if(typeof(resp) != "undefined" && resp != null){    

                        if(resp.status == '1'){

                            // DETALHES PAGAMENTO
                            $(".plano-nome").text(' Gainholder '+resp.plano);
                            $(".plano-preco-titulo").text(' Você está prestes a pagar '+resp.preco);
                            $(".plano-desconto").text(resp.desconto+'%');
                            $(".plano-preco").text(resp.preco);
                            $("#planoId").val(planoId);

                            dataModal = btn.attr("data-modal");
                            $("#" + dataModal).css({"display":"block"});

                        }else if(resp.status == '3'){

                            swal({

                                title: 'Informação',
                                text: resp.mensagem,
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Login',
                                cancelButtonText: 'Cancelar',
                                confirmButtonClass: 'btn btn-success',
                                cancelButtonClass: 'btn btn-danger',
                                buttonsStyling: false

                            }).then(function () {

                                window.location = URL+"/clientes/login/assinantes";


                            }, function (dismiss) {

                                if (dismiss === 'cancel') {
                                    swal(
                                        'Cancelado',
                                        'Operação cancelada pelo usuário.',
                                        'error'
                                        );
                                }
                            });
                        }

                    }else{
                        console.log('Ocorreu um erro na requisição');
                    }

                },

                error: function(resp) {
                    console.log('Ocorreu um erro na resposta');
                }

            });

        });

        $(".close-modal, .modal-sandbox").click(function(){
            $(".modal").css({"display":"none"});
        });

    });