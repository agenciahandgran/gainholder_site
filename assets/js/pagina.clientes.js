$(function(){


	// AÇOES PÁGINA: LOGIN
	$('#formulario-login button[type="submit"]').click(function(e){

		e.preventDefault();	

		// VALIDAÇÃO DO FORMULÁRIO DE CADASTRO DE EXEMPLO
		$('#formulario-login').validate({
			rules:{
				email:{
					required: true,
					email:true
				},
				senha:{
					required: true
				}
			},
			messages:{
				email: {
					required: "Campo obrigatório.",
					email: "E-mail com formato incorreto."
				},
				senha:{
					required: "Campo obrigatório."
				}				
			}
		});

		if($("#formulario-login").valid()){

			$("#formulario-login").submit();
		}

	});

	// AÇÕES PÁGINA : CADASTRO
	$("#data_nascimento").mask('99/99/9999');

	$('#telefone').focusout(function(){
		var phone, element;
		element = $(this);
		element.unmask();
		phone = element.val().replace(/\D/g, '');
		if(phone.length > 10) {
			element.mask("(99) 99999-999?9");
		} else {
			element.mask("(99) 9999-9999?9");	
		}
	}).trigger('focusout');

	$('#formulario-cadastro button[type="submit"]').click(function(e){

		e.preventDefault();	

		// VALIDAÇÃO DO FORMULÁRIO DE CADASTRO
		$('#formulario-cadastro').validate({
			rules:{
				nome_completo: {
					required: true
				},
				email:{
					required: true,
					email:true,
					remote: {
						url: URL+"/library/Requests.php",
						type: 	"GET",
						dataType: "json",
						data: {
							modulo : "clientes",
						    acao   : "verificaEmail"
						}
					}
				},
				data_nascimento:{
					required: true,
					dateBR: true
				},
				senha:{
					required: true
				},
				termos_uso:{
					required: true
				}
			},
			messages:{
				nome_completo: {
					required: "Campo obrigatório."
				},
				email: {
					required: "Campo obrigatório.",
					email: "E-mail com formato incorreto.",
					remote: "Este e-mail já está cadastrado no nosso sistema!"
				},
				data_nascimento:{
					required: "Campo obrigatório.",
					dateBR: " Insira uma data válida."
				},
				senha:{
					required: "Campo obrigatório."
				},
				termos_uso:{
					required: "Campo obrigatório."
				}				
			}
		});

		if($("#formulario-cadastro").valid()){

			$("#formulario-cadastro").submit();
		}

	});

	// AÇÕES PÁGINA: RECUPERAR SENHA
	$('#formulario-recuperar-senha input[type="submit"]').click(function(e){

		e.preventDefault();	

		// VALIDAÇÃO DO FORMULÁRIO DE CADASTRO DE EXEMPLO
		$('#formulario-recuperar-senha').validate({
			rules:{
				email:{
					required: true,
					email:true
				}
			},
			messages:{
				email: {
					required: "Campo obrigatório.",
					email: "E-mail com formato incorreto."
				}				
			}
		});

		if($("#formulario-recuperar-senha").valid()){

			$("#formulario-recuperar-senha").submit();
		}

	});

	// AÇÕES PÁGINA: ESQUECEU SENHA
	$('#formulario-esqueceu-senha input[type="submit"]').click(function(e){

		e.preventDefault();	

		// VALIDAÇÃO DO FORMULÁRIO DE CADASTRO DE EXEMPLO
		$('#formulario-esqueceu-senha').validate({
			rules:{
				senha:{
					required: true,
				},
				confirmarSenha:{
					required: true,
					equalTo: "#senha"
				},
			},
			messages:{
				senha: {
					required: "Campo obrigatório.",
				},
				confirmarSenha: {
					required: "Campo obrigatório",
					equalTo: "Senhas diferentes."
				},				
			}
		});

		if($("#formulario-esqueceu-senha").valid()){

			$("#formulario-esqueceu-senha").submit();
		}

	});

	// AÇÕES PÁGINA: RECUPERAR SENHA
	$('#formulario-alterar-senha input[type="submit"]').click(function(e){

		e.preventDefault();	

		// VALIDAÇÃO DO FORMULÁRIO DE CADASTRO DE EXEMPLO
		$('#formulario-alterar-senha').validate({
			rules:{
				senhaAtual:{
					required: true,
				},
				novaSenha:{
					required: true,
				},
				confirmarSenha:{
					required: true,
					equalTo: "#novaSenha"
				},
			},
			messages:{
				senhaAtual: {
					required: "Campo obrigatório.",
				},
				novaSenha: {
					required: "Campo obrigatório.",
				},
				confirmarSenha: {
					required: "Campo obrigatório",
					equalTo: "Senhas diferentes."
				},				
			}
		});

		if($("#formulario-alterar-senha").valid()){

			$("#formulario-alterar-senha").submit();
		}

	});

	// AÇÕES PÁGINA: EDITAR DADOS
	$('#celular').focusout(function(){
		var phone, element;
		element = $(this);
		element.unmask();
		phone = element.val().replace(/\D/g, '');
		if(phone.length > 10) {
			element.mask("(99) 99999-999?9");
		} else {
			element.mask("(99) 9999-9999?9");	
		}
	}).trigger('focusout');

	$("#cpf").mask('999.999.999-99');
	$("#cnpj").mask('99.999.999/9999-99');	
	$("#rg").mask('99.999.999-9');
	$("#cep").mask('99999-999');

	$("#tipo_pessoa").change(function(){

		var tipoPessoa = $(this).val();

		if(tipoPessoa == 1){
			$("#cnpj").fadeOut().val("");
			$("#cpf").fadeIn();
		}else{
			$("#cpf").fadeOut().val("");
			$("#cnpj").fadeIn();
		}
	});

	$('#formulario-edicao input[type="submit"]').click(function(e){

		e.preventDefault();	

		// VALIDAÇÃO DO FORMULÁRIO DE CADASTRO DE EXEMPLO
		$('#formulario-edicao').validate({
			rules:{
				nome_completo:{
					required: true,
				},
				cpf:{
					required: true,
					cpf: true
				},
				cnpj:{
					required: true,
					cnpj: true
				},
				telefone:{
					required: true,
				},
				cep:{
					required: true,
				},
				numero:{
					required: true,
					number: true
				},
				email:{
					required: true,
					email:true,
					remote: {
						url: URL+"/library/Requests.php",
						type: 	"GET",
						dataType: "json",
						data: {
							modulo : "clientes",
						    acao   : "verificaEmail",
						    emailAtual: $("#emailAtual").val()
						}
					}
				}
			},
			messages:{
				nome_completo:{
					required: "Campo obrigatório.",
				},
				cpf:{
					required: "Campo obrigatório.",
					cpf: "CPF com formato inválido!"
				},
				cnpj:{
					required: "Campo obrigatório.",
					cnpj: "CNPJ com formato inválido!"
				},				
				telefone:{
					required: "Campo obrigatório.",
				},
				cep:{
					required: "Campo obrigatório.",
				},
				numero:{
					required: "Campo obrigatório.",
					number: "Somente números."
				},
				email:{
					required: "Campo obrigatório.",
					email: "Formato incorreto para o e-mail.",
					remote: "Este e-mail já está cadastrado no nosso sistema!"
				}				
			}
		});

		if($("#formulario-edicao").valid()){

			$("#formulario-edicao").submit();
		}

	});

});
