$(function(){

	// AÇOES PÁGINA: CONTATOS

	$('#telefone').focusout(function(){
	    var phone, element;
	    element = $(this);
	    element.unmask();
	    phone = element.val().replace(/\D/g, '');
	    if(phone.length > 10) {
	        element.mask("(99) 99999-999?9");
	    } else {
	        element.mask("(99) 9999-9999?9");	
	    }
	}).trigger('focusout');

	$('#celular').focusout(function(){
	    var phone, element;
	    element = $(this);
	    element.unmask();
	    phone = element.val().replace(/\D/g, '');
	    if(phone.length > 10) {
	        element.mask("(99) 99999-999?9");
	    } else {
	        element.mask("(99) 9999-9999?9");	
	    }
	}).trigger('focusout');

	$("#cnpj").mask("99.999.999/9999-99");

	$('#formulario-contato input[type="submit"]').click(function(e){

		e.preventDefault();	

		// VALIDAÇÃO DO FORMULÁRIO DE CADASTRO DE EXEMPLO
		$('#formulario-contato').validate({
	        rules:{
				nome:{
					required: true,
				},
				telefone:{
					required: true,
				},
				email_corporativo:{
					required: true,
					email: true
				},
				mensagem:{
					required: true,
				}
	        },
	        messages:{
				nome:{
					required: "Campo obrigatório.",
				},
				telefone:{
					required: "Campo obrigatório.",
				},
				email_corporativo:{
					required: "Campo obrigatório.",
					email: "E-mail com formato inválido."
				},
				mensagem:{
					required: "Campo obrigatório.",
				}				
	        }
	    });

	    if($("#formulario-contato").valid()){

	    	$("#formulario-contato").submit();
		}

	});


});