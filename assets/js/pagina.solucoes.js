	$(function(){
		
		etapasIndex = ["#process1", "#process2", "#process3", "#process4", "#process5", "#final"];

		$("#process1").proximaEtapa(etapasIndex);

		// TELA: INDEX - PASSO 1 
		$("#continuar01").click(function(){
			
			// VERIFICA A QUANTIDADE DE NCM'S E DECIMAIS SELECIONADO
			var verificaNcm      = $(".opcaoNcm").listarCampos('.qtdNcm',1);
			var verificaDecimais = $(".casasDecimais").listarCampos('.qtdCasasDecimais',1);

			// VERIFICA AS VALIDAÇÕES PARA ACESSAR A PRÓXIMA ETAPA
			if(!(verificaNcm) && !(verificaDecimais)){
				$("#process2").proximaEtapa(etapasIndex);

			}else{
				exibirMensagemAlerta("Na index é possível seleção de somente 1 NCM. Para outras ações acesse a página da VAMCO.",
					"/solucoes/vanco","Vamco");
			}
			
		});

		$("#continuar02").click(function(){

			var validador = false;

			$(".tiposEmbarques option").each(function(){
				if($(this).is(':selected')){

					if($(this).val() != ""){
						$(".tipoEscolhidoEmbarque").val($(this).val());
						validador = true;
					}else{
						validador = false;
					}
				}
			});
			
			if(validador){
				$("#process3").proximaEtapa(etapasIndex);	
			}else{
				exibirMensagemAlerta("Selecione uma opção para continuar.",
					"/solucoes/index","Voltar");
			}
		});

		$(".produtoValor").maskMoney();

		$(".botaoadd").click(function(){
			
			var qtdProdutoGerado = $(".qtdProdutoGerado");
			var qtdNcm           = $(".qtdNcm").val();
			
			if(qtdProdutoGerado.val() <= 1){

				$(".formulario-produtos").last().clone().prependTo("#campo-adiciona-formulariobox");
				
				$(".formulario-produtos").each(function(i){
					i++;
					$(this).children('.perguntas4').text('Item '+i);
					qtdProdutoGerado.val(i+1);
				});

				$(".formulario-produtos").last().children('#alignform').children().children().not('input[type=submit]').val("");
				$(".formulario-produtos").last().children('#alignform').children().children().children().val("");
				$(".produtoValor").maskMoney();

			}else{
				exibirMensagemAlerta("No index só é permitido até 2 itens. ","/solucoes/index","Inicio");
			}
		});
		
		$("#continuar03").click(function(){

			var verificador = $(".validador").validador();

			if(verificador){
				
				// RECUPERAR OS VALORES PREENCHIDOS NOS CAMPOS DE PRODUTOS
				$(".produtoNome").preencheValores(".produtosNome");
				$(".produtoValor").preencheValores(".produtosPreco");
				$(".produtoQuantidade").preencheValores(".produtosQuantidade");
				$(".produtoCodigoNcm").preencheValores(".produtosCodigosNcm");

				$("#process4").proximaEtapa(etapasIndex);

			}else{
				exibirMensagemAlerta("Existem campos em brancos. Por favor preencha todos!","/solucoes/index","Inicio");
			}
			
		});

		$(".valorFrete").maskMoney();

		$("#continuar04").click(function(){
			
			var verificadorAereo   = $(".validadorFreteAereo").validador();
			var verificadorMaritmo = $(".validadorFreteMaritmo").validador();

			if(verificadorAereo == true || verificadorMaritmo == true){
				
				var tipoFrete  = 0;
				var valorFrete = $(".valorFrete").val(); 

				if(verificadorMaritmo == true){
					tipoFrete = 1;
				}	

				if(verificadorAereo == true){
					tipoFrete = 2;
				}

				$(".tipoFreteSelecionado").val(tipoFrete);
				$(".valorFreteSelecionado").val(valorFrete);
				
				// GERA UM RESUMO APRESENTADO NA TELA
				$(".resumo").gerarResumo(false);
				
				$("#process5").proximaEtapa(etapasIndex);
						
			}else if(verificadorAereo == true && verificadorMaritmo == true){
				exibirMensagemAlerta("Só é permitido a seleção de uma opção!","/solucoes/index","Inicio");
			}else{
				exibirMensagemAlerta("Selecione uma das opções!","/solucoes/index","Inicio");
			}			
		});

		$("#voltar01").click(function(){
			$("#process1").proximaEtapa(etapasIndex);
		});

		$("#voltar02").click(function(){
			$(".formulario-produtos").not(':first').remove();
			$(".qtdProdutoGerado").val(0);
			$(".produtosNome").val("");
			$(".produtosPreco").val("");
			$(".produtosQuantidade").val("");
			$(".produtosCodigosNcm").val("");			
			$("#process2").proximaEtapa(etapasIndex);
		});

		$("#voltar03").click(function(){
			$("#process3").proximaEtapa(etapasIndex);
		});

		$("#voltar04").click(function(){
			$("#process4").proximaEtapa(etapasIndex);
		});

		$("#enviar").click(function(){
			
			$('.background-loader').fadeIn();

			// ENVIA OS DADOS PARA EFETUAÇÃO DOS CÁLCULOS, DISPARO DE E-MAIL E GERA HISTÓRICO
			var texto =	$(".resumo").gerarResumo(true);
			var tipoEmbarque       = $(".tipoEscolhidoEmbarque").val();
			var produtosNome       = $(".produtosNome").val();
			var produtosPreco      = $(".produtosPreco").val();
			var produtosQuantidade = $(".produtosQuantidade").val();
			var produtosCodigosNcm = $(".produtosCodigosNcm").val();
			var tipoFrete          = $(".tipoFreteSelecionado").val();
			var valorFrete         = $(".valorFreteSelecionado").val();

            $.ajax({
                url: URL+"/library/Requests.php",
                type: 'POST',
                data : {
                    modulo             : 'solucoes',
                    acao               : 'gerarHistorico',
                    texto              : texto,
                    tipoEmbarque       : tipoEmbarque,
                    produtosNome       : produtosNome,
                    produtosPreco      : produtosPreco,
                    produtosQuantidade : produtosQuantidade,
                    produtosCodigosNcm : produtosCodigosNcm,
                    tipoFrete          : tipoFrete,
                    valorFrete         : valorFrete 
                },
                dataType: 'json',
                cache: false,
                success: function (resp) {

                    if(typeof(resp) != "undefined" && resp != null){    

                    	if(resp.status == 1){
                    		
                    		$('.background-loader').fadeOut('');
							etapasIndex.push(".banner");
							$("#final").proximaEtapa(etapasIndex);
							$(".mensagemFinal").text(" Sucesso! Foi encaminhado em seu e-mail a análise Index Basic.");
                    	}
            
                    }else{
                        console.log('Ocorreu um erro na requisição');
                    }

                },

                error: function(resp) {
                    console.log('Ocorreu um erro na resposta');
                }
            });

		});

	});

	$.fn.listarCampos = function(send,limite){

		var cont      = 0;
		var validador = true;

		$(this).each(function(){

				// VERIFICA SE ALGUMA OPÇÃO FOI MARCADA
				if($(this).is(':checked')){
					
					cont++;

					// VERIFICA QUANTAS OPÇÕES FORAM MARCADAS
					if(cont == 1){
						
						var qtd = $(this).val();

						// VERIFICA SE A OPÇÃO MARCADA SE ENCAIXA NA ESTIPULAÇÃO DO INDEX
						if(qtd <= limite){
							$(send).val(qtd);
							validador = false;
						}

					}else{
						validador = true;
					}				
				}

			});

		return validador;

	};

	$.fn.proximaEtapa = function(etapas){

		// RECUPERA O NOME DA ID
		var etapaEscolhida = this.selector;

		// LISTA TODAS AS PÁGINAS E EXIBE SOMENTE A NECESSÁRIA O RESTANTE FICA OCULTO 
		$(etapas).each(function(i){

			if(etapaEscolhida == etapas[i]){
				$(etapas[i]).show();
			}else{
				$(etapas[i]).hide();
			}
		});

	};

	function exibirMensagemAlerta(mensagem, redirecionamento, botaoPersonalizado){

		swal({

			title: 'Informação',
			text: mensagem,
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: botaoPersonalizado,
			cancelButtonText: 'Cancelar',
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			buttonsStyling: false

		}).then(function () {

			window.location = URL+redirecionamento;

		}, function (dismiss) {

			if (dismiss === 'cancel') {
				swal(
					'Cancelado',
					'Operação cancelada pelo usuário.',
					'error'
					);
			}
		});
	}

	$.fn.validador = function(){
		
		var verifica = true;
		
		this.each(function(){

			if($(this).val() == ""){
				verifica = false;
			}
		});

		return verifica;
	}

	$.fn.preencheValores = function(field){

		// PERCORRE OS VALORES PREENCHIDOS
		this.each(function(){

			var valorAtual = $(field).val();
			var valor      = $(this).val();

			// ATUALIZA OS VALORES DO CAMPO
			$(field).val(valorAtual+","+valor);

		});
	}

	$.fn.gerarResumo = function(ajax){

		nomes       = new Array();
		precos      = new Array();
		quantidades = new Array();
		codigosNcm  = new Array();

		var tipoEmbarque       = $(".tipoEscolhidoEmbarque").val();
		var produtosNome       = $(".produtosNome").val();
		var produtosPreco      = $(".produtosPreco").val();
		var produtosQuantidade = $(".produtosQuantidade").val();
		var produtosCodigosNcm = $(".produtosCodigosNcm").val();
		var tipoFrete          = $(".tipoFreteSelecionado").val();
		var valorFrete         = $(".valorFreteSelecionado").val();

		var textoHtml = "";

		$(".natureza").text(tipoEmbarque);

		if(tipoFrete == 1){
			$(".incoterm").html("Marítimo: EXW/FOB/FAS <br> Valor do Frete: "+valorFrete+" USD");
		}
		
		if(tipoFrete == 2){
			$(".incoterm").html("Aéreo: EXW/FCA <br> Valor do Frete: "+valorFrete+" USD");			
		}

		nomes       = produtosNome.split(',');
		precos      = produtosPreco.split(',');
		quantidades = produtosQuantidade.split(',');
		codigosNcm  = produtosCodigosNcm.split(',');

		for(i=1; i<nomes.length; i++){
			
			if(!ajax){
				if(nomes[i] != ""){
					$(".painel-resumo").append("<div class='titleitens'>Item "+i+"</div><div class='detalhesitem'>Produto: "+nomes[i]+" <br> Preço: "+precos[i]+" USD <br> Quantidade: "+quantidades[i]+" <br> NCM: "+codigosNcm[i]+"</div>");	
				}				
			}else{
				textoHtml += "<div class='titleitens'>Item "+i+"</div><div class='detalhesitem'>Produto: "+nomes[i]+" <br> Preço: "+precos[i]+" USD <br> Quantidade: "+quantidades[i]+" <br> NCM: "+codigosNcm[i]+"</div>";
			}			
		}

		if(ajax){
			return textoHtml; 
		}
	}