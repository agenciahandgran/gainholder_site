<?php

class ArtigoSiteControl extends Controller{

	private $conteudoModel;
	private $colunistaModel;
	private $pagamentoModel;
	private $assinaturaModel;

	public function __construct(){

			// SETANDO O MÓDULO
		$this->setModulo('artigos');
		$this->conteudoModel   = new ConteudoModel();
		$this->colunistaModel  = new ColunistaModel();
		$this->pagamentoModel  = new PagamentoModel();
		$this->assinaturaModel = new AssinaturaModel();
	}

	public function loadMethod($acao){

		switch ($acao) {

			case 'carregarArtigos':
			$this->pesquisaAjax($_GET['totalDestaque'], $_GET['totalNormais'], $_GET['pgAtualDestaque'], $_GET['pgAtualNormal']);
			break;

			case 'detalhe':
			$this->verificarUsuarioLogado();
			break;

			default:
					# code...
			break;
		}
	}

		//CARREGA A VIEW PRINCIPAL DO MODULO USUÁRIOS
	public function index(){

		$dados['artigos']             = $this->listarArtigos('WHERE tipo = 2 AND destaque = 0 ORDER BY data_publicacao DESC LIMIT 4');

		$dados['destaques']           = $this->listarArtigos(' INNER JOIN colunistas cl ON cl.id_login = c.id_autor WHERE tipo = 2 AND destaque = 1 ORDER BY data_publicacao DESC LIMIT 5'," c.*, ctg.nome as nomeCategoria, ctg.id_categoria as idCategoria, cl.nome_completo");
		$dados['contadorDestaques']   = $this->listarArtigos('WHERE tipo = 2 AND destaque = 1 ORDER BY data_publicacao DESC', " count(c.id_conteudo) as total");
		$dados['contadorNormais']     = $this->listarArtigos('WHERE tipo = 2 AND destaque = 0 ORDER BY
			data_publicacao DESC',' count(c.id_conteudo) as total ');
		$dados['totalPgDestaques']    = $this->recuperarQtdPaginas($dados['contadorDestaques'][0]->getTotal(),5);
		$dados['totalPgNormais']      = $this->recuperarQtdPaginas($dados['contadorNormais'][0]->getTotal(),4);
			// $dados['colunistas']		  = $this->listarColunistas('WHERE c.status = 1 AND l.tipo = 2 AND a.tipo = 2 GROUP BY(l.id_login) ORDER BY RAND() DESC LIMIT 4');

		$dados['colunistas'] = $this->colunistaModel->getListColuinstaByRand();

		$this->loadView('lista.php',$dados);

	}

	public function recuperarQtdPaginas($total,$limite){

		if($total > 0){

			$totalResultados = $total;

			if($totalResultados > $limite){
				$totalPaginas = ceil($totalResultados/$limite);
			}else{
				$totalPaginas = 0;
			}

		}else{
			$totalPaginas = 0;
		}

		return $totalPaginas;
	}

	public function listarArtigos($condicao, $campos=" c.*, ctg.nome as nomeCategoria, ctg.id_categoria as idCategoria "){
		$listar = "";

		if(!empty($condicao)){
			$listar = $this->conteudoModel->getListContentByCondition($condicao, $campos);
		}

		return $listar;
	}

	public function artigo($slug){
		$detalharConteudo = "";

		$slug = substr($slug, 0, strrpos($slug, "."));

		if(!empty($slug)){

			$detalharConteudo = $this->conteudoModel->getRow("c.*, ctg.nome as nomeCategoria, ctg.id_categoria as idCategoria, cl.nome_completo, cl.descricao, cl.foto_perfil ", " INNER JOIN colunistas cl ON cl.id_login = c.id_autor
				WHERE c.tipo = 2 AND c.slug = '".$slug."' ");

			if(isset($detalharConteudo['success'])){
				$dados['artigosRelacionados'] = $this->conteudoModel->getListContentByCondition(' WHERE ctg.id_categoria = '.$detalharConteudo['success']->getCategoria()->getId().' AND tipo = 2  ORDER BY data_publicacao DESC LIMIT 4');
				$dados['artigosAutor'] = $this->conteudoModel->getListContentByCondition(' WHERE c.id_autor = '.$detalharConteudo['success']->getAutor()->getIdLogin().' AND tipo = 2 AND NOT id_conteudo = '.$detalharConteudo['success']->getId().'  ORDER BY data_publicacao DESC LIMIT 4');
				$dados['artigo'] = $detalharConteudo['success'];
			}else{
				$dados['artigo'] = $detalharConteudo;
			}

		}else{
			echo "<script>window.location='".URL_SITE."/artigos'; </script>";
		}

		$this->loadView('detalhe.php',$dados);
	}

	public function pesquisaAjax($totalPgDestaque, $totalPgNormais, $pgAtualDestaque, $pgAtualNormais){
		$retornoJson['html'] = "";
		$html = "";

		if($pgAtualDestaque <= $totalPgDestaque || $pgAtualNormais <= $totalPgNormais){

			if($pgAtualDestaque <= $totalPgDestaque){
				
				$retornoJson['pgAtualNormais']  = $pgAtualNormais;

				$start  = ($pgAtualDestaque * 5) - 5;
				$condicao  = " WHERE tipo = 2 AND destaque = 1 ORDER BY data_publicacao DESC LIMIT $start, 5";

				$retornoJson['pgAtualDestaque'] = $pgAtualDestaque+1;
			}else{
				
				$retornoJson['pgAtualDestaque'] = $pgAtualDestaque;
				
				$start     = ($pgAtualNormais * 4) - 4;
				$condicao  = " WHERE tipo = 2 AND destaque = 0 ORDER BY data_publicacao DESC LIMIT $start, 4";

				$retornoJson['pgAtualNormais']  = $pgAtualNormais+1;
			}

			$resultados  = $this->conteudoModel->getListContentByCondition($condicao);

			if(!isset($resultados['error'])){

				foreach($resultados as $resultado){

					$html .= "<div class='col-xs-12 col-md-6 acessados'>
					<div class='img-destaque col-xs-12 p0'>
					<div class='div-categoria-home'>
					".$resultado->getCategoria()->getNome()."
					</div>
					<img style='max-width: 321px; width: 100%; height: auto; display: block;' src='".URL_SITE."/painel/assets/img/upload/artigos/thumbs/".$resultado->getThumbnail()."' class='img-responsive wp-post-image'/>				            		
					</div>
					<div class='col-xs-12 conteudo-list p0'>
					<div class='titulo-destaque'>
					<p><a href='".URL_SITE."/artigos/artigo/".$resultado->getSlug().".html'>".$resultado->getTitulo()."</a></p>
					<p class='excerpt-conteudo-home'>
					".$resultado->getResumo()."
					</p>
					<p class='date-conteudo-home dataConteudoLista'>".formatarDataExtenso(date('d/m/Y',strtotime($resultado->getDataPublicacao())))."</p>
					</div>
					</div>
					</div>";
				}

				$retornoJson['html'] 	  = $html;
				$retornoJson['resultado'] = true;

			}else{
				$retornoJson['resultado'] = false;
			}
			
		}else{

			$retornoJson['resultado'] = false;
		}	

		echo json_encode($retornoJson);

	}

	public function listarColunistas($condicao){
		$listar = "";

		if(!empty($condicao)){
			$listar = $this->colunistaModel->getListColunistaByCondition($condicao);
		}

		return $listar;
	}


		// VERIFICAR SE O USUÁRIO ESTÁ LOGADO NO SITE PARA LER O CONTEÚDO
	public function verificarUsuarioLogado(){

		if(isset($_SESSION['cliente_site']) || isset($_COOKIE['clienteCookie'])){

			if(isset($_SESSION['cliente_site'])){
				$loginId       = $_SESSION['cliente_site']['id_login'];
			}else{
				$clienteCookie = unserialize($_COOKIE['clienteCookie']);
				$loginId 	   = $clienteCookie['id'];
			}

			if(isset($_GET['grau']) && !empty($_GET['grau'])){

				if($_GET['grau'] != "10"){

					$verificandoUsuarioPermissoes = $this->verificarPermissoes($loginId,$_GET['grau']);

					if(!$verificandoUsuarioPermissoes['status']){

						if(empty($verificandoUsuarioPermissoes['grau'])){
							$respostaJson['nivel']    = 3;
							$respostaJson['mensagem'] = "Atenção! Para acessar algum artigo você precisa ter uma assinatura. Assine agora e tenha acesso aos nossos artigos!";
						}else{
							$respostaJson['nivel']    = 4;
							$respostaJson['mensagem'] = " Atenção! Esse é um artigo de grau ".$verificandoUsuarioPermissoes['grau'].". Verifique se o seu plano possui cobertura para o grau do artigo.";
						}
					}else{
						$respostaJson['status'] = true;
					}
				}else{
					$respostaJson['status'] = true;
				}
			}else{
				$respostaJson['status']   = false;
				$respostaJson['nivel']  = 2;
				$respostaJson['mensagem'] = " Você não possui permissão para acessar esse artigo.";
			}
		}else{
			$respostaJson['status'] = false;
			$respostaJson['nivel']  = 1;
			$respostaJson['mensagem'] = " Faça o seu login para acessar este artigo.";
		}

		echo json_encode($respostaJson);
	}		

		// VERIFICANDO SE O USUÁRIO PODE ACESSAR UM CONTEÚDO ESPECÍFICO
	public function verificarPermissoes($loginId, $grau){

		$permissao['status'] = false;
		$permissao['grau']   = "";

		$verificaPagamento = $this->pagamentoModel->getRow("*", " WHERE id_cliente = ".$loginId." AND status = 1");

		if(isset($verificaPagamento['success'])){

			$verificaPlano = $this->assinaturaModel->getRow("*"," WHERE id_plano = ".$verificaPagamento['success']->getPlano()->getId()." AND status = 1");

			if(isset($verificaPlano['success'])){

				if($verificaPlano['success']->getGrau() >= $grau){
					$permissao['status'] = true;
				}else{
					$permissao['status'] = false;
					$permissao['grau']   = $grau;
				}					
			}
		}

		return $permissao;
	}	
}
?>