<?php

	class AssinanteSiteControl extends Controller{

		private $moduloModel;
		private $assinaturaModel;
		private $clienteModel;
		private $enderecoModel;
		private $pagamentoModel;
		private $pagseguro;

		public function __construct(){

			// SETANDO O MÓDULO
			$this->setModulo('assinantes');
			$this->assinaturaModel = new AssinaturaModel();
			$this->clienteModel    = new ClienteModel();
			$this->enderecoModel   = new EnderecoModel();
			$this->pagseguro       = new Pagseguro(PAGSEGURO_EMAIL, PAGSEGURO_TOKEN);
			$this->pagamentoModel  = new PagamentoModel(); 
		}

		public function loadMethod($acao){

			if(!empty($acao)){

				switch ($acao) {
					case 'detalharDadosPlano':
						$planoId = isset($_GET['planoId']) ? $_GET['planoId'] : '';
						$this->detalharDadosPlano($planoId);
					break;

					case 'detalharDadosUsuario':
						$this->detalharDadosUsuario();
					break;

					case 'atualizarDadosUsuario':
						$this->atualizarDadosUsuario();
					break;
					
					case 'iniciarSessaoPagamento':
						$this->iniciarSessaoPagamento();
					break;

					case 'realizarPagamento':
						$this->realizarPagamento($_GET['token'],$_GET['hash'],$_GET['planoId']);
					break;

					default:
						# code...
					break;
				}

			}
		}

		//CARREGA A VIEW PRINCIPAL DO MODULO USUÁRIOS
		public function index(){

			$dados['planos']     = $this->listarAssinaturas(" WHERE status = 1 ");
			$dados['planoAtual'] = $this->verificarUsuarioPlano();

			$this->loadView('lista.php',$dados);
			
		}

		public function listarAssinaturas($condicao=""){

			$assinaturas = $this->assinaturaModel->getList($condicao);

			return $assinaturas;

		}

		public function detalharDadosPlano($planoId){
			$dados = array();

			if(!empty($planoId)){
		
				if(isset($_SESSION['cliente_site']) || isset($_COOKIE['clienteCookie'])){
					
					$assinatura = $this->assinaturaModel->getRow("*", " WHERE id_plano = ".$planoId);

					if(isset($assinatura['success'])){
					
						$dados['status']   = 1;
						$dados['plano']    = $assinatura['success']->getNome();
						$dados['desconto'] = $assinatura['success']->getDesconto();
						$dados['preco']    = "R$ ".number_format($assinatura['success']->getPreco(),2,',','.');				
					}else{
						$dados['status'] = 2;
						$dados['mensagem'] = "Ocorreu um erro na requisição. Tente mais tarde.";
					}

				}else{
					$dados['status']   = 3;
					$dados['mensagem'] = "Para realizar a assinatura do plano, você precisa estar logado.";
				}
		
			}else{
				$dados['status']   = 2;
				$dados['mensagem'] = "Faltou informações na requisição.";
 			}

 			echo json_encode($dados);
		}

		public function detalharDadosUsuario(){
			$dadosJson = array();

			if(isset($_SESSION['cliente_site']) || isset($_COOKIE['clienteCookie'])){

				if(isset($_SESSION['cliente_site'])){
					$loginId       = $_SESSION['cliente_site']['id_login'];
				}else{
					$clienteCookie = unserialize($_COOKIE['clienteCookie']);
					$loginId 	   = $clienteCookie['id'];
				}

				$verificaCliente = $this->clienteModel->getRow("u.*, l.email, l.status, e.* ", 
					" WHERE l.id_login = '".$loginId."' AND l.status = 1");	

				if(isset($verificaCliente['success'])){

					$dadosJson['cpf']         = $verificaCliente['success']->getCpf();
					$dadosJson['cep']         = $verificaCliente['success']->getEndereco()->getCep();
					$dadosJson['endereco']    = $verificaCliente['success']->getEndereco()->getEndereco();
					$dadosJson['numero']      = $verificaCliente['success']->getEndereco()->getNumero();
					$dadosJson['bairro']      = $verificaCliente['success']->getEndereco()->getBairro();
					$dadosJson['cidade']      = $verificaCliente['success']->getEndereco()->getCidade();
					$dadosJson['uf']          = $verificaCliente['success']->getEndereco()->getUf();
					$dadosJson['complemento'] = $verificaCliente['success']->getEndereco()->getComplemento();

				}else{
					$dadosJson['cpf']         = "";
					$dadosJson['cep']         = "";
					$dadosJson['endereco']    = "";
					$dadosJson['numero']      = "";
					$dadosJson['bairro']      = "";
					$dadosJson['cidade']      = "";
					$dadosJson['uf']          = "";
					$dadosJson['complemento'] = "";
				}							
			}

			echo json_encode($dadosJson);
		}

		public function atualizarDadosUsuario(){

			$dadosJson = array();

			if(isset($_SESSION['cliente_site']) || isset($_COOKIE['clienteCookie'])){

				if(isset($_SESSION['cliente_site'])){
					$loginId       = $_SESSION['cliente_site']['id_login'];
				}else{
					$clienteCookie = unserialize($_COOKIE['clienteCookie']);
					$loginId 	   = $clienteCookie['id'];
				}

				$pontuacaoInvalida = array('.','-','/');

				$dadoCliente['cliente']['cpf'] = str_replace($pontuacaoInvalida, "",$_POST['cpf']);

				$atualizaDadosUsuario = $this->clienteModel->updateCliente($dadoCliente, $loginId);

				if(isset($atualizaDadosUsuario['success'])){

					$verificaCliente = $this->clienteModel->getRow("u.*", 
					" WHERE l.id_login = '".$loginId."' AND l.status = 1");	

					$dadosEnderecoCliente['endereco']['cep']         = str_replace($pontuacaoInvalida, "", $_POST['cep']);
					$dadosEnderecoCliente['endereco']['endereco']    = $_POST['endereco'];
					$dadosEnderecoCliente['endereco']['numero']      = $_POST['numero'];
					$dadosEnderecoCliente['endereco']['bairro']      = $_POST['bairro'];
					$dadosEnderecoCliente['endereco']['cidade']      = $_POST['cidade'];
					$dadosEnderecoCliente['endereco']['uf']          = $_POST['uf'];
					$dadosEnderecoCliente['endereco']['complemento'] = $_POST['complemento'];

					if($_POST['acaoUsuario'] == "1"){

						// CADASTRO
						$acaoEndereco = $this->enderecoModel->insert($dadosEnderecoCliente);

						if(isset($acaoEndereco['success'])){
							$dadosAtualizaCliEndereco['cliente']['id_endereco'] = $this->enderecoModel->getLastId();
							$atualizaDadosCliente  = $this->clienteModel->updateCliente($dadosAtualizaCliEndereco, $loginId);

							$dadosJson['status'] = 1;
						}else{
							$dadosJson['status'] = 2;
						}
				
					}else{

						$enderecoId = $verificaCliente['success']->getEndereco()->getId();
						
						// ATUALIZA
						$acaoEndereco = $this->enderecoModel->update($dadosEnderecoCliente, $enderecoId);

						if(isset($acaoEndereco['success'])){
							$dadosJson['status'] = 1;
						}else{
							$dadosJson['status'] = 2;
						}					
					}
				}else{
					$dadosJson['status'] = 2;
				}

			}else{
				$dadosJson['status'] = 2;
			}

			echo json_encode($dadosJson);
		}

		public function iniciarSessaoPagamento(){
			$dadosJson = array();
		
			$this->pagseguro->iniciarSessao();
		
			$dadosJson['id'] = $this->pagseguro->getSessaoId();	
		
			echo json_encode($dadosJson);
		}

		public function realizarPagamento($tokenCartao,$hash,$planoId){
			$dadosJson = array();
				
			// VERIFICA USUÁRIO 
			if(isset($_SESSION['cliente_site']) || isset($_COOKIE['clienteCookie'])){

				if(isset($_SESSION['cliente_site'])){
					$loginId       = $_SESSION['cliente_site']['id_login'];
				}else{
					$clienteCookie = unserialize($_COOKIE['clienteCookie']);
					$loginId 	   = $clienteCookie['id'];
				}

				// RECUPERANDO DADOS DO CLIENTE E PLANO
				$clienteDados = $this->clienteModel->getRow("u.*, l.email, l.status, e.* ", 
					" WHERE l.id_login = '".$loginId."' AND l.status = 1");	

				$planoDados  = $this->assinaturaModel->getRow("*", " WHERE id_plano = ".$planoId);

				if(isset($clienteDados['success']) && isset($planoDados['success'])){

					// MONTANDO O ARRAY PARA SER ENVIADO AO PAGSEGURO
					$dados['plan']      = $planoDados['success']->getCodigoPagseguro();
					$dados['reference'] = base64_encode(md5(rand(0,100)));

					// DADOS DO ASSINANTE
					$dados['sender']['name']  = $clienteDados['success']->getNome();
					$dados['sender']['email'] = 'c90699514309513440186@sandbox.pagseguro.com.br';//$clienteDados['success']->getLogin()->getEmail();
					$dados['sender']['hash']  = $hash;

					// Telefone
					$dados['sender']['phone']['areaCode'] = substr($clienteDados['success']->getTelefone(), 1,strrpos($clienteDados['success']->getTelefone(), ")")-1);
					$dados['sender']['phone']['number'] = substr(str_replace("-", "", $clienteDados['success']->getTelefone()), 5) ;

					// ENDEREÇO DO ASSINANTE
					$dados['sender']['address']['street']     = $clienteDados['success']->getEndereco()->getEndereco();
					$dados['sender']['address']['number']     = $clienteDados['success']->getEndereco()->getNumero(); 
					$dados['sender']['address']['complement'] = $clienteDados['success']->getEndereco()->getComplemento();
					$dados['sender']['address']['district']   = $clienteDados['success']->getEndereco()->getBairro();					
					$dados['sender']['address']['city']       = $clienteDados['success']->getEndereco()->getCidade();
					$dados['sender']['address']['state']      = $clienteDados['success']->getEndereco()->getUf();
					$dados['sender']['address']['country']    = "BRA";
					$dados['sender']['address']['postalCode'] = $clienteDados['success']->getEndereco()->getCep();

					// DOCUMENTO ASSINANTE
					$dados['sender']['documents'][0]['type']  = "CPF";
					$dados['sender']['documents'][0]['value'] = $clienteDados['success']->getCpf();

					// DADOS PAGAMENTO
					$dados['paymentMethod']['type'] = "CREDITCARD";
					$dados['paymentMethod']['creditCard']['token'] = $tokenCartao;
					$dados['paymentMethod']['creditCard']['holder']['name'] = $clienteDados['success']->getNome();
					$dados['paymentMethod']['creditCard']['holder']['birthDate'] = $clienteDados['success']->getDataNascimento();
					$dados['paymentMethod']['creditCard']['holder']['documents'][0]['type']  = "CPF";
					$dados['paymentMethod']['creditCard']['holder']['documents'][0]['value'] = $clienteDados['success']->getCpf();
					$dados['paymentMethod']['creditCard']['holder']['phone']['areaCode'] = substr($clienteDados['success']->getTelefone(), 1,strrpos($clienteDados['success']->getTelefone(), ")")-1);
					$dados['paymentMethod']['creditCard']['holder']['phone']['number'] = substr(str_replace("-", "", $clienteDados['success']->getTelefone()), 5);
					$dados['paymentMethod']['creditCard']['holder']['billingAddress']['street'] = $clienteDados['success']->getEndereco()->getEndereco();
					$dados['paymentMethod']['creditCard']['holder']['billingAddress']['number']     = $clienteDados['success']->getEndereco()->getNumero(); 
					$dados['paymentMethod']['creditCard']['holder']['billingAddress']['complement'] = $clienteDados['success']->getEndereco()->getComplemento();
					$dados['paymentMethod']['creditCard']['holder']['billingAddress']['district']   = $clienteDados['success']->getEndereco()->getBairro();
					$dados['paymentMethod']['creditCard']['holder']['billingAddress']['city']       = $clienteDados['success']->getEndereco()->getCidade();
					$dados['paymentMethod']['creditCard']['holder']['billingAddress']['state']      = $clienteDados['success']->getEndereco()->getUf();
					$dados['paymentMethod']['creditCard']['holder']['billingAddress']['country']    = "BRA";
					$dados['paymentMethod']['creditCard']['holder']['billingAddress']['postalCode'] = $clienteDados['success']->getEndereco()->getCep();
					
					// ADERINDO O PLANO VIA PAGSEGURO
					$pagamento = $this->pagseguro->aderirPlano($dados);

					// Verifica se está tudo ok com a assinatatura
					if($pagamento != 0){
						
						// INICIO ASSINATURA
						$data = date('Y-m-d');
						
						// REGISTRA O PAGAMENTO
						$dadosPagamento['pagamento']['codigo']     = $pagamento;
						$dadosPagamento['pagamento']['id_cliente'] = $loginId; 
						$dadosPagamento['pagamento']['id_plano']   = $planoId;
						$dadosPagamento['pagamento']['valor']      = $planoDados['success']->getPreco();
						$dadosPagamento['pagamento']['periodo']    = date('Y-m-d', strtotime(' +1 year ', strtotime($data)));
						$dadosPagamento['pagamento']['status']     = 1;
						
						$cadastrandoPagamento = $this->pagamentoModel->insert($dadosPagamento);

						if(isset($cadastrandoPagamento['success'])){
							$dadosJson['status'] = 1;
						}else{
							$dadosJson['status'] = 0;
						}

					}else{
						$dadosJson['status'] = 0;
					}

				}else{
					$dadosJson['status'] = 0;
				}

			}else{

				$dadosJson['status'] = 0;
			}	


			echo json_encode($dadosJson);
		}


		// VERIFICANDO SE O USUÁRIO JÁ POSSUI ALGUM PLANO
		public function verificarUsuarioPlano(){
			$planoId = 0;
			// VERIFICA USUÁRIO 
			if(isset($_SESSION['cliente_site']) || isset($_COOKIE['clienteCookie'])){

				if(isset($_SESSION['cliente_site'])){
					$loginId       = $_SESSION['cliente_site']['id_login'];
				}else{
					$clienteCookie = unserialize($_COOKIE['clienteCookie']);
					$loginId 	   = $clienteCookie['id'];
				}

				$verificandoPlano = $this->pagamentoModel->getRow("*", " WHERE id_cliente = ".$loginId." AND status = 1");

				if(isset($verificandoPlano['success'])){

					$planoId = $verificandoPlano['success']->getPlano()->getId();
				}	

			}	

			return $planoId;			
		}
	
	}
?>