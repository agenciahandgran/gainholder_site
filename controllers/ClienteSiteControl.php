<?php

class ClienteSiteControl extends Controller{

	private $moduloModel;
	private $administradorModel;
	private $clienteModel;
	private $loginModel;
	private $enderecoModel;
	private $pagamentoModel;

	public function __construct(){

			// SETANDO O MÓDULO
		$this->setModulo('clientes');

		$this->administradorModel = new AdministradorModel();
		$this->clienteModel       = new ClienteModel();
		$this->loginModel         = new AdministradorModel();
		$this->enderecoModel      = new EnderecoModel();
		$this->pagamentoModel     = new PagamentoModel();
	}

	public function loadMethod($acao){

		switch ($acao) {
			case 'verificaEmail':
				$email = isset($_GET['email']) ? $_GET['email'] : '';
				$this->verificarEmail($email);
			break;
			
			default:
				# code...
			break;
		}
	}

	//CARREGA A VIEW PRINCIPAL DO MODULO USUÁRIOS
	public function index(){

		$dados['condicao'] = "";

			// CASO O USUÁRIO NÃO ESTEJA EM SESSÃO, A TELA LOGIN SERÁ CARREGADA AO ÍNVES DO PERFIL
		if(isset($_SESSION['cliente_site']) || isset($_COOKIE['clienteCookie'])){

			$this->perfil();
			
		}else{

			$this->login();
		}
	}

	// VERICANDO E-MAIL ANTES DO CADASTRO
	public function verificarEmail($email){
		$resposta = true;
		if(!empty($email)){

			if(isset($_GET['emailAtual']) && !empty($_GET['emailAtual'])){

				if($email != $_GET['emailAtual']){
		
					$verificaLogin = $this->clienteModel->getRow("u.*, l.email, l.status", 
							" WHERE l.email = '".$email."' AND l.status = 1 AND l.tipo = 3");

					if(isset($verificaLogin['success'])){
						$resposta = false;
					}					
				}

			}else{
				$verificaLogin = $this->clienteModel->getRow("u.*, l.email, l.status", 
						" WHERE l.email = '".$email."' AND l.status = 1 AND l.tipo = 3");

				if(isset($verificaLogin['success'])){
					$resposta = false;
				}
			}
		}

		echo json_encode($resposta);
	}

	public function registrar(){

		$dados    = array();
		$cadastro = array();

		if(isset($_POST['modulo']) && $_POST['modulo'] == "cadastro"){

			$cadastro['cliente']['id_login']        = '0';
			$cadastro['cliente']['id_endereco']     = '0';
			$cadastro['cliente']['tipo_legislacao'] = isset($_POST['tipo_legislacao']) ? $_POST['tipo_legislacao'] : '0';
			$cadastro['cliente']['nome_completo']   = isset($_POST['nome_completo']) ? $_POST['nome_completo'] : '';
			$cadastro['cliente']['data_nascimento'] = isset($_POST['data_nascimento']) ? $_POST['data_nascimento'] : '';
			$cadastro['cliente']['cpf']             = isset($_POST['cpf']) ? $_POST['cpf'] : '0'; 
			$cadastro['cliente']['cnpj']            = isset($_POST['cnpj']) ? $_POST['cnpj'] : '0'; 
			$cadastro['cliente']['rg']              = isset($_POST['rg']) ? $_POST['rg']   : '0';
			$cadastro['cliente']['telefone']        = isset($_POST['telefone']) ? $_POST['telefone'] : '';
			$cadastro['cliente']['celular']         = isset($_POST['celular']) ? $_POST['celular'] : '';
			$cadastro['cliente']['genero']          = isset($_POST['genero']) ? $_POST['genero'] : '0';
			$cadastro['cliente']['tipo']            = '3';
			$cadastro['cliente']['hash']            = '';

			$cadastro['login']['email'] = isset($_POST['email']) ? $_POST['email'] : '';

			if(!empty($_POST['senha'])){

				$cadastro['login']['senha'] =  md5($_POST['senha']);
			}

			$cadastro['login']['status'] = '0';
			$cadastro['login']['tipo']   = '1';

			$cadastroExecutado = $this->clienteModel->insert($cadastro);

			if(isset($cadastroExecutado['success'])){

					// DISPARAR E-MAIL PARA VERIFICAÇÃO DE CADASTRO
				$email = array();
				$email['nome']  = $cadastro['cliente']['nome_completo'];
				$email['email'] = $cadastro['login']['email'];
				$email['hash']  = $cadastroExecutado['hash'];	

				$envioEmail = $this->enviarEmail($email);

				if($envioEmail){
					$dados['status'] = '1';
					echo "<script>window.location='".URL_SITE."/clientes/cadastro/".$dados['status']."'; </script>";
				}else{
					$dados['status'] = '0';
					$dados['mensagem'] = "Ocorreu um erro ao verificar e-mail.";
				}

			}else{
				$dados['status'] = '0';
				$dados['mensagem'] = $cadastroExecutado['error'];
			}

			$this->loadView('cadastro.php',$dados);

		}else{
			$this->loadView('cadastro.php','');
		}

	}

	public function login($modulo=null, $url=""){

		if(!isset($_SESSION['cliente_site']) && !isset($_COOKIE['clienteCookie'])){

			if(isset($_POST['modulo']) && $_POST['modulo'] == "login"){

				$email = isset($_POST['email']) ? $_POST['email']      : '';
				$senha = isset($_POST['senha']) ? md5($_POST['senha']) : '';

				$verificaLogin = $this->clienteModel->getRow("u.*, l.email, l.status", 
					" WHERE l.email = '".$email."' AND l.senha = '".$senha."' AND l.status = 1");

				if(isset($verificaLogin['success'])){

					if(isset($_POST['continuar_conectado'])){

						$dadosCliente['id']         = $verificaLogin['success']->getLogin()->getId();
						$dadosCliente['email']      = $verificaLogin['success']->getLogin()->getEmail();
						$dadosCliente['nome']       = $verificaLogin['success']->getNome();
						$dadosCliente['pontuacao']  = 0;

						setcookie('clienteCookie', serialize($dadosCliente), time() +2592000, '/'); 

					}else{

						$_SESSION['cliente_site']['id_login']   = $verificaLogin['success']->getLogin()->getId(); 
						$_SESSION['cliente_site']['email']      = $verificaLogin['success']->getLogin()->getEmail();
						$_SESSION['cliente_site']['nome']       = $verificaLogin['success']->getNome();
						$_SESSION['cliente_site']['pontuacao']  = 0;
					}


					// REDIRICIONAR A UMA TELA ESPECÍFICA APÓS O LOGIN
					if(is_null($modulo)){
					
						echo "<script>window.location='".URL_SITE."/clientes/perfil'; </script>";
					
					}else{

						switch ($modulo) {
							
							case 'conteudos':
								echo "<script>window.location='".URL_SITE."/conteudos/noticia/".$url.".html'; </script>";
							break;

							case 'artigos':
								echo "<script>window.location='".URL_SITE."/artigos/artigo/".$url.".html'; </script>";
							break;
							
							case 'assinantes':
								echo "<script>window.location='".URL_SITE."/assinantes'; </script>";
							break;

							default:
								echo "<script>window.location='".URL_SITE."/clientes/perfil'; </script>";
							break;
						}
					}
					
				}else{
					$dados['mensagem'] = "Usuário ou Senha inválidos.";
					$this->loadView('login.php',$dados);
				}

			}else{
				
				$dados = array();
				$dados['modulo'] = "";
				$dados['url']    = "";

				if(!is_null($modulo)){
					$dados['modulo'] = $modulo;
					$dados['url']    = $url;
				}

				$this->loadView('login.php', $dados);
			}
		}else{ 
			echo "<script>window.location='".URL_SITE."/clientes/perfil'; </script>";
		}
	}

	public function cadastro($parametro=""){
		if(!isset($_SESSION['cliente_site']) && !isset($_COOKIE['clienteCookie'])){
			$dados['status'] = $parametro;
			$this->loadView('cadastro.php',$dados);
		}else{
			echo "<script>window.location='".URL_SITE."/clientes/perfil'; </script>";
		}
	}

	public function recuperarSenha(){
		$emailClasse = new Email();
		$dados       = array();
		
		if(isset($_POST['modulo']) && $_POST['modulo'] == "recuperar-senha"){

			if(isset($_POST['email']) && !empty($_POST['email'])){
				$email         = $_POST['email'];
				$verificaEmail = $this->clienteModel->getRow("u.*, l.email, l.status", 
					" WHERE l.email = '".$email."' AND l.status = 1");

				if(isset($verificaEmail['success'])){

					// ATUALIZA COM NOVO HASH PARA RECUPERAÇÃO DE SENHA
					$hash = rand().$verificaEmail['success']->getId().$verificaEmail['success']->getLogin()->getEmail();

					$clienteAtualizacao['cliente']['hash'] = md5($hash);
					
					$atualizacaoHash = $this->clienteModel->update($clienteAtualizacao,$verificaEmail['success']->getId());

					// ENVIA O E-MAIL PARA ATUALIZAR SENHA
					if($atualizacaoHash){

						$assunto   = "Recuperação de senha";

						$conteudo  = " Olá, ".$verificaEmail['success']->getNome()."<br>";
						$conteudo .= " Foi solicitado uma recuperação de senha nesse endereço. <br>";
						$conteudo .= " Para nova senha, clique <u><a href='".URL_SITE."/clientes/esqueceuSenha/".$clienteAtualizacao['cliente']['hash']."'>aqui</u>"; 

						$respostaEnvio = $emailClasse->Enviar(EMAIL_REMETENTE, EMAIL_REMETENTE_NOME, $verificaEmail['success']->getNome(), $verificaEmail['success']->getLogin()->getEmail(), $assunto, $conteudo);

						if($respostaEnvio){
							
							$dados['status']   = "1";
							$dados['mensagem'] = "Foi enviando um e-mail para esse endereço com instruções de como conseguir uma nova senha.";
						}else{
							$dados['status']   = "2";
							$dados['mensagem'] = "Ocorreu um erro durante a operação, tente novamente mais tarde.";
						}
					}else{

						$dados['status']   = "2";
						$dados['mensagem'] = "Ocorreu um erro durante a operação, tente novamente mais tarde.";
					}

				}else{
					$dados['status']   = "2";
					$dados['mensagem'] = "E-mail não encontrado. Por favor verificá-lo.";
				}
			}

		}	

		$this->loadView('recuperar-senha.php',$dados);
	}

	public function editarDados(){
		$dados = array();

		if(isset($_SESSION['cliente_site']) || isset($_COOKIE['clienteCookie'])){

			if(isset($_SESSION['cliente_site'])){
				$loginId       = $_SESSION['cliente_site']['id_login'];
			}else{
				$clienteCookie = unserialize($_COOKIE['clienteCookie']);
				$loginId 	   = $clienteCookie['id'];
			}

			$verificaCliente = $this->clienteModel->getRow("u.*, l.email, l.status, e.* ", 
				" WHERE l.id_login = '".$loginId."' AND l.status = 1");	

			if(isset($verificaCliente['success'])){

				$dados['cliente'] = $verificaCliente['success'];

			}else{
				$dados['mensagem'] = "Ocorreu um erro ao encontrar os dados.";
			}							

			$this->loadView('editar-dados.php',$dados);

		}else{
			echo "<script>window.location='".URL_SITE."/clientes/login'; </script>";
		}	
		
	}

	public function salvarEdicao($id){
		$dados = array();
		$pontuacaoInvalida = array('.','-','/');

		if(!empty($id)){

			if(isset($_POST['modulo']) && $_POST['modulo'] == 'editar-usuario'){

				if(isset($_SESSION['cliente_site'])){
					$loginId       = $_SESSION['cliente_site']['id_login'];
				}else{
					$clienteCookie = unserialize($_COOKIE['clienteCookie']);
					$loginId 	   = $clienteCookie['id'];
				}

				// ATUALIZA DADOS CLIENTE
				$dadosUsuario['cliente']['tipo_legislacao'] = $_POST['tipo_pessoa'];
				$dadosUsuario['cliente']['nome_completo']   = $_POST['nome_completo'];
				$dadosUsuario['cliente']['cpf']             = !empty($_POST['cpf']) ? str_replace($pontuacaoInvalida, "", $_POST['cpf']) : '0';
				$dadosUsuario['cliente']['cnpj']            = !empty($_POST['cnpj']) ? str_replace($pontuacaoInvalida, "", $_POST['cnpj']) : '0';
				$dadosUsuario['cliente']['rg']              = str_replace($pontuacaoInvalida, "", $_POST['rg']);
				$dadosUsuario['cliente']['telefone']        = $_POST['telefone'];
				$dadosUsuario['cliente']['celular']         = $_POST['celular'];
				$dadosUsuario['cliente']['genero']          = $_POST['genero'];

				$salvarDadosCliente = $this->clienteModel->update($dadosUsuario, $id);

				// ATUALIZA OS DADOS DE LOGIN
				if(isset($_POST['email']) && !empty($_POST['email'])){
					$dadosLogin['login']['email'] = $_POST['email'];
					$atualizarDadosLogin = $this->clienteModel->updateLogin($dadosLogin,$loginId);
				}

				// ATUALIZA OS DADOS DE ENDEREÇO
				if(isset($_POST['cep']) && !empty($_POST['cep'])){

					// CADASTRO INFORMAÇÕES DE ENDEREÇO
					$dadosEndereco['endereco']['cep']         = str_replace($pontuacaoInvalida, "", $_POST['cep']);
					$dadosEndereco['endereco']['endereco']    = $_POST['endereco'];
					$dadosEndereco['endereco']['numero']      = $_POST['numero'];
					$dadosEndereco['endereco']['bairro']      = $_POST['bairro'];
					$dadosEndereco['endereco']['cidade']      = $_POST['cidade'];
					$dadosEndereco['endereco']['uf']          = $_POST['uf'];
					$dadosEndereco['endereco']['complemento'] = $_POST['complemento'];
						
					// CADASTRA NOVO ENDEREÇO
					if($_POST['id_endereco'] == '0'){

						$inserirEndereco = $this->enderecoModel->insert($dadosEndereco);

						if(isset($inserirEndereco['success'])){

							$dadosAtualizaCliEndereco['cliente']['id_endereco'] = $this->enderecoModel->getLastId();

							$atualizaDadosCliente  = $this->clienteModel->update($dadosAtualizaCliEndereco, $id);
						}

					// ATUALIZA ENDERECO	
					}else{
						$atualizarEndereco = $this->enderecoModel->update($dadosEndereco, $_POST['id_endereco']);
					}
				}

				if(isset($salvarDadosCliente['success'])){

					$dados['status']   = '1';
					$dados['mensagem'] = "Dados atualizado com sucesso.";

				}else{
					$dados['status']   = '2';
					$dados['mensagem'] = "Não foi possível atualizar os dados do usuário no momento.";
				}

			}else{
				$dados['status']   = '2';
				$dados['mensagem'] = "Ocorreu um erro durante a edição. Verifique se os dados foram preenchidos.";
			}

		}else{
			$dados['status']   = '2';
			$dados['mensagem'] = "Ocorreu um erro durante a edição. Verifique se os dados foram preenchidos.";
		}

		$dados['id'] = $id;

		$this->loadView('editar-dados.php',$dados);
	}

	public function perfil(){
		$dados = array();
		
		if(isset($_SESSION['cliente_site']) || isset($_COOKIE['clienteCookie'])){

			if(isset($_SESSION['cliente_site'])){

				$dados['id']        = $_SESSION['cliente_site']['id_login'];
				$dados['usuario']   = $_SESSION['cliente_site']['email'];
				$dados['nome']      = $_SESSION['cliente_site']['nome'];
				$dados['email']     = $_SESSION['cliente_site']['email'];
				$dados['pontuacao'] = $_SESSION['cliente_site']['pontuacao'];

			}else{

				$dadosCookie = unserialize($_COOKIE['clienteCookie']);

				$dados['id']        = $dadosCookie['id'];
				$dados['usuario']   = $dadosCookie['email'];
				$dados['nome']      = $dadosCookie['nome'];
				$dados['email']     = $dadosCookie['email'];
				$dados['pontuacao'] = $dadosCookie['pontuacao'];				

			} 

			$verificaAssinatura = $this->pagamentoModel->getRow("p.*, pl.nome as nomePlano", 
				' p  LEFT JOIN planos pl ON pl.id_plano = p.id_plano 
					 WHERE p.id_cliente = '.$dados['id'].' AND p.status = 1 ORDER BY id_pagamento DESC LIMIT 1');

			// RETORNA A ASSINATURA DO CLIENTE CASO ELE POSSUA
			if(isset($verificaAssinatura['success'])){
				$dados['assinatura'] = $verificaAssinatura['success']->getPlano()->getNome();
			}else{
				$dados['assinatura'] = "";
			}

			$this->loadView('perfil.php',$dados);

		}else{
			echo "<script>window.location='".URL_SITE."/clientes/login'; </script>";
		}
	}

	public function esqueceuSenha($hash){
		$dados = array();
		if(!empty($hash)){

			if(isset($_POST['senha']) && !empty($_POST['senha'])){

				$verificaHash = $this->clienteModel->getRow("u.*, l.email, l.status", 
					" WHERE u.hash = '".$hash."' AND l.status = 1");

				if(isset($verificaHash['success'])){

					$loginAtualizacao['login']['senha'] = md5($_POST['senha']);
					$atualizacaoHash = $this->clienteModel->updateLogin($loginAtualizacao,$verificaHash['success']->getLogin()->getId());

					if(isset($atualizacaoHash['success'])){
						$dados['status']   = '1';
						$dados['mensagem'] = "Senha atualizada com sucesso!";
					}else{
						$dados['mensagem'] = "Ocorreu um erro ao atualizar a senha. Tente mais tarde.";
					}
				}else{
					$dados['mensagem'] = "Dados inválidos!";
				}
			}

		}else{
			$dados['mensagem'] = "Dados não encontrados.";
		}

		$this->loadView('esqueceu-senha.php',$dados);
	}

	public function alterarSenha(){
		$dados = array();

		if(isset($_SESSION['cliente_site']) || isset($_COOKIE['clienteCookie'])){

			if(isset($_SESSION['cliente_site'])){
				$loginId = $_SESSION['cliente_site']['id_login'];
			}else{
				$clienteCookie = unserialize($_COOKIE['clienteCookie']);
				$loginId 	   = $clienteCookie['id'];
			}
			
			if(isset($_POST['novaSenha']) && !empty($_POST['novaSenha'])){

				if($_POST['novaSenha'] != $_POST['senhaAtual']){
					
					$senhaAtual    = md5($_POST['senhaAtual']);
					$verificaSenha = $this->clienteModel->getRow("u.*, l.email, l.status", 
						" WHERE l.id_login = '".$loginId."' AND l.senha = '".$senhaAtual."' AND l.status = 1");

					if(isset($verificaSenha['success'])){

						$loginAtualizacao['login']['senha'] = md5($_POST['novaSenha']);
						$atualizacaoHash = $this->clienteModel->updateLogin($loginAtualizacao,$verificaSenha['success']->getLogin()->getId());	

						if(isset($atualizacaoHash['success'])){
							$dados['mensagem'] = " Senha atualizada com sucesso.";
						}else{
							$dados['mensagem'] = " Ocorreu um erro durante a operação. Tente mais tarde.";
						}			

					}else{
						$dados['mensagem'] = " Senha atual digitada está incorreta.";
					}

				}else{
					$dados['mensagem'] = "Senha nova é igual a atual!";
				}	
			}

			$this->loadView('alterar-senha.php',$dados);

		}else{
			echo "<script>window.location='".URL_SITE."/clientes/login'; </script>";
		}	
		
	}

	public function pedido(){
		$dados['teste'] = "";
		$this->loadView('pedido.php',$dados);
	}

	public function logout(){
		
		if(isset($_COOKIE['clienteCookie'])){
			setcookie ("clienteCookie", "", time() - 3600, '/');
		}

		if(isset($_SESSION['cliente_site'])){
			unset($_SESSION['cliente_site']);
		}
		
		echo "<script>window.location='".URL_SITE."/clientes/login'; </script>";
	}

	public function enviarEmail($dados){
		$email         = new Email();
		$respostaEnvio = false;

		if(!empty($dados['email'])){

			$assunto   = " Gainholder - Verificação de cadastro ";
			$conteudo  = " Olá, ".$dados['nome']."<br> Para confirmar sua inscrição click no link abaixo: <br>"; 
			$conteudo .= "<a href='".URL_SITE."/clientes/confirmarToken/".$dados['hash']."'> Confirmar e-mail </a>";

			$respostaEnvio = $email->Enviar(EMAIL_REMETENTE, EMAIL_REMETENTE_NOME, $dados['nome'], $dados['email'], $assunto, $conteudo);

		}

		return $respostaEnvio;
	}

	public function confirmarToken($token){

		if(!empty($token)){

			// VERIFICA A VALIDADE DA HASH
			$verificaToken = $this->clienteModel->getRow("u.*, l.email, l.status", " WHERE u.hash = '".$token."'");

			if(!isset($verificaToken['error'])){
				$atualiza['login']['status'] = '1'; 
				$atualiza['login']['hash']   = '';
				$atualizaStatus = $this->clienteModel->updateLogin($atualiza, $verificaToken['success']->getLogin()->getId());

				// ATUALIZA O STATUS E INICIA SESSÃO DO USUÁRIO
				if(isset($atualizaStatus['success'])){

					$_SESSION['cliente_site']['id_login']  = $verificaToken['success']->getLogin()->getId(); 
					$_SESSION['cliente_site']['email']     = $verificaToken['success']->getLogin()->getEmail();
					$_SESSION['cliente_site']['nome']      = $verificaToken['success']->getNome();
					$_SESSION['cliente_site']['pontuacao'] = 0; 

					echo "<script>window.location='".URL_SITE."/clientes/perfil'; </script>";

				}else{	

					echo "<script>window.location='".URL_SITE."/clientes/cadastro'; </script>";
				}
			}else{
				echo "<script>window.location='".URL_SITE."/clientes/cadastro'; </script>";
			}

		}else{
			echo "<script>window.location='".URL_SITE."/clientes/cadastro'; </script>";
		}
	}
}
?>