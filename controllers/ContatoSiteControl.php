<?php

	class ContatoSiteControl extends Controller{

		private $moduloModel;

		public function __construct(){

			// SETANDO O MÓDULO
			$this->setModulo('contatos');
		}

		//CARREGA A VIEW PRINCIPAL DO MODULO USUÁRIOS
		public function index(){

			$dados['condicao'] = "";

			$this->loadView('lista.php',$dados);
			
		}

		public function enviarEmail(){
			$dados = array();

			$emailClasse = new Email();
		
			if(isset($_POST['modulo']) && $_POST['modulo'] == "contato"){

				$nomeCompleto  = $_POST['nome'];
				$telefone      = $_POST['telefone'];
				$celular       = !empty($_POST['celular'])  ? $_POST['celular'] : " Não informado.";
				$empresa       = !empty($_POST['empresa'])  ? $_POST['empresa'] : " Não informado.";
				$cargo         = !empty($_POST['cargo'])   ? $_POST['cargo'] : " Não informado.";
				$email         = $_POST['email_corporativo'];    
				$cnpj          = !empty($_POST['cnpj']) ? $_POST['cnpj'] : " Não informado.";
				$cidade_estado = !empty($_POST['cidade_estado']) ? $_POST['cidade_estado'] : " Não informado.";
				$pais          = !empty($_POST['pais'])  ? $_POST['pais'] : " Não informado.";
				$mensagem      = $_POST['mensagem'];

				$conteudo  = " <h2> Dados de contato </h2>";
				$conteudo .= "<p><strong>Nome: </strong>".$nomeCompleto."</p>";
				$conteudo .= "<p><strong>Telefone: </strong>".$telefone."</p>";
				$conteudo .= "<p><strong>Celular:  </strong>".$celular."</p>";
				$conteudo .= "<p><strong>Empresa:  </strong>".$empresa."</p>";
				$conteudo .= "<p><strong>Cargo:    </strong>".$cargo."</p>";
				$conteudo .= "<p><strong>E-mail: </strong>".$email."</p>";
				$conteudo .= "<p><strong>CNPJ: </strong>".$cnpj."</p>";
				$conteudo .= "<p><strong>Cidade/Estado: </strong>".$cidade_estado."</p>";
				$conteudo .= "<p><strong>País: </strong>".$pais."</p>";
				$conteudo .= "<p><strong>Mensagem: </strong>".$mensagem."</p>";																												
				$assunto = " Gainholder - Contato ";

				$respostaEnvio = $emailClasse->Enviar(EMAIL_REMETENTE, EMAIL_REMETENTE_NOME, "Suporte", "suporte@gainholder.com", $assunto, $conteudo);


				if($respostaEnvio){
					$dados['mensagem'] = "Contato enviado com sucesso. Aguarde que logo te enviaremos uma resposta.";
				}else{
					$dados['mensagem'] = "Não foi possível enviar a mensagem no momento. Tente mais tarde.";
				}

			}

			$this->loadView("lista.php",$dados);
		}	
	}
?>