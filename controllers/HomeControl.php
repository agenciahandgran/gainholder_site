<?php

	class HomeControl extends Controller{

		private $conteudoModel;
		private $colunistaModel;
		private $bannerModel;

		public function __construct(){

			// SETANDO O MÓDULO
			$this->setModulo('home');
			$this->conteudoModel  = new ConteudoModel();
			$this->colunistaModel = new ColunistaModel();
			$this->bannerModel    = new BannerModel();
		}

		public function loadMethod($acao){

			switch ($acao) {
				
				case 'pesquisa':
					$this->pesquisaAjax($_GET['pesquisa'],$_GET['pgAtual'],$_GET['totalPg']);
				break;

				default:
					# code...
				break;
			}

		}

		//CARREGA A VIEW PRINCIPAL DO MODULO USUÁRIOS
		public function index(){

			$dados['conteudos'] 		  = $this->listarConteudos('WHERE tipo = 1 ORDER BY data_publicacao DESC LIMIT 5 ');
			$dados['artigos']   		  = $this->listarArtigos('WHERE tipo = 2 ORDER BY data_publicacao DESC LIMIT 4');
			// $dados['colunistas']		  = $this->listarColunistas('WHERE c.status = 1 ORDER BY RAND() DESC LIMIT 4');
			$dados['artigosAcessados']	  = $this->listarArtigos('WHERE tipo = 2 ORDER BY visualizacoes DESC LIMIT 4');
			$dados['destaquesConteudos']  = $this->listarConteudos('WHERE tipo = 1 AND destaque = 1 ORDER BY data_publicacao DESC LIMIT 3 ');
			$dados['destaquesArtigos']    = $this->listarConteudos('WHERE tipo = 2 AND destaque = 1 ORDER BY data_publicacao DESC LIMIT 2 ');
			$dados['colunistas'] = $this->colunistaModel->getListColuinstaByRand();
			$dados['bannersTopo']    = $this->bannerModel->getList(" WHERE status = 1 AND posicao = 'topo' ");
			
			$this->loadView('home.php',$dados);
			
		}

		public function listarConteudos($condicao){
			$listar = "";
			
			if(!empty($condicao)){
				$listar = $this->conteudoModel->getListContentByCondition($condicao);
			}

			return $listar;
		}

		public function listarArtigos($condicao){
			$listar = "";

			if(!empty($condicao)){
				$listar = $this->conteudoModel->getListContentByCondition($condicao);
			}

			return $listar;			
		}

		public function listarColunistas($condicao){
			$listar = "";

			if(!empty($condicao)){
				$listar = $this->colunistaModel->getListColunistaByCondition($condicao);
			}

			return $listar;
		}

		public function pesquisa($palavraChave){

			$dados['palavraChave'] = $palavraChave;

			if(!empty($palavraChave)){

				$condicao  = " WHERE c.titulo LIKE '%".$palavraChave."%' OR ";
				$condicao .= " c.resumo LIKE '%".$palavraChave."%' OR ";
				$condicao .= " ctg.nome LIKE '%".$palavraChave."%' OR ";
				$condicao .= " c.texto  LIKE '%".$palavraChave."%' ";
				$condicao .= " ORDER BY data_publicacao DESC LIMIT 2 ";

				$dados['contador']   = $this->conteudoModel->getListContentByCondition($condicao, " count(c.id_conteudo) as total");

				if($dados['contador'][0]->getTotal() > 0){
					
					$totalResultados = $dados['contador'][0]->getTotal();

					if($totalResultados > 2){
						$totalPaginas = ceil($totalResultados/2);
					}else{
						$totalPaginas = 0;
					}

				}else{
					$totalPaginas = 0;
				}
				
				$dados['totalPaginas'] = $totalPaginas;

				$dados['resultados'] = $this->conteudoModel->getListContentByCondition($condicao);

			}else{
				$dados['resultados'] = "";
			}

			$this->loadView('pesquisa.php',$dados);
		}

		public function pesquisaAjax($pesquisa, $pgAtual, $totalPg){
			$retornoJson['html'] = "";
			$html = "";

			if($pgAtual <= $totalPg){
			
				$start  = ($pgAtual * 2) - 2;

				$condicao  = " WHERE c.titulo LIKE '%".$pesquisa."%' OR ";
				$condicao .= " c.resumo LIKE '%".$pesquisa."%' OR ";
				$condicao .= " ctg.nome LIKE '%".$pesquisa."%' OR ";
				$condicao .= " c.texto  LIKE '%".$pesquisa."%' ";
				$condicao .= " ORDER BY data_publicacao DESC LIMIT $start, 2 ";	

				$resultados  = $this->conteudoModel->getListContentByCondition($condicao);

				if(!isset($resultados['error'])){

					foreach($resultados as $resultado){

						$modulo = ($resultado->getTipo() == 1 ? "/conteudos/noticia/" : "/artigos/artigo/");
						
						$html .= "<div class='col-xs-12 col-sm-10 p0'>
							<div class='col-xs-12 texto'>
								<div class='col-xs-12 col-md-10 conteudo-list p0'>
									<div class='titulo-destaque p0'>
										<a href='".URL_SITE.$modulo.$resultado->getSlug().".html'>
											<p>".$resultado->getTitulo()."</p>
										</a>
									</div>
									<p class='excerpt-conteudo-home'>
										".$resultado->getResumo()."
									</p>
									<p class='date-conteudo-home dataConteudoLista'>".formatarDataExtenso(date('d/m/Y', strtotime($resultado->getDataPublicacao())))."</p>
								</div>
							</div>
						</div>";
					}

					$retornoJson['html'] 	  = $html;
					$retornoJson['pgAtual']   = $pgAtual+1;
					$retornoJson['resultado'] = true;

				}else{
					$retornoJson['resultado'] = false;
				}
			
			}else{

				$retornoJson['resultado'] = false;
			}	

			echo json_encode($retornoJson);
			 
		}	

		public function gerarTituloDinamico($modulo){

			$titulo = "Gainholder";

			if(strstr($modulo, "/")){

				$modulos = explode("/", $modulo);
				
				if(isset($modulos[2])){
					$slug = substr($modulos[2], 0, strrpos($modulos[2], "."));
				}

				if($modulos[0] == "conteudos"){

					if(!empty($slug)){
						$detalharConteudo = $this->conteudoModel->getRow("c.*,  ctg.nome as nomeCategoria, ctg.id_categoria as idCategoria ", " WHERE c.tipo = 1 AND c.slug = '".$slug."' ");
					}

					if(isset($detalharConteudo['success'])){
						$titulo = $detalharConteudo['success']->getTitulo()." | ".$titulo;
					}

				}else if($modulos[0] == "artigos"){

					if(!empty($slug)){
						$detalharConteudo = $this->conteudoModel->getRow("c.*, ctg.nome as nomeCategoria, ctg.id_categoria as idCategoria, cl.nome_completo, cl.descricao, cl.foto_perfil ", " INNER JOIN colunistas cl ON cl.id_login = c.id_autor
							WHERE c.tipo = 2 AND c.slug = '".$slug."' ");
					}

					if(isset($detalharConteudo['success'])){
						$titulo = $detalharConteudo['success']->getTitulo()." | ".$titulo;
					}

				}else{
					$titulo = " Gainholder ";
				}
			
			}else{

				if($modulo != "home"){
					$titulo .= " - ".ucfirst($modulo);
				}
			}
			
			return $titulo;
		}
	}
?>