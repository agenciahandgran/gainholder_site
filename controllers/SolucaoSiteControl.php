<?php

	class SolucaoSiteControl extends Controller{

		private $moduloModel;

		public function __construct(){

			// SETANDO O MÓDULO
			$this->setModulo('solucoes');
		}

		public function loadMethod($acao){

			switch ($acao) {
				case 'gerarHistorico':
					$this->gerarHistorico();
				break;
				
				default:
					# code...
					break;
			}
		}

		//CARREGA A VIEW PRINCIPAL DO MODULO USUÁRIOS
		public function index(){

			if(isset($_SESSION['cliente_site']) || isset($_COOKIE['clienteCookie'])){

				$dados['condicao'] = "";

				$this->loadView('index.php',$dados);

			}else{
				echo "<script>window.location='".URL_SITE."/clientes/login'; </script>";
			}
			
		}

		//CARREGA A VIEW PRINCIPAL DO MODULO USUÁRIOS
		public function vanco(){

			if(isset($_SESSION['cliente_site']) || isset($_COOKIE['clienteCookie'])){

				$dados['condicao'] = "";

				$this->loadView('vanco.php',$dados);

			}else{
				echo "<script>window.location='".URL_SITE."/clientes/login'; </script>";
			}
			
			
		}

		public function gerarHistorico(){
			$respostaJson = array();

			if(isset($_POST['texto']) && !empty($_POST['texto'])){

				$emailClasse = new Email();

				$assunto  = "Histórico Index";
				$conteudo = $_POST['texto'];

				$calculos = $this->gerarCalculosIndex($_POST['produtosPreco']);

				$textoHtml  = "<h1> Dados Index </h1>";
				$textoHtml .= "<p> <strong> Natureza: </strong> ".$_POST['tipoEmbarque']." </p>";
				$textoHtml .= "<h3> Produtos </h3>";
				$textoHtml .= "<dl>";

				$produtosNome       = explode(",", $_POST['produtosNome']);
				$produtosPreco      = explode(",", $_POST['produtosPreco']);
				$produtosQuantidade = explode(",", $_POST['produtosQuantidade']);
				$produtosCodigosNcm = explode(",", $_POST['produtosCodigosNcm']);
				
				for($i=0; $i<count($produtosNome); $i++){

					if(!empty($produtosNome[$i])){
						$textoHtml .= "<dt> Nome: </dt>"; 
						$textoHtml .= "<dd> ".$produtosNome[$i]." </dt>"; 
					}

					if(!empty($produtosNome[$i])){
						$textoHtml .= "<dt> Preço: </dt>"; 
						$textoHtml .= "<dd> ".$produtosPreco[$i]." </dt>"; 
					}

					if(!empty($produtosNome[$i])){
						$textoHtml .= "<dt> Quantidade: </dt>"; 
						$textoHtml .= "<dd> ".$produtosQuantidade[$i]." </dt>"; 
					}

					if(!empty($produtosNome[$i])){
						$textoHtml .= "<dt> Código NCM: </dt>"; 
						$textoHtml .= "<dd> ".$produtosCodigosNcm[$i]." </dt>"; 
					}
				}

				$textoHtml .= "</dl>";	

				$textoHtml .= "<h3> Incoterm </h3>";

				if($_POST['tipoFrete'] == '1'){
					$textoHtml .= "<p> Marítimo: EXW/FOB/FAS <br> Valor do Frete ".$_POST['valorFrete']." </p>";
				}
				
				if($_POST['tipoFrete'] == '2'){
					$textoHtml .= "<p> Aéreo: EXW/FCA <br> Valor do Frete ".$_POST['valorFrete']." </p>";
				}	

				if(!empty($calculos)){

					$textoHtml .= "<h2> Calculos Index - Impostos </h2>";

					for($j=0; $j<count($calculos); $j++){

						$textoHtml .= "<h4> Produto: ".$produtosNome[$j+1]." </h2>"; 

						$textoHtml .= "<p><strong> II: </strong>".$calculos[$j]['II']." </p>";
						$textoHtml .= "<p><strong> IPI: </strong>".$calculos[$j]['IPI']." </p>";
						$textoHtml .= "<p><strong> PIS: </strong>".$calculos[$j]['PIS']." </p>";
						$textoHtml .= "<p><strong> COFINS: </strong>".$calculos[$j]['COFINS']." </p>";
					}
				}

				$conteudo = $textoHtml;	

				$respostaEnvio = $emailClasse->Enviar(EMAIL_REMETENTE, EMAIL_REMETENTE_NOME, $_SESSION['cliente_site']['nome'], $_SESSION['cliente_site']['email'], $assunto, $conteudo);

				if($respostaEnvio){
					
					$respostaJson['status']   = "1";
					$respostaJson['mensagem'] = "Foi enviando um e-mail com os cálculos e outras informações preenchidas pelo usuário.";
				}else{
					$respostaJson['status']   = "2";
					$respostaJson['mensagem'] = "Ocorreu um erro durante a operação, tente novamente mais tarde.";
				}				

			}else{
				$respostaJson['status'] = "2";
			} 

			echo json_encode($respostaJson);
		}

		
		public function gerarCalculosIndex($precos){
			$calculos = array();
			if(!empty($precos)){

				$precos = explode(",", $precos);

				$valorFreteInternacional = 1000;
				$seguro = 50;
				$outros = 100;
				$cont   = 0;

				foreach($precos as $preco){

					if(!empty($preco)){

						$preco = number_format($preco,2,',','.');
	
						$valorMercadoria = (int) $preco + $valorFreteInternacional + $seguro + $outros;

						$impostoB = $valorMercadoria - ($valorMercadoria / 100 * 10);
						$imposto2 = $valorMercadoria * $impostoB;

						$impostoC = $valorMercadoria - ($valorMercadoria / 100 * 15);
						$imposto3 = ($valorMercadoria + $impostoB) * $impostoC;

						$impostoD = $valorMercadoria - ($valorMercadoria / 100 * 2);
						$imposto4 = ($valorMercadoria * $impostoD); 

						$impostoE = $valorMercadoria - ($valorMercadoria / 100 * 9);
						$imposto5 = ($valorMercadoria * $impostoE);

						$calculos[$cont]['II']     = number_format($imposto2,0,',','.');
						$calculos[$cont]['IPI']    = number_format($imposto3,0,',','.');
						$calculos[$cont]['PIS']    = number_format($imposto4,0,',','.');
						$calculos[$cont]['COFINS'] = number_format($imposto5,0,',','.');

						$cont++;
					}
				}
			}

			return $calculos;
		}
	}
?>