<?php

// COOKIES
ob_start();

// SESSÕES

// DURAÇÃO UMA HORA
session_set_cookie_params(3600);
session_start();

include_once('painel/config/Config.php');
include_once('painel/library/Autoload.php');
include_once('painel/helpers/Helpers.php');

// URL PROTOCOL
$protocolo = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https' : 'http';

// URL
define('URL_SITE', $protocolo . '://' . $_SERVER['HTTP_HOST'] . SITE_PATH);

if(!isset($_GET['pag'])){
    echo "<script>window.location='".URL_SITE."/home'</script>";
}

$homeControl   = new HomeControl();
$artigosRodape = $homeControl->listarArtigos('WHERE tipo = 2 ORDER BY data_publicacao DESC LIMIT 5');

$tituloDinamico = "Gainholder";

if(isset($_GET['pag'])){
    $tituloDinamico = $homeControl->gerarTituloDinamico($_GET['pag']);    
}

?>
<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
    <head>
        <meta charset="UTF-8">
        <title><?php echo $tituloDinamico; ?></title>

        <link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="#" rel="shortcut icon">
        <link href="#" rel="apple-touch-icon-precomposed">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="keywords" content="">
        <meta name="description" content="">
        <link rel='dns-prefetch' href='//s.w.org' />
        <link rel="icon" type="image/png" href="<?php echo URL_SITE ?>/assets/img/icon.png" />

        <style type="text/css">
            img.wp-smiley,
            img.emoji {
                display: inline !important;
                border: none !important;
                box-shadow: none !important;
                height: 1em !important;
                width: 1em !important;
                margin: 0 .07em !important;
                vertical-align: -0.1em !important;
                background: none !important;
                padding: 0 !important;
            }

        </style>

        <link rel="stylesheet" type="text/css" href="<?php echo URL_SITE ?>/assets/css/theme-my-login.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo URL_SITE ?>/assets/css/accesspress.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo URL_SITE ?>/assets/css/cf7.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo URL_SITE ?>/assets/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo URL_SITE ?>/assets/css/font-awesome.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo URL_SITE ?>/assets/css/font-awesome.min26ef.css" />

        <link rel="stylesheet" type="text/css" href="<?php echo URL_SITE ?>/assets/css/style.css" />

        <link rel="stylesheet" type="text/css" href="<?php echo URL_SITE ?>/assets/css/homemobile.css" media="screen and (max-width:1000px)" />

        <link rel="stylesheet" type="text/css" href="<?php echo URL_SITE ?>/assets/css/homepc.css" media="screen and (min-width:1001px)" />

        <link rel="stylesheet" type="text/css" href="<?php echo URL_SITE ?>/assets/css/sweetalert2.css">

        <script type="text/javascript" src="<?php echo URL_SITE ?>/assets/js/jquery-1.11.3.js"></script>

        <script type="text/javascript" src="<?php echo URL_SITE ?>/assets/js/bootstrap.js"></script>

        <script type="text/javascript" src="<?php echo URL_SITE ?>/assets/js/sweetalert2.js"></script>

        <script type="text/javascript" src="<?php echo URL_SITE; ?>/assets/js/plugins.js"></script>

        <script type="text/javascript" src="<?php echo URL_SITE; ?>/assets/js/geral.js"></script>

        <script type="text/javascript" src="<?php echo URL_SITE ?>/assets/js/jquery.correios.js"></script>

        <script type="text/javascript" src="<?php echo URL_SITE ?>/painel/assets/js/jquery.maskMoney.min.js"></script>
        <?php
            if(isset($_GET['pag'])){

                // SE EXISITR JS PARA A PÁGINA
                $modulo = strstr($_GET['pag'], '/') ? substr($_GET['pag'],0,strpos($_GET['pag'], '/')) : $_GET['pag'];

                if (file_exists('assets/js/pagina.' . $modulo . '.js')) {

                    echo '<script type="text/javascript" src="' . URL_SITE. '/assets/js/pagina.' . $modulo . '.js"></script>';
                }
            }
        ?>
        <script type="text/javascript">
            var URL = "<?php echo URL_SITE; ?>";
            $(document).ready(function(){
                $(".cep").mask("99999-999");

                /* BUSCA RÁPIDA DE CEP */
                $('.cep').keyup(function(event) {

                    if(event.keyCode == '13') {

                        event.preventDefault();
                    }

                    Delay(

                        function(){

                            BuscarCep(URL);
                        }, 

                        800
                    );  

                });
            });
        </script>
    </head>
    <body class="home blog">
        <?php

            // O TOPO SÓ NÃO SERÁ CARREGADO NA TELA QUE ATUALIZA A SENHA, FEITA PÓS UM PEDIDO DE REDEFINIÇÃO
            if(isset($_GET['pag'])){

                $explode = explode('/',$_GET['pag']);

                if(!in_array('novaSenha', $explode)){

                    include('topo.php');
                }
            }else{

                include('topo.php');
            }
                                            // 
            if(isset($_GET['pag'])){
                $pagina = new Loader($_GET['pag']);
                $pagina->redirecionar();
            }

            include('rodape.php');
        ?>

    </body>
</html>
<?php
    ob_end_flush(); // Flush the output from the buffer
?>
