<?php 

/*************************************
* CARREGAMENTO DAS PÁGINAS
**************************************/

class Loader{

	private $modulo;

	public function __construct($modulo){
		$this->modulo = $modulo;
	}

	private function listaPaginasPermitidas(){

		$paginas = array(

			'home' => array(
				'url'        => 'home/home.php',
				'controller' => 'home'				
			),

			'institucionais' => array(
				'url'        => 'institucional/historia.php',
				'controller' => 'institucional'
			),

			'conteudos' => array(
				'url'        => 'conteudos/lista.php',
				'controller' => 'conteudoSite'
			),

			'artigos' => array(
				'url'        => 'artigos/lista.php',
				'controller' => 'artigoSite'
			),

			'contatos' => array(
				'url'        => 'contatos/lista.php',
				'controller' => 'contatoSite'
			),

			'assinantes' => array(
				'url'        => 'assinantes/lista.php',
				'controller' => 'assinanteSite'
			),

			'clientes' => array(
				'url'  		 => 'clientes/login.php',
				'controller' => 'clienteSite'
			),
			'solucoes' => array(
				'url'  		 => 'solucoes/index.php',
				'controller' => 'solucaoSite'
			),
			'logout' => array(
				'url' => '',
			)

		);

		return $paginas;
	}

	public function getControllerPage($acao = false){

		if(!empty($this->modulo)){
			$paginas = $this->listaPaginasPermitidas();
			$modulo  = $this->transformaUrl();

			if(isset($paginas[$modulo['modulo']])){

				if(!$acao){

					$resultado = $paginas[$modulo['modulo']]['controller'];
				}else{

					$resultado['controller'] = $paginas[$modulo['modulo']]['controller'];
					$resultado['acao']       = $modulo['acao'];

					if(isset($modulo['id'])){

						$resultado['id'] = $modulo['id'];
					}

					if(isset($modulo['pagina'])){
						$resultado['pagina'] = $modulo['pagina'];
					}

					if(isset($modulo['adicional'])){
						$resultado['adicional'] = $modulo['adicional'];
					}
				}
			}else{

				$resultado = null;
			}
		}else{

			$resultado = null;
		}

		return $resultado;
	}

	public function redirecionar(){
		$paginas = $this->listaPaginasPermitidas();
		$modulo  = $this->transformaUrl();

		if(isset($paginas[$modulo['modulo']])){

			if($modulo['modulo'] != 'logout'){	

				if(file_exists('views/'.$modulo['modulo'])){
					include('views/template/view.php');
				}else{
					errorPage(ERROR_PAGE_NOT_FOUND);    
				}
			}else{
				$this->logout();
			}

		}else{
			errorPage(ERROR_CLASS_NOT_FOUND);    
		}
	}

	private function transformaUrl(){
		if(strstr($this->modulo,'/')){
			$acao = explode('/', $this->modulo);
			$modulo = array('modulo' => $acao[0], 'acao' => $acao[1]);
			
			if(isset($acao[2])){
				$modulo['id'] = $acao[2]; 
			}

			if(isset($acao[3])){
				$modulo['pagina'] = $acao[3];
			}

			if(isset($acao[4])){
				$modulo['adicional'] = $acao[4];
			}
			
		}else{
			$modulo = array('modulo' => $this->modulo, 'acao' => '');
		}

		return $modulo;
	}

	private function logout(){
		unset($_SESSION['USER']);
		echo "<script>window.location='".URL.''."'</script>";
	}
}

?>