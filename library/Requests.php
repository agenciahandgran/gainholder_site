<?php

session_start();

/**  RECEBE UMA REQUISIÇÃO VIA GET/POST E REALIZA O TRATAMENTO PARA O MÉTODO CORRETO */
require_once ('../painel/config/Config.php');
require_once ('../painel/helpers/Helpers.php');
require_once ('../painel/library/Autoload.php');

$protocolo = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https' : 'http';

define('URL_SITE', $protocolo . '://' . $_SERVER['HTTP_HOST'] . SITE_PATH);

if(isset($_REQUEST['modulo']) && !empty($_REQUEST['modulo'])){

	$controller = $_REQUEST['modulo'];
	$acao       = isset($_REQUEST['acao']) ? $_REQUEST['acao'] : null; 
	
	switch ($controller){

		case 'home':

			$homeControl = new HomeControl();
			$homeControl->loadMethod($acao);

		break;	

		case 'clientes':
			$clienteSiteControl = new ClienteSiteControl();
			$clienteSiteControl->loadMethod($acao);
		break;

		case 'conteudo':
			$conteudoSiteControl = new ConteudoSiteControl();
			$conteudoSiteControl->loadMethod($acao);
		break;

		case 'artigo':
			$artigoSiteControl = new ArtigoSiteControl();
			$artigoSiteControl->loadMethod($acao);
		break;

		case 'correios':

			if(isset($_GET['cep']) && !empty($_GET['cep'])){

				$consultaCEP = array();

				// CORREIO
				$correio = new Correio($_GET['cep']);
				$cep     = $correio->ConsultarCep();

				if(isset($cep['Erro'])){

					$result = $cep;
				}else{

					$consultaCEP['estado']	   = $cep['uf'];
					$consultaCEP['cidade']	   = $cep['cidade'];
					$consultaCEP['bairro']	   = $cep['bairro'];
					$consultaCEP['logradouro'] = $cep['logradouro'];
					$result = $consultaCEP;
				}
			}

			print_r(json_encode($result));
		break;

		case 'assinatura':
			$assinanteSiteControl = new AssinanteSiteControl();
			$assinanteSiteControl->loadMethod($acao);
		break;

		case 'solucoes':
			$solucaoSiteControl = new SolucaoSiteControl();
			$solucaoSiteControl->loadMethod($acao);
		break;

		case 'exemplos':

			$exemploControl = new ExemploControl();
			$exemploControl->loadMethod($acao,$id);

		break;
	}
}