$(function(){
	
	var urlRequest      = URL+"/library/Requests.php";

	$("#datatable-responsive").DataTable( {
		"language": {
			"lengthMenu":     "Exibir _MENU_ artigos",
			"search":         "Pesquisar:",
			"infoEmpty":      "Exibindo 0 de 0 de um total 0 resultados",
			"info":           "Exibindo _START_ de _END_ em um total de _TOTAL_ resultados",
			"paginate": {
				"first":      "Primeira",
				"last":       "Última",
				"next":       "Próxima",
				"previous":   "Anterior"
			},
			"emptyTable":     "Nenhum resultado encontrado."
		}
	});

	// INTERAÇÃO COM OS MÓDULOS DE PERMISSÃO 
	$('.opcao-todos-modulos').on('ifChanged', function(event){				

		var pai = $(this).parent();

		if(pai.hasClass('checked')){
			$(".icheckbox_flat-green").removeClass('checked');
			$(".opcao-modulo").removeAttr('checked');
		}else{
			$(".icheckbox_flat-green").addClass('checked');
			$(".opcao-modulo").attr('checked','checked');
		}
		
	});

	// INTERAÇÃO EDIÇÃO IMAGEM
	$(".trocar-foto-perfil").click(function(){
		$("#artigo_banner").trigger('click');
	});

	$('#artigo_banner').change(function(event) {
		
		$(".imagem-perfil").fadeIn("fast").attr('src',webkitURL.createObjectURL(event.target.files[0]));

	});	

	// MODAL PARA EXCLUSÃO DO REGISTRO
	$(".excluirRegistro").click(function(){

		var idRegistro = $(this).attr('data-id');

		swal({

			title: 'Informação ao remover',
			text: "Você tem certeza que deseja excluir esse registro ?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sim, remover!',
			cancelButtonText: 'Cancelar!',
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			buttonsStyling: false

		}).then(function () {

			$.ajax({
				url: URL+"/library/Requests.php",
				type: 'GET',
				data : {
					modulo    : 'artigos',
					acao      : 'delete',
					id        : idRegistro
				},
				dataType: 'json',
				cache: false,
				success: function (resp) {

					if(typeof(resp) != "undefined" && resp != null){

						if(resp.resultado){

							swal(
								'Informação',
								'Registro removido.',
								'success'
								);

							setTimeout(function(){
								location.reload();
							}, 2000);	
						}

					}else{
						swal(
							'Cancelado',
							'Ocorreu um erro durante a operação.',
							'error'
							);
					}

				},

				error: function(resp) {
					swal(
						'Cancelado',
						'Ocorreu um erro durante a operação.',
						'error'
						);
				}

			});
			
		}, function (dismiss) {

		  // dismiss can be 'cancel', 'overlay',
		  // 'close', and 'timer'
		  if (dismiss === 'cancel') {
		  	swal(
		  		'Cancelado',
		  		'Operação cancelada pelo usuário.',
		  		'error'
		  		);
		  }
		})
	});

	// INTERAÇÃO COM O EDITOR DE TEXTO
	$("#edit").keydown(function(){
		texto = $(this).text();

		if(texto != ""){
			$("#salvar-alteracoes-editor").prop('disabled',false);
		}else{
			$("#salvar-alteracoes-editor").prop('disabled',true);
		}
	});

	$.FroalaEditor.DefineIcon('imageInfo', {NAME: 'info'});
	
	$.FroalaEditor.RegisterCommand('imageInfo', {
		title: 'Info',
		focus: false,
		undo: false,
		refreshAfterCallback: false,
		callback: function () {
			var $img = this.image.get();
			alert($img.attr('src'));
		}
	});

	$('#edit').froalaEditor({

	    // Set the image upload parameter.
	    imageUploadParam: 'imagemEditorTexto',

	    // Set the image upload URL.
	    imageUploadURL: URL+"/library/Requests.php",

	    // Additional upload params.
	    imageUploadParams: {modulo: 'artigos', acao:'uploadFotos'},

	    // Set request type.s
	    imageUploadMethod: 'POST',

	    fontFamily: {
	    	"Roboto,sans-serif": 'Roboto',	
	    	"Arial, sans-serif": 'Arial',
	    	"Verdana, sans-serif" : 'Verdana'
	    }

	    // // Set max image size to 5MB.
	    // imageMaxSize: 5 * 1024 * 1024,

	    // // Allow to upload PNG and JPG.
	    // imageAllowedTypes: ['jpeg', 'jpg', 'png']
	})
	.on('froalaEditor.image.beforeUpload', function (e, editor, images) {
        // Return false if you want to stop the image upload.
        console.log('Evento chamado antes da finalização do upload.');
    })
	.on('froalaEditor.image.uploaded', function (e, editor, response) {
        // Image was uploaded to the server.
        console.log('Upload realizado com sucesso');
    })
	.on('froalaEditor.image.inserted', function (e, editor, $img, response) {
        // Image was inserted in the editor.
        console.log('Imagem inserida no editor');
    })
	.on('froalaEditor.image.replaced', function (e, editor, $img, response) {
        // Image was replaced in the editor.
        console.log('Evento para substituição da imagem');
    })
	.on('froalaEditor.image.error', function (e, editor, error, response) {

    // Bad link.
    if (error.code == 1) { 
    	console.log('ERRO LINK');
    }

    // No link in upload response.
    else if (error.code == 2) { 
    	console.log('Link não encontrado na resposta do upload.');
    }

    // Error during image upload.
    else if (error.code == 3) { 
    	console.log('Erro durante o upload da imagem.');
    }

    // Parsing response failed.
    else if (error.code == 4) {
    	console.log('Resposta falhou.');
    }

    // Image too text-large.
    else if (error.code == 5) {
    	console.log('Imagem com tamanho excedido.');
    }

    // Invalid image type.
    else if (error.code == 6) {
    	console.log('Imagem com tipo inválido.');
    }

    // Image can be uploaded only to same domain in IE 8 and IE 9.
    else if (error.code == 7) {
    	console.log('Imagem só pode ser enviado para mesmo domínio em I8 e IE 9');
    }

    // Response contains the original server response to the request if available.
	});

	$("div#edit").froalaEditor({
		// Options edit image
		imageEditButtons: ['imageDisplay', 'imageAlign', 'imageInfo', 'imageRemove']
	});

	// SALVAR DADOS DO EDITOR
	$("#salvar-alteracoes-editor").click(function(){

		var texto = $('div#edit').froalaEditor('html.get');
		$(".artigo_texto").val(texto);

		$('.bs-example-modal-lg').modal('toggle');
	});

	// LIMITADOR CARACTERES RESUMO
    $("#artigo_resumo").keypress(function(e){
	
      var qtdCaracteres  = $(this).val().length;

      if(qtdCaracteres == 95){        
     	return false;
      }else{
      	var contAtual = (qtdCaracteres+1); 
      	$(".contador-resumo").text(contAtual);	
      }

    });

    $("#artigo_resumo").keydown(function(e){
	
      var qtdCaracteres  = $(this).val().length;

      $(".contador-resumo").text(qtdCaracteres);
    });    

    $("#artigo_resumo").on('paste', function(event){

       var campo  = $(this);
        
        setTimeout(function(){
          
          var qtdCaracteres = campo.val().length;

	      if(qtdCaracteres > 95){        
	        campo.val("");
	      }else{
	      	var contAtual = (qtdCaracteres+1); 
      		$("contador-resumo").text(contAtual);	
	      }  
        },0); 

    });		

});
