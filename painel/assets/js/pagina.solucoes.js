$(function(){
	
	var urlRequest      = URL+"/library/Requests.php";

	$("#datatable-responsive").DataTable( {
		"language": {
			"lengthMenu":     "Exibir _MENU_ soluções",
			"search":         "Pesquisar:",
			"infoEmpty":      "Exibindo 0 de 0 de um total 0 resultados",
			"info":           "Exibindo _START_ de _END_ em um total de _TOTAL_ resultados",
			"paginate": {
				"first":      "Primeira",
				"last":       "Última",
				"next":       "Próxima",
				"previous":   "Anterior"
			},
			"emptyTable":     "Nenhum resultado encontrado."
		}
	});

	// INTERAÇÃO COM OS MÓDULOS DE PERMISSÃO 
	$('.opcao-todos-modulos').on('ifChanged', function(event){				

		var pai = $(this).parent();

		if(pai.hasClass('checked')){
			$(".icheckbox_flat-green").removeClass('checked');
			$(".opcao-modulo").removeAttr('checked');
		}else{
			$(".icheckbox_flat-green").addClass('checked');
			$(".opcao-modulo").attr('checked','checked');
		}
		
	});

	// INTERAÇÃO EDIÇÃO IMAGEM
	$(".trocar-foto-perfil").click(function(){
		$("#solucao_banner").trigger('click');
	});

	$('#solucao_banner').change(function(event) {
		
		$(".imagem-perfil").fadeIn("fast").attr('src',webkitURL.createObjectURL(event.target.files[0]));

	});	

	// MODAL PARA EXCLUSÃO DO REGISTRO
	$(".excluirRegistro").click(function(){

		var idRegistro = $(this).attr('data-id');

		swal({

			title: 'Informação ao remover',
			text: "Você tem certeza que deseja excluir esse registro ?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sim, remover!',
			cancelButtonText: 'Cancelar!',
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			buttonsStyling: false

		}).then(function () {

			$.ajax({
				url: URL+"/library/Requests.php",
				type: 'GET',
				data : {
					modulo    : 'solucoes',
					acao      : 'delete',
					id        : idRegistro
				},
				dataType: 'json',
				cache: false,
				success: function (resp) {

					if(typeof(resp) != "undefined" && resp != null){

						if(resp.resultado){

							swal(
								'Informação',
								'Registro removido.',
								'success'
								);

							setTimeout(function(){
								location.reload();
							}, 2000);	
						}

					}else{
						swal(
							'Cancelado',
							'Ocorreu um erro durante a operação.',
							'error'
							);
					}

				},

				error: function(resp) {
					swal(
						'Cancelado',
						'Ocorreu um erro durante a operação.',
						'error'
						);
				}

			});
			
		}, function (dismiss) {

		  // dismiss can be 'cancel', 'overlay',
		  // 'close', and 'timer'
		  if (dismiss === 'cancel') {
		  	swal(
		  		'Cancelado',
		  		'Operação cancelada pelo usuário.',
		  		'error'
		  		);
		  }
		})
	});

	// INTERAÇÃO COM O EDITOR DE TEXTO
	$("#editor-one").keydown(function(){
		texto = $(this).text();

		if(texto != ""){
			$("#salvar-alteracoes-editor").prop('disabled',false);
		}else{
			$("#salvar-alteracoes-editor").prop('disabled',true);
		}
	});

	// SALVAR DADOS DO EDITOR
	$("#salvar-alteracoes-editor").click(function(){
		texto = $("#editor-one").html();
		$(".solucao_texto").val(texto);

		$('.bs-example-modal-lg').modal('toggle');
	});

});
