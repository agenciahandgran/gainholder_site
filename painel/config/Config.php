<?php
	
	// EXIBE ERROS
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	// TIMEZONE & ENCODING
	date_default_timezone_set("Brazil/East");
	mb_internal_encoding('UTF-8');

	// DEFINIÇÕES POR AMBIENTE
    switch ($_SERVER['HTTP_HOST']) {

    	// AMBIENTE LOCAL
    	case 'localhost':

		    //CONFIGURAÇÃO URL RAIZ
			$url = 'http://'.$_SERVER['HTTP_HOST'].'/projetos/2018/gainholder_site/painel';
			define('URL',$url);	

    		// DIRETÓRIO
    		define('SITE_PATH',	'/projetos/2018/gainholder_site');
    		define('SITE_HOME', '/projetos/2018/gainholder_site/');

    		// BANCO DE DADOS
    		define('HOST',	   'localhost');
			define('DB_NAME',  'gainholder');
			define('USER',	   'root');
			define('PASSWORD', '');

			define('EMAIL_PRINCIPAL', '');

			// CONFIGURAÇÕES DE E-MAIL PARA TESTES NO LOCALHOST
			define('EMAIL_HOST', 		   'smtp.mailtrap.io');
			define('EMAIL_PORTA', 		   '2525');
			define('EMAIL_SSL', 		   'tls');
			define('EMAIL_REMETENTE', 	   'f86db2e01c-e0828f@inbox.mailtrap.io');
			define('EMAIL_REMETENTE_NOME', 'Teste');
			define('EMAIL_USUARIO', 	   '78cac45d9d7441');
			define('EMAIL_SENHA', 		   '471db797d58c9d');

			// CONFIGURAÇÕES PAGSEGURO
			define('PAGSEGURO_EMAIL', 'tiagoelhouri@gmail.com');
			define('PAGSEGURO_TOKEN', '29BC0A258F9A40E4921983A80A6020D1');

    	break;

   		// AMBIENTE DE HOMOLOGAÇÃO
    	case 'gainholder.handgran.com.br':

		    //CONFIGURAÇÃO URL RAIZ
			$url = 'http://'.$_SERVER['HTTP_HOST'].'/painel';
			define('URL',$url);	

    		// DIRETÓRIO
    		define('SITE_PATH',	'');
    		define('SITE_HOME', '/');

    		// BANCO DE DADOS
    		define('HOST',	   'localhost');
			define('DB_NAME',  'hgcom_gainholder');
			define('USER',	   'hgcom_teste');
			define('PASSWORD', 'teste');

			define('EMAIL_PRINCIPAL', '');

			// CONFIGURAÇÕES DE E-MAIL PARA AMBIENTE DE HOMOLOGAÇÃO E PRODUÇÃO
			define('EMAIL_HOST', 		   'smtp.gmail.com');
			define('EMAIL_PORTA', 		   '465');
			define('EMAIL_SSL', 		   'ssl');
			define('EMAIL_REMETENTE', 	   'agenciahandgran@gmail.com');
			define('EMAIL_REMETENTE_NOME', 'Handgran');
			define('EMAIL_USUARIO', 	   'agenciahandgran@gmail.com');
			define('EMAIL_SENHA', 		   'HandRoi2018');

    	break;

      	// AMBIENTE DE PRODUÇÃO
    	case '':

    		define('URL',			'/painel/');
    		define('SITE_PATH',				'/');
    		define('SITE_HOME',				'/');

    		define('HOST',			'');
			define('DB_NAME',				'');
			define('USER',			'');
			define('PASSWORD',		'');

			define('EMAIL_PRINCIPAL', '');

    	break;
    }

    //API'S 
	define('FACEBOOK_KEY','');
	define('TWITTER_KEY','');
	define('GOOGLE_KEY','');

	//URL'S REDES SOCIAIS
	define('SITE_FACEBOOK','');
	define('SITE_TWITTER','');


	// CONSTANTES ERROS SISTEMA
	const ERROR_PAGE_NOT_FOUND   = "Página não encontrada.";
	const ERROR_ID_NOT_FOUND     = "Registro não encontrado.";
	const ERROR_CLASS_NOT_FOUND  = "Modulo não existente.";
	const ERROR_METHOD_ACTION    = "Essa ação não existe no módulo atual.";
	const ERROR_PERMISSION_PAGE  = "Você não possui permissão para acessar essa página.";
	const ERROR_MODULO_INCORRECT = "Módulo incorreto para realizar essa ação.";
	const ERROR_SEARCH_FORMAT    = "Formato inválido para realizar a busca.";

	// CONSTANTE PASTA PADRÃO UPLOAD 
	const URL_PATH_UPLOAD        = "assets/img/upload/";

	// AÇÕES ENTRE O FORMULÁRIO
	const INSERT                 = 1;
	const UPDATE                 = 2;

	// CONSTANTE PARA DEFINIR SE O SITE SER ACESSADO SER REDIRECIONADO PARA O PAINEL 0 - Não, 1 - Sim;
	const REDIRECT_PAINEL = 1;


?>