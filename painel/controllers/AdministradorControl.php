<?php 

	/**************************************
	* CONTROLLER ADMINISTRADOR
	**************************************/

	class AdministradorControl extends Controller{

		private $administradorModel;
		private $moduloModel;
		private $usuarioModel;

		public function __construct(){

			// SETANDO O MÓDULO
			$this->setModulo('administradores');

			$permission = new Permissions();
			$this->setPaginasPermitidas($permission);

			if($this->verifyPermissionPage()){
				// CARREGANDO A MODEL PRINCIPAL
				$this->administradorModel 		= new AdministradorModel();
				$this->moduloModel        		= new ModuloModel();
				$this->usuarioModel       		= new ClienteModel();
				$this->limite             		= 20;
				$this->caminhoImagensFotoPerfil = "administrador_perfil";
			}else{

				errorPage(ERROR_PERMISSION_PAGE);
				exit;
			}
		}

		//CARREGA A VIEW PRINCIPAL DO MODULO
		public function index($pg = 1){

			$dados['pg']              = ''; 
			$dados['condicao']        = ''; 
			$dados['url']             = ''; 
			$dados['limite']          = ''; 
			$dados['administradores'] = $this->listAdministradores($pg);
			$this->loadView('lista.php',$dados);
		}

		// CARREGA QUAL MÉTODO IRÁ RECEBER A REQUISIÇÃO AJAX
		public function loadMethod($acao, $id){

			if($acao != null){

				switch($acao):

				case 'delete':
					$this->delete($id);
				break;

				// case 'verificaEmail':

				// 	// VALIDAÇÃO DE E-MAIL JÁ EXISTENTE NO BANCO (ADMINISTRADORES)
				// 	$administradorEmail     = isset($_POST['emailAdmin'])     ? $_POST['emailAdmin'] : null;
				// 	$administradorEmailEdit = isset($_POST['emailAdminEdit']) ? $_POST['emailAdminEdit'] : null;
				// 	$administradorEmailRef  = isset($_POST['emailAdminRef'])  ? $_POST['emailAdminRef'] : null;

				// 	$resultado = $this->verificaEmail($administradorEmail, $administradorEmailEdit, $administradorEmailRef);
				// 	echo(json_encode($resultado));

				// break;

				endswitch;
			}
		}

		//TRATA OS DADOS PARA SEREM INSERIDOS NO BANCO
		public function cadastrar(){

			$resultado['title']     		= 'Cadastrar'; 
			$resultado['action']    		= 'cadastrar';
			$resultado['legend']    		= 'cadastro';
			$resultado['name']      		= 'novoAdministrador';
			$resultado['modulos']   		= $this->moduloModel->getList();			
			$resultado['modulosPermitidos'] = $this->moduloModel->getListPermissions("");
			$resultado['resultado'] 		= false;

			if(isset($_REQUEST['modulo']) && $_REQUEST['modulo'] == "administrador"){

				$dados  = $this->validarCampos(INSERT);
				$insert = $this->administradorModel->insert($dados);

				if($insert){
					$resultado['resultado'] = true;
				}else{
					$resultado['resultado'] = false;
				}

				$resultado['mensagem']  = $insert;
				$this->loadView('formulario.php',$resultado);
			}else{

				$this->loadView('formulario.php',$resultado);
			}
		}

		//TRATA OS DADOS PARA SEREM EDITADOS E ATUALIZADOS NO BANCO
		public function atualizar($id){
			if($id != null AND is_numeric($id)){

				if (isset($_REQUEST['modulo']) && $_REQUEST['modulo'] == "administrador") {
					
					$dados  = $this->validarCampos(UPDATE);
					$edicao = $this->administradorModel->update($dados,$id);

					$resultado['resultado'] =  false;

					if(isset($edicao['success'])){

						$resultado['mensagem']  = $edicao;
						$resultado['resultado'] =  true;
					}else{

						$resultado['mensagem']  = $edicao;
					}

					$this->editar($id,$resultado);
				}else{

					errorPage(ERROR_MODULO_INCORRECT);
				}
			}else{

				errorPage(ERROR_ID_NOT_FOUND);
			}
		}

		//RECEBE UM DADO ESPECÍFICO PELO ID
		public function editar($id,$param = null){

			$permissoes = new Permissions();

			if($id != null AND is_numeric($id)){

				$administrador = $this->administradorModel->getRow('l.id_login ,  l.email, l.senha, l.status, l.tipo, a.nome_completo, a.id as idAdministrador, a.foto_perfil',
					' WHERE l.id_login ='.$id.' AND l.tipo =  1');

				if(!isset($administrador['error'])){

					$resultado['administrador'] = $administrador['success'];
				}else{

					$resultado['administrador'] = "error";
				}
			}else{

				$resultado['administrador'] = "error";
			}

			if($resultado['administrador'] != "error"){

				$resultado['title']  = 'Editar'; 
				$resultado['action'] = 'atualizar/'.$id;
				$resultado['legend'] = 'edição';
				$resultado['name']   = 'editarAdministrador';

				$resultado['modulos'] = $this->moduloModel->getList();
				$permissoesPermitidas = $permissoes->listarModulosPermitidos($id);
				$modulosEscolhidos    = array();


				if($permissoesPermitidas[0] != null){
					foreach($permissoesPermitidas as $permissaoPermitida){
						array_push($modulosEscolhidos, $permissaoPermitida->id_modulo);
					}
				}

				$resultado['modulosPermitidos'] = $modulosEscolhidos;


				$resultado['resultado']  = false;
				if($param != null){

					$resultado['resultado'] = true;
					$resultado['mensagem']  = $param['mensagem'];
					$resultado['id']        = $id;
				}

				$this->loadView('formulario.php',$resultado);
			}else{

				errorPage(ERROR_ID_NOT_FOUND);
			}
		}

		//RECEBE UMA LISTA COM TODOS OS REGISTROS
		public function listAdministradores($pg=null,$busca=false){

			if($busca != false){

				$listar  = $this->administradorModel->getList($pg);

			}else{

				$start  = ($pg * $this->limite) - $this->limite;
				$busca  = " WHERE u.nivelId = 1 ORDER BY u.nome ASC LIMIT $start, $this->limite";
				$listar = $this->administradorModel->getList($busca);
			}

			return $listar;
		}

		//DELETA UM REGISTRO ESPECÍFICO PELO ID
		public function delete($id,$usuarioId=null){

			$resultado['resultado'] = true;
			if($id != null){

				$delete = $this->administradorModel->delete($id);

				if(!isset($delete['error'])){
				
					$resultado['mensagem'] = $delete['success'];
				
				}else{

					$resultado['resultado'] = false;
					$resultado['mensagem']  = $delete['error'];
				}
			}else{

				$resultado['resultado'] = false;
				$resultado['mensagem']  = 'Erro ao remover.';
			}

			echo json_encode($resultado);
		}

		// TRATAMENTO DOS DADOS ANTES DO ARMAZENAMENTO
		private function validarCampos($acao){
			$dados = array();


			// CASO EXISTA ALGUMA VALIDAÇÃO ESPECÍFICA EM UMA AÇÃO
			switch($acao){

				case 1:

					$dados['login']['email']    = isset($_POST['admin_email']) ? $_POST['admin_email'] : '';

					if(!empty($_POST['admin_senha'])){

						$dados['login']['senha'] =  md5($_POST['admin_senha']);
					}

					$dados['login']['status']    = $_POST['admin_status'];
					$dados['login']['tipo']      = '1';

					// VERIFICAÇÃO DO ENVIO DE IMAGENS ATRAVÉS DE CADASTROS
					if(!empty($_FILES['admin_foto_perfil']['name'])){

						$imagemFotoPerfil = $this->administradorModel->imagemUpload($_FILES['admin_foto_perfil'],$this->caminhoImagensFotoPerfil);
					}else{

						$imagemFotoPerfil = "";
					}

					// VALIDAÇÕES ESPECÍFICAS NA INSERÇÃO
					$dados['administrador'] = array(
						'nome_completo'   => $_POST['admin_nome'].' '.$_POST['admin_sobrenome'],
						'foto_perfil'     => $imagemFotoPerfil,
						'status' 		  => $_POST['admin_status']
					);


					if(isset($_POST['modulos_escolhido']) && count($_POST['modulos_escolhido']) >= 1){

						$dados['modulos'] = $_POST['modulos_escolhido'];
					}

				break;

				case 2:
					
					// VALIDAÇÕES ESPECÍFICAS NA ATUALIZAÇÃO
					$dados['login']['status']    = $_POST['admin_status'];
					$dados['login']['tipo']      = '1';

					// VALIDAÇÕES ESPECÍFICAS NA INSERÇÃO
					$dados['administrador']['nome_completo'] = $_POST['admin_nome'].' '.$_POST['admin_sobrenome'];

					// VERIFICAÇÃO DO ENVIO DE IMAGENS ATRAVÉS DE CADASTROS
					if(!empty($_FILES['admin_foto_perfil']['name'])){

						$dados['administrador']['foto_perfil'] = $this->administradorModel->imagemUpload($_FILES['admin_foto_perfil'],$this->caminhoImagensFotoPerfil);

					}

					$dados['administrador']['status'] 		 = $_POST['admin_status'];

					if(isset($_POST['modulos_escolhido']) && count($_POST['modulos_escolhido']) >= 1){

						$dados['modulos'] = $_POST['modulos_escolhido'];
					}

				break;

				default:
					// AÇÃO INDEFINIDA OU INVÁLIDA
					die('Ação indefinida');
				break;

			}

			return $dados;
		}

		public function verificaEmail($emailAdmin, $emailAdminEdit, $emailAdminRef){

			if(!empty($emailAdmin)){

				$administrador = $this->administradorModel->getRow("l.id as loginId, u.id as usuarioId, l.email","WHERE l.email ='".$emailAdmin."' ");

				if(!isset($administrador['error'])){

					$resultado = false;
				}else{

					$resultado = true;
				}
			}elseif($emailAdminEdit != $emailAdminRef){

				$administrador = $this->administradorModel->getRow("l.id as loginId, u.id as usuarioId, l.email","WHERE l.email ='".$emailAdminEdit."' ");

				if(!isset($administrador['error'])){

					$resultado = false;
				}else{

					$resultado = true;
				}

			}else{

				$resultado = true;
			}

			return $resultado;
		}

		public function buscar(){

			// NOMES DAS COLUNAS A SEREM PESQUISADAS
			$this->colunas   = array("u.nome","l.email","");
			$this->ordenacao = "ORDER BY u.nome ASC";
			$pesquisa        = parent::buscar();

			// DADOS A SEREM RETORNADOS A PÁGINA
			$dados['pg']              = $pesquisa['indicePaginacao'];
			$dados['condicao']        = $pesquisa['condicaoPaginacao'];
			$dados['limite']          = $this->limite;
			$dados['url']             = URL."administradores/buscar/".$pesquisa['paginaAtual']."/";
			$dados['limite']          = $this->limite;
			$dados['administradores'] = $this->listAdministradores($pesquisa['busca'],true);
			$this->loadView('lista.php',$dados);
		}
	}
	?>