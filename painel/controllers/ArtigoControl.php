<?php 

	/**************************************
	* CONTROLLER ARTIGO
	**************************************/

	class ArtigoControl extends Controller{

		private $artigoModel;
		private $moduloModel;
		private $usuarioModel;
		private $categoriaModel;

		public function __construct(){

			// SETANDO O MÓDULO
			$this->setModulo('artigos');

			$permission = new Permissions();
			$this->setPaginasPermitidas($permission);

			if($this->verifyPermissionPage()){
				// CARREGANDO A MODEL PRINCIPAL
				$this->artigoModel   		    = new ArtigoModel();
				$this->moduloModel        		= new ModuloModel();
				$this->usuarioModel       		= new ClienteModel();
				$this->categoriaModel           = new CategoriaModel();
				$this->limite             		= 20;
				$this->caminhoImagensFotoPerfil = "artigos";
			}else{

				errorPage(ERROR_PERMISSION_PAGE);
				exit;
			}
		}

		//CARREGA A VIEW PRINCIPAL DO MODULO
		public function index($pg = 1){

			$dados['pg']          = ''; 
			$dados['condicao']    = ''; 
			$dados['url']         = ''; 
			$dados['limite']      = ''; 
			$dados['artigos']  = $this->listArtigos($pg);
			$this->loadView('lista.php',$dados);
		}

		// CARREGA QUAL MÉTODO IRÁ RECEBER A REQUISIÇÃO AJAX
		public function loadMethod($acao, $id){

			if($acao != null){

				switch($acao):

				case 'uploadFotos':
					$this->uploadFotos($_FILES['imagemEditorTexto']);
				break;

				case 'delete':
					$this->delete($id);
				break;

				endswitch;
			}
		}

		//TRATA OS DADOS PARA SEREM INSERIDOS NO BANCO
		public function cadastrar(){

			$resultado['title']      = 'Cadastrar'; 
			$resultado['action']     = 'cadastrar';
			$resultado['legend']     = 'cadastro';
			$resultado['name']       = 'novoartigo';
			$resultado['resultado']  = false;
			$resultado['categorias'] = $this->categoriaModel->getList("");

			if(isset($_REQUEST['modulo']) && $_REQUEST['modulo'] == "artigo"){

				$dados  = $this->validarCampos(INSERT);
				$insert = $this->artigoModel->insert($dados);

				if($insert){
					$resultado['resultado'] = true;
				}else{
					$resultado['resultado'] = false;
				}

				$resultado['mensagem']  = $insert;
				$this->loadView('formulario.php',$resultado);
			}else{

				$this->loadView('formulario.php',$resultado);
			}
		}

		//TRATA OS DADOS PARA SEREM EDITADOS E ATUALIZADOS NO BANCO
		public function atualizar($id){
			if($id != null AND is_numeric($id)){

				if (isset($_REQUEST['modulo']) && $_REQUEST['modulo'] == "artigo") {
					
					$dados  = $this->validarCampos(UPDATE);
					$edicao = $this->artigoModel->update($dados,$id);

					$resultado['resultado'] =  false;

					if(isset($edicao['success'])){

						$resultado['mensagem']  = $edicao;
						$resultado['resultado'] =  true;
					}else{

						$resultado['mensagem']  = $edicao;
					}

					$this->editar($id,$resultado);
				}else{

					errorPage(ERROR_MODULO_INCORRECT);
				}
			}else{

				errorPage(ERROR_ID_NOT_FOUND);
			}
		}

		//RECEBE UM DADO ESPECÍFICO PELO ID
		public function editar($id,$param = null){

			if($id != null AND is_numeric($id)){

				$artigo = $this->artigoModel->getRow('a.*, ctg.nome as nomeCategoria, ctg.id_categoria as idCategoria',' WHERE a.id_conteudo ='.$id.' AND  a.tipo = 2');

				if(!isset($artigo['error'])){

					$resultado['artigo'] = $artigo['success'];
				}else{

					$resultado['artigo'] = "error";
				}
			}else{

				$resultado['artigo'] = "error";
			}

			if($resultado['artigo'] != "error"){

				$resultado['title']  	 = 'Editar'; 
				$resultado['action'] 	 = 'atualizar/'.$id;
				$resultado['legend'] 	 = 'edição';
				$resultado['name']   	 = 'editarArtigo';
				$resultado['resultado']  = false;
				$resultado['categorias'] = $this->categoriaModel->getList("");

				if($param != null){

					$resultado['resultado'] = true;
					$resultado['mensagem']  = $param['mensagem'];
					$resultado['id']        = $id;
				}

				$this->loadView('formulario.php',$resultado);
			}else{

				errorPage(ERROR_ID_NOT_FOUND);
			}
		}

		//RECEBE UMA LISTA COM TODOS OS REGISTROS
		public function listArtigos($pg=null,$busca=false){
			$buscaRestrita = "";

			if($busca != false){

				$listar  = $this->artigoModel->getList($pg);

			}else{
				
				$start  = ($pg * $this->limite) - $this->limite;

				if($_SESSION['ADMIN']['TIPO'] == '2'){
					$buscaRestrita = "AND id_autor = ".$_SESSION['ADMIN']['ID'];
				}

				$busca  = " $buscaRestrita ORDER BY c.id_conteudo DESC ";
				$listar = $this->artigoModel->getList($busca);
			}

			return $listar;
		}

		//DELETA UM REGISTRO ESPECÍFICO PELO ID
		public function delete($id,$usuarioId=null){

			$resultado['resultado'] = true;
			if($id != null){

				$delete = $this->artigoModel->delete($id);

				if(!isset($delete['error'])){

					$resultado['mensagem'] = $delete['success'];

				}else{

					$resultado['resultado'] = false;
					$resultado['mensagem']  = $delete['error'];
				}
			}else{

				$resultado['resultado'] = false;
				$resultado['mensagem']  = 'Erro ao remover.';
			}

			echo json_encode($resultado);
		}

		// TRATAMENTO DOS DADOS ANTES DO ARMAZENAMENTO
		private function validarCampos($acao){

			$dados = array();

			// CASO EXISTA ALGUMA VALIDAÇÃO ESPECÍFICA EM UMA AÇÃO
			switch($acao){

				case 1:

					// VERIFICAÇÃO DO ENVIO DE IMAGENS ATRAVÉS DE CADASTROS
					if(!empty($_FILES['artigo_banner']['name'])){

						// UPLOAD
						$imagemNova = $this->artigoModel->imagemUpload($_FILES['artigo_banner'],$this->caminhoImagensFotoPerfil);

						$dados['artigo']['banner']    = $imagemNova['imagemBanner'];
						$dados['artigo']['thumbnail'] = $imagemNova['imagemThumbnail'];

					}else{

						$dados['artigo']['banner']    = "";
						$dados['artigo']['thumbnail'] = "";
					}

					$dados['artigo']['id_categoria']    = $_POST['artigo_categoria'];
					$dados['artigo']['titulo'] 	      	= $_POST['artigo_titulo'];
					$dados['artigo']['texto']  	      	= $_POST['artigo_texto'];
					$dados['artigo']['data_publicacao'] = date('Y-m-d H:i');
					$dados['artigo']['status'] 	      	= isset($_POST['artigo_status']) ? $_POST['artigo_status'] : '';
					$dados['artigo']['tipo'] 	        = '2';
					$dados['artigo']['id_autor']        = $_SESSION['ADMIN']['ID'];
					$dados['artigo']['nivel']  	      	= $_POST['artigo_nivel'];	
					$dados['artigo']['visualizacoes']   = '0';
					$dados['artigo']['destaque']        = isset($_POST['artigo_destaque']) ? '1' : '0';
					$dados['artigo']['slug']            = removeAccents($_POST['artigo_titulo'],"-");
					$dados['artigo']['resumo']          = $_POST['artigo_resumo'];

				break;

				case 2:

					// VALIDAÇÕES ESPECÍFICAS NA ATUALIZAÇÃO

					// VERIFICAÇÃO DO ENVIO DE IMAGENS ATRAVÉS DE CADASTROS
					if(!empty($_FILES['artigo_banner']['name'])){

						// UPLOAD
						$imagemNova = $this->artigoModel->imagemUpload($_FILES['artigo_banner'],$this->caminhoImagensFotoPerfil);

						$dados['artigo']['banner']    = $imagemNova['imagemBanner'];
						$dados['artigo']['thumbnail'] = $imagemNova['imagemThumbnail'];

					}

					$dados['artigo']['id_categoria']    = $_POST['artigo_categoria'];
					$dados['artigo']['titulo'] 	      	= $_POST['artigo_titulo'];
					$dados['artigo']['texto']  	      	= $_POST['artigo_texto'];
					$dados['artigo']['status'] 	      	= isset($_POST['artigo_status']) ? $_POST['artigo_status'] : '';
					$dados['artigo']['nivel']  	      	= $_POST['artigo_nivel'];	
					$dados['artigo']['destaque']        = isset($_POST['artigo_destaque']) ? '1' : '0';
					$dados['artigo']['slug']            = removeAccents($_POST['artigo_titulo'],"-");
					$dados['artigo']['resumo']          = $_POST['artigo_resumo'];
				
				break;

				default:
					// AÇÃO INDEFINIDA OU INVÁLIDA
					die('Ação indefinida');
				break;

			}

			return $dados;
		}

		public function uploadFotos($imagem){
			$respostaJson = array();

			if(!empty($imagem) && is_array($imagem)){
				$imagemNova = $this->artigoModel->imagemUpload($imagem,'artigos_imagens_edicao',true);	

				if(isset($imagemNova['link'])){
					$imagemNova = URL.substr($imagemNova['link'],2);
				}	
			}else{
				$imagemNova = "";	
			}

			$respostaJson['link']  = $imagemNova;

			echo json_encode($respostaJson);
		}
	}
	?>