<?php 

	/**************************************
	* CONTROLLER ASSINATURA
	**************************************/

	class AssinaturaControl extends Controller{

		private $assinaturaModel;
		private $moduloModel;
		private $usuarioModel;
		private $categoriaModel;
		private $pagseguro;

		public function __construct(){

			// SETANDO O MÓDULO
			$this->setModulo('assinaturas');

			$permission = new Permissions();
			$this->setPaginasPermitidas($permission);

			if($this->verifyPermissionPage()){
				// CARREGANDO A MODEL PRINCIPAL
				$this->assinaturaModel   		= new AssinaturaModel();
				$this->moduloModel        		= new ModuloModel();
				$this->usuarioModel       		= new ClienteModel();
				$this->categoriaModel           = new CategoriaModel();
				$this->pagseguro                = new Pagseguro(PAGSEGURO_EMAIL, PAGSEGURO_TOKEN);
				$this->limite             		= 20;
				$this->caminhoImagensFotoPerfil = "";
			}else{

				errorPage(ERROR_PERMISSION_PAGE);
				exit;
			}
		}

		//CARREGA A VIEW PRINCIPAL DO MODULO
		public function index($pg = 1){

			$dados['pg']          = ''; 
			$dados['condicao']    = ''; 
			$dados['url']         = ''; 
			$dados['limite']      = ''; 
			$dados['assinaturas'] = $this->listAssinaturas($pg);
			$this->loadView('lista.php',$dados);
		}

		// CARREGA QUAL MÉTODO IRÁ RECEBER A REQUISIÇÃO AJAX
		public function loadMethod($acao, $id){

			if($acao != null){

				switch($acao):

				case 'delete':
				$this->delete($id);
				break;

				endswitch;
			}
		}

		//TRATA OS DADOS PARA SEREM INSERIDOS NO BANCO
		public function cadastrar(){

			$resultado['title']      = 'Cadastrar'; 
			$resultado['action']     = 'cadastrar';
			$resultado['legend']     = 'cadastro';
			$resultado['name']       = 'novoAssinatura';
			$resultado['resultado']  = false;

			if(isset($_REQUEST['modulo']) && $_REQUEST['modulo'] == "assinatura"){

				$dados  = $this->validarCampos(INSERT);
				
				$registrandoPagseguro = $this->registrarPlanoPagseguro($dados);
				$dados['assinatura']['codigo_pagseguro'] = $registrandoPagseguro;
				
				$insert = $this->assinaturaModel->insert($dados);

				if($insert){
					$resultado['resultado'] = true;
				}else{
					$resultado['resultado'] = false;
				}

				$resultado['mensagem']  = $insert;
				$this->loadView('formulario.php',$resultado);
			}else{

				$this->loadView('formulario.php',$resultado);
			}
		}

		//TRATA OS DADOS PARA SEREM EDITADOS E ATUALIZADOS NO BANCO
		public function atualizar($id){
			if($id != null AND is_numeric($id)){

				if (isset($_REQUEST['modulo']) && $_REQUEST['modulo'] == "assinatura") {
					
					$dados  = $this->validarCampos(UPDATE);
					$edicao = $this->assinaturaModel->update($dados,$id);

					$resultado['resultado'] =  false;

					if(isset($edicao['success'])){

						$resultado['mensagem']  = $edicao;
						$resultado['resultado'] =  true;
					}else{

						$resultado['mensagem']  = $edicao;
					}

					$this->editar($id,$resultado);
				}else{

					errorPage(ERROR_MODULO_INCORRECT);
				}
			}else{

				errorPage(ERROR_ID_NOT_FOUND);
			}
		}

		//RECEBE UM DADO ESPECÍFICO PELO ID
		public function editar($id,$param = null){

			if($id != null AND is_numeric($id)){

				$assinatura = $this->assinaturaModel->getRow('*',' WHERE id_plano ='.$id);

				if(!isset($assinatura['error'])){

					$resultado['assinatura'] = $assinatura['success'];
				}else{

					$resultado['assinatura'] = "error";
				}
			}else{

				$resultado['assinatura'] = "error";
			}

			if($resultado['assinatura'] != "error"){

				$resultado['title']  	 = 'Editar'; 
				$resultado['action'] 	 = 'atualizar/'.$id;
				$resultado['legend'] 	 = 'edição';
				$resultado['name']   	 = 'editarassinatura';
				$resultado['resultado']  = false;

				if($param != null){

					$resultado['resultado'] = true;
					$resultado['mensagem']  = $param['mensagem'];
					$resultado['id']        = $id;
				}

				$this->loadView('formulario.php',$resultado);
			}else{

				errorPage(ERROR_ID_NOT_FOUND);
			}
		}

		//RECEBE UMA LISTA COM TODOS OS REGISTROS
		public function listAssinaturas($pg=null,$busca=false){

			if($busca != false){

				$listar  = $this->assinaturaModel->getList($pg);

			}else{

				$start  = ($pg * $this->limite) - $this->limite;
				$busca  = " WHERE status = 1 ";
				$listar = $this->assinaturaModel->getList($busca);
			}

			return $listar;
		}

		//DELETA UM REGISTRO ESPECÍFICO PELO ID
		public function delete($id,$usuarioId=null){

			$resultado['resultado'] = true;
			
			if($id != null){

				$delete = $this->assinaturaModel->delete($id);

				if(!isset($delete['error'])){
				
					$resultado['mensagem'] = $delete['success'];

				}else{

					$resultado['resultado'] = false;
					$resultado['mensagem']  = $delete['error'];
				}
			}else{

				$resultado['resultado'] = false;
				$resultado['mensagem']  = 'Erro ao remover.';
			}

			echo json_encode($resultado);
		}

		// TRATAMENTO DOS DADOS ANTES DO ARMAZENAMENTO
		private function validarCampos($acao){

			$dados = array();

			// CASO EXISTA ALGUMA VALIDAÇÃO ESPECÍFICA EM UMA AÇÃO
			switch($acao){

				case 1:

				$dados['assinatura']['nome']     = $_POST['assinatura_nome'];
				$dados['assinatura']['preco']    = $_POST['assinatura_preco'];
				$dados['assinatura']['desconto'] = $_POST['assinatura_desconto'];
				$dados['assinatura']['grau']     = $_POST['assinatura_grau'];
				$dados['assinatura']['status']   = $_POST['assinatura_status'];

				break;

				case 2:

					// VALIDAÇÕES ESPECÍFICAS NA ATUALIZAÇÃO
				$dados['assinatura']['nome']     = $_POST['assinatura_nome'];
				$dados['assinatura']['preco']    = formatReal($_POST['assinatura_preco'],'.');
				$dados['assinatura']['desconto'] = $_POST['assinatura_desconto'];					
				$dados['assinatura']['grau']     = $_POST['assinatura_grau'];
				$dados['assinatura']['status']   = $_POST['assinatura_status'];
				
				break;

				default:
					// AÇÃO INDEFINIDA OU INVÁLIDA
				die('Ação indefinida');
				break;

			}

			return $dados;
		}


		/*****
		*
		* Fazendo chamadas na API do Pagseguro
		*
		*******/


		// Registrando Plano
		public function registrarPlanoPagseguro($dados){
			$codigoPagseguro = 0;
			
			if(is_array($dados)){
			
				//MODELO DADOS JSON
				$dados = array(
					'preApproval' => array(
						'name'   => $dados['assinatura']['nome'],
						'charge' => 'AUTO',
						'period' => 'MONTHLY',
						'amountPerPayment' => formatReal($dados['assinatura']['preco'],'.'),
						'expiration' => array(
							'value' => 1,
							'unit'  => 'YEARS'
						),
					),	
					'receiver' => array(
						'email' => 'tiagoelhouri@gmail.com'
					)
				);


				$codigoPagseguro = $this->pagseguro->criarPlano($dados);
			}

			return $codigoPagseguro;
		}
	}
	?>