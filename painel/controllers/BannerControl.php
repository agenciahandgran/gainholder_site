<?php 

	/**************************************
	* CONTROLLER BANNER
	**************************************/

	class BannerControl extends Controller{

		private $bannerModel;

		public function __construct(){

			// SETANDO O MÓDULO
			$this->setModulo('banners');

			$permission = new Permissions();
			$this->setPaginasPermitidas($permission);

			if($this->verifyPermissionPage()){
				// CARREGANDO A MODEL PRINCIPAL
				$this->bannerModel  = new BannerModel();
				$this->limite = 20;
				$this->caminhoImagensFotoPerfil = "banners";
			}else{

				errorPage(ERROR_PERMISSION_PAGE);
				exit;
			}
		}

		//CARREGA A VIEW PRINCIPAL DO MODULO
		public function index($pg = 1){

			$dados['pg']          = ''; 
			$dados['condicao']    = ''; 
			$dados['url']         = ''; 
			$dados['limite']      = ''; 
			$dados['banners']  = $this->listBanners($pg);
			$this->loadView('lista.php',$dados);
		}

		// CARREGA QUAL MÉTODO IRÁ RECEBER A REQUISIÇÃO AJAX
		public function loadMethod($acao, $id){

			if($acao != null){

				switch($acao):

				case 'delete':
					$this->delete($id);
				break;

				endswitch;
			}
		}

		//TRATA OS DADOS PARA SEREM INSERIDOS NO BANCO
		public function cadastrar(){

			$resultado['title']     = 'Cadastrar'; 
			$resultado['action']    = 'cadastrar';
			$resultado['legend']    = 'cadastro';
			$resultado['name']      = 'novoBanner';
			$resultado['resultado'] = false;

			if(isset($_REQUEST['modulo']) && $_REQUEST['modulo'] == "banner"){

				$dados  = $this->validarCampos(INSERT);
				$insert = $this->bannerModel->insert($dados);

				if($insert){
					$resultado['resultado'] = true;
				}else{
					$resultado['resultado'] = false;
				}

				$resultado['mensagem']  = $insert;
				$this->loadView('formulario.php',$resultado);
			}else{

				$this->loadView('formulario.php',$resultado);
			}
		}

		//TRATA OS DADOS PARA SEREM EDITADOS E ATUALIZADOS NO BANCO
		public function atualizar($id){
			if($id != null AND is_numeric($id)){

				if (isset($_REQUEST['modulo']) && $_REQUEST['modulo'] == "banner") {
					
					$dados  = $this->validarCampos(UPDATE);
					$edicao = $this->bannerModel->update($dados,$id);

					$resultado['resultado'] =  false;

					if(isset($edicao['success'])){

						$resultado['mensagem']  = $edicao;
						$resultado['resultado'] =  true;
					}else{

						$resultado['mensagem']  = $edicao;
					}

					$this->editar($id,$resultado);
				}else{

					errorPage(ERROR_MODULO_INCORRECT);
				}
			}else{

				errorPage(ERROR_ID_NOT_FOUND);
			}
		}

		//RECEBE UM DADO ESPECÍFICO PELO ID
		public function editar($id,$param = null){

			if($id != null AND is_numeric($id)){

				$banner = $this->bannerModel->getRow('*',' WHERE id_banner ='.$id);

				if(!isset($banner['error'])){

					$resultado['banner'] = $banner['success'];
				}else{

					$resultado['banner'] = "error";
				}
			}else{

				$resultado['banner'] = "error";
			}

			if($resultado['banner'] != "error"){

				$resultado['title']  	 = 'Editar'; 
				$resultado['action'] 	 = 'atualizar/'.$id;
				$resultado['legend'] 	 = 'edição';
				$resultado['name']   	 = 'editarBanner';
				$resultado['resultado']  = false;

				if($param != null){

					$resultado['resultado'] = true;
					$resultado['mensagem']  = $param['mensagem'];
					$resultado['id']        = $id;
				}

				$this->loadView('formulario.php',$resultado);
			}else{

				errorPage(ERROR_ID_NOT_FOUND);
			}
		}

		//RECEBE UMA LISTA COM TODOS OS REGISTROS
		public function listBanners($pg=null,$busca=false){

			if($busca != false){

				$listar  = $this->bannerModel->getList(" WHERE status = 1 ");

			}else{

				$start  = ($pg * $this->limite) - $this->limite;
				$busca  = " WHERE status = 1 ORDER BY ordem ASC LIMIT $start, $this->limite";
				$listar  = $this->bannerModel->getList(" WHERE status = 1 ");
			}

			return $listar;
		}

		//DELETA UM REGISTRO ESPECÍFICO PELO ID
		public function delete($id,$usuarioId=null){

			$resultado['resultado'] = true;
			if($id != null){

				$delete = $this->bannerModel->delete($id);

				if(!isset($delete['error'])){

					$resultado['mensagem'] = $delete['success'];

				}else{

					$resultado['resultado'] = false;
					$resultado['mensagem']  = $delete['error'];
				}
			}else{

				$resultado['resultado'] = false;
				$resultado['mensagem']  = 'Erro ao remover.';
			}

			echo json_encode($resultado);
		}

		// TRATAMENTO DOS DADOS ANTES DO ARMAZENAMENTO
		private function validarCampos($acao){

			$dados = array();

			// CASO EXISTA ALGUMA VALIDAÇÃO ESPECÍFICA EM UMA AÇÃO
			switch($acao){

				case 1:

					// VERIFICAÇÃO DO ENVIO DE IMAGENS ATRAVÉS DE CADASTROS
					if(!empty($_FILES['banner_imagem']['name'])){

						//  UPLOAD BANNER
						$imagemNova = $this->bannerModel->imagemUpload($_FILES['banner_imagem'],$this->caminhoImagensFotoPerfil);

						$dados['banner']['imagem']    = $imagemNova;

					}else{

						$dados['banner']['imagem']    = "";
					}

					$dados['banner']['titulo']     = $_POST['banner_titulo'];
					$dados['banner']['descricao']  = $_POST['banner_descricao'];
					$dados['banner']['link']       = $_POST['banner_link'];
					$dados['banner']['posicao']    = $_POST['banner_posicao'];
					$dados['banner']['ordem']	   = $this->bannerModel->getLastOrder();
					$dados['banner']['status']     = $_POST['banner_status'];  

				break;

				case 2:

					// VALIDAÇÕES ESPECÍFICAS NA ATUALIZAÇÃO

					if(!empty($_FILES['banner_imagem']['name'])){

						//  UPLOAD BANNER
						$imagemNova = $this->bannerModel->imagemUpload($_FILES['banner_imagem'],$this->caminhoImagensFotoPerfil);

						$dados['banner']['imagem']    = $imagemNova;

					}

					$dados['banner']['titulo']     = $_POST['banner_titulo'];
					$dados['banner']['descricao']  = $_POST['banner_descricao'];
					$dados['banner']['link']       = $_POST['banner_link'];
					$dados['banner']['posicao']    = $_POST['banner_posicao'];
					$dados['banner']['status']     = $_POST['banner_status']; 
				
				break;

				default:
					// AÇÃO INDEFINIDA OU INVÁLIDA
					die('Ação indefinida');
				break;

			}

			return $dados;
		}

	}
	?>