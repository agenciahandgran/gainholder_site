<?php 

	/**************************************
	* CONTROLLER CATEGORIA
	**************************************/

	class CategoriaControl extends Controller{

		private $categoriaModel;
		private $moduloModel;
		private $usuarioModel;

		public function __construct(){

			// SETANDO O MÓDULO
			$this->setModulo('categorias');

			$permission = new Permissions();
			$this->setPaginasPermitidas($permission);

			if($this->verifyPermissionPage()){
				// CARREGANDO A MODEL PRINCIPAL
				$this->categoriaModel   		= new CategoriaModel();
				$this->moduloModel        		= new ModuloModel();
				$this->usuarioModel       		= new ClienteModel();
				$this->limite             		= 20;
				//$this->caminhoImagensFotoPerfil = "";
			}else{

				errorPage(ERROR_PERMISSION_PAGE);
				exit;
			}
		}

		//CARREGA A VIEW PRINCIPAL DO MODULO
		public function index($pg = 1){

			$dados['pg']          = ''; 
			$dados['condicao']    = ''; 
			$dados['url']         = ''; 
			$dados['limite']      = ''; 
			$dados['categorias']  = $this->listCategorias($pg);
			$this->loadView('lista.php',$dados);
		}

		// CARREGA QUAL MÉTODO IRÁ RECEBER A REQUISIÇÃO AJAX
		public function loadMethod($acao, $id){

			if($acao != null){

				switch($acao):

				case 'delete':
					$this->delete($id);
				break;

				endswitch;
			}
		}

		//TRATA OS DADOS PARA SEREM INSERIDOS NO BANCO
		public function cadastrar(){

			$resultado['title']     = 'Cadastrar'; 
			$resultado['action']    = 'cadastrar';
			$resultado['legend']    = 'cadastro';
			$resultado['name']      = 'novaCategoria';
			$resultado['resultado'] = false;

			if(isset($_REQUEST['modulo']) && $_REQUEST['modulo'] == "categoria"){

				$dados  = $this->validarCampos(INSERT);
				$insert = $this->categoriaModel->insert($dados);

				if($insert){
					$resultado['resultado'] = true;
				}else{
					$resultado['resultado'] = false;
				}

				$resultado['mensagem']  = $insert;
				$this->loadView('formulario.php',$resultado);
			}else{

				$this->loadView('formulario.php',$resultado);
			}
		}

		//TRATA OS DADOS PARA SEREM EDITADOS E ATUALIZADOS NO BANCO
		public function atualizar($id){
			if($id != null AND is_numeric($id)){

				if (isset($_REQUEST['modulo']) && $_REQUEST['modulo'] == "categoria") {
					
					$dados  = $this->validarCampos(UPDATE);
					$edicao = $this->categoriaModel->update($dados,$id);

					$resultado['resultado'] =  false;

					if(isset($edicao['success'])){

						$resultado['mensagem']  = $edicao;
						$resultado['resultado'] =  true;
					}else{

						$resultado['mensagem']  = $edicao;
					}

					$this->editar($id,$resultado);
				}else{

					errorPage(ERROR_MODULO_INCORRECT);
				}
			}else{

				errorPage(ERROR_ID_NOT_FOUND);
			}
		}

		//RECEBE UM DADO ESPECÍFICO PELO ID
		public function editar($id,$param = null){

			if($id != null AND is_numeric($id)){

				$categoria = $this->categoriaModel->getRow('*',' WHERE id_categoria ='.$id);

				if(!isset($categoria['error'])){

					$resultado['categoria'] = $categoria['success'];
				}else{

					$resultado['categoria'] = "error";
				}
			}else{

				$resultado['categoria'] = "error";
			}

			if($resultado['categoria'] != "error"){

				$resultado['title']  	 = 'Editar'; 
				$resultado['action'] 	 = 'atualizar/'.$id;
				$resultado['legend'] 	 = 'edição';
				$resultado['name']   	 = 'editarCategoria';
				$resultado['resultado']  = false;

				if($param != null){

					$resultado['resultado'] = true;
					$resultado['mensagem']  = $param['mensagem'];
					$resultado['id']        = $id;
				}

				$this->loadView('formulario.php',$resultado);
			}else{

				errorPage(ERROR_ID_NOT_FOUND);
			}
		}

		//RECEBE UMA LISTA COM TODOS OS REGISTROS
		public function listCategorias($pg=null,$busca=false){

			if($busca != false){

				$listar  = $this->categoriaModel->getList($pg);

			}else{

				$start  = ($pg * $this->limite) - $this->limite;
				$busca  = " WHERE u.nivelId = 1 ORDER BY u.nome ASC LIMIT $start, $this->limite";
				$listar = $this->categoriaModel->getList($busca);
			}

			return $listar;
		}

		//DELETA UM REGISTRO ESPECÍFICO PELO ID
		public function delete($id,$usuarioId=null){

			$resultado['resultado'] = true;
			if($id != null){

				$delete = $this->categoriaModel->delete($id);

				if(!isset($delete['error'])){

					$resultado['mensagem'] = $delete['success'];

				}else{

					$resultado['resultado'] = false;
					$resultado['mensagem']  = $delete['error'];
				}
			}else{

				$resultado['resultado'] = false;
				$resultado['mensagem']  = 'Erro ao remover.';
			}

			echo json_encode($resultado);
		}

		// TRATAMENTO DOS DADOS ANTES DO ARMAZENAMENTO
		private function validarCampos($acao){

			$dados = array();

			// CASO EXISTA ALGUMA VALIDAÇÃO ESPECÍFICA EM UMA AÇÃO
			switch($acao){

				case 1:

					$dados['categoria']['nome']    = $_POST['categoria_nome'];
					$dados['categoria']['slug']    = strtolower(removeAccents($_POST['categoria_nome'], "-"));
					$dados['categoria']['status']  = $_POST['categoria_status'];	

				break;

				case 2:

					// VALIDAÇÕES ESPECÍFICAS NA ATUALIZAÇÃO

					$dados['categoria']['nome']    = $_POST['categoria_nome'];
					$dados['categoria']['slug']    = strtolower(removeAccents($_POST['categoria_nome'], "-"));
					$dados['categoria']['status']  = $_POST['categoria_status'];	
				
				break;

				default:
					// AÇÃO INDEFINIDA OU INVÁLIDA
					die('Ação indefinida');
				break;

			}

			return $dados;
		}

	}
	?>