<?php 

	/**************************************
	* CONTROLLER CLIENTE
	**************************************/

	class ClienteControl extends Controller{

		private $clienteModel;
		private $moduloModel;

		public $caminhoImagensFotoPerfil;

		public function __construct($acesso = "cms"){

			// SETANDO O MÓDULO
			$this->setModulo('clientes');

			$permission = new Permissions();
			$this->setPaginasPermitidas($permission);

			if($this->verifyPermissionPage()){

				// CARREGANDO A MODEL PRINCIPAL
				$this->clienteModel 			= new ClienteModel();
				$this->limite                   = 10;
				$this->caminhoImagensFotoPerfil = "clientes_fotoPerfil";
			}else{

				errorPage(ERROR_PERMISSION_PAGE);
				exit;
			}
		}

		//CARREGA A VIEW PRINCIPAL DO MODULO
		public function index($pg = 1){

			$dados['pg']       = $pg;
			$dados['condicao'] = "";
			$dados['url']      = URL.'clientes/index/';
			$dados['limite']   = $this->limite;
			$dados['clientes'] = $this->listClientes("",true);
			$this->loadView('lista.php',$dados);
		}

		// CARREGA QUAL MÉTODO IRÁ RECEBER A REQUISIÇÃO AJAX
		public function loadMethod($acao, $id){

			if($acao != null){

				switch($acao):

					case 'delete':
						$idcliente = isset($_GET['idcliente']) ? $_GET['idcliente'] : '';
						$this->delete($id,$idcliente);
					break;

					case 'verificaEmail':

						// VALIDAÇÃO DE E-MAIL JÁ EXISTENTE NO BANCO (USUÁRIOS)
						$clienteEmail     = isset($_POST['emailcliente'])     ? $_POST['emailcliente'] : null;
						$clienteEmailEdit = isset($_POST['emailclienteEdit']) ? $_POST['emailclienteEdit'] : null;
						$clienteEmailRef  = isset($_POST['emailclienteRef'])  ? $_POST['emailclienteRef'] : null;

						$resultado = $this->verificaEmail($clienteEmail, $clienteEmailEdit, $clienteEmailRef);
						echo(json_encode($resultado));

					break;

				endswitch;
			}
		}

		//TRATA OS DADOS PARA SEREM INSERIDOS NO BANCO
		public function cadastrar(){
			
			$resultado['title']  = 'Cadastrar'; 
			$resultado['action'] = 'cadastrar';
			$resultado['legend'] = 'Novo';
			$resultado['name']   = 'novocliente';
			$resultado['resultado'] = false;

			if(isset($_REQUEST['modulo']) && $_REQUEST['modulo'] == "cliente"){

				$dados  = $this->validarCampos(INSERT);

				$insert = $this->clienteModel->insert($dados,1);

				if($insert){
					$resultado['resultado'] = true;
				}else{
					$resultado['resultado'] = false;
				}

				$resultado['mensagem']  = $insert;
				$this->loadView('formulario.php',$resultado);
			}else{

				$this->loadView('formulario.php',$resultado);
			}
		}

		//TRATA OS DADOS PARA SEREM EDITADOS E ATUALIZADOS NO BANCO
		public function atualizar($id){

			if($id != null AND is_numeric($id)){

				if (isset($_REQUEST['modulo']) && $_REQUEST['modulo'] == "cliente") {
					
					$dados  = $this->validarCampos(UPDATE);
					$edicao = $this->clienteModel->update($dados,$id);

					$resultado['resultado'] =  false;

					if(isset($edicao['success'])){

						$resultado['mensagem']  = $edicao;
						$resultado['resultado'] =  true;
					}else{

						$resultado['mensagem']  = $edicao;
					}

					$this->editar($id,$resultado);
				}else{

					errorPage(ERROR_MODULO_INCORRECT);
				}
				
			}else{

				errorPage(ERROR_ID_NOT_FOUND);
			}
		}

		//RECEBE UM DADO ESPECÍFICO PELO ID
		public function editar($id,$param = null){

			if($id != null AND is_numeric($id)){

				$cliente = $this->clienteModel->getRow("u.*, l.email, l.status, e.* ",'WHERE u.id ='.$id);
				
				if(!isset($cliente['error'])){

					$resultado['cliente'] = $cliente['success'];
				}else{

					$resultado['cliente'] = "error";
				}
			
			}else{

				$resultado['cliente'] = "error";
			}

			if($resultado['cliente'] != "error"){
			
				$resultado['title']  = 'Editar'; 
				$resultado['action'] = 'atualizar/'.$id;
				$resultado['legend'] = 'Editar';
				$resultado['name']   = 'editarcliente';
				
				$resultado['resultado']  = false;
				if($param != null){

					$resultado['resultado'] = true;
					$resultado['mensagem']  = $param['mensagem'];
					$resultado['id']        = $id;
				}

				$this->loadView('formulario.php',$resultado);
			}else{

				errorPage(ERROR_ID_NOT_FOUND);
			}
			
		}

		//RECEBE UMA LISTA COM TODOS OS REGISTROS 
		public function listClientes($pg=null,$busca=false){

			if($busca != false){

				$listar  = $this->clienteModel->getList("u.*, l.email, l.status, e.* ", $pg);
			}else{

				$inicio  = ($pg * $this->limite) - $this->limite;
				$busca  = " ORDER BY u.nome ASC LIMIT $inicio, $this->limite ";
				$listar = $this->clienteModel->getList("u.*, l.email, l.status, e.* ",$busca);
			}

			return $listar;
		}

		//DELETA UM REGISTRO ESPECÍFICO PELO ID
		public function delete($id,$idcliente=null){

			$resultado['resultado'] = true;
			if($id != null){

				$administradorModel = new AdministradorModel();
				$delete = $administradorModel->delete($id);
				if(!isset($delete['error'])){

					if($idcliente != null){

						$deletecliente = $this->clienteModel->delete($idcliente);
					}

					$resultado['mensagem'] = $delete['success'];	
				}else{

					$resultado['resultado'] = false;
					$resultado['mensagem']  = $delete['error'];
				}
			}else{

				$resultado['resultado'] = false;
				$resultado['mensagem']  = 'Erro ao remover.';
			}

			echo json_encode($resultado);
			
		}

		// TRATAMENTO DOS DADOS ANTES DO ARMAZENAMENTO
		private function validarCampos($acao){

			$dados = array();

			// VERIFICAÇÃO DO ENVIO DE IMAGENS ATRAVÉS DE CADASTROS
			if(!empty($_FILES['clientesFotoPerfil']['name'])){

				$imagemFotoPerfil = $this->clienteModel->imagemUpload($_FILES['clientesFotoPerfil'],$this->caminhoImagensFotoPerfil);
			}else{

				$imagemFotoPerfil = "";
			}

			$dados = array(
				'cliente' => array(
							'nivelId'        => $_POST['nivelcliente'],
							'nome'           => $_POST['nomeCompletocliente'],
							'fotoPerfil'     => $imagemFotoPerfil,
							'sexo'           => isset($_POST['sexocliente']) ? $_POST['sexocliente'] : " ",
							'dataNascimento' => $_POST['dataNascimentocliente'],
							'telefone'       => $_POST['telefonecliente'],
							'celular'        => $_POST['celularcliente'],
							'cpf'            => $_POST['cpfcliente'],
				)
			);

			$dados['login']['email'] = isset($_POST['emailcliente']) ? $_POST['emailcliente'] : $_POST['emailclienteEdit'];

			if(!empty($_POST['senhacliente'])){

				$dados['login']['senha'] =  md5($_POST['senhacliente']);
			}

			$dados['login']['status']    = $_POST['acessocliente'];

			// CASO EXISTA ALGUMA VALIDAÇÃO ESPECÍFICA EM UMA AÇÃO
			switch($acao){

				case 1:
					// VALIDAÇÕES ESPECÍFICAS NA INSERÇÃO
				break;

				case 2:
					// VALIDAÇÕES ESPECÍFICAS NA ATUALIZAÇÃO
					$dados['login']['id'] = $_POST['idLogin'];
				break;

				default:
					// AÇÃO INDEFINIDA OU INVÁLIDA
					die('Ação indefinida');
				break;

			}

			return $dados;
		}

		public function verificaEmail($emailcliente, $emailclienteEdit, $emailclienteRef){

			if(!empty($emailcliente)){

				$cliente = $this->clienteModel->getRow("u.id as idcliente, l.id as idLogin, l.email","WHERE l.email ='".$emailcliente."' ");

				if(!isset($cliente['error'])){

					$resultado = false;
				}else{

					$resultado = true;
				}
			}elseif($emailclienteEdit != $emailclienteRef){

				$cliente = $this->clienteModel->getRow("u.id as clienteId, l.id as loginId, l.email","WHERE l.email ='".$emailclienteEdit."' ");

				if(!isset($cliente['error'])){

					$resultado = false;
				}else{

					$resultado = true;
				}

			}else{

				$resultado = true;
			}

			return $resultado;
		}

		public function detalharcliente($clienteId){

			$resultado = $this->clienteModel->getRow('u.id as clienteId, l.id as loginId, l.email, l.status, nivelId, nome, fotoPerfil, sexo, dataNascimento, telefone, celular, cpf',
														'WHERE u.id ='.$clienteId);

			return $resultado;
		}

		public function cadastrarclienteApi($clienteNome,$clienteEmail,$clienteSenha,$clienteUid){

			$dados = array(
				'cliente' => array(
							'nome'           => $clienteNome,
							'fotoPerfil'     => "",
							'nivelId'        => 2,
							'sexo'           => "",
							'dataNascimento' => "",
							'telefone'       => "",
							'celular'        => "",
							'cpf'            => "",
				),
				'login' => array(
							'email'  => $clienteEmail,
							'senha'  => $clienteSenha,
							'status' => 1
				)
			);

			$insert = $this->clienteModel->insert($dados,1);

			if(isset($insert['success'])){

				$resultado['success'] = $insert['success'];
			}else{

				$resultado = $insert;
			}

			return $resultado;
		}

		public function buscar(){

			// NOMES DAS COLUNAS A SEREM PESQUISADAS
			$this->colunas   = array("u.nome","");
			$this->ordenacao = "ORDER BY u.nome ASC";
			$pesquisa        = parent::buscar();

			// DADOS A SEREM RETORNADOS A PÁGINA
			$dados['pg']       = $pesquisa['indicePaginacao'];
			$dados['condicao'] = $pesquisa['condicaoPaginacao'];
			$dados['limite']   = $this->limite;
			$dados['url']      = URL."clientes/buscar/".$pesquisa['paginaAtual']."/";
			$dados['clientes'] = $this->listclientes($pesquisa['busca'],true);
			$this->loadView('lista.php',$dados);
		}
	}
?>