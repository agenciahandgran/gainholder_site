<?php 

	/**************************************
	* CONTROLLER COLUNISTA
	**************************************/

	class ColunistaControl extends Controller{

		private $colunistaModel;
		private $moduloModel;
		private $usuarioModel;

		public function __construct(){

			// SETANDO O MÓDULO
			$this->setModulo('colunistas');

			$permission = new Permissions();
			$this->setPaginasPermitidas($permission);

			if($this->verifyPermissionPage()){
				// CARREGANDO A MODEL PRINCIPAL
				$this->colunistaModel   		= new ColunistaModel();
				$this->moduloModel        		= new ModuloModel();
				$this->usuarioModel       		= new ClienteModel();
				$this->limite             		= 20;
				$this->caminhoImagensFotoPerfil = "colunista_perfil";
			}else{

				errorPage(ERROR_PERMISSION_PAGE);
				exit;
			}
		}

		//CARREGA A VIEW PRINCIPAL DO MODULO
		public function index($pg = 1){

			$dados['pg']          = ''; 
			$dados['condicao']    = ''; 
			$dados['url']         = ''; 
			$dados['limite']      = ''; 
			$dados['colunistas']  = $this->listColunistas($pg);
			$this->loadView('lista.php',$dados);
		}

		// CARREGA QUAL MÉTODO IRÁ RECEBER A REQUISIÇÃO AJAX
		public function loadMethod($acao, $id){

			if($acao != null){

				switch($acao):

					case 'delete':
						$this->delete($id);
					break;

				endswitch;
			}
		}

		//TRATA OS DADOS PARA SEREM INSERIDOS NO BANCO
		public function cadastrar(){

			$resultado['title']     		= 'Cadastrar'; 
			$resultado['action']    		= 'cadastrar';
			$resultado['legend']    		= 'cadastro';
			$resultado['name']      		= 'novoColunista';
			$resultado['modulos']   		= $this->moduloModel->getList();			
			$resultado['modulosPermitidos'] = $this->moduloModel->getListPermissions("");
			$resultado['resultado'] 		= false;

			if(isset($_REQUEST['modulo']) && $_REQUEST['modulo'] == "colunista"){

				$dados  = $this->validarCampos(INSERT);
				$insert = $this->colunistaModel->insert($dados);

				if($insert){
					$resultado['resultado'] = true;
				}else{
					$resultado['resultado'] = false;
				}

				$resultado['mensagem']  = $insert;
				$this->loadView('formulario.php',$resultado);
			}else{

				$this->loadView('formulario.php',$resultado);
			}
		}

		//TRATA OS DADOS PARA SEREM EDITADOS E ATUALIZADOS NO BANCO
		public function atualizar($id){
			if($id != null AND is_numeric($id)){

				if (isset($_REQUEST['modulo']) && $_REQUEST['modulo'] == "colunista") {
					
					$dados  = $this->validarCampos(UPDATE);
					$edicao = $this->colunistaModel->update($dados,$id);

					$resultado['resultado'] =  false;

					if(isset($edicao['success'])){

						$resultado['mensagem']  = $edicao;
						$resultado['resultado'] =  true;
					}else{

						$resultado['mensagem']  = $edicao;
					}

					$this->editar($id,$resultado);
				}else{

					errorPage(ERROR_MODULO_INCORRECT);
				}
			}else{

				errorPage(ERROR_ID_NOT_FOUND);
			}
		}

		//RECEBE UM DADO ESPECÍFICO PELO ID
		public function editar($id,$param = null){

			$permissoes = new Permissions();

			if($id != null AND is_numeric($id)){

				$colunista = $this->colunistaModel->getRow('l.id_login ,  l.email, l.senha, l.status, l.tipo, c.nome_completo, c.id as idColunista, c.foto_perfil, c.descricao',
					' WHERE l.id_login ='.$id.' AND l.tipo =  2');

				if(!isset($colunista['error'])){

					$resultado['colunista'] = $colunista['success'];
				}else{

					$resultado['colunista'] = "error";
				}
			}else{

				$resultado['colunista'] = "error";
			}

			if($resultado['colunista'] != "error"){

				$resultado['title']  = 'Editar'; 
				$resultado['action'] = 'atualizar/'.$id;
				$resultado['legend'] = 'edição';
				$resultado['name']   = 'editarColunista';

				$resultado['modulos'] = $this->moduloModel->getList();
				$permissoesPermitidas = $permissoes->listarModulosPermitidos($id);
				$modulosEscolhidos    = array();


				if($permissoesPermitidas[0] != null){
					foreach($permissoesPermitidas as $permissaoPermitida){
						array_push($modulosEscolhidos, $permissaoPermitida->id_modulo);
					}
				}

				$resultado['modulosPermitidos'] = $modulosEscolhidos;


				$resultado['resultado']  = false;
				if($param != null){

					$resultado['resultado'] = true;
					$resultado['mensagem']  = $param['mensagem'];
					$resultado['id']        = $id;
				}

				$this->loadView('formulario.php',$resultado);
			}else{

				errorPage(ERROR_ID_NOT_FOUND);
			}
		}

		//RECEBE UMA LISTA COM TODOS OS REGISTROS
		public function listColunistas($pg=null,$busca=false){

			if($busca != false){

				$listar  = $this->colunistaModel->getList($pg);

			}else{

				$start  = ($pg * $this->limite) - $this->limite;
				$busca  = " WHERE u.nivelId = 1 ORDER BY u.nome ASC LIMIT $start, $this->limite";
				$listar = $this->colunistaModel->getList($busca);
			}

			return $listar;
		}

		//DELETA UM REGISTRO ESPECÍFICO PELO ID
		public function delete($id,$usuarioId=null){

			$resultado['resultado'] = true;
			if($id != null){

				$delete = $this->colunistaModel->delete($id);

				if(!isset($delete['error'])){
				
					$resultado['mensagem'] = $delete['success'];
				
				}else{

					$resultado['resultado'] = false;
					$resultado['mensagem']  = $delete['error'];
				}
			}else{

				$resultado['resultado'] = false;
				$resultado['mensagem']  = 'Erro ao remover.';
			}

			echo json_encode($resultado);
		}

		// TRATAMENTO DOS DADOS ANTES DO ARMAZENAMENTO
		private function validarCampos($acao){

			$dados = array();

			// CASO EXISTA ALGUMA VALIDAÇÃO ESPECÍFICA EM UMA AÇÃO
			switch($acao){

				case 1:

					$dados['login']['email']    = isset($_POST['colunista_email']) ? $_POST['colunista_email'] : '';

					if(!empty($_POST['colunista_senha'])){

						$dados['login']['senha'] =  md5($_POST['colunista_senha']);
					}

					$dados['login']['status']    = $_POST['colunista_status'];
					$dados['login']['tipo']      = '2';

					// VERIFICAÇÃO DO ENVIO DE IMAGENS ATRAVÉS DE CADASTROS
					if(!empty($_FILES['colunista_foto_perfil']['name'])){

						$imagemFotoPerfil = $this->colunistaModel->imagemUpload($_FILES['colunista_foto_perfil'],$this->caminhoImagensFotoPerfil);
					}else{

						$imagemFotoPerfil = "";
					}

					// VALIDAÇÕES ESPECÍFICAS NA INSERÇÃO
					$dados['colunista'] = array(
						'nome_completo'   => $_POST['colunista_nome'].' '.$_POST['colunista_sobrenome'],
						'foto_perfil'     => $imagemFotoPerfil,
						'status' 		  => $_POST['colunista_status'],
						'descricao'       => $_POST['colunista_descricao']
					);


					if(isset($_POST['modulos_escolhido']) && count($_POST['modulos_escolhido']) >= 1){

						$dados['modulos'] = $_POST['modulos_escolhido'];
					}

				break;

				case 2:
					
					// VALIDAÇÕES ESPECÍFICAS NA ATUALIZAÇÃO
					$dados['login']['status']    = $_POST['colunista_status'];
					$dados['login']['tipo']      = '2';

					// VALIDAÇÕES ESPECÍFICAS NA INSERÇÃO
					$dados['colunista']['nome_completo'] = $_POST['colunista_nome'].' '.$_POST['colunista_sobrenome'];

					// VERIFICAÇÃO DO ENVIO DE IMAGENS ATRAVÉS DE CADASTROS
					if(!empty($_FILES['colunista_foto_perfil']['name'])){

						$dados['colunista']['foto_perfil'] = $this->colunistaModel->imagemUpload($_FILES['colunista_foto_perfil'],$this->caminhoImagensFotoPerfil);

					}

					$dados['colunista']['status'] 	 = $_POST['colunista_status'];
					$dados['colunista']['descricao'] = $_POST['colunista_descricao'];

					if(isset($_POST['modulos_escolhido']) && count($_POST['modulos_escolhido']) >= 1){

						$dados['modulos'] = $_POST['modulos_escolhido'];
					}

				break;

				default:
					// AÇÃO INDEFINIDA OU INVÁLIDA
					die('Ação indefinida');
				break;

			}

			return $dados;
		}

	}
	?>