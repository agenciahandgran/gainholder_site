<?php 

	/**************************************
	* CONTROLLER CONTEÚDO
	**************************************/

	class ConteudoControl extends Controller{

		private $conteudoModel;
		private $moduloModel;
		private $usuarioModel;
		private $categoriaModel;

		public function __construct(){

			// SETANDO O MÓDULO
			$this->setModulo('conteudos');

			$permission = new Permissions();
			$this->setPaginasPermitidas($permission);

			if($this->verifyPermissionPage()){
				// CARREGANDO A MODEL PRINCIPAL
				$this->conteudoModel   			= new ConteudoModel();
				$this->moduloModel        		= new ModuloModel();
				$this->usuarioModel       		= new ClienteModel();
				$this->categoriaModel           = new CategoriaModel();
				$this->limite             		= 20;
				$this->caminhoImagensFotoPerfil = "conteudos";
			}else{

				errorPage(ERROR_PERMISSION_PAGE);
				exit;
			}
		}

		//CARREGA A VIEW PRINCIPAL DO MODULO
		public function index($pg = 1){

			$dados['pg']          = ''; 
			$dados['condicao']    = ''; 
			$dados['url']         = ''; 
			$dados['limite']      = ''; 
			$dados['conteudos']  = $this->listConteudos($pg);
			$this->loadView('lista.php',$dados);
		}

		// CARREGA QUAL MÉTODO IRÁ RECEBER A REQUISIÇÃO AJAX
		public function loadMethod($acao, $id){

			if($acao != null){

				switch($acao):

					case 'uploadFotos':
						$this->uploadFotos($_FILES['imagemEditorTexto']);
					break;

					case 'delete':
						$this->delete($_GET['id']);
					break;

				endswitch;
			}
		}

		//TRATA OS DADOS PARA SEREM INSERIDOS NO BANCO
		public function cadastrar(){

			$resultado['title']      = 'Cadastrar'; 
			$resultado['action']     = 'cadastrar';
			$resultado['legend']     = 'cadastro';
			$resultado['name']       = 'novoConteudo';
			$resultado['categorias'] = $this->categoriaModel->getList("");
			$resultado['resultado']  = false;

			if(isset($_REQUEST['modulo']) && $_REQUEST['modulo'] == "conteudo"){

				$dados  = $this->validarCampos(INSERT);
				$insert = $this->conteudoModel->insert($dados);

				if($insert){
					$resultado['resultado'] = true;
				}else{
					$resultado['resultado'] = false;
				}

				$resultado['mensagem']  = $insert;
				$this->loadView('formulario.php',$resultado);
			}else{

				$this->loadView('formulario.php',$resultado);
			}
		}

		//TRATA OS DADOS PARA SEREM EDITADOS E ATUALIZADOS NO BANCO
		public function atualizar($id){

			if($id != null AND is_numeric($id)){

				if (isset($_REQUEST['modulo']) && $_REQUEST['modulo'] == "conteudo") {
					
					$dados  = $this->validarCampos(UPDATE);
					$edicao = $this->conteudoModel->update($dados,$id);

					$resultado['resultado'] =  false;

					if(isset($edicao['success'])){

						$resultado['mensagem']  = $edicao;
						$resultado['resultado'] =  true;
					}else{

						$resultado['mensagem']  = $edicao;
					}

					$this->editar($id,$resultado);
				}else{

					errorPage(ERROR_MODULO_INCORRECT);
				}
			}else{

				errorPage(ERROR_ID_NOT_FOUND);
			}
		}

		//RECEBE UM DADO ESPECÍFICO PELO ID
		public function editar($id,$param = null){

			if($id != null AND is_numeric($id)){

				$conteudo = $this->conteudoModel->getRow('c.*, ctg.nome as nomeCategoria, ctg.id_categoria as idCategoria',' WHERE c.id_conteudo ='.$id.' AND c.tipo = 1');

				if(!isset($conteudo['error'])){

					$resultado['conteudo'] = $conteudo['success'];
				}else{

					$resultado['conteudo'] = "error";
				}
			}else{

				$resultado['conteudo'] = "error";
			}

			if($resultado['conteudo'] != "error"){

				$resultado['title']  	 = 'Editar'; 
				$resultado['action'] 	 = 'atualizar/'.$id;
				$resultado['legend'] 	 = 'edição';
				$resultado['name']   	 = 'editarConteudo';
				$resultado['categorias'] = $this->categoriaModel->getList("");
				$resultado['resultado']  = false;

				if($param != null){

					$resultado['resultado'] = true;
					$resultado['mensagem']  = $param['mensagem'];
					$resultado['id']        = $id;
				}

				$this->loadView('formulario.php',$resultado);
			}else{

				errorPage(ERROR_ID_NOT_FOUND);
			}
		}

		//RECEBE UMA LISTA COM TODOS OS REGISTROS
		public function listConteudos($pg=null,$busca=false){

			if($busca != false){

				$listar  = $this->conteudoModel->getList($pg);

			}else{

				$start  = ($pg * $this->limite) - $this->limite;
				$busca  = " WHERE u.nivelId = 1 ORDER BY u.nome ASC LIMIT $start, $this->limite";
				$listar = $this->conteudoModel->getList($busca);
			}

			return $listar;
		}

		//DELETA UM REGISTRO ESPECÍFICO PELO ID
		public function delete($id,$usuarioId=null){

			$resultado['resultado'] = true;
			if($id != null){

				$delete = $this->conteudoModel->delete($id);

				if(!isset($delete['error'])){
				
					$resultado['mensagem'] = $delete['success'];
				
				}else{

					$resultado['resultado'] = false;
					$resultado['mensagem']  = $delete['error'];
				}
			}else{

				$resultado['resultado'] = false;
				$resultado['mensagem']  = 'Erro ao remover.';
			}

			echo json_encode($resultado);
		}

		// TRATAMENTO DOS DADOS ANTES DO ARMAZENAMENTO
		private function validarCampos($acao){

			$dados = array();

			// CASO EXISTA ALGUMA VALIDAÇÃO ESPECÍFICA EM UMA AÇÃO
			switch($acao){

				case 1:

					// VERIFICAÇÃO DO ENVIO DE IMAGENS ATRAVÉS DE CADASTROS
					if(!empty($_FILES['conteudo_banner']['name'])){

						//  UPLOAD BANNER
						$imagemNova = $this->conteudoModel->imagemUpload($_FILES['conteudo_banner'],$this->caminhoImagensFotoPerfil);

						$dados['conteudo']['banner']    = $imagemNova['imagemBanner'];
						$dados['conteudo']['thumbnail'] = $imagemNova['imagemThumbnail'];

					}else{

						$dados['conteudo']['banner']    = "";
						$dados['conteudo']['thumbnail'] = "";
					}

					$dados['conteudo']['id_categoria']    = $_POST['conteudo_categoria'];
					$dados['conteudo']['titulo'] 	      = $_POST['conteudo_titulo'];
					$dados['conteudo']['texto']  	      = $_POST['conteudo_texto'];
					$dados['conteudo']['data_publicacao'] = date('Y-m-d H:i');
					$dados['conteudo']['status'] 	      = isset($_POST['conteudo_status']) ? $_POST['conteudo_status'] : '0';
					$dados['conteudo']['tipo'] 	          = '1';
					$dados['conteudo']['id_autor']        = $_SESSION['ADMIN']['ID'];
					$dados['conteudo']['nivel']  	      = $_POST['conteudo_nivel'];
					$dados['conteudo']['visualizacoes']   = '0';
					$dados['conteudo']['destaque']        = isset($_POST['conteudo_destaque']) ? '1' : '0';
					$dados['conteudo']['slug']            = removeAccents($_POST['conteudo_titulo'],"-");
					$dados['conteudo']['resumo']          = $_POST['conteudo_resumo'];

				break;

				case 2:
					
					// VALIDAÇÕES ESPECÍFICAS NA ATUALIZAÇÃO
					
					// VERIFICAÇÃO DO ENVIO DE IMAGENS ATRAVÉS DE CADASTROS
					if(!empty($_FILES['conteudo_banner']['name'])){

						$imagemNova = $this->conteudoModel->imagemUpload($_FILES['conteudo_banner'],$this->caminhoImagensFotoPerfil);

						$dados['conteudo']['banner']    = $imagemNova['imagemBanner'];
						$dados['conteudo']['thumbnail'] = $imagemNova['imagemThumbnail'];
					}

					$dados['conteudo']['id_categoria']    = $_POST['conteudo_categoria'];
					$dados['conteudo']['titulo'] 	      = $_POST['conteudo_titulo'];
					$dados['conteudo']['texto']  	      = $_POST['conteudo_texto'];
					$dados['conteudo']['status'] 	      = isset($_POST['conteudo_status']) ? $_POST['conteudo_status'] : '0';
					$dados['conteudo']['tipo'] 	          = '1';
					$dados['conteudo']['nivel']  	      = $_POST['conteudo_nivel'];	
					$dados['conteudo']['visualizacoes']   = '0';
					$dados['conteudo']['destaque']        = isset($_POST['conteudo_destaque']) ? '1' : '0';
					$dados['conteudo']['slug']            = removeAccents($_POST['conteudo_titulo'],"-");
					$dados['conteudo']['resumo']          = $_POST['conteudo_resumo'];

				break;

				default:
					// AÇÃO INDEFINIDA OU INVÁLIDA
					die('Ação indefinida');
				break;

			}

			return $dados;
		}

		public function uploadFotos($imagem){
			$respostaJson = array();

			if(!empty($imagem) && is_array($imagem)){
				$imagemNova = $this->conteudoModel->imagemUpload($imagem,'conteudos_imagens_edicao',true);	

				if(isset($imagemNova['link'])){
					$imagemNova = URL.substr($imagemNova['link'],2);
				}	
			}else{
				$imagemNova = "";	
			}

			$respostaJson['link']  = $imagemNova;

			echo json_encode($respostaJson);
		}

	}
	?>