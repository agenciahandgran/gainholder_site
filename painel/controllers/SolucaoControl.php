<?php 

	/**************************************
	* CONTROLLER SOLUÇÃO
	**************************************/

	class SolucaoControl extends Controller{

		private $solucaoModel;
		private $moduloModel;
		private $usuarioModel;
		private $categoriaModel;

		public function __construct(){

			// SETANDO O MÓDULO
			$this->setModulo('solucoes');

			$permission = new Permissions();
			$this->setPaginasPermitidas($permission);

			if($this->verifyPermissionPage()){
				// CARREGANDO A MODEL PRINCIPAL
				$this->solucaoModel   			= new SolucaoModel();
				$this->moduloModel        		= new ModuloModel();
				$this->usuarioModel       		= new ClienteModel();
				$this->categoriaModel           = new CategoriaModel();
				$this->limite             		= 20;
				$this->caminhoImagensFotoPerfil = "solucao_banners";
			}else{

				errorPage(ERROR_PERMISSION_PAGE);
				exit;
			}
		}

		//CARREGA A VIEW PRINCIPAL DO MODULO
		public function index($pg = 1){

			$dados['pg']          = ''; 
			$dados['condicao']    = ''; 
			$dados['url']         = ''; 
			$dados['limite']      = ''; 
			$dados['solucoes']    = $this->listSolucoes($pg);
			$this->loadView('lista.php',$dados);
		}

		// CARREGA QUAL MÉTODO IRÁ RECEBER A REQUISIÇÃO AJAX
		public function loadMethod($acao, $id){

			if($acao != null){

				switch($acao):

					case 'delete':
						$this->delete($id);
					break;

				endswitch;
			}
		}

		//TRATA OS DADOS PARA SEREM INSERIDOS NO BANCO
		public function cadastrar(){

			$resultado['title']      = 'Cadastrar'; 
			$resultado['action']     = 'cadastrar';
			$resultado['legend']     = 'cadastro';
			$resultado['name']       = 'novaSolucao';
			$resultado['categorias'] = $this->categoriaModel->getList("");
			$resultado['resultado']  = false;

			if(isset($_REQUEST['modulo']) && $_REQUEST['modulo'] == "solucao"){

				$dados  = $this->validarCampos(INSERT);
				$insert = $this->solucaoModel->insert($dados);

				if($insert){
					$resultado['resultado'] = true;
				}else{
					$resultado['resultado'] = false;
				}

				$resultado['mensagem']  = $insert;
				$this->loadView('formulario.php',$resultado);
			}else{

				$this->loadView('formulario.php',$resultado);
			}
		}

		//TRATA OS DADOS PARA SEREM EDITADOS E ATUALIZADOS NO BANCO
		public function atualizar($id){
			if($id != null AND is_numeric($id)){

				if (isset($_REQUEST['modulo']) && $_REQUEST['modulo'] == "solucao") {
					
					$dados  = $this->validarCampos(UPDATE);
					$edicao = $this->solucaoModel->update($dados,$id);

					$resultado['resultado'] =  false;

					if(isset($edicao['success'])){

						$resultado['mensagem']  = $edicao;
						$resultado['resultado'] =  true;
					}else{

						$resultado['mensagem']  = $edicao;
					}

					$this->editar($id,$resultado);
				}else{

					errorPage(ERROR_MODULO_INCORRECT);
				}
			}else{

				errorPage(ERROR_ID_NOT_FOUND);
			}
		}

		//RECEBE UM DADO ESPECÍFICO PELO ID
		public function editar($id,$param = null){

			if($id != null AND is_numeric($id)){

				$solucao = $this->solucaoModel->getRow('s.*, ctg.nome as nomeCategoria, ctg.id_categoria as idCategoria',' WHERE s.id_solucao ='.$id);

				if(!isset($solucao['error'])){

					$resultado['solucao'] = $solucao['success'];
				}else{

					$resultado['solucao'] = "error";
				}
			}else{

				$resultado['solucao'] = "error";
			}

			if($resultado['solucao'] != "error"){

				$resultado['title']  	 = 'Editar'; 
				$resultado['action'] 	 = 'atualizar/'.$id;
				$resultado['legend'] 	 = 'edição';
				$resultado['name']   	 = 'editarConteudo';
				$resultado['categorias'] = $this->categoriaModel->getList("");
				$resultado['resultado']  = false;

				if($param != null){

					$resultado['resultado'] = true;
					$resultado['mensagem']  = $param['mensagem'];
					$resultado['id']        = $id;
				}

				$this->loadView('formulario.php',$resultado);
			}else{

				errorPage(ERROR_ID_NOT_FOUND);
			}
		}

		//RECEBE UMA LISTA COM TODOS OS REGISTROS
		public function listSolucoes($pg=null,$busca=false){

			if($busca != false){

				$listar  = $this->solucaoModel->getList($pg);

			}else{

				$start  = ($pg * $this->limite) - $this->limite;
				$busca  = " WHERE u.nivelId = 1 ORDER BY u.nome ASC LIMIT $start, $this->limite";
				$listar = $this->solucaoModel->getList($busca);
			}

			return $listar;
		}

		//DELETA UM REGISTRO ESPECÍFICO PELO ID
		public function delete($id,$usuarioId=null){

			$resultado['resultado'] = true;
			if($id != null){

				$delete = $this->solucaoModel->delete($id);

				if(!isset($delete['error'])){
				
					$resultado['mensagem'] = $delete['success'];
				
				}else{

					$resultado['resultado'] = false;
					$resultado['mensagem']  = $delete['error'];
				}
			}else{

				$resultado['resultado'] = false;
				$resultado['mensagem']  = 'Erro ao remover.';
			}

			echo json_encode($resultado);
		}

		// TRATAMENTO DOS DADOS ANTES DO ARMAZENAMENTO
		private function validarCampos($acao){

			$dados = array();

			// CASO EXISTA ALGUMA VALIDAÇÃO ESPECÍFICA EM UMA AÇÃO
			switch($acao){

				case 1:

					// VERIFICAÇÃO DO ENVIO DE IMAGENS ATRAVÉS DE CADASTROS
					if(!empty($_FILES['solucao_banner']['name'])){

						$dados['solucao']['banner'] = $this->solucaoModel->imagemUpload($_FILES['solucao_banner'],$this->caminhoImagensFotoPerfil);
					}else{

						$dados['solucao']['banner'] = "";
					}

					$dados['solucao']['banner_titulo']    = $_POST['solucao_banner_titulo'];
					$dados['solucao']['banner_descricao'] = $_POST['solucao_banner_descricao'];
					$dados['solucao']['id_categoria']     = $_POST['solucao_categoria'];
					$dados['solucao']['titulo'] 	      = $_POST['solucao_titulo'];
					$dados['solucao']['texto']  	      = $_POST['solucao_texto'];
					$dados['solucao']['status'] 	      = $_POST['solucao_status'];
					$dados['solucao']['tipo'] 	          = '1';								
				break;

				case 2:
					
					// VALIDAÇÕES ESPECÍFICAS NA ATUALIZAÇÃO
					
					// VERIFICAÇÃO DO ENVIO DE IMAGENS ATRAVÉS DE CADASTROS
					if(!empty($_FILES['solucao_banner']['name'])){

						$dados['solucao']['banner'] = $this->solucaoModel->imagemUpload($_FILES['solucao_banner'],$this->caminhoImagensFotoPerfil);
					}

					$dados['solucao']['banner_titulo']    = $_POST['solucao_banner_titulo'];
					$dados['solucao']['banner_descricao'] = $_POST['solucao_banner_descricao'];
					$dados['solucao']['id_categoria']     = $_POST['solucao_categoria'];
					$dados['solucao']['titulo'] 	      = $_POST['solucao_titulo'];
					$dados['solucao']['texto']  	      = $_POST['solucao_texto'];
					$dados['solucao']['status'] 	      = $_POST['solucao_status'];
					$dados['solucao']['tipo'] 	          = '1';	
				
				break;

				default:
					// AÇÃO INDEFINIDA OU INVÁLIDA
					die('Ação indefinida');
				break;

			}

			return $dados;
		}

	}
	?>