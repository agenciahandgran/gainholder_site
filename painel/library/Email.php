<?php

	// header('Content-Type: text/html; charset=utf-8');

	//require($_SERVER['DOCUMENT_ROOT'] . SITE_PATH . 'PHPMailer/class.phpmailer.php');


	//require('PHPMailer/Exception.php');
	require('PHPMailer/PHPMailer.php');
	require('PHPMailer/SMTP.php');
	
	class Email {

		// MÉTODOS
		function __construct(){		

		}

		function Enviar($remetente, $remetenteNome, $destinatario, $destinatarioEmail, $assunto, $conteudo,$arquivo=null,$arquivo2=null,$destinatarioCC=null){

			$mail = new PHPMailer(true);            // Passing `true` enables exceptions
			
			try {

			    //Server settings
			    $mail->CharSet   = "UTF-8";
			    $mail->SMTPDebug = 0;             // Enable verbose debug output
			    $mail->isSMTP();                    // Set mailer to use SMTP
			    $mail->Host 	  = EMAIL_HOST;     // Specify main and backup SMTP servers
			    $mail->SMTPAuth   = true;           // Enable SMTP authentication
			    $mail->Username   = EMAIL_USUARIO;  // SMTP username
			    $mail->Password   = EMAIL_SENHA;    // SMTP password
			    $mail->SMTPSecure = EMAIL_SSL;      // Enable TLS encryption, `ssl` also accepted
			    $mail->Port       = EMAIL_PORTA;    // TCP port to connect to

				// $mail->smtpConnect(
				//     array(
				//         "ssl" => array(
				//             "verify_peer" => false,
				//             "verify_peer_name" => false,
				//             "allow_self_signed" => true
				//         )
				//     )
				// );

			    //Recipients
			    $mail->setFrom($remetente, $remetenteNome);
			    $mail->addAddress($destinatarioEmail, $destinatario);     // Add a recipient

			    //Content
			    $mail->isHTML(true);                                  // Set email format to HTML
			    $mail->Subject = $assunto;
			    $mail->Body    = $conteudo;

			    // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

			    if($mail->send()){
			    	$retorno = true;
			    }else{
			    	$retorno = false;
			    }

			} catch (Exception $e) {
				$retorno = false;
			}

			return $retorno;

		}

		// function Enviar($remetente, $remetenteNome, $destinatario, $destinatarioEmail, $assunto, $conteudo,$arquivo=null,$arquivo2=null,$destinatarioCC=null){

		// 	$mail = new PHPMailer();


		// 	// echo 'Informações email: <br>';
		// 	// echo "Porta: ".EMAIL_PORTA." <br>";
		// 	// echo "SSL: ".EMAIL_SSL."<br>";
		// 	// echo "HOST: ".EMAIL_USUARIO."<br>";
		// 	// echo "Senha: ".EMAIL_SENHA."<br>";
		// 	// echo "Remetente: ".$remetente."<br>";
		// 	// echo "Destinatário: ".$destinatarioEmail."<br>";
		// 	// echo "Conteúdo: ".$conteudo."<br>";
		// 	// echo "Assunto: ".$assunto."<br>";


		// 	$mail->CharSet 		='UTF-8';
		// 	$mail->IsSMTP();
		// 	$mail->SMTPAuth 	= false;
		// 	$mail->Port 		= EMAIL_PORTA;
		// 	$mail->SMTPSecure 	= EMAIL_SSL;
		// 	$mail->Host 		= EMAIL_HOST;
		// 	$mail->Username 	= EMAIL_USUARIO;
		// 	$mail->Password 	= EMAIL_SENHA;

		// 	$mail->IsHTML(true);	

		// 	$mail->SetFrom($remetente, $remetenteNome);
		// 	$mail->AddAddress($destinatarioEmail, $destinatario);

		// 	if($destinatarioCC != null){
		// 		$mail->addCC($destinatarioCC);
		// 	}

		// 	$mail->Subject = $assunto;

		// 	$mail->MsgHTML($conteudo);

		// 	if($arquivo != null){
		// 		$mail->addStringAttachment(file_get_contents($arquivo), 'boleto.pdf');
		// 	}

		// 	if($arquivo2 != null){
		// 		$mail->addStringAttachment(file_get_contents($arquivo2), 'demonstrativo.pdf');
		// 	}

		// 	$enviaEmail = $mail->Send();

		// 	if($enviaEmail == 1)
		// 	{

		// 	   return 1;
		// 	}
		// 	else
		// 	{
		// 		echo "Error: ".$mail->ErrorInfo;		
		// 	   return $mail->ErrorInfo;		
		// 	}

		// 	// exit;
		// }

		function __destruct(){

		}

	}



?>