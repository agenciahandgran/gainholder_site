<?php  

class Pagseguro{

	private $email;
	private $token;
	private $url_sandbox   = "https://ws.sandbox.pagseguro.uol.com.br";
	private $url_pagamento = "https://ws.pagseguro.uol.com.br";
	private $sandbox;
	private $sessaoId;

	// RECEBENDO PARÂMETROS PARA AUTENTICAÇÃO 
	public function __construct($email,$token,$sandbox=true){
		$this->email   = $email;
		$this->token   = $token;
		$this->sandbox = $sandbox;
	}

	// static function statusPlano(){}

	// static function statusAdesao(){}

	// static function statusOrdemPagamento(){}

	// static function statusTransacao(){}

	// static function codigoErros(){}

	// INICIANDO UMA SESSÃO DE PAGAMENTO
	public function iniciarSessao(){
		
		$dadosRequisicao = $this->requisicaoApi("/v2/sessions");

		// RECUPERA REQUISIÇÃO XML
		$sessao = new SimpleXMLElement($dadosRequisicao);

		if(isset($sessao->id)){
			$this->sessaoId = $sessao->id;	
		}else{
			$this->sessaoId = 0;
		}	
	}

	public function getSessaoId(){
		return $this->sessaoId;
	}

	public function criarPlano($dados){

		if(is_array($dados)){
	
			// ENVIANDO OS DADOS NO FORMATO JSON
			$dados = json_encode($dados);

			$dadosRequisicao = $this->requisicaoApi("/pre-approvals/request", $dados, true);	
	 
			// RECUPERA REQUISIÇÃO XML
			$plano = new SimpleXMLElement($dadosRequisicao);
			
			if(isset($plano->code)){
				$codigoPlano = $plano->code;
			}else{
				$codigoPlano = 0;
			}

		}else{
			$codigoPlano = 0;
		}

		return $codigoPlano;
	}

	//ADERIR AO PLANO
	public function aderirPlano($dados){
		
		if(is_array($dados)){

			// ENVIANDO OS DADOS NO FORMATO JSON
			$dados = json_encode($dados);

			$dadosRequisicao = $this->requisicaoApi("/pre-approvals",$dados,true);

			// RECUPERA REQUISIÇÃO XML
			$pagamento = new SimpleXMLElement($dadosRequisicao);
		
			if(isset($pagamento->code)){
				$codigo = $pagamento->code[0];	
			}else{
				$codigo = 0;
			}	

		}else{
			$codigo = 0;
		}

		return $codigo;	
	}


	// public function editarAdesao($dados){}

	// public function suspenderAdesao($dados){}

	public function cancelarAdesao($codigoAdesao){
		$dados = array();

		$dadosRequisicao = $this->requisicaoApi("/pre-approvals/".$codigoPlano."/cancel",$dados,true,"PUT");	

		// print_r($dadosRequisicao);exit;
	}

	// public function consultarStatusAdesao($codigoPlano){
	// 	$dados = array();
	// 	$dadosRequisicao = $this->requisicaoApi("/pre-approvals/".$codigoPlano."/status",$dados,false,"PUT");	

	// 	print_r($dadosRequisicao);exit;	
	// }

	// public function mudarAdesaoPagamento($dados){}

	// public function listarPagamentos($dados){}

	// public function repetirPagamento($dados){}

	// public function consultarPagamento($dados){}

	// REALIZA AS REQUISIÇÕES PARA API DO PAGSEGURO
	private function requisicaoApi($urlMetodo, $dados = array(), $json=false, $metodo="POST"){
		$headers = array();
		$curl = curl_init();

		if($json){
			$headers = array("Content-type: application/json;charset=ISO-8859-1",
							 "Accept: application/vnd.pagseguro.com.br.v3+xml;charset=ISO-8859-1");
		}else{
			$headers = array("Content-Type: application/xml; charset=ISO-8859-1");
		}
		
		// VERIFICA SE É SANDBOX OU PRODUÇÃO (URL)
		if($this->sandbox){
			$url = $this->url_sandbox;
		}else{
			$url = $this->url_pagamento;
		}

		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
	
		if($metodo == "POST"){
			curl_setopt($curl, CURLOPT_POST, true);
		}else{
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");	
		}

		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_URL, $url.$urlMetodo."?email=".$this->email."&token=".$this->token);
		
		curl_setopt($curl, CURLOPT_POSTFIELDS, $dados);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		 
		// RESPOSTA REQUISIÇÃO
		$resposta = curl_exec($curl);

		curl_close($curl);		

		return $resposta;	
	}

}
