<?php

	/********************************
	 * MODEL DE ADMINISTRADORES
	 * ******************************/

	class AdministradorModel{

		const TABLE = "login";
		private $mysql;

		public function __construct(){

			$this->mysql = new Mysql();
		}

		//TRATA OS ADMINISTRADORES PARA SEREM INSERIDOS NO BANCO
		public function insert($dados){

			if(is_array($dados)){

				$separador = fieldColumnSeparator($dados['login']);
				$sqlInsert = "INSERT INTO ".self::TABLE." (".$separador['fields'].") VALUES(:email,:senha,:status,:tipo)";
				$campos    = array('email'  => $dados['login']['email'], 
								   'senha'  => $dados['login']['senha'], 
								   'status' => $dados['login']['status'],
								   'tipo'   => $dados['login']['tipo']
 								);
				$executar  = $this->mysql->execute($sqlInsert,$campos);

				if($executar){

					$idLogin = $this->getLastId();

					$dados['administrador']['id_login'] = $idLogin;

					$separador = fieldColumnSeparator($dados['administrador']);
					$sqlInsertAdmin = "INSERT INTO administradores (".$separador['fields'].") VALUES(:nome_completo,:foto_perfil,:status,:id_login)";
					
					$campos    = array( 
									   'nome_completo'  => $dados['administrador']['nome_completo'], 
									   'foto_perfil'    => $dados['administrador']['foto_perfil'],
									   'status'         => $dados['administrador']['status'],
									   'id_login'       => $dados['administrador']['id_login']
	 								);
					$executar  = $this->mysql->execute($sqlInsertAdmin,$campos);

					if($executar){

						if(isset($dados['modulos'])){

							for($i=0; $i<count($dados['modulos']); $i++){
	
								$sqlInsertPermissoes = "INSERT INTO permissoes (id_login, id_modulo, status) VALUES(:id_login,:id_modulo,:status)";
								
								$campos    = array('id_login'  => $idLogin, 
												   'id_modulo' => $dados['modulos'][$i], 
												   'status'    => '1'
				 								);
								$executar  = $this->mysql->execute($sqlInsertPermissoes,$campos);
							}
						}

						$result['success'] = "Administrador cadastrado com sucesso!";

					}else{
						$result['error']  = "Não foi possível realizar o cadastro.";
					}

				}else{

					$result['error']  = "Não foi possível realizar o cadastro.";
				}
			}else{

				$result['error'] = "Não foi possível realizar o cadastro.";
			}

			return $result;
		}

		// INSERE UMA UNICA LINHA ESPECÍFICA
		public function insertRow($dados){

			if(is_array($dados)){

				$separador = fieldColumnSeparator($dados['login']);
				$sqlInsert = "INSERT INTO ".self::TABLE." (".$separador['fields'].") VALUES(:email,:senha,:status,:tipo)";
				$campos    = array(
									'email'      => $dados['login']['email'], 
									'senha'      => $dados['login']['senha'], 
									'status'     => $dados['login']['status'],
									'tipo'       => $dados['login']['tipo']
								);

				$executar  = $this->mysql->execute($sqlInsert,$campos);

				if($executar){

					$result['success'] = "Usuário cadastrado com sucesso!";
				}else{

					$result['error']  = "Não foi possível realizar o cadastro.";
				}
			}else{

				$result['error'] = "Não foi possível realizar o cadastro.";
			}

			return $result;
		}

		//TRATA OS ADMINISTRADORES PARA SEREM EDITADOS E ATUALIZADOS NO BANCO
		public function update($dados,$cond){

	  		$fieldsEdit = updateSeparator($dados['login']);// RETORNA UMA QUERY DINAMICA
	        $sqlEditar  = "UPDATE ".self::TABLE." SET ".$fieldsEdit['result']." WHERE id_login = ".$cond;
	        $campos     = array("");
	        $editarEst  = $this->mysql->execute($sqlEditar,$campos);

	        if($editarEst){

		  		$fieldsEditAdmin = updateSeparator($dados['administrador']);// RETORNA UMA QUERY DINAMICA
		        $sqlEditarAdmin  = "UPDATE administradores SET ".$fieldsEditAdmin['result']." WHERE id_login = ".$cond;
		        $campos     	 = array("");
		        $editarEst  	 = $this->mysql->execute($sqlEditarAdmin,$campos);

				if(isset($dados['modulos'])){
						
					$sqlDelete = "DELETE FROM permissoes WHERE id_login =:id";
					$campos    = array("id" => $cond);
					$excluir   = $this->mysql->execute($sqlDelete,$campos);	

					for($i=0; $i<count($dados['modulos']); $i++){

						$sqlInsertPermissoes = "INSERT INTO permissoes (id_login, id_modulo, status) VALUES(:id_login,:id_modulo,:status)";
						
						$campos    = array('id_login'  => $cond, 
										   'id_modulo' => $dados['modulos'][$i], 
										   'status'    => '1'
		 								);
						$executar  = $this->mysql->execute($sqlInsertPermissoes,$campos);
					}
				
				}else{

					$sqlDelete = "DELETE FROM permissoes WHERE id_login =:id";
					$campos    = array("id" => $cond);
					$excluir   = $this->mysql->execute($sqlDelete,$campos);	
				}

	        	$resultado['success'] = "Atualizado com sucesso!";
	        }else{

	        	$resultado['error']  = "Não foi possivel editar o administrador.";
	        }

	        return $resultado;
		}

		//DELETA UM DADO ESPECÍFICO PELO ID
		public function delete($id){

			if(is_numeric($id)){

				$sqlDelete = "DELETE FROM ".self::TABLE." WHERE id_login=:id";
				$campos    = array("id" => $id);
				$excluir   = $this->mysql->execute($sqlDelete,$campos);

				if($excluir){

					$resultado['success'] = "Usuario removido.";
				}else{

					$resultado['error'] = "Erro ao remover";
				} 
			}else{

				$resultado['error'] = "Erro ao remover";
			}

			return $resultado;
		}

		//RECUPERA UM LISTA DE DADOS
		public function getList($condicao){

			$sqlListar = " SELECT l.id_login ,  l.email, l.senha, l.status, l.tipo, a.nome_completo, a.id as idAdministrador
						   FROM ".self::TABLE." l 
						   INNER JOIN administradores a ON l.id_login = a.id_login 
						   WHERE l.tipo =  1 
						   ORDER BY a.nome_completo ASC ";

			$lista = $this->mysql->getList($sqlListar);

			if(!in_array(null, $lista)){

				$resultado = $this->makeList($lista);
			}else{

				$resultado['error'] = "Nenhum resultado encontrado.";
			}

			return $resultado;
		}

		//RETORNA UMA LINHA ESPECÍFICA
		public function getRow($campos, $cond){

			$sqlDetalhar = "SELECT {$campos} FROM ".self::TABLE." l 
							INNER JOIN administradores a ON l.id_login = a.id_login  {$cond}";

			$campos   = array("");
			$detalhar = $this->mysql->getRow($sqlDetalhar,$campos);

			if($detalhar){

				$resultado['success'] = $this->makeList($detalhar);
			}else{

				$resultado['error'] = "Usuário não encontrado.";
			}

			return $resultado;
		}

		//RETORNA O ÚLTIMO ID INSERIDO NA TABELA
		public function getLastId(){

			$sqlLastId = "SELECT id_login FROM ".self::TABLE." ORDER BY id_login DESC LIMIT 1";
			$dados     = array("");
			$lastId    = $this->mysql->getRow($sqlLastId,$dados);
	
			if(!empty($lastId)){
			    $result = $lastId->id_login;
			}else{
			    $result = null;       
			}

			return $result;
		}

		/**
        * TRATA A IMAGEM PARA SER INSERIDA NO BANCO E SALVA NA PASTA DE FOTOS DO EXEMPLOS
        * @return string
        */
		public function imagemUpload($imagem,$pasta){

			$resultado = "";

			if(is_array($imagem)){

				if($imagem != null){

					$extensoesPermitidas = array('.jpg','.png','.gif','.svg');

					//TRATA A IMAGEM PARA SER ARMAZENADA NO BANCO
					$ext        = strtolower(substr($imagem['name'],strrpos($imagem['name'],".")));
					$imagemNome = base64_encode($imagem['name'].rand(0,10000));

					$pasta = URL_PATH_UPLOAD.$pasta.'/';

					if(in_array($ext, $extensoesPermitidas)){

						$imagemNome = date('Y-m-d-H-i-s').$imagemNome;

						$upload     = new UploadArquivo($imagem, 300);

						// PASTA PADRÃO PARA SER ARMAZENADO O ARQUIVO
						$upload->setPath($pasta);

						// NOME QUE SERÁ SALVO O ARQUIVO NA PASTA
						$upload->setName($imagemNome);

						// REALIZA O UPLOAD
						$uploadArquivo = $upload->upload("", true);

						if($uploadArquivo['Success']){

							$resultado = $imagemNome.$ext;

						}else{

							$resultado = "";
						}

					}else{

						$resultado = "";
					}
				}
			}else{

				$resultado = $imagem;
			}

			return $resultado;
		}

		/**
		*  CRIA UMA LISTA DE OBJETOS ATRAVÉS DE UMA QUERY EXECUTADA
		*  @param query executada 
		*  @return array ou nulo
		*/
		public function makeList($list){

			if(!empty($list) && $list != null){

				if(count($list) > 0){

					$result = array();

					if(!is_object($list)){

						foreach($list as $row):

						    array_push($result, $this->loadObjectsFromSql($row));

						endforeach;

					}else{

						$result = $this->loadObjectsFromSql($list);
					}
				}else{

					$result = null;
				}
			}else{

				$result = null;
			}

			return $result;
		}

		//TRANSFORMA OS RESULTADOS EM OBJETO
		public function loadObjectsFromSql($row){

			$login = new Login(isset($row->id_login) ? $row->id_login : null);
			$login->setEmail(isset($row->email) ? $row->email : null);
			$login->setStatus(isset($row->status) ? $row->status : null);
			$login->setTipo(isset($row->tipo) ? $row->tipo : null);

			$administrador = new Administrador();

			if(isset($row->idAdministrador)){
				$administrador->setId($row->idAdministrador);
			}

			if(isset($row->nome_completo)){
				$administrador->setNomeCompleto($row->nome_completo);
			}

			if(isset($row->foto_perfil)){
				$administrador->setFotoPerfil($row->foto_perfil);
			}

			$login->setUsuario($administrador);

			return $login;
		}
	}
?>