<?php

	/********************************
	 * MODEL DE ARTIGOS
	 * ******************************/

	class ArtigoModel{

		const TABLE = "conteudos";
		private $mysql;

		public function __construct(){

			$this->mysql = new Mysql();
		}

		//TRATA OS CONTEÚDOS PARA SEREM INSERIDOS NO BANCO
		public function insert($dados){

			if(is_array($dados)){

				$separador = fieldColumnSeparator($dados['artigo']);
				$sqlInsert = "INSERT INTO ".self::TABLE." (".$separador['fields'].") VALUES(:banner,:thumbnail,:id_categoria,:titulo,:texto,:data_publicacao,:status,:tipo,:id_autor,:nivel,:visualizacoes,:destaque,:slug,:resumo)";
				$campos    = array(
					'banner'          => $dados['artigo']['banner'],
					'thumbnail'       => $dados['artigo']['thumbnail'],					
					'id_categoria'    => $dados['artigo']['id_categoria'], 
					'titulo'          => $dados['artigo']['titulo'],
					'texto'  		  => $dados['artigo']['texto'],
					'data_publicacao' => $dados['artigo']['data_publicacao'],
					'status' 		  => $dados['artigo']['status'],
					'tipo'     		  => $dados['artigo']['tipo'],
					'id_autor' 		  => $dados['artigo']['id_autor'],
					'nivel'    		  => $dados['artigo']['nivel'],
					'visualizacoes'   => $dados['artigo']['visualizacoes'],
					'destaque'        => $dados['artigo']['destaque'],
					'slug'            => $dados['artigo']['slug'],
					'resumo'          => $dados['artigo']['resumo']
				);
				$executar  = $this->mysql->execute($sqlInsert,$campos);

				if($executar){

					$result['success'] = "Artigo cadastrado com sucesso!";

				}else{

					$result['error']  = "Não foi possível realizar o cadastro.";
				}
			}else{

				$result['error'] = "Não foi possível realizar o cadastro.";
			}

			return $result;
		}

		//TRATA OS ADMINISTRADORES PARA SEREM EDITADOS E ATUALIZADOS NO BANCO
		public function update($dados,$cond){

	  		$fieldsEdit = updateSeparator($dados['artigo']);// RETORNA UMA QUERY DINAMICA
	  		$sqlEditar  = "UPDATE ".self::TABLE." SET ".$fieldsEdit['result']." WHERE id_conteudo = ".$cond;
	  		$campos     = array("");
	  		$editarEst  = $this->mysql->execute($sqlEditar,$campos);

	  		if($editarEst){
		  		$resultado['success'] = "Atualizado com sucesso!";
		  	}else{

		  		$resultado['error']  = "Não foi possivel editar o conteúdo.";
		  	}

		  	return $resultado;
		  }

		//DELETA UM DADO ESPECÍFICO PELO ID
		  public function delete($id){

		  	if(is_numeric($id)){

		  		$sqlDelete = "DELETE FROM ".self::TABLE." WHERE id_conteudo=:id";
		  		$campos    = array("id" => $id);
		  		$excluir   = $this->mysql->execute($sqlDelete,$campos);

		  		if($excluir){

		  			$resultado['success'] = "Conteúdo removido.";
		  		}else{

		  			$resultado['error'] = "Erro ao remover";
		  		} 
		  	}else{

		  		$resultado['error'] = "Erro ao remover";
		  	}

		  	return $resultado;
		  }

		//RECUPERA UM LISTA DE DADOS
		  public function getList($condicao){

		  	$sqlListar = " SELECT c.*, ctg.nome as nomeCategoria, ctg.id_categoria as idCategoria, at.nome_completo, at.id as idColunista 
		  				   FROM ".self::TABLE." c 
		  				   INNER JOIN categorias ctg ON ctg.id_categoria = c.id_categoria
		  				   INNER JOIN login l ON l.id_login = c.id_autor 
		  				   INNER JOIN colunistas at ON at.id_login = l.id_login
		  				   WHERE c.tipo = 2 AND l.tipo = 2 {$condicao}";

		  	$lista = $this->mysql->getList($sqlListar);

		  	if(!in_array(null, $lista)){

		  		$resultado = $this->makeList($lista);
		  	}else{

		  		$resultado['error'] = "Nenhum resultado encontrado.";
		  	}

		  	return $resultado;
		  }

		//RETORNA UMA LINHA ESPECÍFICA
		  public function getRow($campos, $cond){

		  	$sqlDetalhar = "SELECT {$campos} FROM ".self::TABLE." a
		  	INNER JOIN categorias ctg ON ctg.id_categoria = a.id_categoria {$cond}";

		  	$campos   = array("");
		  	$detalhar = $this->mysql->getRow($sqlDetalhar,$campos);

		  	if($detalhar){

		  		$resultado['success'] = $this->makeList($detalhar);
		  	}else{

		  		$resultado['error'] = "Usuário não encontrado.";
		  	}

		  	return $resultado;
		  }

		//RETORNA O ÚLTIMO ID INSERIDO NA TABELA
		  public function getLastId(){

		  	$sqlLastId = "SELECT id_conteudo FROM ".self::TABLE." WHERE tipo = 2 ORDER BY id_conteudo DESC LIMIT 1";
		  	$dados     = array("");
		  	$lastId    = $this->mysql->getRow($sqlLastId,$dados);

		  	if(!empty($lastId)){
		  		$result = $lastId->id_login;
		  	}else{
		  		$result = null;       
		  	}

		  	return $result;
		  }

		/**
        * TRATA A IMAGEM PARA SER INSERIDA NO BANCO E SALVA NA PASTA DE FOTOS DO EXEMPLOS
        * @return string
        */
		public function imagemUpload($imagem,$pasta,$ajax=false){

			$resultado       = "";
			$imagemBanner 	 = "";
			$imagemThumbnail = "";

			if(is_array($imagem)){

				if($imagem != null){

					$extensoesPermitidas = array('.jpg','.png','.gif','.svg');

					//TRATA A IMAGEM PARA SER ARMAZENADA NO BANCO
					$ext        = strtolower(substr($imagem['name'],strrpos($imagem['name'],".")));
					$imagemNome = base64_encode($imagem['name'].rand(0,10000));

					if(!$ajax){
						$pasta = URL_PATH_UPLOAD.$pasta.'/';
					}else{
						$pasta = '../'.URL_PATH_UPLOAD.$pasta.'/';
					}

					if(in_array($ext, $extensoesPermitidas)){

						$upload = new UploadArquivo($imagem);

						// PASTA PADRÃO PARA SER ARMAZENADO O ARQUIVO
						$upload->setPath($pasta);

						// NOME QUE SERÁ SALVO O ARQUIVO NA PASTA
						$upload->setName($imagemNome);

						// REALIZA O UPLOAD
						$uploadArquivo = $upload->upload("", true);

						if(isset($uploadArquivo['Success'])){

							$resultado = $imagemNome.$ext;

							if(!$ajax){
								
								// REDIMENSIONAMENTO BANNER
								$banner = new Upload($pasta.$resultado);
								$banner->resize(750,360);
								$banner->save($pasta.'banners/banner_'.$resultado);	

								$imagemBanner = 'banner_'.$resultado;

								// REDIMENSIONAMENTO THUMBNAIL
								$thumb = new Upload($pasta.$resultado);
								$thumb->resize(265,158);
								$thumb->save($pasta.'thumbs/thumb_'.$resultado);

								$imagemThumbnail = 'thumb_'.$resultado;

								// REMOVE O ARQUIVO TEMPORÁRIO 
								unlink($pasta.$resultado);
							}

						}else{

							$resultado = "";
						}

					}else{

						$resultado = "";
					}
				}
			}else{

				$resultado = $imagem;
			}

			if(!$ajax){
				return array('imagemBanner' => $imagemBanner, 'imagemThumbnail' => $imagemThumbnail);
			}else{
				return array('link' => $pasta.$resultado);
			}
			
		}

		/**
		*  CRIA UMA LISTA DE OBJETOS ATRAVÉS DE UMA QUERY EXECUTADA
		*  @param query executada 
		*  @return array ou nulo
		*/
		public function makeList($list){

			if(!empty($list) && $list != null){

				if(count($list) > 0){

					$result = array();

					if(!is_object($list)){

						foreach($list as $row):

							array_push($result, $this->loadObjectsFromSql($row));

						endforeach;

					}else{

						$result = $this->loadObjectsFromSql($list);
					}
				}else{

					$result = null;
				}
			}else{

				$result = null;
			}

			return $result;
		}

		//TRANSFORMA OS RESULTADOS EM OBJETO
		public function loadObjectsFromSql($row){

			$conteudo = new Conteudo($row->id_conteudo);
			$conteudo->setTitulo(isset($row->titulo) ? $row->titulo : '');

			$categoria = new Categoria();

			if(isset($row->idCategoria)){
				$categoria->setId($row->idCategoria);
			}

			if(isset($row->nomeCategoria)){
				$categoria->setNome($row->nomeCategoria);
			}

			$conteudo->setCategoria($categoria);

			$conteudo->setBanner(isset($row->banner) ? $row->banner : '');
			$conteudo->setThumbnail(isset($row->thumbnail) ? $row->thumbnail : '');
			$conteudo->setTexto(isset($row->texto) ? $row->texto : '');
			$conteudo->setDataPublicacao(isset($row->data_publicacao) ? $row->data_publicacao : '');
			$conteudo->setStatus(isset($row->status) ? $row->status : '');
			$conteudo->setTipo(isset($row->tipo) ? $row->tipo : '');

			$colunista = new Colunista();

			if(isset($row->id_autor)){
				$colunista->setIdLogin($row->id_autor);
			}

			if(isset($row->idColunista)){
				$colunista->setId($row->idColunista);
			}

			if(isset($row->nome_completo)){
				$colunista->setNomeCompleto($row->nome_completo);
			}

			$conteudo->setAutor($colunista);
			$conteudo->setNivel(isset($row->nivel) ? $row->nivel : '');
			$conteudo->setVisualizacoes(isset($row->visualizacoes) ? $row->visualizacoes : '0');
			$conteudo->setDestaque(isset($row->destaque) ? $row->destaque : '0');
			$conteudo->setSlug(isset($row->slug) ? $row->slug : '');
			$conteudo->setResumo(isset($row->resumo) ? $row->resumo : '');

			return $conteudo;

		}
	}
	?>