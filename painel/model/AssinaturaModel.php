<?php

	/********************************
	 * MODEL DE PLANOS
	 * ******************************/

	class AssinaturaModel{

		const TABLE = "planos";
		private $mysql;

		public function __construct(){

			$this->mysql = new Mysql();
		}

		//TRATA OS CONTEÚDOS PARA SEREM INSERIDOS NO BANCO
		public function insert($dados){

			if(is_array($dados)){

				$separador = fieldColumnSeparator($dados['assinatura']);
				$sqlInsert = "INSERT INTO ".self::TABLE." (".$separador['fields'].") VALUES(:nome,:preco,:desconto,:grau,:status,:codigo_pagseguro)";

				$campos    = array(
					'nome'             => $dados['assinatura']['nome'],
					'preco'            => formatReal($dados['assinatura']['preco'],"."),
					'desconto'         => $dados['assinatura']['desconto'],
					'grau'             => $dados['assinatura']['grau'],
					'status'           => $dados['assinatura']['status'],
					'codigo_pagseguro' => $dados['assinatura']['codigo_pagseguro'][0]
				);

				$executar  = $this->mysql->execute($sqlInsert,$campos);

				if($executar){

					$result['success'] = "Plano cadastrado com sucesso!";

				}else{

					$result['error']  = "Não foi possível realizar o cadastro.";
				}
			}else{

				$result['error'] = "Não foi possível realizar o cadastro.";
			}

			return $result;
		}

		// INSERE UMA UNICA LINHA ESPECÍFICA
		// public function insertRow($dados){

		// 	if(is_array($dados)){

		// 		$separador = fieldColumnSeparator($dados['login']);
		// 		$sqlInsert = "INSERT INTO ".self::TABLE." (".$separador['fields'].") VALUES(:email,:senha,:status,:usuarioId,:permissoes)";
		// 		$campos    = array(
		// 							'usuarioId'  => $dados['login']['usuarioId'],
		// 							'email'      => $dados['login']['email'], 
		// 							'senha'      => $dados['login']['senha'], 
		// 							'status'     => $dados['login']['status'], 
		// 							'permissoes' => $dados['login']['permissoes']
		// 						);

		// 		$executar  = $this->mysql->execute($sqlInsert,$campos);

		// 		if($executar){

		// 			$result['success'] = "Usuário cadastrado com sucesso!";
		// 		}else{

		// 			$result['error']  = "Não foi possível realizar o cadastro.";
		// 		}
		// 	}else{

		// 		$result['error'] = "Não foi possível realizar o cadastro.";
		// 	}

		// 	return $result;
		// }

		//TRATA OS ADMINISTRADORES PARA SEREM EDITADOS E ATUALIZADOS NO BANCO
		public function update($dados,$cond){

	  		$fieldsEdit = updateSeparator($dados['assinatura']);// RETORNA UMA QUERY DINAMICA
	  		$sqlEditar  = "UPDATE ".self::TABLE." SET ".$fieldsEdit['result']." WHERE id_plano = ".$cond;
	  		$campos     = array("");
	  		$editarEst  = $this->mysql->execute($sqlEditar,$campos);

	  		if($editarEst){
		  		$resultado['success'] = "Atualizado com sucesso!";
		  	}else{

		  		$resultado['error']  = "Não foi possivel editar o conteúdo.";
		  	}

		  	return $resultado;
		  }

		//DELETA UM DADO ESPECÍFICO PELO ID
		  public function delete($id){

		  	if(is_numeric($id)){

		  		$sqlDelete = "DELETE FROM ".self::TABLE." WHERE id_plano=:id";
		  		$campos    = array("id" => $id);
		  		$excluir   = $this->mysql->execute($sqlDelete,$campos);

		  		if($excluir){

		  			$resultado['success'] = "Conteúdo removido.";
		  		}else{

		  			$resultado['error'] = "Erro ao remover";
		  		} 
		  	}else{

		  		$resultado['error'] = "Erro ao remover";
		  	}

		  	return $resultado;
		  }

		//RECUPERA UM LISTA DE DADOS
		  public function getList($condicao){

		  	$sqlListar = " SELECT * FROM ".self::TABLE." {$condicao} ORDER BY preco ASC";

		  	$lista = $this->mysql->getList($sqlListar);

		  	if(!in_array(null, $lista)){

		  		$resultado = $this->makeList($lista);
		  	}else{

		  		$resultado['error'] = "Nenhum resultado encontrado.";
		  	}

		  	return $resultado;
		  }

		//RETORNA UMA LINHA ESPECÍFICA
		  public function getRow($campos, $cond){

		  	$sqlDetalhar = "SELECT {$campos} FROM ".self::TABLE."  {$cond}";

		  	$campos   = array("");
		  	$detalhar = $this->mysql->getRow($sqlDetalhar,$campos);

		  	if($detalhar){

		  		$resultado['success'] = $this->makeList($detalhar);
		  	}else{

		  		$resultado['error'] = "Usuário não encontrado.";
		  	}

		  	return $resultado;
		  }

		//RETORNA O ÚLTIMO ID INSERIDO NA TABELA
		  public function getLastId(){

		  	$sqlLastId = "SELECT id_plano FROM ".self::TABLE." ORDER BY id_plano DESC LIMIT 1";
		  	$dados     = array("");
		  	$lastId    = $this->mysql->getRow($sqlLastId,$dados);

		  	if(!empty($lastId)){
		  		$result = $lastId->id_plano;
		  	}else{
		  		$result = null;       
		  	}

		  	return $result;
		  }

		/**
        * TRATA A IMAGEM PARA SER INSERIDA NO BANCO E SALVA NA PASTA DE FOTOS DO EXEMPLOS
        * @return string
        */
		public function imagemUpload($imagem,$pasta){

			$resultado = "";

			if(is_array($imagem)){

				if($imagem != null){

					$extensoesPermitidas = array('.jpg','.png','.gif','.svg');

					//TRATA A IMAGEM PARA SER ARMAZENADA NO BANCO
					$ext        = strtolower(substr($imagem['name'],strrpos($imagem['name'],".")));
					$imagemNome = base64_encode($imagem['name'].rand(0,10000));

					$pasta = URL_PATH_UPLOAD.$pasta.'/';

					if(in_array($ext, $extensoesPermitidas)){

						$imagemNome = date('Y-m-d-H-i-s').$imagemNome;

						$upload     = new UploadArquivo($imagem, 300);

						// PASTA PADRÃO PARA SER ARMAZENADO O ARQUIVO
						$upload->setPath($pasta);

						// NOME QUE SERÁ SALVO O ARQUIVO NA PASTA
						$upload->setName($imagemNome);

						// REALIZA O UPLOAD
						$uploadArquivo = $upload->upload("", true);

						if($uploadArquivo['Success']){

							$resultado = $imagemNome.$ext;

						}else{

							$resultado = "";
						}

					}else{

						$resultado = "";
					}
				}
			}else{

				$resultado = $imagem;
			}

			return $resultado;
		}

		/**
		*  CRIA UMA LISTA DE OBJETOS ATRAVÉS DE UMA QUERY EXECUTADA
		*  @param query executada 
		*  @return array ou nulo
		*/
		public function makeList($list){

			if(!empty($list) && $list != null){

				if(count($list) > 0){

					$result = array();

					if(!is_object($list)){

						foreach($list as $row):

							array_push($result, $this->loadObjectsFromSql($row));

						endforeach;

					}else{

						$result = $this->loadObjectsFromSql($list);
					}
				}else{

					$result = null;
				}
			}else{

				$result = null;
			}

			return $result;
		}

		//TRANSFORMA OS RESULTADOS EM OBJETO
		public function loadObjectsFromSql($row){

			$plano = new Assinatura(isset($row->id_plano) ? $row->id_plano : '');
			$plano->setNome(isset($row->nome) ? $row->nome : '');
			$plano->setPreco(isset($row->preco) ? $row->preco : '');
			$plano->setDesconto(isset($row->desconto) ? $row->desconto : '');
			$plano->setGrau(isset($row->grau) ? $row->grau : '');
			$plano->setStatus(isset($row->status) ? $row->status : '');
			$plano->setCodigoPagseguro(isset($row->codigo_pagseguro) ? $row->codigo_pagseguro : '');

			return $plano;
		}
	}
	?>