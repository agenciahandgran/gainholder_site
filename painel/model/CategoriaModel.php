<?php

	/********************************
	 * MODEL DE CATEGORIAS
	 * ******************************/

	class CategoriaModel{

		const TABLE = "categorias";
		private $mysql;

		public function __construct(){

			$this->mysql = new Mysql();
		}

		//TRATA OS CONTEÚDOS PARA SEREM INSERIDOS NO BANCO
		public function insert($dados){

			if(is_array($dados)){

				$separador = fieldColumnSeparator($dados['categoria']);
				$sqlInsert = "INSERT INTO ".self::TABLE." (".$separador['fields'].") VALUES(:nome,:slug,:status)";
				$campos    = array(
					'nome'    => $dados['categoria']['nome'], 					
					'slug'    => $dados['categoria']['slug'], 
					'status'  => $dados['categoria']['status'],
				);
				$executar  = $this->mysql->execute($sqlInsert,$campos);

				if($executar){

					$result['success'] = "Categoria cadastrada com sucesso!";

				}else{

					$result['error']  = "Não foi possível realizar o cadastro.";
				}
			}else{

				$result['error'] = "Não foi possível realizar o cadastro.";
			}

			return $result;
		}

		//TRATA OS ADMINISTRADORES PARA SEREM EDITADOS E ATUALIZADOS NO BANCO
		public function update($dados,$cond){

	  		$fieldsEdit = updateSeparator($dados['categoria']);// RETORNA UMA QUERY DINAMICA
	  		$sqlEditar  = "UPDATE ".self::TABLE." SET ".$fieldsEdit['result']." WHERE id_categoria = ".$cond;
	  		$campos     = array("");
	  		$editarEst  = $this->mysql->execute($sqlEditar,$campos);

	  		if($editarEst){
		  		$resultado['success'] = "Atualizado com sucesso!";
		  	}else{

		  		$resultado['error']  = "Não foi possivel editar o conteúdo.";
		  	}

		  	return $resultado;
		  }

		//DELETA UM DADO ESPECÍFICO PELO ID
		  public function delete($id){

		  	if(is_numeric($id)){

		  		$sqlDelete = "DELETE FROM ".self::TABLE." WHERE id_categoria=:id";
		  		$campos    = array("id" => $id);
		  		$excluir   = $this->mysql->execute($sqlDelete,$campos);

		  		if($excluir){

		  			$resultado['success'] = "Categoria removida.";
		  		}else{

		  			$resultado['error'] = "Erro ao remover";
		  		} 
		  	}else{

		  		$resultado['error'] = "Erro ao remover";
		  	}

		  	return $resultado;
		  }

		//RECUPERA UM LISTA DE DADOS
		  public function getList($condicao){

		  	$sqlListar = " SELECT * FROM ".self::TABLE." ORDER BY nome ASC";

		  	$lista = $this->mysql->getList($sqlListar);

		  	if(!in_array(null, $lista)){

		  		$resultado = $this->makeList($lista);
		  	}else{

		  		$resultado['error'] = "Nenhum resultado encontrado.";
		  	}

		  	return $resultado;
		  }

		//RETORNA UMA LINHA ESPECÍFICA
		  public function getRow($campos, $cond){

		  	$sqlDetalhar = "SELECT {$campos} FROM ".self::TABLE." {$cond}";

		  	$campos   = array("");
		  	$detalhar = $this->mysql->getRow($sqlDetalhar,$campos);

		  	if($detalhar){

		  		$resultado['success'] = $this->makeList($detalhar);
		  	}else{

		  		$resultado['error'] = "Usuário não encontrado.";
		  	}

		  	return $resultado;
		  }

		//RETORNA O ÚLTIMO ID INSERIDO NA TABELA
		  public function getLastId(){

		  	$sqlLastId = "SELECT id_categoria FROM ".self::TABLE." ORDER BY id_categoria DESC LIMIT 1";
		  	$dados     = array("");
		  	$lastId    = $this->mysql->getRow($sqlLastId,$dados);

		  	if(!empty($lastId)){
		  		$result = $lastId->id_categoria;
		  	}else{
		  		$result = null;       
		  	}

		  	return $result;
		  }

		/**
        * TRATA A IMAGEM PARA SER INSERIDA NO BANCO E SALVA NA PASTA DE FOTOS DO EXEMPLOS
        * @return string
        */
		public function imagemUpload($imagem,$pasta){

			$resultado = "";

			if(is_array($imagem)){

				if($imagem != null){

					$extensoesPermitidas = array('.jpg','.png','.gif','.svg');

					//TRATA A IMAGEM PARA SER ARMAZENADA NO BANCO
					$ext        = strtolower(substr($imagem['name'],strrpos($imagem['name'],".")));
					$imagemNome = base64_encode($imagem['name'].rand(0,10000));

					$pasta = URL_PATH_UPLOAD.$pasta.'/';

					if(in_array($ext, $extensoesPermitidas)){

						$imagemNome = date('Y-m-d-H-i-s').$imagemNome;

						$upload     = new UploadArquivo($imagem, 300);

						// PASTA PADRÃO PARA SER ARMAZENADO O ARQUIVO
						$upload->setPath($pasta);

						// NOME QUE SERÁ SALVO O ARQUIVO NA PASTA
						$upload->setName($imagemNome);

						// REALIZA O UPLOAD
						$uploadArquivo = $upload->upload("", true);

						if($uploadArquivo['Success']){

							$resultado = $imagemNome.$ext;

						}else{

							$resultado = "";
						}

					}else{

						$resultado = "";
					}
				}
			}else{

				$resultado = $imagem;
			}

			return $resultado;
		}

		/**
		*  CRIA UMA LISTA DE OBJETOS ATRAVÉS DE UMA QUERY EXECUTADA
		*  @param query executada 
		*  @return array ou nulo
		*/
		public function makeList($list){

			if(!empty($list) && $list != null){

				if(count($list) > 0){

					$result = array();

					if(!is_object($list)){

						foreach($list as $row):

							array_push($result, $this->loadObjectsFromSql($row));

						endforeach;

					}else{

						$result = $this->loadObjectsFromSql($list);
					}
				}else{

					$result = null;
				}
			}else{

				$result = null;
			}

			return $result;
		}

		//TRANSFORMA OS RESULTADOS EM OBJETO
		public function loadObjectsFromSql($row){

			$categoria = new Categoria($row->id_categoria);
			$categoria->setNome(isset($row->nome) ? $row->nome : '');
			$categoria->setSlug(isset($row->slug) ? $row->slug : '');
			$categoria->setStatus(isset($row->status) ? $row->status : '');

			return $categoria;

		}
	}
	?>