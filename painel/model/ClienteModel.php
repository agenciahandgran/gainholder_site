<?php

	/********************************
	 * MODEL DE CLIENTES
	 * ******************************/

	class ClienteModel{

		const TABLE = "usuarios";
		private $mysql;

		public function __construct(){
			$this->mysql = new Mysql();
		}

		//TRATA OS DADOS PARA SEREM INSERIDOS NO BANCO
		public function insert($dados,$status=null){

			if(is_array($dados)){

				$separador = fieldColumnSeparator($dados['cliente']);
				$sqlInsert = "INSERT INTO ".self::TABLE." (".$separador['fields'].") VALUES(:id_login,:id_endereco,:tipo_legislacao,:nome_completo,:data_nascimento,:cpf,:cnpj,:rg,:telefone,:celular,:genero,:tipo,:hash)";
				
				$campos    = array(
								'id_login'        => $dados['cliente']['id_login'],
								'id_endereco'     => $dados['cliente']['id_endereco'],
								'tipo_legislacao' => $dados['cliente']['tipo_legislacao'],
								'nome_completo'   => $dados['cliente']['nome_completo'],
								'data_nascimento' => $dados['cliente']['data_nascimento'],
								'cpf'             => $dados['cliente']['cpf'],
								'cnpj'            => $dados['cliente']['cnpj'],
								'rg'              => $dados['cliente']['rg'],
								'telefone'        => $dados['cliente']['telefone'],
								'celular'         => $dados['cliente']['celular'],
								'genero'          => $dados['cliente']['genero'],
								'tipo'            => '3',
								'hash'            => $dados['cliente']['hash']
							);

				$executar  = $this->mysql->execute($sqlInsert,$campos);

				if($executar){

					// CADASTRO INFORMAÇÕES DE LOGIN
					$idCliente = $this->getLastId();

					$dadosLogin['login']['email']  = $dados['login']['email'];
					$dadosLogin['login']['senha']  = $dados['login']['senha'];
					$dadosLogin['login']['status'] = $dados['login']['status'];
					$dadosLogin['login']['tipo']   = '3';

					$administradorModel = new AdministradorModel();
					$inserirLogin       = $administradorModel->insertRow($dadosLogin);

					if(isset($inserirLogin['success'])){

						// ATUALIZA OS DADOS DO CLIENTE COM O ID DO LOGIN E HASH
						$hash = rand().$idCliente.$dados['login']['email'];

						$clienteAtualizacao['cliente']['id_login'] = $administradorModel->getLastId();
						$clienteAtualizacao['cliente']['hash']     = md5($hash);
						
						$atualizacao = $this->update($clienteAtualizacao,$idCliente);

						if($atualizacao){
							$result['success'] = "Dados cadastrados com sucesso!";
							$result['hash']    = $clienteAtualizacao['cliente']['hash'];
						}else{
							$result['error'] = "Erro ao atualizar!";
						}

					}else{

						$deleteLogin  = $this->delete($this->getLastId());

						if(isset($deleteLogin['success'])){

							$result['success'] = "Deletado Usuário por erro na operação!";
						}else{

							$result['error'] = "Erro ao remover login!";
						}
					}

				}else{

					$result['error']  = "Não foi possível realizar o cadastro. Verifique se as informações estão corretas.";
				}
			}else{

				$result['error'] = "Não foi possível realizar o cadastro. Verifique se as informações estão corretas.";
			}

			return $result;
		}

		//TRATA OS DADOS PARA SEREM EDITADOS E ATUALIZADOS NO BANCO
		public function update($dados,$cond){

			$fieldsEdit = updateSeparator($dados['cliente']);// RETORNA UMA QUERY DINAMICA
	        $sqlEditar  = "UPDATE ".self::TABLE." SET ".$fieldsEdit['result']." WHERE id_usuario = ".$cond;
	        $campos     = array("");

	        $editarEst  = $this->mysql->execute($sqlEditar,$campos);

	        if($editarEst){

				$resultado['success'] = "Atualizado com sucesso!";
	        
	        }else{

	        	$resultado['error']  = "Não foi possivel editar o usuário.";
	        }

	        return $resultado;
		}


		//TRATA OS DADOS PARA SEREM EDITADOS E ATUALIZADOS NO BANCO
		public function updateCliente($dados,$cond){

			$fieldsEdit = updateSeparator($dados['cliente']);// RETORNA UMA QUERY DINAMICA
	        $sqlEditar  = "UPDATE ".self::TABLE." SET ".$fieldsEdit['result']." WHERE id_login = ".$cond;
	        $campos     = array("");

	        $editarEst  = $this->mysql->execute($sqlEditar,$campos);

	        if($editarEst){

				$resultado['success'] = "Atualizado com sucesso!";
	        
	        }else{

	        	$resultado['error']  = "Não foi possivel editar o usuário.";
	        }

	        return $resultado;
		}

		//TRATA OS DADOS PARA SEREM EDITADOS E ATUALIZADOS NO BANCO
		public function updateLogin($dados,$cond){

			$fieldsEdit = updateSeparator($dados['login']);// RETORNA UMA QUERY DINAMICA
	        $sqlEditar  = "UPDATE login SET ".$fieldsEdit['result']." WHERE id_login = ".$cond;
	        $campos     = array("");

	        $editarEst  = $this->mysql->execute($sqlEditar,$campos);

	        if($editarEst){

				$resultado['success'] = "Atualizado com sucesso!";
				
	        }else{

	        	$resultado['error']  = "Não foi possivel editar o usuário.";
	        }

	        return $resultado;
		}

		//DELETA UM DADO ESPECÍFICO PELO ID
		public function delete($id){

			if(is_numeric($id)){

				$sqlDelete = "DELETE FROM ".self::TABLE." WHERE id_usuario=:id";
				$campos    = array("id_usuario" => $id);
				$excluir   = $this->mysql->execute($sqlDelete,$campos);

				if($excluir){

					$resultado['success'] = "Usuário removido.";
				}else{

					$resultado['error'] = "Não foi possível excluir esse usuário";
				}
			}else{

				$resultado['error'] = "Não foi possível excluir esse usuário";
			}

			return $resultado;
		}

		//RECEBE UMA LISTA COM TODOS OS REGISTROS
		public function getList($campos, $condicao){

			$sqlListar = "SELECT {$campos} FROM ".self::TABLE." u
						 INNER JOIN login l ON (l.id_login = u.id_login)
						 LEFT JOIN enderecos e ON (e.id_endereco = u.id_endereco)
						 {$condicao} ORDER BY u.nome_completo ASC ";

			$lista = $this->mysql->getList($sqlListar);

			if(!in_array(null, $lista)){
				$resultado = $this->makeList($lista);
			}else{
				$resultado['error'] = "Nenhum resultado encontrado.";
			}

			return $resultado;
		}

		//RECEBE UM REGISTRO ESPECÍFICO PELO ID
		public function getRow($campos, $cond){

			$sqlDetalhar = "SELECT {$campos} FROM ".self::TABLE." u 
							INNER JOIN login l ON (l.id_login = u.id_login) 
							LEFT JOIN enderecos e ON (e.id_endereco = u.id_endereco)
							{$cond}";
							
			$campos   = array("");
			$detalhar = $this->mysql->getRow($sqlDetalhar,$campos);

			if($detalhar){

				$resultado['success'] = $this->makeList($detalhar);
			}else{

				$resultado['error'] = "Usuário não encontrado.";
			}

			return $resultado;
		}

		//RETORNA O ÚLTIMO ID INSERIDO NA TABELA
		public function getLastId(){

			$sqlLastId = "SELECT id_usuario FROM " .self::TABLE." ORDER BY id_usuario DESC LIMIT 1";
			$dados     = array("");
			$lastId    = $this->mysql->getRow($sqlLastId,$dados);

			if(!empty($lastId)){

				$result = $lastId->id_usuario;
			}else{

				$result = null;
			}

			return $result;
		}

		/**
		*  CRIA UMA LISTA DE OBJETOS ATRAVÉS DE UMA QUERY EXECUTADA
		*  @param query executada 
		*  @return array ou nulo
		*/
		public function makeList($list){

			if(!empty($list) && $list != null){

				if(count($list) > 0){

					$result = array();
					if(!is_object($list)){

						foreach($list as $row):

						    array_push($result, $this->loadObjectsFromSql($row));

						endforeach;

					}else{

						$result = $this->loadObjectsFromSql($list);
					}
				}else{

					$result = null;
				}
			}else{

				$result = null;
			}

			return $result;
		}

		/**
        * TRATA A IMAGEM PARA SER INSERIDA NO BANCO E SALVA NA PASTA DE FOTOS DO EXEMPLOS
        * @return string
        */
		public function imagemUpload($imagem,$pasta){

			$resultado = "";

			if(is_array($imagem)){

				if($imagem != null){

					$extensoesPermitidas = array('.jpg','.png','.gif','.svg');

					//TRATA A IMAGEM PARA SER ARMAZENADA NO BANCO
					$ext        = strtolower(substr($imagem['name'],strrpos($imagem['name'],".")));
					$imagemNome = base64_encode($imagem['name'].rand(0,10000));

					$pasta = "../".URL_PATH_UPLOAD.$pasta.'/';

					if(in_array($ext, $extensoesPermitidas)){

						$imagemNome = date('Y-m-d-H-i-s').$imagemNome;

						$upload = new UploadArquivo($imagem, 300);

						// PASTA PADRÃO PARA SER ARMAZENADO O ARQUIVO
						$upload->setPath($pasta);

						// NOME QUE SERÁ SALVO O ARQUIVO NA PASTA
						$upload->setName($imagemNome);

						// REALIZA O UPLOAD
						$uploadArquivo = $upload->upload("", true);

						if($uploadArquivo['Success']){

							$resultado = $imagemNome.$ext;

						}else{

							$resultado = "";
						}

					}else{

						$resultado = "";
					}
				}
			}else{

				$resultado = $imagem;
			}

			return $resultado;
		}

		//TRANSFORMA OS RESULTADOS EM OBJETO
		public function loadObjectsFromSql($row){

			$usuario = new Usuario(isset($row->id_usuario) ? $row->id_usuario : -1);
			$usuario->setNivel(isset($row->tipo) ? $row->tipo : '');
			$usuario->setTipoLegislacao(isset($row->tipo_legislacao) ? $row->tipo_legislacao : '');
			$usuario->setNome(isset($row->nome_completo) ? $row->nome_completo : '');
			$usuario->setSexo(isset($row->genero) ? $row->genero : '');
			$usuario->setTelefone(isset($row->telefone) ? $row->telefone : '');
			$usuario->setCelular(isset($row->celular) ? $row->celular : '');
			$usuario->setDataNascimento(isset($row->data_nascimento) ? $row->data_nascimento : '');
			$usuario->setCpf(isset($row->cpf) ? $row->cpf : '');
			$usuario->setCnpj(isset($row->cnpj) ? $row->cnpj : '');
			$usuario->setRg(isset($row->rg) ? $row->rg : '');

			$login = new Login(isset($row->id_login) ? $row->id_login : -1);

			if(isset($row->email)){

				$login->setEmail($row->email);
			}

			if(isset($row->senha)){

				$login->setSenha($row->senha);
			}

			if(isset($row->status)){

				$login->setStatus($row->status);
			}

			$usuario->setLogin($login);

			$endereco = new Endereco(isset($row->id_endereco) ? $row->id_endereco : 0);

			if(isset($row->cep)){
				$endereco->setCep($row->cep);
			}

			if(isset($row->endereco)){
				$endereco->setEndereco($row->endereco);
			}

			if(isset($row->numero)){
				$endereco->setNumero($row->numero);
			}

			if(isset($row->bairro)){
				$endereco->setBairro($row->bairro);
			}

			if(isset($row->cidade)){
				$endereco->setCidade($row->cidade);
			}

			if(isset($row->uf)){
				$endereco->setUf($row->uf);
			}

			if(isset($row->complemento)){
				$endereco->setComplemento($row->complemento);
			}

			$usuario->setEndereco($endereco);

			return $usuario;
		}
	}
?>