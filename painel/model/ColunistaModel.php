<?php

	/********************************
	 * MODEL DE COLUNISTAS
	 * ******************************/

	class ColunistaModel{

		const TABLE = "login";
		private $mysql;

		public function __construct(){

			$this->mysql = new Mysql();
		}

		//TRATA OS ADMINISTRADORES PARA SEREM INSERIDOS NO BANCO
		public function insert($dados){

			if(is_array($dados)){

				$separador = fieldColumnSeparator($dados['login']);
				$sqlInsert = "INSERT INTO ".self::TABLE." (".$separador['fields'].") VALUES(:email,:senha,:status,:tipo)";
				$campos    = array('email'  => $dados['login']['email'], 
					'senha'  => $dados['login']['senha'], 
					'status' => $dados['login']['status'],
					'tipo'   => $dados['login']['tipo']
				);
				$executar  = $this->mysql->execute($sqlInsert,$campos);

				if($executar){

					$idLogin = $this->getLastId();

					$dados['colunista']['id_login'] = $idLogin;

					$separador 			= fieldColumnSeparator($dados['colunista']);
					$sqlInsertColunista = "INSERT INTO colunistas (".$separador['fields'].") VALUES(:nome_completo,:foto_perfil,:status,:descricao,:id_login)";
					
					$campos    = array( 
						'nome_completo'  => $dados['colunista']['nome_completo'], 
						'foto_perfil'    => $dados['colunista']['foto_perfil'],
						'status'         => $dados['colunista']['status'],
						'descricao'      => $dados['colunista']['descricao'],
						'id_login'       => $dados['colunista']['id_login']
					);
					$executar  = $this->mysql->execute($sqlInsertColunista,$campos);

					if($executar){

						if(isset($dados['modulos'])){

							for($i=0; $i<count($dados['modulos']); $i++){

								$sqlInsertPermissoes = "INSERT INTO permissoes (id_login, id_modulo, status) VALUES(:id_login,:id_modulo,:status)";
								
								$campos    = array('id_login'  => $idLogin, 
									'id_modulo' => $dados['modulos'][$i], 
									'status'    => '1'
								);
								$executar  = $this->mysql->execute($sqlInsertPermissoes,$campos);
							}
						}

						$result['success'] = "Colunista cadastrado com sucesso!";

					}else{
						$result['error']  = "Não foi possível realizar o cadastro.";
					}

				}else{

					$result['error']  = "Não foi possível realizar o cadastro.";
				}
			}else{

				$result['error'] = "Não foi possível realizar o cadastro.";
			}

			return $result;
		}

		// INSERE UMA UNICA LINHA ESPECÍFICA
		public function insertRow($dados){

			if(is_array($dados)){

				$separador = fieldColumnSeparator($dados['login']);
				$sqlInsert = "INSERT INTO ".self::TABLE." (".$separador['fields'].") VALUES(:email,:senha,:status,:usuarioId,:permissoes)";
				$campos    = array(
					'usuarioId'  => $dados['login']['usuarioId'],
					'email'      => $dados['login']['email'], 
					'senha'      => $dados['login']['senha'], 
					'status'     => $dados['login']['status'], 
					'permissoes' => $dados['login']['permissoes']
				);

				$executar  = $this->mysql->execute($sqlInsert,$campos);

				if($executar){

					$result['success'] = "Usuário cadastrado com sucesso!";
				}else{

					$result['error']  = "Não foi possível realizar o cadastro.";
				}
			}else{

				$result['error'] = "Não foi possível realizar o cadastro.";
			}

			return $result;
		}

		//TRATA OS ADMINISTRADORES PARA SEREM EDITADOS E ATUALIZADOS NO BANCO
		public function update($dados,$cond){

	  		$fieldsEdit = updateSeparator($dados['login']);// RETORNA UMA QUERY DINAMICA
	  		$sqlEditar  = "UPDATE ".self::TABLE." SET ".$fieldsEdit['result']." WHERE id_login = ".$cond;
	  		$campos     = array("");
	  		$editarEst  = $this->mysql->execute($sqlEditar,$campos);

	  		if($editarEst){

		  		$fieldsEditAdmin = updateSeparator($dados['colunista']);// RETORNA UMA QUERY DINAMICA
		  		$sqlEditarAdmin  = "UPDATE colunistas SET ".$fieldsEditAdmin['result']." WHERE id_login = ".$cond;

		  		$campos     	 = array("");
		  		$editarEst  	 = $this->mysql->execute($sqlEditarAdmin,$campos);

		  		if(isset($dados['modulos'])){

		  			$sqlDelete = "DELETE FROM permissoes WHERE id_login =:id";
		  			$campos    = array("id" => $cond);
		  			$excluir   = $this->mysql->execute($sqlDelete,$campos);	

		  			for($i=0; $i<count($dados['modulos']); $i++){

		  				$sqlInsertPermissoes = "INSERT INTO permissoes (id_login, id_modulo, status) VALUES(:id_login,:id_modulo,:status)";

		  				$campos    = array('id_login'  => $cond, 
		  					'id_modulo' => $dados['modulos'][$i], 
		  					'status'    => '1'
		  				);
		  				$executar  = $this->mysql->execute($sqlInsertPermissoes,$campos);
		  			}

		  		}else{

		  			$sqlDelete = "DELETE FROM permissoes WHERE id_login =:id";
		  			$campos    = array("id" => $cond);
		  			$excluir   = $this->mysql->execute($sqlDelete,$campos);	
		  		}

		  		$resultado['success'] = "Atualizado com sucesso!";
		  	}else{

		  		$resultado['error']  = "Não foi possivel editar o colunista.";
		  	}

		  	return $resultado;
		  }

		//DELETA UM DADO ESPECÍFICO PELO ID
		  public function delete($id){

		  	if(is_numeric($id)){

		  		$sqlDelete = "DELETE FROM ".self::TABLE." WHERE id_login=:id";
		  		$campos    = array("id" => $id);
		  		$excluir   = $this->mysql->execute($sqlDelete,$campos);

		  		if($excluir){

		  			$resultado['success'] = "Colunista removido.";
		  		}else{

		  			$resultado['error'] = "Erro ao remover";
		  		} 
		  	}else{

		  		$resultado['error'] = "Erro ao remover";
		  	}

		  	return $resultado;
		  }

		//RECUPERA UM LISTA DE DADOS
		  public function getList($condicao){

		  	$sqlListar = " SELECT l.id_login ,  l.email, l.senha, l.status, l.tipo, c.nome_completo, c.id as idColunista, c.descricao
		  	FROM ".self::TABLE." l 
		  	INNER JOIN colunistas c ON l.id_login = c.id_login 
		  	WHERE l.tipo =  2 
		  	ORDER BY c.nome_completo ASC ";

		  	$lista = $this->mysql->getList($sqlListar);

		  	if(!in_array(null, $lista)){

		  		$resultado = $this->makeList($lista);
		  	}else{

		  		$resultado['error'] = "Nenhum resultado encontrado.";
		  	}

		  	return $resultado;
		  }

		//RETORNA UMA LINHA ESPECÍFICA
		  public function getRow($campos, $cond){

		  	$sqlDetalhar = "SELECT {$campos} FROM ".self::TABLE." l 
		  	INNER JOIN colunistas c ON l.id_login = c.id_login  {$cond}";

		  	$campos   = array("");
		  	$detalhar = $this->mysql->getRow($sqlDetalhar,$campos);

		  	if($detalhar){

		  		$resultado['success'] = $this->makeList($detalhar);
		  	}else{

		  		$resultado['error'] = "Usuário não encontrado.";
		  	}

		  	return $resultado;
		  }

		// LISTAGEM DE CONTEÚDOS DE ACORDO COM A CONDIÇÃO PASSADA
		  public function getListColunistaByCondition($condicao){
		  	$sqlListar = "  SELECT l.id_login ,  l.email, l.senha, l.status, l.tipo, c.nome_completo, c.id as idColunista, c.descricao, c.foto_perfil, a.id_conteudo, a.resumo, a.slug
		  	FROM ".self::TABLE." l 
		  	INNER JOIN colunistas c ON l.id_login = c.id_login 
		  	INNER JOIN conteudos a ON a.id_autor = l.id_login
		  	$condicao";

		  	echo $sqlListar;

		  	$lista = $this->mysql->getList($sqlListar);

		  	if(!in_array(null, $lista)){

		  		$resultado = $this->makeList($lista);
		  	}else{

		  		$resultado['error'] = "Nenhum resultado encontrado.";
		  	}

		  	return $resultado;	
		  }		

		  public function getListColuinstaByRand(){
		  	$sqlListar = "  SELECT c.resumo, c.slug, c.data_publicacao, cl.id_login, cl.foto_perfil, cl.nome_completo 
		  					FROM conteudos  c
						  	INNER JOIN colunistas cl ON cl.id_login = c.id_autor
						  	WHERE c.tipo = 2
						  	ORDER BY RAND() LIMIT 4";

		  	$lista = $this->mysql->getList($sqlListar);

		  	if(!in_array(null, $lista)){

		  		$resultado = $this->makeList($lista);
		  	}else{

		  		$resultado['error'] = "Nenhum resultado encontrado.";
		  	}

		  	return $resultado;	
		  }

		//RETORNA O ÚLTIMO ID INSERIDO NA TABELA
		  public function getLastId(){

		  	$sqlLastId = "SELECT id_login FROM ".self::TABLE." ORDER BY id_login DESC LIMIT 1";
		  	$dados     = array("");
		  	$lastId    = $this->mysql->getRow($sqlLastId,$dados);

		  	if(!empty($lastId)){
		  		$result = $lastId->id_login;
		  	}else{
		  		$result = null;       
		  	}

		  	return $result;
		  }

		/**
        * TRATA A IMAGEM PARA SER INSERIDA NO BANCO E SALVA NA PASTA DE FOTOS DO EXEMPLOS
        * @return string
        */
		public function imagemUpload($imagem,$pasta){

			$resultado = "";

			if(is_array($imagem)){

				if($imagem != null){

					$extensoesPermitidas = array('.jpg','.png','.gif','.svg');

					//TRATA A IMAGEM PARA SER ARMAZENADA NO BANCO
					$ext        = strtolower(substr($imagem['name'],strrpos($imagem['name'],".")));
					$imagemNome = base64_encode($imagem['name'].rand(0,10000));

					$pasta = URL_PATH_UPLOAD.$pasta.'/';

					if(in_array($ext, $extensoesPermitidas)){

						$imagemNome = date('Y-m-d-H-i-s').$imagemNome;

						$upload     = new UploadArquivo($imagem, 300);

						// PASTA PADRÃO PARA SER ARMAZENADO O ARQUIVO
						$upload->setPath($pasta);

						// NOME QUE SERÁ SALVO O ARQUIVO NA PASTA
						$upload->setName($imagemNome);

						// REALIZA O UPLOAD
						$uploadArquivo = $upload->upload("", true);

						if($uploadArquivo['Success']){

							$resultado = $imagemNome.$ext;

						}else{

							$resultado = "";
						}

					}else{

						$resultado = "";
					}
				}
			}else{

				$resultado = $imagem;
			}

			return $resultado;
		}

		/**
		*  CRIA UMA LISTA DE OBJETOS ATRAVÉS DE UMA QUERY EXECUTADA
		*  @param query executada 
		*  @return array ou nulo
		*/
		public function makeList($list){

			if(!empty($list) && $list != null){

				if(count($list) > 0){

					$result = array();

					if(!is_object($list)){

						foreach($list as $row):

							array_push($result, $this->loadObjectsFromSql($row));

						endforeach;

					}else{

						$result = $this->loadObjectsFromSql($list);
					}
				}else{

					$result = null;
				}
			}else{

				$result = null;
			}

			return $result;
		}

		//TRANSFORMA OS RESULTADOS EM OBJETO
		public function loadObjectsFromSql($row){

			$login = new Login(isset($row->id_login) ? $row->id_login : null);
			$login->setEmail(isset($row->email) ? $row->email : null);
			$login->setStatus(isset($row->status) ? $row->status : null);
			$login->setTipo(isset($row->tipo) ? $row->tipo : null);

			$colunista = new Colunista();

			if(isset($row->idColunista)){
				$colunista->setId($row->idColunista);
			}

			if(isset($row->nome_completo)){
				$colunista->setNomeCompleto($row->nome_completo);
			}

			if(isset($row->foto_perfil)){
				$colunista->setFotoPerfil($row->foto_perfil);
			}

			if(isset($row->descricao)){
				$colunista->setDescricao($row->descricao);
			}

			$conteudo = new Conteudo();

			if(isset($row->id_conteudo)){
				$conteudo->setId($row->id_conteudo);
			}

			if(isset($row->resumo)){
				$conteudo->setResumo($row->resumo);
			}

			if(isset($row->slug)){
				$conteudo->setSlug($row->slug);
			}

			if(isset($row->data_publicacao)){
				$conteudo->setDataPublicacao($row->data_publicacao);
			}

			$colunista->setArtigo($conteudo);

			$login->setUsuario($colunista);

			return $login;
		}
	}
	?>