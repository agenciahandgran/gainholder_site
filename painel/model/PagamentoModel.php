<?php

	/********************************
	 * MODEL DE PAGAMENTOS
	 * ******************************/

	class PagamentoModel{

		const TABLE = "pagamentos";
		private $mysql;

		public function __construct(){

			$this->mysql = new Mysql();
		}

		//TRATA OS CONTEÚDOS PARA SEREM INSERIDOS NO BANCO
		public function insert($dados){

			if(is_array($dados)){

				$separador = fieldColumnSeparator($dados['pagamento']);
				$sqlInsert = "INSERT INTO ".self::TABLE." (".$separador['fields'].") VALUES(:codigo,:id_cliente,:id_plano,:valor,:periodo,:status)";

				$campos    = array(
					'codigo'      => $dados['pagamento']['codigo'],
					'id_cliente'  => $dados['pagamento']['id_cliente'],
					'id_plano'    => $dados['pagamento']['id_plano'],
					'valor'       => formatReal($dados['pagamento']['valor'],"."),
					'periodo'     => $dados['pagamento']['periodo'],
					'status'      => $dados['pagamento']['status']
				);

				$executar  = $this->mysql->execute($sqlInsert,$campos);

				if($executar){

					$result['success'] = "Pagamento cadastrado com sucesso!";

				}else{

					$result['error']  = "Não foi possível realizar o cadastro.";
				}
			}else{

				$result['error'] = "Não foi possível realizar o cadastro.";
			}

			return $result;
		}

		//TRATA OS ADMINISTRADORES PARA SEREM EDITADOS E ATUALIZADOS NO BANCO
		public function update($dados,$cond){

	  		$fieldsEdit = updateSeparator($dados['pagamento']);// RETORNA UMA QUERY DINAMICA
	  		$sqlEditar  = "UPDATE ".self::TABLE." SET ".$fieldsEdit['result']." WHERE id_pagamento = ".$cond;
	  		$campos     = array("");
	  		$editarEst  = $this->mysql->execute($sqlEditar,$campos);

	  		if($editarEst){
		  		$resultado['success'] = "Atualizado com sucesso!";
		  	}else{

		  		$resultado['error']  = "Não foi possivel editar o pagamento.";
		  	}

		  	return $resultado;
		  }

		//DELETA UM DADO ESPECÍFICO PELO ID
		  public function delete($id){

		  	if(is_numeric($id)){

		  		$sqlDelete = "DELETE FROM ".self::TABLE." WHERE id_pagamento=:id";
		  		$campos    = array("id" => $id);
		  		$excluir   = $this->mysql->execute($sqlDelete,$campos);

		  		if($excluir){

		  			$resultado['success'] = "Conteúdo removido.";
		  		}else{

		  			$resultado['error'] = "Erro ao remover";
		  		} 
		  	}else{

		  		$resultado['error'] = "Erro ao remover";
		  	}

		  	return $resultado;
		  }

		//RECUPERA UM LISTA DE DADOS
		  public function getList($condicao){

		  	$sqlListar = " SELECT * FROM ".self::TABLE." {$condicao} ORDER BY periodo ASC";

		  	$lista = $this->mysql->getList($sqlListar);

		  	if(!in_array(null, $lista)){

		  		$resultado = $this->makeList($lista);
		  	}else{

		  		$resultado['error'] = "Nenhum resultado encontrado.";
		  	}

		  	return $resultado;
		  }

		//RETORNA UMA LINHA ESPECÍFICA
		  public function getRow($campos, $cond){

		  	$sqlDetalhar = "SELECT {$campos} FROM ".self::TABLE."  {$cond}";

		  	$campos   = array("");
		  	$detalhar = $this->mysql->getRow($sqlDetalhar,$campos);

		  	if($detalhar){

		  		$resultado['success'] = $this->makeList($detalhar);
		  	}else{

		  		$resultado['error'] = "Usuário não encontrado.";
		  	}

		  	return $resultado;
		  }

		//RETORNA O ÚLTIMO ID INSERIDO NA TABELA
		  public function getLastId(){

		  	$sqlLastId = "SELECT id_pagamento FROM ".self::TABLE." ORDER BY id_pagamento DESC LIMIT 1";
		  	$dados     = array("");
		  	$lastId    = $this->mysql->getRow($sqlLastId,$dados);

		  	if(!empty($lastId)){
		  		$result = $lastId->id_plano;
		  	}else{
		  		$result = null;       
		  	}

		  	return $result;
		  }

		/**
		*  CRIA UMA LISTA DE OBJETOS ATRAVÉS DE UMA QUERY EXECUTADA
		*  @param query executada 
		*  @return array ou nulo
		*/
		public function makeList($list){

			if(!empty($list) && $list != null){

				if(count($list) > 0){

					$result = array();

					if(!is_object($list)){

						foreach($list as $row):

							array_push($result, $this->loadObjectsFromSql($row));

						endforeach;

					}else{

						$result = $this->loadObjectsFromSql($list);
					}
				}else{

					$result = null;
				}
			}else{

				$result = null;
			}

			return $result;
		}

		//TRANSFORMA OS RESULTADOS EM OBJETO
		public function loadObjectsFromSql($row){

			$pagamento = new Pagamento(isset($row->id_pagamento) ? $row->id_pagamento : '');
			$pagamento->setCodigo(isset($row->codigo) ? $row->codigo : '');

			$cliente = new Usuario();

			if(isset($row->id_cliente)){
				$cliente->setId($row->id_cliente);
			}

			$pagamento->setCliente($cliente);

			$plano = new Assinatura();

			if(isset($row->id_plano)){
				$plano->setId($row->id_plano);
			}

			if(isset($row->nomePlano)){
				$plano->setNome($row->nomePlano);
			}

			$pagamento->setPlano($plano);

			$pagamento->setValor(isset($row->valor) ? $row->valor : '');
			$pagamento->setPeriodo(isset($row->periodo) ? $row->periodo : '');
			$pagamento->setStatus(isset($row->status) ? $row->status : '');

			return $pagamento;
		}
	}
	?>