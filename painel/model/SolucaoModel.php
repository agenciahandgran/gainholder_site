<?php

	/********************************
	 * MODEL DE SOLUÇÕES
	 * ******************************/

	class SolucaoModel{

		const TABLE = "solucoes";
		private $mysql;

		public function __construct(){

			$this->mysql = new Mysql();
		}

		//TRATA OS CONTEÚDOS PARA SEREM INSERIDOS NO BANCO
		public function insert($dados){

			if(is_array($dados)){

				$separador = fieldColumnSeparator($dados['solucao']);
				$sqlInsert = "INSERT INTO ".self::TABLE." (".$separador['fields'].") VALUES(:banner,:banner_titulo,:banner_descricao,:id_categoria,:titulo,:texto,:status,:tipo)";

				$campos    = array(
					'banner'           => $dados['solucao']['banner'], 
					'banner_titulo'    => $dados['solucao']['banner_titulo'],
					'banner_descricao' => $dados['solucao']['banner_descricao'],
					'id_categoria'     => $dados['solucao']['id_categoria'], 
					'titulo'           => $dados['solucao']['titulo'],
					'texto'  		   => $dados['solucao']['texto'],
					'status' 		   => $dados['solucao']['status'],
					'tipo'     		   => $dados['solucao']['tipo']
				);

				$executar  = $this->mysql->execute($sqlInsert,$campos);

				if($executar){

					$result['success'] = "Solução cadastrada com sucesso!";

				}else{

					$result['error']  = "Não foi possível realizar o cadastro.";
				}
			}else{

				$result['error'] = "Não foi possível realizar o cadastro.";
			}

			return $result;
		}

		// INSERE UMA UNICA LINHA ESPECÍFICA
		// public function insertRow($dados){

		// 	if(is_array($dados)){

		// 		$separador = fieldColumnSeparator($dados['login']);
		// 		$sqlInsert = "INSERT INTO ".self::TABLE." (".$separador['fields'].") VALUES(:email,:senha,:status,:usuarioId,:permissoes)";
		// 		$campos    = array(
		// 							'usuarioId'  => $dados['login']['usuarioId'],
		// 							'email'      => $dados['login']['email'], 
		// 							'senha'      => $dados['login']['senha'], 
		// 							'status'     => $dados['login']['status'], 
		// 							'permissoes' => $dados['login']['permissoes']
		// 						);

		// 		$executar  = $this->mysql->execute($sqlInsert,$campos);

		// 		if($executar){

		// 			$result['success'] = "Usuário cadastrado com sucesso!";
		// 		}else{

		// 			$result['error']  = "Não foi possível realizar o cadastro.";
		// 		}
		// 	}else{

		// 		$result['error'] = "Não foi possível realizar o cadastro.";
		// 	}

		// 	return $result;
		// }

		//TRATA OS ADMINISTRADORES PARA SEREM EDITADOS E ATUALIZADOS NO BANCO
		public function update($dados,$cond){

	  		$fieldsEdit = updateSeparator($dados['solucao']);// RETORNA UMA QUERY DINAMICA
	  		$sqlEditar  = "UPDATE ".self::TABLE." SET ".$fieldsEdit['result']." WHERE id_solucao = ".$cond;
	  		$campos     = array("");
	  		$editarEst  = $this->mysql->execute($sqlEditar,$campos);

	  		if($editarEst){
		  		$resultado['success'] = "Atualizado com sucesso!";
		  	}else{

		  		$resultado['error']  = "Não foi possivel editar o conteúdo.";
		  	}

		  	return $resultado;
		  }

		//DELETA UM DADO ESPECÍFICO PELO ID
		  public function delete($id){

		  	if(is_numeric($id)){

		  		$sqlDelete = "DELETE FROM ".self::TABLE." WHERE id_solucao =:id";
		  		$campos    = array("id" => $id);
		  		$excluir   = $this->mysql->execute($sqlDelete,$campos);

		  		if($excluir){

		  			$resultado['success'] = "Solução removida.";
		  		}else{

		  			$resultado['error'] = "Erro ao remover";
		  		} 
		  	}else{

		  		$resultado['error'] = "Erro ao remover";
		  	}

		  	return $resultado;
		  }

		//RECUPERA UM LISTA DE DADOS
		  public function getList($condicao){

		  	$sqlListar = " SELECT s.*, ctg.nome as nomeCategoria, ctg.id_categoria as idCategoria 
		  				   FROM ".self::TABLE." s 
		  				   INNER JOIN categorias ctg ON ctg.id_categoria = s.id_categoria
		  				   ORDER BY s.titulo ASC";

		  	$lista = $this->mysql->getList($sqlListar);

		  	if(!in_array(null, $lista)){

		  		$resultado = $this->makeList($lista);
		  	}else{

		  		$resultado['error'] = "Nenhum resultado encontrado.";
		  	}

		  	return $resultado;
		  }

		//RETORNA UMA LINHA ESPECÍFICA
		  public function getRow($campos, $cond){

		  	$sqlDetalhar = "SELECT {$campos} FROM ".self::TABLE." s
		  	INNER JOIN categorias ctg ON ctg.id_categoria = s.id_categoria {$cond}";

		  	$campos   = array("");
		  	$detalhar = $this->mysql->getRow($sqlDetalhar,$campos);

		  	if($detalhar){

		  		$resultado['success'] = $this->makeList($detalhar);
		  	}else{

		  		$resultado['error'] = "Solução não encontrada.";
		  	}

		  	return $resultado;
		  }

		//RETORNA O ÚLTIMO ID INSERIDO NA TABELA
		  public function getLastId(){

		  	$sqlLastId = "SELECT id_solucao FROM ".self::TABLE." WHERE tipo = 1 ORDER BY id_solucao DESC LIMIT 1";
		  	$dados     = array("");
		  	$lastId    = $this->mysql->getRow($sqlLastId,$dados);

		  	if(!empty($lastId)){
		  		$result = $lastId->id_login;
		  	}else{
		  		$result = null;       
		  	}

		  	return $result;
		  }

		/**
        * TRATA A IMAGEM PARA SER INSERIDA NO BANCO E SALVA NA PASTA DE FOTOS DO EXEMPLOS
        * @return string
        */
		public function imagemUpload($imagem,$pasta){

			$resultado = "";

			if(is_array($imagem)){

				if($imagem != null){

					$extensoesPermitidas = array('.jpg','.png','.gif','.svg');

					//TRATA A IMAGEM PARA SER ARMAZENADA NO BANCO
					$ext        = strtolower(substr($imagem['name'],strrpos($imagem['name'],".")));
					$imagemNome = base64_encode($imagem['name'].rand(0,10000));

					$pasta = URL_PATH_UPLOAD.$pasta.'/';

					if(in_array($ext, $extensoesPermitidas)){

						$imagemNome = date('Y-m-d-H-i-s').$imagemNome;

						$upload     = new UploadArquivo($imagem, 300);

						// PASTA PADRÃO PARA SER ARMAZENADO O ARQUIVO
						$upload->setPath($pasta);

						// NOME QUE SERÁ SALVO O ARQUIVO NA PASTA
						$upload->setName($imagemNome);

						// REALIZA O UPLOAD
						$uploadArquivo = $upload->upload("", true);

						if($uploadArquivo['Success']){

							$resultado = $imagemNome.$ext;

						}else{

							$resultado = "";
						}

					}else{

						$resultado = "";
					}
				}
			}else{

				$resultado = $imagem;
			}

			return $resultado;
		}

		/**
		*  CRIA UMA LISTA DE OBJETOS ATRAVÉS DE UMA QUERY EXECUTADA
		*  @param query executada 
		*  @return array ou nulo
		*/
		public function makeList($list){

			if(!empty($list) && $list != null){

				if(count($list) > 0){

					$result = array();

					if(!is_object($list)){

						foreach($list as $row):

							array_push($result, $this->loadObjectsFromSql($row));

						endforeach;

					}else{

						$result = $this->loadObjectsFromSql($list);
					}
				}else{

					$result = null;
				}
			}else{

				$result = null;
			}

			return $result;
		}

		//TRANSFORMA OS RESULTADOS EM OBJETO
		public function loadObjectsFromSql($row){

			$solucao = new Solucao($row->id_solucao);
			$solucao->setTitulo(isset($row->titulo) ? $row->titulo : '');

			$categoria = new Categoria();

			if(isset($row->idCategoria)){
				$categoria->setId($row->idCategoria);
			}

			if(isset($row->nomeCategoria)){
				$categoria->setNome($row->nomeCategoria);
			}

			$solucao->setCategoria($categoria);

			$solucao->setBanner(isset($row->banner) ? $row->banner : '');
			$solucao->setBannerTitulo(isset($row->banner_titulo) ? $row->banner_titulo : '');
			$solucao->setBannerDescricao(isset($row->banner_descricao) ? $row->banner_descricao : '');
			$solucao->setTexto(isset($row->texto) ? $row->texto : '');
			$solucao->setStatus(isset($row->status) ? $row->status : '');
			$solucao->setTipo(isset($row->tipo) ? $row->tipo : '');

			return $solucao;

		}
	}
	?>