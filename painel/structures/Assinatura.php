<?php  

/*******************************************************
* GETERS E SETERS - TABELA {PLANOS}
*********************************************************/
class Assinatura{

	private $id_plano;
	private $nome;
	private $preco;
	private $desconto;
	private $grau;
	private $status;
	private $codigoPagseguro;

	public function __construct($id=-1){
		$this->id_plano = $id;
	}

	public function setId($id){
		$this->id_plano = $id;
	}

	public function getId(){
		return $this->id_plano;
	}

	public function setNome($nome){
		$this->nome = $nome;
	}

	public function getNome(){
		return $this->nome;
	}

	public function setPreco($preco){
		$this->preco = $preco;
	}

	public function getPreco(){
		return $this->preco;
	}

	public function setDesconto($desconto){
		$this->desconto = $desconto;
	}

	public function getDesconto(){
		return $this->desconto;
	}

	public function setGrau($grau){
		$this->grau = $grau;
	}

	public function getGrau(){
		return $this->grau;
	}

	public function setStatus($status){
		$this->status = $status;
	}

	public function getStatus(){
		return $this->status;
	}

	public function setCodigoPagseguro($codigoPagseguro){
		$this->codigoPagseguro = $codigoPagseguro;
	}

	public function getCodigoPagseguro(){
		return $this->codigoPagseguro;
	}
}


?>