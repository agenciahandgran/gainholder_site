<?php  

/*******************************************************
* GETERS E SETERS - TABELA {BANNERS}
*********************************************************/

class Banner{


	private $id;
	private $titulo;
	private $imagem;
	private $descricao;
	private $link;
	private $posicao;
	private $ordem;
	private $status;

	public function __construct($id=0){
		$this->id = $id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getId(){
		return $this->id;
	}

	public function setTitulo($titulo){
		$this->titulo = $titulo;
	}

	public function getTitulo(){
		return $this->titulo;
	}

	public function setImagem($imagem){
		$this->imagem = $imagem;
	}

	public function getImagem(){
		return $this->imagem;
	}

	public function setDescricao($descricao){
		$this->descricao = $descricao;
	}

	public function getDescricao(){
		return $this->descricao;
	}

	public function setLink($link){
		$this->link = $link;
	}

	public function getLink(){
		return $this->link;
	}

	public function setPosicao($posicao){
		$this->posicao = $posicao;
	}

	public function getPosicao(){
		return $this->posicao;
	}

	public function setOrdem($ordem){
		$this->ordem = $ordem;
	}

	public function getOrdem(){
		return $this->ordem;
	}

	public function setStatus($status){
		$this->status = $status;
	}	

	public function getStatus(){
		return $this->status;
	}
}