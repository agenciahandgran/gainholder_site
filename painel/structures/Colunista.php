 <?php  

/*******************************************************
* GETERS E SETERS - TABELA {COLUNISTAS}
*********************************************************/

class Colunista{

	private $id;
	private $id_login;
	private $nome_completo;
	private $foto_perfil;
	private $descricao;
	private $status;
	private $tipo;

	// OBJETO ARTIGO
	private $artigo;

	public function __construct($id=-1){
		$this->id = $id;
	}	

	public function setId($id){
		$this->id = $id;
	}	

	public function getId(){
		return $this->id;
	}

	public function setIdLogin($idLogin){
		$this->id_login = $idLogin;
	}

	public function getIdLogin(){
		return $this->id_login;
	}

	public function setNomeCompleto($nomeCompleto){
		$this->nome_completo = $nomeCompleto;
	}
        
	public function getNomeCompleto(){
		return $this->nome_completo;
	}

	public function setFotoPerfil($fotoPerfil){
		$this->foto_perfil = $fotoPerfil; 
	}

	public function getFotoPerfil(){
		return $this->foto_perfil;
	}

	public function setDescricao($descricao){
		$this->descricao = $descricao;
	}

	public function getDescricao(){
		return $this->descricao;
	}

	public function setStatus($status){
		$this->status = $status;
	}

	public function getStatus(){
		return $this->status;
	}

	public function setTipo($tipo){
		$this->tipo = $tipo;
	}

	public function getTipo(){
		return $this->tipo;
	}

	// OBJETO ARTIGO
	public function setArtigo(Conteudo $artigo){
		$this->artigo = $artigo;
	}

	public function getArtigo(){
		return $this->artigo;
	}
}

?>