<?php  

/*******************************************************
* GETERS E SETERS - TABELA {CONTEÚDOS}
*********************************************************/

class Conteudo{

	private $id;
	private $id_categoria;
	private $banner;
	private $thumbnail;
	private $titulo;
	private $texto;
	private $data_publicacao;
	private $status;
	private $tipo;
	private $id_autor;
	private $nivel;
	private $visualizacoes;
	private $destaque;
	private $slug;
	private $resumo;

	/* Contador dos resultados de pesquisa */
	private $total;
	
	public function __construct($id=-1){
		$this->id = $id;
	}	

	public function setId($id){
		$this->id = $id;
	}	

	public function getId(){
		return $this->id;
	}

	public function setCategoria(Categoria $idCategoria){
		$this->id_categoria = $idCategoria;
	}

	public function getCategoria(){
		return $this->id_categoria;
	}

	public function setBanner($banner){
		$this->banner = $banner;
	}

	public function getBanner(){
		return $this->banner;
	}

	public function setThumbnail($thumbnail){
		$this->thumbnail = $thumbnail;
	}

	public function getThumbnail(){
		return $this->thumbnail;
	}

	public function setTitulo($titulo){
		$this->titulo = $titulo;
	}

	public function getTitulo(){
		return $this->titulo;
	}

	public function setTexto($texto){
		$this->texto = $texto;
	}

	public function getTexto(){
		return $this->texto;
	}

	public function setDataPublicacao($dataPublicacao){
		$this->dataPublicacao = $dataPublicacao;
	}

	public function getDataPublicacao(){
		return $this->dataPublicacao;
	}

	public function setTipo($tipo){
		$this->tipo = $tipo;
	}

	public function getTipo(){
		return $this->tipo;
	}

	public function setAutor($idAutor){
		$this->id_autor = $idAutor;
	}	

	public function getAutor(){
		return $this->id_autor;
	}

	public function setNivel($nivel){
		$this->nivel = $nivel;
	}

	public function getNivel(){
		return $this->nivel;
	}

	public function setStatus($status){
		$this->status = $status;
	}

	public function getStatus(){
		return $this->status;
	}

	public function setVisualizacoes($visualizacoes){
		$this->visualizacoes = $visualizacoes;
	}

	public function getVisualizacoes(){
		return $this->visualizacoes;
	}

	public function setDestaque($destaque){
		$this->destaque = $destaque;
	}

	public function getDestaque(){
		return $this->destaque;
	}

	public function setSlug($slug){
		$this->slug = $slug;		
	}

	public function getSlug(){
		return $this->slug;
	}

	public function setResumo($resumo){
		$this->resumo = $resumo;
	}

	public function getResumo(){
		return $this->resumo;
	}

	/* Contador dos resultados de pesquisa */
	public function setTotal($total){
		$this->total = $total;
	}

	public function getTotal(){
		return $this->total;
	}
}

?>