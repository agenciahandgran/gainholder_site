<?php  

/*******************************************************
* GETERS E SETERS - TABELA {ENDERECOS}
*********************************************************/

class Endereco{

	private $id;
	private $cep;
	private $endereco;
	private $cidade;
	private $uf;
	private $numero;
	private $bairro;
	private $complemento;

	public function __construct($id=0){
		$this->id = $id;
	}	

	public function setId($id){
		$this->id = $id;
	}	

	public function getId(){
		return $this->id;
	}

	public function setCep($cep){
		$this->cep = $cep;
	}

	public function getCep(){
		return $this->cep;
	}

	public function setEndereco($endereco){
		$this->endereco = $endereco;
	}

	public function getEndereco(){
		return $this->endereco;
	}

	public function setCidade($cidade){
		$this->cidade = $cidade;
	}

	public function getCidade(){
		return $this->cidade;
	}

	public function setUf($uf){
		$this->uf = $uf;
	}

	public function getUf(){
		return $this->uf;
	}

	public function setNumero($numero){
		$this->numero = $numero;
	}

	public function getNumero(){
		return $this->numero;
	}

	public function setBairro($bairro){
		$this->bairro = $bairro;
	}

	public function getBairro(){
		return $this->bairro;
	}

	public function setComplemento($complemento){
		$this->complemento = $complemento;
	}

	public function getComplemento(){
		return $this->complemento;
	}
}

?>