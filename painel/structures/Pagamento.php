<?php  

class Pagamento{

	private $id;
	private $codigo;

	// Clientes
	private $usuario;

	// Assinaturas
	private $plano;

	private $valor;
	private $periodo;
	private $status;

	public function __construct($id=0){
		$this->id = $id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getId(){
		return $this->id;
	}

	public function setCodigo($codigo){
		$this->codigo = $codigo;
	}

	public function getCodigo(){
		return $this->codigo;
	}

	public function setCliente(Usuario $usuario){
		$this->usuario = $usuario;
	}

	public function getCliente(){
		return $this->usuario;
	}

	public function setPlano(Assinatura $plano){
		$this->plano = $plano;
	}

	public function getPlano(){
		return $this->plano;
	}

	public function setValor($valor){
		$this->valor = $valor;
	}

	public function getValor(){
		return $this->valor;
	}

	public function setPeriodo($periodo){
		$this->periodo = $periodo;
	}

	public function getPeriodo(){
		return $this->periodo;
	}

	public function setStatus($status){
		$this->status = $status;
	}

	public function getStatus(){
		return $this->status;
	}
}

?>