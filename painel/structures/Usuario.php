<?php  

	/*******************************************************
	* GETERS E SETERS - TABELA {USUÁRIO}
	*********************************************************/

	class Usuario{

		private $id;
		private $nome;
		private $tipo;
		private $tipo_legislacao;
		private $sexo;
		private $dataNascimento;
		private $telefone;
		private $celular;
		private $cpf;
		private $cnpj;
		private $rg;
		private $nivel;

		//OBJETO LOGIN
		private $login;

		//OBJETO ENDERECO
		private $endereco;

		public function __construct($id=-1){
			$this->id = $id;
		}

		public function setId($id){
			$this->id = $id;
		}

		public function getId(){
			return $this->id;
		}

		public function setNome($nome){
			$this->nome = $nome;
		}

		public function getNome(){
			return $this->nome;
		}

		public function setTipo($tipo){
			$this->tipo = $tipo;
		}

		public function getTipo(){
			return $this->tipo;
		}

		public function setTipoLegislacao($tipoLegislacao){
			$this->tipo_legislacao = $tipoLegislacao;
		}

		public function getTipoLegislacao(){
			return $this->tipo_legislacao;
		}

		public function setSexo($sexo){
			$this->sexo = $sexo;
		}

		public function getSexo(){
			return $this->sexo;
		}

		public function setDataNascimento($dataNascimento){
			$this->dataNascimento = $dataNascimento;
		}

		public function getDataNascimento(){
			return $this->dataNascimento;
		}

		public function setTelefone($telefone){
			$this->telefone = $telefone;
		}

		public function getTelefone(){
			return $this->telefone;
		}

		public function setCelular($celular){
			$this->celular = $celular;
		}

		public function getCelular(){
			return $this->celular;
		}

		public function setCpf($cpf){
			$this->cpf = $cpf;
		}

		public function getCpf(){
			return $this->cpf;
		}

		public function setCnpj($cnpj){
			$this->cnpj = $cnpj;
		}

		public function getCnpj(){
			return $this->cnpj;
		}

		public function setRg($rg){
			$this->rg = $rg;
		}

		public function getRg(){
			return $this->rg;
		}

		public function setNivel($nivel){
			$this->nivel = $nivel;
		}

		public function getNivel(){
			return $this->nivel;
		}

		// OBJETO LOGIN
		public function setLogin(Login $login){
			$this->login = $login;
		}

		public function getLogin(){
			return $this->login;
		}

		// OBJETO ENDEREÇO
		public function setEndereco(Endereco $endereco){
			$this->endereco = $endereco;
		}

		public function getEndereco(){
			return $this->endereco;
		}
	}
?>