<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/froala_editor_plugin/css/froala_editor.css">
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/froala_editor_plugin/css/froala_style.css">
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/froala_editor_plugin/css/plugins/code_view.css">
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/froala_editor_plugin/css/plugins/draggable.css">
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/froala_editor_plugin/css/plugins/colors.css">
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/froala_editor_plugin/css/plugins/emoticons.css">
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/froala_editor_plugin/css/plugins/image_manager.css">
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/froala_editor_plugin/css/plugins/image.css">
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/froala_editor_plugin/css/plugins/line_breaker.css">
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/froala_editor_plugin/css/plugins/table.css">
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/froala_editor_plugin/css/plugins/char_counter.css">
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/froala_editor_plugin/css/plugins/video.css">
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/froala_editor_plugin/css/plugins/fullscreen.css">
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/froala_editor_plugin/css/plugins/file.css">
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/froala_editor_plugin/css/plugins/quick_insert.css">
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/froala_editor_plugin/css/plugins/help.css">
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/froala_editor_plugin/css/third_party/spell_checker.css">
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>/assets/froala_editor_plugin/css/plugins/special_characters.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">

<div class="x_panel">
	<div class="x_title">
		<h2>Artigos <small><?php echo $legend; ?></small></h2>
		<div class="clearfix"></div>
	</div>

	<div class="x_content">
		<?php 
		
		if($resultado){

			if(isset($mensagem['success'])){	
				echo logs($mensagem['success'],'alert-success');	

				if(!isset($id)){
					echo "<script>setTimeout(function(){window.location='".URL.'/artigos'."'} , 900);</script>";	
				}
			}else{
				echo logs($mensagem['error'],'alert-danger');
			}
		}	

		?>
		<br />
		<form id="formulario-artigo" method="post" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left" action="<?= URL ?>/artigos/<?= $action ?>" novalidate>

			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="artigo_categoria">Categoria <span class="required">*</span>
				</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<select class="form-control col-md-7 col-xs-12" name="artigo_categoria" id="artigo_categoria" required="required">
						<option value="">Selecione uma categoria...</option>
						<?php if(!isset($categorias['error'])): ?>
							<?php foreach($categorias as $categoria): ?>
								<?php 
									if(isset($artigo)){
										$selected = ($categoria->getId() == $artigo->getCategoria()->getId()) ? 'selected' : ''; 
									}else{
										$selected = "";
									}

								?>
								<option value="<?php echo $categoria->getId(); ?>" <?php echo $selected; ?>><?php echo $categoria->getNome(); ?></option>
							<?php endforeach; ?>
						<?php endif; ?>					
					</select>
				</div>
			</div>

			<?php if(isset($artigo) && $artigo->getBanner() != null): ?>

				<div class="row">	
					<div class="col-md-55  col-md-offset-3">
						<div class="thumbnail">
							<div class="image view view-first">
								<img style="width: 100%; display: block;" src="<?php echo URL.'/'.URL_PATH_UPLOAD.'artigos/banners/'.$artigo->getBanner() ?>" alt="image" class="imagem-perfil"/>
								<div class="mask">
									<p style="display: none;"><?php echo $artigo->getTitulo(); ?></p>
									<div class="tools tools-bottom">
										<a href="#" class="trocar-foto-perfil"><i class="fa fa-pencil"></i></a>
									</div>
								</div>
							</div>
							<div class="caption">
								<p>Foto de perfil atual</p>
							</div>
							<input id="artigo_banner" class="form-control col-md-7 col-xs-12" type="file" name="artigo_banner" style="display: none;">
						</div>
					</div>
				</div>
			
			<?php else: ?>
			
				<div class="form-group">
					<label for="artigo_banner" class="control-label col-md-3 col-sm-3 col-xs-12">Imagem Principal</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input id="artigo_banner" required="required" class="form-control col-md-7 col-xs-12" type="file" name="artigo_banner">
					</div>
				</div>
			
			<?php endif; ?>

			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="artigo_titulo">Titulo <span class="required">*</span>
				</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input type="text" name="artigo_titulo" id="artigo_titulo" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo isset($artigo) ? $artigo->getTitulo() : ''; ?>">
				</div>
			</div>

			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="artigo_resumo">Resumo <span class="required">*</span>
				</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<textarea class="form-control col-md-7 col-xs-12" name="artigo_resumo" id="artigo_resumo" required="required" placeholder="Máximo 90 caracteres"><?php echo isset($artigo) ? $artigo->getResumo() : ''; ?></textarea>
					<p>Quantidade de caracteres: &nbsp;<span class="contador-resumo">0</span></p>
				</div>
			</div>

			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div id="gender" class="btn-group" data-toggle="buttons">
						<label class="btn btn-<?php echo isset($artigo) ? ($artigo->getStatus() == '1' ? 'primary' : 'default') : 'default'; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
							<input type="radio" name="artigo_status" value="1" <?php echo isset($artigo) ? ($artigo->getStatus() == '1' ? 'checked="checked"' : '') : ''; ?>> &nbsp; Permitido &nbsp;
						</label>
						<label class="btn btn btn-<?php echo isset($artigo) ? ($artigo->getStatus() == '2' ? 'primary' : 'default') : 'default'; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
							<input type="radio" name="artigo_status" value="2" <?php echo isset($artigo) ? ($artigo->getStatus() == '2' ? 'checked="checked"' : '') : ''; ?>> Bloqueado
						</label>
					</div>
				</div>
			</div>

			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="conteudo_texto">Texto <span class="required">*</span>
				</label>
				<input type="hidden" name="artigo_texto" class="artigo_texto">
				<div class="col-md-6 col-sm-6 col-xs-12" style="margin-left: -10px;">
					<a class="btn btn-app" data-toggle="modal" data-target=".bs-example-modal-lg">
						<i class="fa fa-edit"></i> Editor
					</a>
					<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">

								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
									</button>
									<h4 class="modal-title" id="myModalLabel">Editor de texto</h4>
								</div>
								<div class="modal-body">
									<div class="x_content">
										<div id="alerts"></div>
										
										<div class="btn-toolbar editor" data-role="editor-toolbar" style="display: none;" data-target="#editor-one">
											<div class="btn-group">
												<a class="btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="fa fa-font"></i><b class="caret"></b></a>
												<ul class="dropdown-menu">
													<li>
														<a data-edit="fontName Thin" style="font-family: 'Roboto', sans-serif; font-weight: 100;">Thin</a>
													</li>
													<li>
														<a data-edit="fontName Thin Italic" style="font-family: 'Roboto', sans-serif; font-weight: 100; font-style: italic;">Thin Italic</a>
													</li>
													<li>
														<a data-edit="fontName Light" style="font-family: 'Roboto', sans-serif; font-weight: 300; font-style: normal;">Light</a>
													</li>
													<li>
														<a data-edit="fontName Light Italic" style="font-family: 'Roboto', sans-serif; font-weight: 300; font-style: italic;">Light Italic</a>
													</li>
													<li><a data-edit="fontName Verdana" style="font-family:'Verdana'">Verdana</a></li>

												</ul>
											</div>

											<div class="btn-group">
												<a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
												<ul class="dropdown-menu">
													<li>
														<a data-edit="fontSize 5">
															<p style="font-size:17px">Huge</p>
														</a>
													</li>
													<li>
														<a data-edit="fontSize 3">
															<p style="font-size:14px">Normal</p>
														</a>
													</li>
													<li>
														<a data-edit="fontSize 1">
															<p style="font-size:11px">Small</p>
														</a>
													</li>
												</ul>
											</div>

											<div class="btn-group">
												<a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
												<a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
												<a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
												<a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
											</div>

											<div class="btn-group">
												<a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
												<a class="btn" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
												<a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a>
												<a class="btn" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
											</div>

											<div class="btn-group">
												<a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
												<a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
												<a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
												<a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
											</div>

											<div class="btn-group">
												<a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="fa fa-link"></i></a>
												<div class="dropdown-menu input-append">
													<input class="span2" placeholder="URL" type="text" data-edit="createLink">
													<button class="btn" type="button">Add</button>
												</div>
												<a class="btn" data-edit="unlink" title="Remove Hyperlink"><i class="fa fa-cut"></i></a>
											</div>

											<div class="btn-group">
												<a class="btn" data-original-title="Insert picture (or just drag &amp; drop)" id="pictureBtn"><i class="fa fa-picture-o"></i></a>
												<input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" style="opacity: 0; position: absolute; top: 0px; left: 0px; width: 37px; height: 30px;">
											</div>

											<div class="btn-group">
												<a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
												<a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
											</div>
										</div>

										<div id="editor-one" style="display: none;" class="editor-wrapper placeholderText" contenteditable="true"><?php echo isset($artigo) ? $artigo->getTexto() : ''; ?></div>

										<!-- Editor atualizado -->
										<div id="editor">
											<div id='edit' style="margin-top: 30px;">
												<?php echo isset($artigo) ? $artigo->getTexto() : ''; ?>
											</div>
										</div>						

										<textarea name="descr" id="descr" style="display:none;"></textarea>

										<br>

										<div class="ln_solid"></div>

									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-primary" id="salvar-alteracoes-editor" disabled>Salvar Alterações</button>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="conteudo_titulo">Destaque
				</label>
				<input class="form-check-input col-md-1" type="checkbox" name="artigo_destaque" id="artigo_destaque" style="width: 40px; height: 19px; margin-top:10px;" <?php echo (isset($artigo) ? ($artigo->getDestaque() == "1" ? 'checked' : '') : ''); ?>>
			</div>				
			<div class="item form-group">
				<legend>Configurar nível de acesso do conteúdo (assinantes)</legend>
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="artigo_nivel">Grau de acesso <span class="required">*</span>
				</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<select class="form-control col-md-7 col-xs-12" name="artigo_nivel" id="artigo_nivel" required="required">
						<option value="">Selecione o nível de acesso...</option>
						<option value="10" <?php echo isset($conteudo) ?  ( $conteudo->getNivel() == '10' ? 'selected' : '' ) : ''; ?>>Gratuito</option>	
						<option value="1" <?php  echo isset($artigo) ?  ( $artigo->getNivel() == '1' ? 'selected' : '' ) : ''; ?>>Grau 1</option>
						<option value="2"  <?php echo isset($artigo) ?  ( $artigo->getNivel() == '2' ? 'selected' : '' ) : ''; ?>>Grau 2</option>
						<option value="3"  <?php echo isset($artigo) ?  ( $artigo->getNivel() == '3' ? 'selected' : '' ) : ''; ?>>Grau 3</option>					
					</select>
				</div>
			</div>

			<div class="ln_solid"></div>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					<input type="hidden" name="modulo" value="artigo">
					<button type="submit" class="btn btn-success"><?php echo $title; ?></button>
					<a href="<?php echo URL ?>/artigos" class="btn btn-info">Voltar</a>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/froala_editor.min.js" ></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/align.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/char_counter.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/code_beautifier.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/code_view.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/colors.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/draggable.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/emoticons.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/entities.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/file.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/font_size.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/font_family.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/fullscreen.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/image.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/image_manager.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/line_breaker.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/inline_style.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/link.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/lists.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/paragraph_format.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/paragraph_style.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/quick_insert.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/quote.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/table.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/save.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/url.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/video.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/help.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/print.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/third_party/spell_checker.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/special_characters.min.js"></script>
<script type="text/javascript" src="<?php echo URL ?>/assets/froala_editor_plugin/js/plugins/word_paste.min.js"></script>
