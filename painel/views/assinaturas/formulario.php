<div class="x_panel">
	<div class="x_title">
		<h2>Assinaturas <small><?php echo $legend; ?></small></h2>
		<div class="clearfix"></div>
	</div>

	<div class="x_content">
		<?php 
		
		if($resultado){

			if(isset($mensagem['success'])){	
				echo logs($mensagem['success'],'alert-success');	

				if(!isset($id)){
					echo "<script>setTimeout(function(){window.location='".URL.'/assinaturas'."'} , 900);</script>";	
				}
			}else{
				echo logs($mensagem['error'],'alert-danger');
			}
		}	

		?>
		<br />
		<form id="formulario-assinatura" method="post" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left" action="<?= URL ?>/assinaturas/<?= $action ?>" novalidate>

			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="assinatura_nome">Nome <span class="required">*</span>
				</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input type="text" name="assinatura_nome" id="assinatura_nome" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo isset($assinatura) ? $assinatura->getNome() : ''; ?>">
				</div>
			</div>

			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="assinatura_preco">Preço <span class="required">*</span>
				</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input type="text" name="assinatura_preco" id="assinatura_preco" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo isset($assinatura) ? number_format($assinatura->getPreco(),2,'.',',') : ''; ?>">
				</div>
			</div>

			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="assinatura_desconto">Desconto(%) <span class="required">*</span>
				</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input type="text" name="assinatura_desconto" id="assinatura_desconto" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo isset($assinatura) ? $assinatura->getDesconto() : ''; ?>">
				</div>
			</div>

			<div class="item form-group">
				<legend>Configurar nível de acesso permitido pelo plano (assinantes)</legend>
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="conteudo_nivel">
					Grau de acesso <span class="required">*</span>
				</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<select class="form-control col-md-7 col-xs-12" name="assinatura_grau" id="assinatura_grau" required="required">
						<option value="">Selecione o grau de acesso...</option>
						<option value="1" <?php echo isset($assinatura) ?  ( $assinatura->getGrau() == '1' ? 'selected' : '' ) : ''; ?>>Grau 1</option>
						<option value="2"  <?php echo isset($assinatura) ?  ( $assinatura->getGrau() == '2' ? 'selected' : '' ) : ''; ?>>Grau 2</option>
						<option value="3"  <?php echo isset($assinatura) ?  ( $assinatura->getGrau() == '3' ? 'selected' : '' ) : ''; ?>>Grau 3</option>						
					</select>
				</div>
			</div>

			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div id="gender" class="btn-group" data-toggle="buttons">
						<label class="btn btn-<?php echo isset($assinatura) ? ($assinatura->getStatus() == '1' ? 'primary' : 'default') : 'default'; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
							<input type="radio" name="assinatura_status" value="1" <?php echo isset($assinatura) ? ($assinatura->getStatus() == '1' ? 'checked="checked"' : '') : ''; ?>> &nbsp; Permitido &nbsp;
						</label>
						<label class="btn btn btn-<?php echo isset($assinatura) ? ($assinatura->getStatus() == '2' ? 'primary' : 'default') : 'default'; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
							<input type="radio" name="assinatura_status" value="2" <?php echo isset($assinatura) ? ($assinatura->getStatus() == '2' ? 'checked="checked"' : '') : ''; ?>> Bloqueado
						</label>
					</div>
				</div>
			</div>

			<div class="ln_solid"></div>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					<input type="hidden" name="modulo" value="assinatura">
					<button type="submit" class="btn btn-success"><?php echo $title; ?></button>
					<a href="<?php echo URL ?>/assinaturas" class="btn btn-info">Voltar</a>
				</div>
			</div>
		</form>
	</div>
</div>

