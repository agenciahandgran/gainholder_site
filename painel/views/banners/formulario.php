<div class="x_panel">
	<div class="x_title">
		<h2>Banners <small><?php echo $legend; ?></small></h2>
		<div class="clearfix"></div>
	</div>

	<div class="x_content">
		<?php 
		
		if($resultado){

			if(isset($mensagem['success'])){	
				echo logs($mensagem['success'],'alert-success');	

				if(!isset($id)){
					echo "<script>setTimeout(function(){window.location='".URL.'/banners'."'} , 900);</script>";	
				}
			}else{
				echo logs($mensagem['error'],'alert-danger');
			}
		}	

		?>
		<br />
		<form id="formulario-banner" method="post" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left" action="<?= URL ?>/banners/<?= $action ?>" novalidate>

			<?php if(isset($banner) && $banner->getImagem() != null): ?>

				<div class="row">	
					<div class="col-md-55  col-md-offset-3">
						<div class="thumbnail">
							<div class="image view view-first">
								<img style="width: 100%; display: block;" src="<?php echo URL.'/'.URL_PATH_UPLOAD.'banners/'.$banner->getImagem() ?>" alt="image" class="imagem-perfil"/>
								<div class="mask">
									<p style="display: none;"><?php echo $banner->getTitulo(); ?></p>
									<div class="tools tools-bottom">
										<a href="#" class="trocar-foto-perfil"><i class="fa fa-pencil"></i></a>
									</div>
								</div>
							</div>
							<div class="caption">
								<p>Banner atual</p>
							</div>
							<input id="banner_imagem" class="form-control col-md-7 col-xs-12" type="file" name="banner_imagem" style="display: none;">
						</div>
					</div>
				</div>

			<?php else: ?>

				<div class="form-group">
					<label for="banner_imagem" class="control-label col-md-3 col-sm-3 col-xs-12">Imagem</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input id="banner_imagem" required="required" class="form-control col-md-7 col-xs-12" type="file" name="banner_imagem">
					</div>
				</div>

			<?php endif; ?>

			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="banner_titulo">Titulo <span class="required">*</span>
				</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input type="text" name="banner_titulo" id="banner_titulo" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo isset($banner) ? $banner->getTitulo() : ''; ?>">
				</div>
			</div>

			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="banner_descricao">Descrição <span class="required">*</span>
				</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<textarea class="form-control col-md-7 col-xs-12" id="banner_descricao" name="banner_descricao" cols="40" rows="8" required="required"><?php echo isset($banner) ? $banner->getDescricao() : ''; ?></textarea>
				</div>
			</div>			

			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="banner_link">Url <span class="required">*</span>
				</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input type="text" name="banner_link" id="banner_link" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo isset($banner) ? $banner->getLink() : ''; ?>">
				</div>
			</div>


			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="banner_link">Posição <span class="required">*</span>
				</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<select class="form-control col-md-7 col-xs-12" name="banner_posicao" id="banner_posicao" required="required">
						<option value="">Selecione a posição...</option>
						<option value="topo" <?php echo isset($banner) ?  ( $banner->getPosicao() == 'topo' ? 'selected' : '' ) : ''; ?>>Topo</option>
						<option value="rodape" <?php echo isset($banner) ?  ( $banner->getPosicao() == 'rodape' ? 'selected' : '' ) : ''; ?>>Rodapé</option>			
					</select>
				</div>
			</div>						

			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div id="gender" class="btn-group" data-toggle="buttons">
						<label class="btn btn-<?php echo isset($banner) ? ($banner->getStatus() == '1' ? 'primary' : 'default') : 'default'; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
							<input type="radio" name="banner_status" value="1" <?php echo isset($banner) ? ($banner->getStatus() == '1' ? 'checked="checked"' : '') : ''; ?>> &nbsp; Permitido &nbsp;
						</label>
						<label class="btn btn btn-<?php echo isset($banner) ? ($banner->getStatus() == '2' ? 'primary' : 'default') : 'default'; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
							<input type="radio" name="banner_status" value="2" <?php echo isset($banner) ? ($banner->getStatus() == '2' ? 'checked="checked"' : '') : ''; ?>> Bloqueado
						</label>
					</div>
				</div>
			</div>

			<div class="ln_solid"></div>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					<input type="hidden" name="modulo" value="banner">
					<button type="submit" class="btn btn-success"><?php echo $title; ?></button>
					<a href="<?php echo URL ?>/banners" class="btn btn-info">Voltar</a>
				</div>
			</div>
		</form>
	</div>
</div>

