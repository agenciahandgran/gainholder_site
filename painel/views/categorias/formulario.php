<div class="x_panel">
	<div class="x_title">
		<h2>Categorias <small><?php echo $legend; ?></small></h2>
		<div class="clearfix"></div>
	</div>

	<div class="x_content">
		<?php 
		
		if($resultado){

			if(isset($mensagem['success'])){	
				echo logs($mensagem['success'],'alert-success');	

				if(!isset($id)){
					echo "<script>setTimeout(function(){window.location='".URL.'/categorias'."'} , 900);</script>";	
				}
			}else{
				echo logs($mensagem['error'],'alert-danger');
			}
		}	

		?>
		<br />
		<form id="formulario-categoria" method="post" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left" action="<?= URL ?>/categorias/<?= $action ?>" novalidate>

			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="categoria_nome">Nome <span class="required">*</span>
				</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input type="text" name="categoria_nome" id="categoria_nome" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo isset($categoria) ? $categoria->getNome() : ''; ?>">
				</div>
			</div>
			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div id="gender" class="btn-group" data-toggle="buttons">
						<label class="btn btn-<?php echo isset($categoria) ? ($categoria->getStatus() == '1' ? 'primary' : 'default') : 'default'; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
							<input type="radio" name="categoria_status" value="1" <?php echo isset($categoria) ? ($categoria->getStatus() == '1' ? 'checked="checked"' : '') : ''; ?>> &nbsp; Permitido &nbsp;
						</label>
						<label class="btn btn btn-<?php echo isset($categoria) ? ($categoria->getStatus() == '2' ? 'primary' : 'default') : 'default'; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
							<input type="radio" name="categoria_status" value="2" <?php echo isset($categoria) ? ($categoria->getStatus() == '2' ? 'checked="checked"' : '') : ''; ?>> Bloqueado
						</label>
					</div>
				</div>
			</div>

			<div class="ln_solid"></div>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					<input type="hidden" name="modulo" value="categoria">
					<button type="submit" class="btn btn-success"><?php echo $title; ?></button>
					<a href="<?php echo URL ?>/categorias" class="btn btn-info">Voltar</a>
				</div>
			</div>
		</form>
	</div>
</div>

