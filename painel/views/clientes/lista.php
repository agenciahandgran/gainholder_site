<div class="x_panel">
    <div class="x_title">
        <h2>Gerenciar Clientes<small>Listagem</small></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <div class="buttons" style="display: none;">
            <a href="<?php echo URL ?>/clientes/cadastrar" class="btn btn-primary btn-sm">
            <i class="fa fa-file"></i> Novo cliente</a>           
        </div>

        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Tipo</th>
                    <th>CPF</th>
                    <th>Telefone</th>
                    <th>Status</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php if(!isset($clientes['error'])): ?>
                    <?php foreach($clientes as $cliente): ?>
                        <tr>
                            <td><?php echo $cliente->getNome(); ?></td>
                            <td><?php echo $cliente->getNivel();   ?></td>
                            <td><?php echo $cliente->getCpf(); ?></td>
                            <td><?php echo $cliente->getTelefone(); ?></td>
                            <td><?php echo verificarStatus($cliente->getNivel()); ?></td>
                            <td>
<!--                                 <a href="<?php echo URL ?>/clientes/editar/<?php echo $cliente->getId(); ?>" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Editar item" data-original-title="Editar item" ><i class="fa fa-pencil" aria-hidden="true"></i></a> -->
                                <a href="<?php echo URL ?>/clientes/detalhar/<?php echo $cliente->getId(); ?>" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Visualizar Dados" data-original-title="Visualizar Dados" ><i class="fa fa-search" aria-hidden="true"></i></a>
                                <a href="#" data-id="<?php echo $cliente->getId(); ?>" class="btn btn-danger btn-xs excluirRegistro" data-toggle="tooltip" data-placement="top" title="Excluir item" data-original-title="Excluir item"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr colspan="5">
                        <td>Nenhum resultado encontrado.</td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>

