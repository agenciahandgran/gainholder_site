<div class="x_panel">
	<div class="x_title">
		<h2>Colunistas <small><?php echo $legend; ?></small></h2>
		<div class="clearfix"></div>
	</div>

	<div class="x_content">
		<?php 
		
		if($resultado){

			if(isset($mensagem['success'])){	
				echo logs($mensagem['success'],'alert-success');	

				if(!isset($id)){
					echo "<script>setTimeout(function(){window.location='".URL.'/colunistas'."'} , 900);</script>";	
				}
			}else{
				echo logs($mensagem['error'],'alert-danger');
			}
		}	

		?>
		<br />
		<form id="formulario-colunista" method="post" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left" action="<?= URL ?>/colunistas/<?= $action ?>" novalidate>

			<?php if(isset($colunista) && $colunista->getUsuario()->getFotoPerfil() != null): ?>

				<div class="row">	
					<div class="col-md-55  col-md-offset-3">
						<div class="thumbnail">
							<div class="image view view-first">
								<img style="width: 100%; display: block;" src="<?php echo URL.'/'.URL_PATH_UPLOAD.'colunista_perfil/'.$colunista->getUsuario()->getFotoPerfil() ?>" alt="image" class="imagem-perfil"/>
								<div class="mask">
									<p><?php echo $colunista->getUsuario()->getNomeCompleto(); ?></p>
									<div class="tools tools-bottom">
										<a href="#" class="trocar-foto-perfil"><i class="fa fa-pencil"></i></a>
									</div>
								</div>
							</div>
							<div class="caption">
								<p>Foto de perfil atual</p>
							</div>
							<input id="colunista_foto_perfil" class="form-control col-md-7 col-xs-12" type="file" name="colunista_foto_perfil" style="display: none;">
						</div>
					</div>
				</div>
			
			<?php else: ?>
			
				<div class="form-group">
					<label for="colunista_foto_perfil" class="control-label col-md-3 col-sm-3 col-xs-12">Foto perfil</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input id="colunista_foto_perfil" required="required" class="form-control col-md-7 col-xs-12" type="file" name="colunista_foto_perfil">
					</div>
				</div>
			
			<?php endif; ?>

			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="colunista_nome">Nome <span class="required">*</span>
				</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input type="text" name="colunista_nome" id="colunista_nome" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo isset($colunista) ?  substr($colunista->getUsuario()->getNomeCompleto(), 0, strpos($colunista->getUsuario()->getNomeCompleto(), ' ')) : ''; ?>">
				</div>
			</div>
			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="colunista_sobrenome">Sobrenome <span class="required">*</span>
				</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input type="text" id="colunista_sobrenome" name="colunista_sobrenome" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo isset($colunista) ? substr($colunista->getUsuario()->getNomeCompleto(), strpos($colunista->getUsuario()->getNomeCompleto(), ' ')+1) : ''; ?>">
				</div>
			</div>
			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div id="gender" class="btn-group" data-toggle="buttons">
						<label class="btn btn-<?php echo isset($colunista) ? ($colunista->getStatus() == '1' ? 'primary' : 'default') : 'default'; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
							<input type="radio" name="colunista_status" value="1" <?php echo isset($colunista) ? ($colunista->getStatus() == '1' ? 'checked="checked"' : '') : ''; ?>> &nbsp; Permitido &nbsp;
						</label>
						<label class="btn btn btn-<?php echo isset($colunista) ? ($colunista->getStatus() == '2' ? 'primary' : 'default') : 'default'; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
							<input type="radio" name="colunista_status" value="2" <?php echo isset($colunista) ? ($colunista->getStatus() == '2' ? 'checked="checked"' : '') : ''; ?>> Bloqueado
						</label>
					</div>
				</div>
			</div>
			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="colunista_descricao">Descrição <span class="required">*</span>
				</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<textarea class="form-control col-md-7 col-xs-12" id="colunista_descricao" name="colunista_descricao" cols="40" rows="8" required="required"><?php echo isset($colunista) ? $colunista->getUsuario()->getDescricao() : ''; ?></textarea>
				</div>
			</div>
			<?php if(!isset($colunista)): ?>
				<div class="item form-group">
					<legend> Configurar Acessos </legend>
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="colunista_email">E-mail <span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="email" id="colunista_email" name="colunista_email" required="required" class="form-control col-md-7 col-xs-12">
					</div>
				</div>
				<div class="item form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="colunista_senha">Senha <span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="password" id="colunista_senha"  name="colunista_senha" required="required" class="form-control col-md-7 col-xs-12">
					</div>
				</div>
				<div class="item form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="colunista_senha_repeticao">Repetir Senha <span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="password" id="colunista_senha_repeticao" name="colunista_senha_repeticao" required="required" data-validate-linked="colunista_senha" class="form-control col-md-7 col-xs-12">
					</div>
				</div>
			<?php endif; ?>			
			<div class="form-group">
				<legend> Configurar Permissões </legend>
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Módulos:</label>
				<div class="col-md-3 col-sm-9 col-xs-12">
					<?php 

					if(isset($modulos)): 

						foreach($modulos as $modulo):

							$moduloSelecionado = "";

							if(isset($colunista)){

								if(in_array($modulo->getId(), $modulosPermitidos)){
									$moduloSelecionado = "checked='checked'";
								}

							}

							$alinhamento = $modulo->getIdPai() != 0 ? "style='margin-left: 20px;'" : "";
							?>
							<div <?php echo $alinhamento;  ?> class="checkbox" data-id-pai="<?php echo $modulo->getIdPai(); ?>">
								<label>
									<input type="checkbox" name="modulos_escolhido[]" class="flat opcao-modulo"  value="<?php echo $modulo->getId(); ?>" <?php echo $moduloSelecionado; ?> > <?php echo $modulo->getNome(); ?>
								</label>
							</div>
							<?php  
						endforeach;
					endif;
					?>
				</div>
				<div class="col-md-3 col-sm-9 col-xs-12">
					<div class="checkbox">
						<label>
							<input type="checkbox" class="flat opcao-todos-modulos" value="0" > Todos
						</label>
					</div>
				</div>
			</div>
			<div class="ln_solid"></div>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					<input type="hidden" name="modulo" value="colunista">
					<button type="submit" class="btn btn-success"><?php echo $title; ?></button>
					<a href="<?php echo URL ?>/colunistas" class="btn btn-info">Voltar</a>
				</div>
			</div>
		</form>
	</div>
</div>

