<div class="x_panel">
	<div class="x_title">
		<h2>Gerenciar Acessos<small>Colunistas</small></h2>
		<div class="clearfix"></div>
	</div>
	<div class="x_content">

		<div class="buttons">
			<a href="<?php echo URL ?>/colunistas/cadastrar" class="btn btn-primary btn-sm">
			<i class="fa fa-file"></i> Novo colunista</a>			
		</div>

		<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Nome</th>
					<th>Sobrenome</th>
					<th>E-mail</th>
					<th>Status</th>
					<th>Ações</th>
				</tr>
			</thead>
			<tbody>
				<?php if(!isset($colunistas['error'])): ?>
					<?php foreach($colunistas as $colunista): ?>
						<tr>
							<td><?php echo substr($colunista->getUsuario()->getNomeCompleto(), 0, strpos($colunista->getUsuario()->getNomeCompleto(), ' ')); ?></td>
							<td><?php echo substr($colunista->getUsuario()->getNomeCompleto(), strpos($colunista->getUsuario()->getNomeCompleto(), ' '));   ?></td>
							<td><?php echo $colunista->getEmail(); ?></td>
							<td><?php echo verificarStatus($colunista->getStatus()); ?></td>
							<td>
								<a href="<?php echo URL ?>/colunistas/editar/<?php echo $colunista->getId(); ?>" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Editar item" data-original-title="Editar item" ><i class="fa fa-pencil" aria-hidden="true"></i></a>
								<a href="#" data-id="<?php echo $colunista->getId(); ?>" class="btn btn-danger btn-xs excluirRegistro" data-toggle="tooltip" data-placement="top" title="Excluir item" data-original-title="Excluir item"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>

