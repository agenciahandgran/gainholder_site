<div class="x_panel">
	<div class="x_title">
		<h2>Administradores <small><?php echo $legend; ?></small></h2>
		<div class="clearfix"></div>
	</div>

	<div class="x_content">
		<?php 
		
		if($resultado){

			if(isset($mensagem['success'])){	
				echo logs($mensagem['success'],'alert-success');	

				if(!isset($id)){
					echo "<script>setTimeout(function(){window.location='".URL.'/administradores'."'} , 900);</script>";	
				}
			}else{
				echo logs($mensagem['error'],'alert-danger');
			}
		}	

		?>
		<br />
		<form id="formulario-administrador" method="post" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left" action="<?= URL ?>/administradores/<?= $action ?>" novalidate>

			<?php if(isset($administrador) && $administrador->getUsuario()->getFotoPerfil() != null): ?>

				<div class="row">	
					<div class="col-md-55  col-md-offset-3">
						<div class="thumbnail">
							<div class="image view view-first">
								<img style="width: 100%; display: block;" src="<?php echo URL.'/'.URL_PATH_UPLOAD.'administrador_perfil/'.$administrador->getUsuario()->getFotoPerfil() ?>" alt="image" class="imagem-perfil"/>
								<div class="mask">
									<p><?php echo $administrador->getUsuario()->getNomeCompleto(); ?></p>
									<div class="tools tools-bottom">
										<a href="#" class="trocar-foto-perfil"><i class="fa fa-pencil"></i></a>
									</div>
								</div>
							</div>
							<div class="caption">
								<p>Foto de perfil atual</p>
							</div>
							<input id="admin_foto_perfil" class="form-control col-md-7 col-xs-12" type="file" name="admin_foto_perfil" style="display: none;">
						</div>
					</div>
				</div>
			
			<?php else: ?>
			
				<div class="form-group">
					<label for="admin_foto_perfil" class="control-label col-md-3 col-sm-3 col-xs-12">Foto perfil</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input id="admin_foto_perfil" required="required" class="form-control col-md-7 col-xs-12" type="file" name="admin_foto_perfil">
					</div>
				</div>
			
			<?php endif; ?>

			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="admin_nome">Nome <span class="required">*</span>
				</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input type="text" name="admin_nome" id="admin_nome" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo isset($administrador) ?  substr($administrador->getUsuario()->getNomeCompleto(), 0, strpos($administrador->getUsuario()->getNomeCompleto(), ' ')) : ''; ?>">
				</div>
			</div>
			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="admin_sobrenome">Sobrenome <span class="required">*</span>
				</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input type="text" id="admin_sobrenome" name="admin_sobrenome" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo isset($administrador) ? substr($administrador->getUsuario()->getNomeCompleto(), strpos($administrador->getUsuario()->getNomeCompleto(), ' ')+1) : ''; ?>">
				</div>
			</div>
			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div id="gender" class="btn-group" data-toggle="buttons">
						<label class="btn btn-<?php echo isset($administrador) ? ($administrador->getStatus() == '1' ? 'primary' : 'default') : 'default'; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
							<input type="radio" name="admin_status" value="1" <?php echo isset($administrador) ? ($administrador->getStatus() == '1' ? 'checked="checked"' : '') : ''; ?>> &nbsp; Permitido &nbsp;
						</label>
						<label class="btn btn btn-<?php echo isset($administrador) ? ($administrador->getStatus() == '2' ? 'primary' : 'default') : 'default'; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
							<input type="radio" name="admin_status" value="2" <?php echo isset($administrador) ? ($administrador->getStatus() == '2' ? 'checked="checked"' : '') : ''; ?>> Bloqueado
						</label>
					</div>
				</div>
			</div>
			<?php if(!isset($administrador)): ?>
				<div class="item form-group">
					<legend> Configurar Acessos </legend>
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="admin_email">E-mail <span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="email" id="admin_email" name="admin_email" required="required" class="form-control col-md-7 col-xs-12">
					</div>
				</div>
				<div class="item form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="admin_senha">Senha <span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="password" id="admin_senha"  name="admin_senha" required="required" class="form-control col-md-7 col-xs-12">
					</div>
				</div>
				<div class="item form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="admin_senha_repeticao">Repetir Senha <span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="password" id="admin_senha_repeticao" name="admin_senha_repeticao" required="required" data-validate-linked="admin_senha" class="form-control col-md-7 col-xs-12">
					</div>
				</div>
			<?php endif; ?>			
			<div class="form-group">
				<legend> Configurar Permissões </legend>
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Módulos:</label>
				<div class="col-md-3 col-sm-9 col-xs-12">
					<?php 

					if(isset($modulos)): 

						foreach($modulos as $modulo):

							$moduloSelecionado = "";

							if(isset($administrador)){

								if(in_array($modulo->getId(), $modulosPermitidos)){
									$moduloSelecionado = "checked='checked'";
								}

							}

							$alinhamento = $modulo->getIdPai() != 0 ? "style='margin-left: 20px;'" : "";
							?>
							<div <?php echo $alinhamento;  ?> class="checkbox" data-id-pai="<?php echo $modulo->getIdPai(); ?>">
								<label>
									<input type="checkbox" name="modulos_escolhido[]" class="flat opcao-modulo"  value="<?php echo $modulo->getId(); ?>" <?php echo $moduloSelecionado; ?> > <?php echo $modulo->getNome(); ?>
								</label>
							</div>
							<?php  
						endforeach;
					endif;
					?>
				</div>
				<div class="col-md-3 col-sm-9 col-xs-12">
					<div class="checkbox">
						<label>
							<input type="checkbox" class="flat opcao-todos-modulos" value="0" > Todos
						</label>
					</div>
				</div>
			</div>
			<div class="ln_solid"></div>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					<input type="hidden" name="modulo" value="administrador">
					<button type="submit" class="btn btn-success"><?php echo $title; ?></button>
					<a href="<?php echo URL ?>/administradores" class="btn btn-info">Voltar</a>
				</div>
			</div>
		</form>
	</div>
</div>


