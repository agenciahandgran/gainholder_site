<div class="x_panel">
	<div class="x_title">
		<h2>Gerenciar Notícias<small>Conteúdos</small></h2>
		<div class="clearfix"></div>
	</div>
	<div class="x_content">

		<div class="buttons">
			<a href="<?php echo URL ?>/conteudos/cadastrar" class="btn btn-primary btn-sm">
			<i class="fa fa-file"></i> Novo conteúdo</a>			
		</div>

		<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Titulo</th>
					<th>Categoria</th>
					<th>Data</th>
					<th>Status</th>
					<th>Ações</th>
				</tr>
			</thead>
			<tbody id="tabela-conteudo">
				<?php if(!isset($conteudos['error'])): ?>
					<?php foreach($conteudos as $conteudo): ?>
						<tr>
							<td><?php echo trim_text($conteudo->getTitulo(), 50); ?></td>
							<td><?php echo $conteudo->getCategoria()->getNome(); ?></td>
							<td><?php echo date('d/m/Y', strtotime($conteudo->getDataPublicacao())); ?></td>
							<td><?php echo verificarStatus($conteudo->getStatus()); ?></td>
							<td>
								<a href="<?php echo URL ?>/conteudos/editar/<?php echo $conteudo->getId(); ?>" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Editar item" data-original-title="Editar item" ><i class="fa fa-pencil" aria-hidden="true"></i></a>
								<a href="#" data-id="<?php echo $conteudo->getId(); ?>" class="btn btn-danger btn-xs excluirRegistro" data-toggle="tooltip" data-placement="top" title="Excluir item" data-original-title="Excluir item"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>

