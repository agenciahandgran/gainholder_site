<?php

if(!isset($url[1])):

	if($_SERVER['REQUEST_METHOD'] == "POST"){

		$email = !empty($_POST['emailLogin']) ? $_POST['emailLogin'] : NULL;

		$login = new Login();
		$login->setEmail($email);

		$permissions = new Permissions($login);
		$resultado   = $permissions->recuperarSenha('painel');
	}
	?>

	<div>

		<a class="hiddenanchor" id="signup"></a>
		<a class="hiddenanchor" id="signin"></a>

		<div class="login_wrapper">
			<div class="animate form login_form">
				<section class="login_content">
					<form action="#" method="post" id="formulario-recuperar-senha">
						<h1>Recuperar senha</h1>
						<?php 
						if(isset($resultado)) {

							if(isset($resultado['error'])){
								echo logs($resultado['error'],'alert-danger');
							}else{
								echo logs($resultado['success'],'alert-success');
							} 	
						} 
						?>
						<div>
							<input type="text" name="emailLogin" id="emailLogin" class="form-control" placeholder="E-mail" required="required" />
						</div>
						<div>
							<a class="btn btn-default submit" href="#">Reenviar senha</a>
						</div>

						<div class="clearfix"></div>

						<div class="separator">

							<div class="clearfix"></div>
							<br />

							<div>
								<img src="<?php echo URL ?>/assets/img/upload/logo_png_cor.png" style="margin-bottom: 30px;">
								<p>© <?php echo date('Y'); ?> Todos os direitos reservados.</p>
							</div>
						</div>
					</form>
				</section>
			</div>
		</div>
	</div>

	<script>
		$(function(){

			$('#formulario-recuperar-senha .submit').click(function(e){

				e.preventDefault();

					// VALIDAÇÃO DO FORMULÁRIO DE CADASTRO
					$('#formulario-recuperar-senha').validate({
						rules:{
							emailLogin: {
								required: true,
								email: true
							}
						},
						messages:{
							emailLogin: {
								required: "Campo obrigatório.",
								email: "Digite um e-mail válido."
							}
						}
					});

					if($("#formulario-recuperar-senha").valid()){

						$("#formulario-recuperar-senha").submit();
					}

				});

		});
	</script>

<?php else: ?>

	<?php

	if($_SERVER['REQUEST_METHOD'] == "POST"){

		$senha = !empty($_POST['senhaLogin']) ? md5($_POST['senhaLogin']) : NULL;

		$login = new Login();
		$login->setSenha($senha);

		$permissions = new Permissions($login);
		$resultado   = $permissions->renovarSenha($url[1]);
	}
	?>

	<div>

		<a class="hiddenanchor" id="signup"></a>
		<a class="hiddenanchor" id="signin"></a>

		<div class="login_wrapper">
			<div class="animate form login_form">
				<section class="login_content">
					<form action="#" method="post" id="formulario-nova-senha">
						<h1>Nova senha</h1>
						<?php 
						if(isset($resultado)) {

							if(isset($resultado['error'])){

								echo logs($resultado['error'],'alert-danger');
							}else{

								echo logs($resultado['success'],'alert-success');
							}
						}
						?>
						<div>
							<input type="password" name="senhaLogin" id="senhaLogin" class="form-control" placeholder="Senha" required="required" />
						</div>
						<div>
							<a class="btn btn-default submit" href="#">Confirmar</a>
						</div>

						<div class="clearfix"></div>

						<div class="separator">

							<div class="clearfix"></div>
							<br />

							<div>
								<img src="<?php echo URL ?>/assets/img/upload/logo_png_cor.png" style="margin-bottom: 30px;">
								<p>© <?php echo date('Y'); ?> Todos os direitos reservados.</p>
							</div>
						</div>
					</form>
				</section>
			</div>
		</div>
	</div>
	<script>
		$(function(){

			$('#formulario-nova-senha .submit').click(function(e){

				e.preventDefault();

					// VALIDAÇÃO DO FORMULÁRIO DE CADASTRO
					$('#formulario-nova-senha').validate({
						rules:{
							senhaLogin: {
								required: true,
							}
						},
						messages:{
							emailLogin: {
								required: "Campo obrigatório.",
							}
						}
					});

					if($("#formulario-nova-senha").valid()){

						$("#formulario-nova-senha").submit();
					}

				});

		});
	</script>

<?php endif; ?>

