<div class="x_panel">
	<div class="x_title">
		<h2>Soluções <small><?php echo $legend; ?></small></h2>
		<div class="clearfix"></div>
	</div>

	<div class="x_content">
		<?php 
		
		if($resultado){

			if(isset($mensagem['success'])){	
				echo logs($mensagem['success'],'alert-success');	

				if(!isset($id)){
					echo "<script>setTimeout(function(){window.location='".URL.'/solucoes'."'} , 900);</script>";	
				}
			}else{
				echo logs($mensagem['error'],'alert-danger');
			}
		}	

		?>
		<br />
		<form id="formulario-solucao" method="post" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left" action="<?= URL ?>/solucoes/<?= $action ?>" novalidate>

			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="solucao_categoria">
					Categoria <span class="required">*</span>
				</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<select class="form-control col-md-7 col-xs-12" name="solucao_categoria" id="solucao_categoria" required="required">
						<option value="">Selecione uma categoria...</option>
						<?php if(!isset($categorias['error'])): ?>
							<?php foreach($categorias as $categoria): ?>
								<?php 
									if(isset($solucao)){
										$selected = ($categoria->getId() == $solucao->getCategoria()->getId()) ? 'selected' : ''; 
									}else{
										$selected = "";
									}
								?>
								<option value="<?php echo $categoria->getId(); ?>" <?php echo $selected; ?>><?php echo $categoria->getNome(); ?></option>
							<?php endforeach; ?>
						<?php endif; ?>					
					</select>
				</div>
			</div>

			<legend>Informações Banner</legend>

			<?php if(isset($solucao) && $solucao->getBanner() != null): ?>

				<div class="row">	
					<div class="col-md-55  col-md-offset-3">
						<div class="thumbnail">
							<div class="image view view-first">
								<img style="width: 100%; display: block;" src="<?php echo URL.'/'.URL_PATH_UPLOAD.'solucao_banners/'.$solucao->getBanner() ?>" alt="image" class="imagem-perfil"/>
								<div class="mask">
									<p><?php echo $solucao->getTitulo(); ?></p>
									<div class="tools tools-bottom">
										<a href="#" class="trocar-foto-perfil"><i class="fa fa-pencil"></i></a>
									</div>
								</div>
							</div>
							<div class="caption">
								<p>Banner atual</p>
							</div>
							<input id="solucao_banner" class="form-control col-md-7 col-xs-12" type="file" name="solucao_banner" style="display: none;">
						</div>
					</div>
				</div>
			
			<?php else: ?>
			
				<div class="form-group">
					<label for="solucao_banner" class="control-label col-md-3 col-sm-3 col-xs-12">Banner</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input id="solucao_banner" required="required" class="form-control col-md-7 col-xs-12" type="file" name="solucao_banner">
					</div>
				</div>
			
			<?php endif; ?>

			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="solucao_banner_titulo"> Banner titulo <span class="required">*</span>
				</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input type="text" name="solucao_banner_titulo" id="solucao_banner_titulo" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo isset($solucao) ? $solucao->getBannerTitulo() : ''; ?>">
				</div>
			</div>

			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="solucao_banner_descricao">Banner descrição <span class="required">*</span>
				</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<textarea class="form-control col-md-7 col-xs-12" id="solucao_banner_descricao" name="solucao_banner_descricao" cols="40" rows="8" required="required"><?php echo isset($solucao) ? $solucao->getBannerDescricao() : ''; ?></textarea>
				</div>
			</div>	

			<legend>Informações Gerais</legend>		

			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="solucao_titulo">Titulo <span class="required">*</span>
				</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input type="text" name="solucao_titulo" id="solucao_titulo" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo isset($solucao) ? $solucao->getTitulo() : ''; ?>">
				</div>
			</div>
			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div id="gender" class="btn-group" data-toggle="buttons">
						<label class="btn btn-<?php echo isset($solucao) ? ($solucao->getStatus() == '1' ? 'primary' : 'default') : 'default'; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
							<input type="radio" name="solucao_status" value="1" <?php echo isset($solucao) ? ($solucao->getStatus() == '1' ? 'checked="checked"' : '') : ''; ?>> &nbsp; Permitido &nbsp;
						</label>
						<label class="btn btn btn-<?php echo isset($solucao) ? ($solucao->getStatus() == '2' ? 'primary' : 'default') : 'default'; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
							<input type="radio" name="solucao_status" value="2" <?php echo isset($solucao) ? ($solucao->getStatus() == '2' ? 'checked="checked"' : '') : ''; ?>> Bloqueado
						</label>
					</div>
				</div>
			</div>

			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="conteudo_texto">Texto <span class="required">*</span>
				</label>
				<input type="hidden" name="solucao_texto" class="solucao_texto">
				<div class="col-md-6 col-sm-6 col-xs-12" style="margin-left: -10px;">
					<a class="btn btn-app" data-toggle="modal" data-target=".bs-example-modal-lg">
						<i class="fa fa-edit"></i> Editor
					</a>
					<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">

								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
									</button>
									<h4 class="modal-title" id="myModalLabel">Editor de texto</h4>
								</div>
								<div class="modal-body">
									<div class="x_content">
										<div id="alerts"></div>
										<div class="btn-toolbar editor" data-role="editor-toolbar" data-target="#editor-one">
											<div class="btn-group">
												<a class="btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="fa fa-font"></i><b class="caret"></b></a>
												<ul class="dropdown-menu">
													<li><a data-edit="fontName Serif" style="font-family:'Serif'">Serif</a></li><li><a data-edit="fontName Sans" style="font-family:'Sans'">Sans</a></li><li><a data-edit="fontName Arial" style="font-family:'Arial'">Arial</a></li><li><a data-edit="fontName Arial Black" style="font-family:'Arial Black'">Arial Black</a></li><li><a data-edit="fontName Courier" style="font-family:'Courier'">Courier</a></li><li><a data-edit="fontName Courier New" style="font-family:'Courier New'">Courier New</a></li><li><a data-edit="fontName Comic Sans MS" style="font-family:'Comic Sans MS'">Comic Sans MS</a></li><li><a data-edit="fontName Helvetica" style="font-family:'Helvetica'">Helvetica</a></li><li><a data-edit="fontName Impact" style="font-family:'Impact'">Impact</a></li><li><a data-edit="fontName Lucida Grande" style="font-family:'Lucida Grande'">Lucida Grande</a></li><li><a data-edit="fontName Lucida Sans" style="font-family:'Lucida Sans'">Lucida Sans</a></li><li><a data-edit="fontName Tahoma" style="font-family:'Tahoma'">Tahoma</a></li><li><a data-edit="fontName Times" style="font-family:'Times'">Times</a></li><li><a data-edit="fontName Times New Roman" style="font-family:'Times New Roman'">Times New Roman</a></li><li><a data-edit="fontName Verdana" style="font-family:'Verdana'">Verdana</a></li>
												</ul>
											</div>

											<div class="btn-group">
												<a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
												<ul class="dropdown-menu">
													<li>
														<a data-edit="fontSize 5">
															<p style="font-size:17px">Huge</p>
														</a>
													</li>
													<li>
														<a data-edit="fontSize 3">
															<p style="font-size:14px">Normal</p>
														</a>
													</li>
													<li>
														<a data-edit="fontSize 1">
															<p style="font-size:11px">Small</p>
														</a>
													</li>
												</ul>
											</div>

											<div class="btn-group">
												<a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
												<a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
												<a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
												<a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
											</div>

											<div class="btn-group">
												<a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
												<a class="btn" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
												<a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a>
												<a class="btn" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
											</div>

											<div class="btn-group">
												<a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
												<a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
												<a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
												<a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
											</div>

											<div class="btn-group">
												<a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="fa fa-link"></i></a>
												<div class="dropdown-menu input-append">
													<input class="span2" placeholder="URL" type="text" data-edit="createLink">
													<button class="btn" type="button">Add</button>
												</div>
												<a class="btn" data-edit="unlink" title="Remove Hyperlink"><i class="fa fa-cut"></i></a>
											</div>

											<div class="btn-group">
												<a class="btn" data-original-title="Insert picture (or just drag &amp; drop)" id="pictureBtn"><i class="fa fa-picture-o"></i></a>
												<input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" style="opacity: 0; position: absolute; top: 0px; left: 0px; width: 37px; height: 30px;">
											</div>

											<div class="btn-group">
												<a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
												<a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
											</div>
										</div>

										<div id="editor-one" class="editor-wrapper placeholderText" contenteditable="true"><?php echo isset($solucao) ? $solucao->getTexto() : ''; ?></div>

										<textarea name="descr" id="descr" style="display:none;"></textarea>

										<br>

										<div class="ln_solid"></div>

									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-primary" id="salvar-alteracoes-editor" disabled>Salvar Alterações</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	

			<div class="ln_solid"></div>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					<input type="hidden" name="modulo" value="solucao">
					<button type="submit" class="btn btn-success"><?php echo $title; ?></button>
					<a href="<?php echo URL ?>/solucoes" class="btn btn-info">Voltar</a>
				</div>
			</div>
		</form>
	</div>
</div>

