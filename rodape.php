        <div class="row foot2">
        	<div class="container containerFooter p0">
        		<footer class="footer" id="footer">
        			<div class="col-xs-12 p0 contentInfo">
        				<div class="col-xs-12 col-md-4 p0 divInfos">
        					<img src="<?php echo URL_SITE ?>/assets/img/logo-bco.png" />

        					<p><p>A Gainholder &eacute; especializada em importa&ccedil;&atilde;o e exporta&ccedil;&atilde;o, estamos focados em fornecer solu&ccedil;&otilde;es completas para projetos exigentes atrav&eacute;s de modelos de execu&ccedil;&atilde;o confi&aacute;veis, eficazes e flex&iacute;veis. Somos reconhecidos por completar desafios dentro de quadros or&ccedil;ament&aacute;rios rigorosos e hor&aacute;rios exigentes, dentro do prazo e com qualidade. Nossas entregas abrangem desde estudos conceituais e de viabilidade, engenharia operacional, aquisi&ccedil;&atilde;o, gerenciamento de plataformas e produtos.</p></p>
        					<!-- <h3 class="h31">Central de Atendimento</h3>
        					<h3 class="h32">SAC 0800 941 8020</h3>
        					<h3 class="h32">Capitais </h3>
        					<h3 class="h33"><a href="mailto:central@gainholder.com">central@gainholder.com</a></h3>
        					<h3 class="h32"><a href="mailto:offshore@gainholder.com">central@gainholder.com</a></h3> -->
        				</div>

        				<div class="col-xs-12 col-md-2 divPosts" style="display: none;">
        					<div class="col-xs-12 p0 sobre">
        						<h3>Sobre</h3>
        						<p><a href="http://localhost/gainholder.com.br/a-gainholder/">Empresa</a></p>
        						<p><a href="http://localhost/gainholder.com.br/trabalhe-conosco/">Trabalhe Conosco</a></p>
        						<p><a href="http://localhost/gainholder.com.br/seja-um-pointer/">Seja um Pointer</a></p>
        						<p><a href="http://localhost/gainholder.com.br/parceiros/">Parceiros</a></p>
        						<p><a href="http://localhost/gainholder.com.br/contato/">Entre em Contato</a></p>
        					</div>
        					<div class="col-xs-12 p0 sobre">
        						<h3>Revista Gainholder</h3>
        						<p><a href="http://localhost/clientes/gainholer/html/revista/julho-2017/">Julho 2017</a></p>
        					</div>
        				</div>
        				<div class="col-xs-12 col-md-2 divPosts" style="display: none;">
        					<div class="col-xs-12 p0 sobre">
        						<h3>Soluções</h3>
        						<p><a href="http://localhost/clientes/gainholer/html/solucoes/habilitacao/">Habilitação</a></p>
        						<p><a href="http://localhost/clientes/gainholer/html/solucoes/homologacao/">Homologação</a></p>
        						<p><a href="http://localhost/clientes/gainholer/html/solucoes/index/">Index</a></p>
        						<p><a href="http://localhost/clientes/gainholer/html/solucoes/excore/">Excore</a></p>
        						<p><a href="http://localhost/clientes/gainholer/html/solucoes/report/">Report</a></p>
        						<p><a href="http://localhost/clientes/gainholer/html/solucoes/host/">Host</a></p>
        						<p><a href="http://localhost/clientes/gainholer/html/solucoes/supply-chain-4pl/">Supply Chain 4PL</a></p>
        						<p><a href="http://localhost/clientes/gainholer/html/solucoes/end-2-end/">End-2-End</a></p>
        					</div>
        				</div>
        			
                                        <?php if(!empty($artigosRodape) && !isset($artigosRodape['error'])): ?>
                                                <div class="col-xs-12 col-md-4 divPosts">
                                                        <div class="col-xs-12 p0 sobre">
                                                                <h3>Últimos Artigos</h3>
                                                                <?php foreach($artigosRodape as $artigoRodape): ?>
                                                                        <p>
                                                                        <a href="<?php echo URL_SITE ?>/artigos/artigo/<?php echo $artigoRodape->getSlug(); ?>.html"> - <?php echo trim_text($artigoRodape->getTitulo(),50); ?></a>
                                                                        </p>
                                                                <?php endforeach; ?>
                                                        </div>
                                                </div> 
                                        <?php endif; ?>

                                        <div class="col-xs-12 col-md-4  p0 divPosts">
        					<h3 class="h31">Central de Atendimento</h3>
                                                <h3 class="h32">SAC 0800 941 8020</h3>
                                                <h3 class="h32">Capitais </h3>
                                                <h3 class="h33"><a href="#">central@gainholder.com</a></h3>
        				</div>

        			</div>
        		</footer>
        	</div>    </div>

        	<div class="row foot3">
        		<div class="container containerFooter p0">
        			<div class="col-xs-12 col-md-6 p0 sobre">
        				<div class="col-xs-12 col-md-3 p0">
        					<h3>Siga a Gainholder</h3>
        				</div>

        				<div class="col-xs-12 col-md-9 p0">
        					<ul>
        						<li><a href="https://www.facebook.com/" target="_blank"><span class="facebook"></span></a></li>
        						<li><a href="https://www.linkedin.com/" target="_blank"><span class="linkedin"></span></a></li>
        						<li><a href="https://twitter.com/" target="_blank"><span class="twitter"></span></a></li>
        						<li><a href="https://www.youtube.com/" target="_blank"><span class="youtube"></span></a></li>
        					</ul>
        				</div>

        			</div>
        			<div class="col-xs-12 col-md-6 idioma form-group" style="display: none;">
        				<select class="form-control">
        					<option value="pt">Português</option>
        					<option value="es">Inglês</option>
        					<option value="en">Espanhol</option>
        				</select>
        			</div>
        		</div>
        		<div class="container foot4"></div>
        	</div>

                
<script>
    //REALIZAR BUSCA
    $(".avisoTML").click(function(){

        $(".modalTermos").show();

    });
    $(".modalTermos").click(function(){

        $(this).fadeOut();

    });
    $(".closeModal").click(function(){

        $(".modalTermos").fadeOut();

    });
</script>