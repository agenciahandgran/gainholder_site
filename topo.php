     

<div class="row rowHeader" id="home">
    <div class="container divHeader navbar-expanded navbar-fixed-top p0">
        <!-- header -->
        <header class="header clear container" role="banner">
            <div class="col-xs-12 col-md-3 p0 divH1">
                <!-- BTN MOBILE -->
                <div class="divMenuMobile hidden-sm hidden-md hidden-lg">
                    <span class="menuMobile fa fa-bars" aria-hidden="true"></span>
                </div>
                <!-- logo -->
                <div class="logo col-xs-12 p0">
                    <a href="<?php echo URL_SITE ?>">
                        <!-- svg logo - toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script -->
                        <img src="<?php echo URL_SITE ?>/assets/img/logo.png" alt="Decisão" class="logo-img">
                    </a>
                </div>

                <!-- MENU MOBILE -->
                <div class="navMobile col-xs-12 col-sm-9 p0 ">
                    <nav class="nav navbar navbar-default col-xs-12" role="navigation">
                        <div class="menu-principal-container"><ul id="menu-principal" class="menu"><li id="menu-item-64" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-64"><span><a href="<?php echo URL_SITE ?>/institucionais">A Gainholder</a></span></li>
                           <!--  <li id="menu-item-69" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-69"><span><a href="http://localhost/clientes/gainholer/html/categoria-solucoes/" style="color:#ccc;">Soluções</a></span>
                                <ul  class="sub-menu">
                                    <li id="menu-item-70" class="menu-item menu-item-type-taxonomy menu-item-object-categoria-solucoes menu-item-has-children menu-item-70"><span><a href="http://localhost/clientes/gainholer/html/categoria-solucoes/exclusivo-gainholder/">Exclusivo Gainholder</a></span>
                                        <ul  class="sub-menu">
                                            <li id="menu-item-87" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-87"><span><a href="http://localhost/clientes/gainholer/html/solucoes/habilitacao/">Habilitação</a></span></li>
                                            <li id="menu-item-88" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-88"><span><a href="http://localhost/clientes/gainholer/html/solucoes/homologacao/">Homologação</a></span></li>
                                            <li id="menu-item-89" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-89"><span><a href="http://localhost/clientes/gainholer/html/solucoes/index/">Index</a></span></li>
                                            <li id="menu-item-86" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-86"><span><a href="http://localhost/clientes/gainholer/html/solucoes/excore/">Excore</a></span></li>
                                            <li id="menu-item-106" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-106"><span><a href="http://localhost/clientes/gainholer/html/solucoes/report/">Report</a></span></li>
                                            <li id="menu-item-105" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-105"><span><a href="http://localhost/clientes/gainholer/html/solucoes/host/">Host</a></span></li>
                                            <li id="menu-item-104" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-104"><span><a href="http://localhost/clientes/gainholer/html/solucoes/supply-chain-4pl/">Supply Chain 4PL</a></span></li>
                                            <li id="menu-item-103" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-103"><span><a href="http://localhost/clientes/gainholer/html/solucoes/end-2-end/">End-2-End</a></span></li>
                                        </ul>
                                    </li>
                                    <li id="menu-item-72" class="menu-item menu-item-type-taxonomy menu-item-object-categoria-solucoes menu-item-has-children menu-item-72"><span><a href="http://localhost/clientes/gainholer/html/categoria-solucoes/para-importacao/">Para Importação</a></span>
                                        <ul  class="sub-menu">
                                            <li id="menu-item-186" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-186"><span><a href="http://localhost/clientes/gainholer/html/solucoes/habilitacao/">Habilitação</a></span></li>
                                            <li id="menu-item-95" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-95"><span><a href="http://localhost/clientes/gainholer/html/solucoes/pesquisa-de-mercado/">Pesquisa de Mercado</a></span></li>
                                            <li id="menu-item-109" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-109"><span><a href="http://localhost/clientes/gainholer/html/solucoes/conta-e-ordem/">Conta e Ordem</a></span></li>
                                            <li id="menu-item-108" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-108"><span><a href="http://localhost/clientes/gainholer/html/solucoes/encomenda/">Encomenda</a></span></li>
                                            <li id="menu-item-107" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-107"><span><a href="http://localhost/clientes/gainholer/html/solucoes/classificacao-fiscal/">Classificação Fiscal</a></span></li>
                                        </ul>
                                    </li>
                                    <li id="menu-item-71" class="menu-item menu-item-type-taxonomy menu-item-object-categoria-solucoes menu-item-has-children menu-item-71"><span><a href="http://localhost/clientes/gainholer/html/categoria-solucoes/para-exportacao/">Para Exportação</a></span>
                                        <ul  class="sub-menu">
                                            <li id="menu-item-119" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-119"><span><a href="http://localhost/clientes/gainholer/html/solucoes/homologacao-2/">Homologação</a></span></li>
                                            <li id="menu-item-118" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-118"><span><a href="http://localhost/clientes/gainholer/html/solucoes/exportacao-direta/">Exportação Direta</a></span></li>
                                            <li id="menu-item-117" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-117"><span><a href="http://localhost/clientes/gainholer/html/solucoes/exportacao-indireta/">Exportação Indireta</a></span></li>
                                            <li id="menu-item-115" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-115"><span><a href="http://localhost/clientes/gainholer/html/solucoes/inspecao-de-embarque/">Inspeção de Embarque</a></span></li>
                                        </ul>
                                    </li>
                                    <li id="menu-item-73" class="menu-item menu-item-type-taxonomy menu-item-object-categoria-solucoes menu-item-has-children menu-item-73"><span><a href="http://localhost/clientes/gainholer/html/categoria-solucoes/segmentados/">Segmentados</a></span>
                                        <ul  class="sub-menu">
                                            <li id="menu-item-129" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-129"><span><a href="http://localhost/clientes/gainholer/html/solucoes/carros-de-luxo/">Carros de Luxo</a></span></li>
                                            <li id="menu-item-128" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-128"><span><a href="http://localhost/clientes/gainholer/html/solucoes/commodities/">Commodities</a></span></li>
                                            <li id="menu-item-127" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-127"><span><a href="http://localhost/clientes/gainholer/html/solucoes/energia/">Energia</a></span></li>
                                            <li id="menu-item-126" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-126"><span><a href="http://localhost/clientes/gainholer/html/solucoes/maquinarios/">Maquinários</a></span></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li> -->
                            <li id="menu-item-150" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-150"><span><a href="<?php echo URL_SITE ?>/conteudos">Conteúdos</a></span></li>
                            <li id="menu-item-65" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-65"><span><a href="<?php echo URL_SITE ?>/artigos">Artigos</a></span></li>
                            <li id="menu-item-66" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-66"><span><a href="<?php echo URL_SITE ?>/contatos">Contato</a></span></li>
                            <li id="menu-item-187" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-187"><span><a href="<?php echo URL_SITE ?>/assinantes">Seja um Assinante</a></span></li>
                        </ul>
                    </div>
                </nav>
            </div>                    
            <!-- /logo -->
        </div>
        <div class="col-xs-12 col-md-9 p0">
            <nav class="nav navbar navbar-default hidden-xs" role="navigation">
                <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse"><ul id="menu-principal-1" class="nav navbar-nav"><li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-64"><a title="A Gainholder" href="<?php echo URL_SITE ?>/institucionais" <?php echo ($_GET['pag'] == "institucionais" ? "style='color:#d81616'" : ""); ?>>A Gainholder</a></li>
                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-69 dropdown"><a title="Soluções" href="#" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Soluções <span class="caret"></span></a>
                        <ul role="menu" class=" dropdown-menu" >
                            <li itemscope="itemscope" class="menu-item menu-item-type-taxonomy menu-item-object-categoria-solucoes menu-item-has-children menu-item-70 dropdown"><a title="Exclusivo Gainholder" href="http://localhost/clientes/gainholer/html/categoria-solucoes/exclusivo-gainholder/">Exclusivo Gainholder</a>
                                <ul role="menu" class=" dropdown-menu" >
                                    <li itemscope="itemscope" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-89"><a title="Index" href="<?php echo URL_SITE ?>/solucoes/index">Index</a></li>
                                    <li itemscope="itemscope" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-86"><a title="Excore" href="<?php echo URL_SITE ?>/solucoes/vanco">Vamco</a></li>
                                </ul>
                            </li>
                            <li style="display: none;" itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-taxonomy menu-item-object-categoria-solucoes menu-item-has-children menu-item-72 dropdown"><a title="Para Importação" href="http://localhost/clientes/gainholer/html/categoria-solucoes/para-importacao/">Para Importação</a>
                                <ul role="menu" class=" dropdown-menu" >
                                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-186"><a title="Habilitação" href="http://localhost/clientes/gainholer/html/solucoes/habilitacao/">Habilitação</a></li>
                                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-95"><a title="Pesquisa de Mercado" href="http://localhost/clientes/gainholer/html/solucoes/pesquisa-de-mercado/">Pesquisa de Mercado</a></li>
                                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-109"><a title="Conta e Ordem" href="http://localhost/clientes/gainholer/html/solucoes/conta-e-ordem/">Conta e Ordem</a></li>
                                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-108"><a title="Encomenda" href="http://localhost/clientes/gainholer/html/solucoes/encomenda/">Encomenda</a></li>
                                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-107"><a title="Classificação Fiscal" href="http://localhost/clientes/gainholer/html/solucoes/classificacao-fiscal/">Classificação Fiscal</a></li>
                                </ul>
                            </li>
                            <li style="display: none;" itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-taxonomy menu-item-object-categoria-solucoes menu-item-has-children menu-item-71 dropdown"><a title="Para Exportação" href="http://localhost/clientes/gainholer/html/categoria-solucoes/para-exportacao/">Para Exportação</a>
                                <ul role="menu" class=" dropdown-menu" >
                                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-119"><a title="Homologação" href="http://localhost/clientes/gainholer/html/solucoes/homologacao-2/">Homologação</a></li>
                                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-118"><a title="Exportação Direta" href="http://localhost/clientes/gainholer/html/solucoes/exportacao-direta/">Exportação Direta</a></li>
                                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-117"><a title="Exportação Indireta" href="http://localhost/clientes/gainholer/html/solucoes/exportacao-indireta/">Exportação Indireta</a></li>
                                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-115"><a title="Inspeção de Embarque" href="http://localhost/clientes/gainholer/html/solucoes/inspecao-de-embarque/">Inspeção de Embarque</a></li>
                                </ul>
                            </li>
                            <li style="display: none;" itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-taxonomy menu-item-object-categoria-solucoes menu-item-has-children menu-item-73 dropdown"><a title="Segmentados" href="http://localhost/clientes/gainholer/html/categoria-solucoes/segmentados/">Segmentados</a>
                                <ul role="menu" class=" dropdown-menu" >
                                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-129"><a title="Carros de Luxo" href="http://localhost/clientes/gainholer/html/solucoes/carros-de-luxo/">Carros de Luxo</a></li>
                                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-128"><a title="Commodities" href="http://localhost/clientes/gainholer/html/solucoes/commodities/">Commodities</a></li>
                                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-127"><a title="Energia" href="http://localhost/clientes/gainholer/html/solucoes/energia/">Energia</a></li>
                                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-post_type menu-item-object-solucoes menu-item-126"><a title="Maquinários" href="http://localhost/clientes/gainholer/html/solucoes/maquinarios/">Maquinários</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-150"><a title="Conteúdos" href="<?php echo URL_SITE ?>/conteudos" <?php echo ($_GET['pag'] == "conteudos" ? "style='color:#d81616'" : ""); ?>>Conteúdos</a></li>
                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-65"><a title="Artigos" href="<?php echo URL_SITE ?>/artigos" <?php echo ($_GET['pag'] == "artigos" ? "style='color:#d81616'" : ""); ?>>Artigos</a></li>
                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-66"><a title="Contato" href="<?php echo URL_SITE ?>/contatos" <?php echo ($_GET['pag'] == "contatos" ? "style='color:#d81616'" : ""); ?>>Contato</a></li>
                    <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-187"><a title="Seja um Assinante" href="<?php echo URL_SITE ?>/assinantes" <?php echo ($_GET['pag'] == "assinantes" ? "style='color:#d81616'" : ""); ?>>Seja um Assinante</a></li>
                </ul></div>								
                <div class="col-xs-12 col-md-3 p0 menu-assinantes">
                    <ul>
                        <li>
                            <?php if(!isset($_SESSION['cliente_site']) && !isset($_COOKIE['clienteCookie'])): ?>
                                <a href="<?php echo URL_SITE ?>/clientes/login">Login</a> | <a href="<?php echo URL_SITE ?>/clientes/cadastro">Cadastro</a>
                            <?php else: ?>

                                <?php if(isset($_SESSION['cliente_site'])): ?>
                                    <p>Olá, <strong><a href="<?php echo URL_SITE ?>/clientes/perfil" style="font-weight: bolder;"><?php echo substr($_SESSION['cliente_site']['nome'], 0,strpos($_SESSION['cliente_site']['nome'], " ")); ?></a></strong></p>
                                    <p style="text-align: right; display: none;"><?php echo $_SESSION['cliente_site']['pontuacao']; ?>&nbsp;GHP</p>
                                <?php else:  
                                    $clienteCookie = unserialize($_COOKIE['clienteCookie']);
                                ?>
                                    <p>Olá, <strong><a href="<?php echo URL_SITE ?>/clientes/perfil" style="font-weight: bolder;"><?php echo substr($clienteCookie['nome'], 0,strpos($clienteCookie['nome'], " ")); ?></a></strong></p>
                                    <p style="text-align: right; display: none;"><?php echo $clienteCookie['pontuacao']; ?>&nbsp;GHP</p>
                                <?php endif; ?>

                            <?php endif; ?>
                        </li>
                        <li>
                            <div class="formBusca">
                                <form role="search" method="post" class="search-form" action="">
                                    <div class="input-busca">
                                        <input type="search" class="search-field" id="palavra-chave" placeholder="Busca..." name="palavra-chave" title="Pesquisar">
                                    </div>
                                    <div class="botao-busca">
                                        <input type="button" class="search-submit" id="busca-site" value="">
                                    </div>
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav> 
        </div>
    </header>
</div>
</div>


<script >
    $( ".divMenuMobile" ).click(function() {
        $(".header .navMobile").slideToggle();
    });

    $(document).ready(function(){
        $(document).keypress(function(e) {
            if(e.which == 13) {
                e.preventDefault();
                $("#busca-site").trigger("click");
            }
        });
    });

    $(function() {

        //REALIZAR BUSCA
        $("#busca-site").click(function(){

            var nome  = $("#palavra-chave").val()  != "" ? $("#palavra-chave").val()  : "indefinido";
            
            window.location = "<?php echo URL_SITE; ?>/home/pesquisa/"+nome;

        });

       
    });

</script>