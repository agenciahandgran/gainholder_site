<div class="container">
	<div class="col-xs-12 breadcrumbs hidden-xs" typeof="BreadcrumbList" vocab="http://schema.org/">
		<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to Gainholder Inglês." href="<?php echo URL_SITE; ?>" class="home"><span property="name">Home</span></a><meta property="position" content="1"></span> | <span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to Artigos." href="<?php echo URL_SITE; ?>/artigos" class="post post-artigos-archive"><span property="name">Artigos</span></a><meta property="position" content="2"></span> | <span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to the Exterior Categorias Artigos archives." href="<?php echo URL_SITE ?>/home/pesquisa/<?php echo $artigo->getCategoria()->getNome(); ?>" class="taxonomy categorias-artigos"><span property="name" ><?php echo $artigo->getCategoria()->getNome(); ?></span></a><meta property="position" content="3"></span> | <span property="itemListElement" typeof="ListItem"><span property="name" style="color: #e30613"><?php echo $artigo->getTitulo(); ?></span><meta property="position" content="4"></span>		

		<input type="hidden" name="redirecionamento" id="redirecionamento" value="<?php echo $artigo->getSlug(); ?>">
		<input type="hidden" name="nivelAcesso" id="nivelAcesso" value="<?php echo $artigo->getNivel(); ?>">			
	</div>
</div>

<div class="row conteudo-home">
	<div role="main" class="container conteudo-home internas categorias artigos conteudo p0">
		<section>
			<div class="col-xs-12 col-sm-12 p0">
				<div class="col-xs-12 col-sm-8 p0">
					<div class="col-xs-12 texto">
						<div class="col-xs-12 destaque">
							<div class="img-destaque col-xs-12 p0">
								<img width="620" height="360" src="<?php echo URL_SITE ?>/painel/assets/img/upload/artigos/banners/<?php echo $artigo->getBanner(); ?>" class="img-responsive wp-post-image" alt="" />
							</div>
							<div class="col-xs-12 col-md-12 conteudo-list p0">

								<div class="titulo-destaque p0">
									<div class="col-xs-12 p0">
										<div class="div-categoria-internas p0">
											<?php echo $artigo->getCategoria()->getNome(); ?></div>
										</div>
										<a href="index.html">
											<p>
												<?php echo $artigo->getTitulo(); ?>					
											</p>
										</a>
									</div>
									<div class="col-xs-12 p0">
										<p class="date-conteudo-home"><span>Por <span><?php echo $artigo->getAutor()->getNomeCompleto(); ?></span><?php echo formatarDataExtenso(date('d/m/Y',strtotime($artigo->getDataPublicacao())),'d'); ?></span> | Tempo de leitura: <span class="tempo_leiura"> <?php echo estimarTempoLeitura($artigo->getTexto()); ?>
										</span></p>
										<hr>
									</div>
									<div class="excerpt-content-home disable-select">
										<div class="textoSingle paragrafoSingle">
											<?php echo $artigo->getTexto(); ?>
										</div>
									</div>
									<div class="bg-bco"></div>
								</div>

							</a>
						</div>

					</div>
					<div class="col-xs-12 btn-load-more-content">
						<input type="button" name="load-conteudo" value="Continuar Lendo" class="btn-banner btn-default btn-continuar-lendo" />
					</div>
					<div class="col-xs-12 col-md-9 div-relacionados">
						
						<?php if(isset($artigosRelacionados) && !isset($artigosRelacionados['error'])): ?>

							<h3>Artigos Relacionados</h3>

							<?php foreach($artigosRelacionados as $artigoRelacionado): ?>
								<?php if($artigoRelacionado->getId() != $artigo->getId()): ?>
									<div class="col-xs-12 col-md-6 content-relacionados padLeft">
										<p class="post-content">
											<a href="<?php echo URL_SITE ?>/artigos/artigo/<?php echo $artigoRelacionado->getSlug() ?>.html"><?php echo $artigoRelacionado->getTitulo(); ?></a>
										</p>
										<div class="post-excerpt">
											<p><?php echo $artigoRelacionado->getResumo(); ?></p>
										</div>
										<div class="post-excerpt date"><?php echo formatarDataExtenso(date('d/m/Y', strtotime($artigoRelacionado->getDataPublicacao())));?></div>
									</div>
								<?php endif; ?>
							<?php endforeach; ?>

						<?php endif; ?>

					</div>
				</div>

				<div class="col-xs-12 col-sm-4 p0">
					<div class="col-xs-12 p0 texto">
						<div class="col-xs-12 p0 div-artigo-home colunista">

							<div class="col-xs-12 artigo-list colunista">
								<div class="imagem">
									<img width="70" height="70" src="<?php echo URL_SITE ?>/painel/assets/img/upload/colunista_perfil/<?php echo $artigo->getAutor()->getFotoPerfil(); ?>" class="attachment-colunista_capa size-colunista_capa wp-post-image" alt="" />
								</div>
								<div class="titulo-artigo colunista">
									<a href="#"><p><span><?php echo $artigo->getAutor()->getNomeCompleto(); ?></span></p></a>
									<p class="excerpt-artigo-home artigo">
										<?php echo $artigo->getAutor()->getDescricao(); ?>
									</p>
								</div>
							</div>

						</div>

						<div class="col-xs-12 p0 outros-posts div-artigo-home ">
							<div class="col-xs-12 relacionado">
								<h3>Todos os artigos deste autor</h3>
								<h4>Últimos artigos de <?php echo $artigo->getAutor()->getNomeCompleto(); ?></h4>
								<?php if(isset($artigosAutor) && !isset($artigosAutor['error'])): ?>
									<div class="col-xs-12 p0 div-artigo-home">
										<?php foreach($artigosAutor as $artigoAutor):?>
											<div class="categoria col-xs-12 p0">
												<?php echo $artigoAutor->getCategoria()->getNome(); ?>		
											</div>
											<div class="col-xs-12 artigo-list p0">
												<div class="titulo-artigo">
													<a href="<?php echo URL_SITE ?>/artigos/artigo/<?php echo $artigoAutor->getSlug() ?>.html">
														<p><?php echo $artigoAutor->getTitulo(); ?></p>
													</a>
													<p class="date-artigo-home" style=" font-weight: normal !important;  height: auto !important;"><?php echo formatarDataExtenso(date('d/m/Y', strtotime($artigoAutor->getDataPublicacao())));?></p>
												</div>
											</div>
										<?php endforeach; ?>
									</div>
								<?php endif; ?>

								</div>
							</div>
						</div>
						<div class="col-xs-12 p0 banner-conteudo-2">
							<img src="<?php echo URL_SITE ?>/assets/img/banner-conteudo-2.jpg" class="img-responsive">
						</div>
					</div>
				</div>			



			</section>


		</div>
	</div>

	<script type="text/javascript">
		$(function(){

			$('.btn-continuar-lendo').on('click',function(){

				var URL           = "<?php echo URL_SITE; ?>";
				var botaoLeiaMais = $(this);
				var redirecionar  = $("#redirecionamento").val();
				var grau          = $("#nivelAcesso").val();

				$.ajax({
					url: URL+"/library/Requests.php",
					type: 'GET',
					data : {
						modulo : 'artigo',
						acao   : 'detalhe',
						grau   : grau
					},
					dataType: 'json',
					cache: false,
					success: function (resp) {

						if(typeof(resp) != "undefined" && resp != null){	

							if(resp.status){

								$('.conteudo-list').addClass('expand');
								$('.bg-bco').remove();
								botaoLeiaMais.remove();

							}else{

								var botaoTexto = "";

								if(resp.nivel == 1){
									botaoTexto = 'Login';
								}

								if(resp.nivel == 3){
									botaoTexto = 'Assinar';
								}

								if(resp.nivel == 4){
									botaoTexto = 'Verificar plano';
								}
							
								swal({

									title: 'Informação',
									text: resp.mensagem,
									type: 'warning',
									showCancelButton: true,
									confirmButtonColor: '#3085d6',
									cancelButtonColor: '#d33',
									confirmButtonText: botaoTexto,
									cancelButtonText: 'Cancelar',
									confirmButtonClass: 'btn btn-success',
									cancelButtonClass: 'btn btn-danger',
									buttonsStyling: false

								}).then(function () {

									if(resp.nivel == 1){
										window.location = URL+"/clientes/login/artigos/"+redirecionar;
									}

									if(resp.nivel == 3 || resp.nivel == 4){
										window.location = URL+"/assinantes";
									}

								}, function (dismiss) {

									if (dismiss === 'cancel') {
										swal(
											'Cancelado',
											'Operação cancelada pelo usuário.',
											'error'
											);
									}
								});
							}

						}else{
							console.log('Ocorreu um erro na requisição');
						}

					},

					error: function(resp) {
						console.log('Ocorreu um erro na resposta');
					}

				});
			});
		});

	</script>