<link rel="stylesheet" type="text/css" href="<?php echo URL_SITE ?>/assets/css/style5152.css">

<style type="text/css">
.plano-nome{
    text-align: left;
    font-size: 25px;
    margin-top: 0px;
}
.plano-preco-titulo{
    text-align: left;
    font-size: 14px;
    color:#848080!important;
}
.modal table{
    width: 525px;
}
.modal th{
    text-align: left;
    font-size: 14px;
    color:#848080!important;  
    padding: 12px;
}

.modal tr > th{
    border-bottom: #aaa 1px solid;
}

.modal td{
    text-align: left;
    padding: 12px;
    font-size: 14px;
    width: 115px;
    line-height: 20px;
    font-weight: bold;
}

#all{
    width: 935px!important;
    margin-left: -170px;
}

.termos-uso-modal{
    text-align: left;
    margin-top: 10px;
    font-size: 14px;
    color: #949393;
}

.termos-uso-modal a{
    text-decoration: underline;
}

.background-loader{
    display: block;
    z-index: 10000;
    background: #00000070;
    position: fixed;
    width: 100%;
    height: 100%;
    left: 0;
    top: 0;
    overflow: auto;
}

.background-loader p{
    font-size: 20px;
    font-family: 'Robot', sans-serif;
    text-align: center;
    color: #fff;
    font-weight: bolder;
    text-transform: uppercase;
}

.loader {
    border: 16px solid #f3f3f3; /* Light grey */
    border-top: 16px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 120px;
    height: 120px;
    animation: spin 2s linear infinite;
    margin: auto;
    margin-top: 25%;    
}

@keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
}

</style>

<div class="row divHeader">


    <div class="row">
        <div class="col-xs-12 p0 boxBanner bannerCapa hidden-xs">
            <div class="col-xs-12 p0 container-banner">
                <aside id="banner_capa" style="width:100%" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <div class="carousel-inner">
                        <div class="item active" style="background-image:url('<?php echo URL_SITE ?>/assets/img/banner-assinante.jpg'); background-repeat:no-repeat; background-position:center top; background-size:cover; height:385px;width: 100%; margin: 0 auto; border: none;">

                            <div class="container container-banners">

                                <div class="banner-info container">
                                    <div class="col-xs-12 col-sm-8">
                                        <h4>Seja um Assinante Premium</h4>
                                        <p>Fique a frente do mercado, visualize oportunidades e saiba como tomar as melhores decisões.</p>
                                    </div>                      
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Controls -->
                </aside>
            </div>
        </div>
    </div>
</div>


<?php if(isset($planos) && !isset($planos['error'])): ?>

    <div class="row azul" style="display: none;">
        <div class="container p0">
            <div class="col-xs-12 p0 div-planos-azul">
                <h3 class="assinatura">Assinaturas Gainholder</h3>

                <?php foreach ($planos as $plano): ?>
                    <div class="col-xs-12 col-md-3 div-plano">
                        <div class="col-xs-12 p0 azul">
                            <p class="assinatura">Assinatura</p>
                            <h2><?php echo $plano->getNome(); ?></h2>
                            <div class="col-xs-12 p0">
                                <p class="valor">
                                    <span class="cifrao">R$</span>
                                    <span>
                                        <?php 
                                        if(strstr($plano->getPreco(), ".")){
                                            echo substr($plano->getPreco(), 0, strpos($plano->getPreco(), "."));
                                        }else{
                                            echo $plano->getPreco();
                                        }    
                                        ?>
                                    </span>
                                    <span class="mes">/mês</span>
                                </p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

<?php endif; ?>

</div>

<div class="row conteudo-home seja-assinante">
    <div role="main" class="container conteudo-home internas categorias assinante p0">
        <section>
            <h3 class="assinatura">Assinaturas Gainholder</h3>
            <div class="col-xs-12 texto p0">
                <div class="col-xs-12 p0 div-planos-azul interno" style="display: none;">
                    <?php if(isset($planos) && !isset($planos['error'])): ?>
                        <?php foreach($planos as $plano): ?>
                            <div class="col-xs-12 col-md-3 div-plano interno ">
                                <div class="col-xs-12 p0 conteudo">
                                    <ul>
                                        <li style="display: none;">C.I Limitado a 5 meses</li>
                                        <li style="display: none;">C.E Limitado a 5 meses</li>
                                        <li>Conteúdos pagos Grau <?php echo $plano->getGrau(); ?></li>
                                    </ul>
                                </div>
                                <div class="col-xs-12 p0 botao">
                                    <a href="#" class="btn btn-primary btn-block text-uppercase modal-pagamento" data-modal="modal-name" data-id="<?php echo $plano->getId(); ?>">Assine agora</a>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>


                <div class="col-xs-12 p0 div-banner" style="display: none;">
                    <div class="col-xs-12 col-md-3">
                        <p class="tituloBanner"><span>Assinatura</span>Enterprise</p>
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <p class="tituloBanner valor"><span class="cifrao">R$</span><span class="preco">1500</span><span class="mes">/mês</span></p>
                    </div>
                    <div class="col-xs-12 col-md-3 div-msg-banner">
                    Fique a frente do mercado e saiba como tomas as melhores decisões	                	</div>
                    <div class="col-xs-12 col-md-3">
                        <a href="#" class="btn btn-primary btn-block text-uppercase">Saiba mais</a>
                    </div>
                </div>
                <input type="hidden" name="planoAtual" id="planoAtual" value="<?php echo $planoAtual; ?>">
                <div class="table-responsive">
                    <table class="table">
                        <colgroup></colgroup>
                        <colgroup></colgroup>
                        <colgroup></colgroup>
                        <colgroup></colgroup>
                        <colgroup></colgroup>
                        <colgroup></colgroup>
                        <thead>
                            <tr>
                                <th class="primeira">&nbsp;</th>
                                <th><h2>Free</h2></th>
                                <?php if(isset($planos) && !isset($planos['error'])): ?>
                                    <?php foreach($planos as $plano): ?>
                                        <th>
                                            <h2><?php echo $plano->getNome(); ?></h2>
                                        </th>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <th class="recursos">Visualização Site Grau 1*</th>
                                <td></td>

                                <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                <td><i class="fa fa-check" aria-hidden="true"></i></td>
                            </tr>
                            <tr>
                                <th class="recursos">Visualização Site Grau 2**</th>
                                <td></td>

                                <td></td>
                                <td></td>
                                <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                <td><i class="fa fa-check" aria-hidden="true"></i></td>
                            </tr>
                            <tr>
                                <th class="recursos">Visualização Site Grau 3***</th>
                                <td></td>

                                <td></td>
                                <td></td>
                                <td></td>
                                <td><i class="fa fa-check" aria-hidden="true"></i></td>
                            </tr>
                            <tr>
                                <th class="recursos">Custo de Importação<br><span>(C.I.)</span></th>
                                <td class="custos">Limitado a 1 mês</td>

                                <td class="custos">Limitado a 5 meses</td>
                                <td class="custos">Limitado a 10 meses</td>
                                <td class="custos">Limitado a 10 meses</td>
                                <td class="custos">Limitado a 20 meses</td>
                            </tr>
                            <tr>
                                <th class="recursos">Custo de Exportação<br><span>(C.E.)</span></th>
                                <td class="custos">Limitado a 1 mês</td>

                                <td class="custos">Limitado a 5 meses</td>
                                <td class="custos">Limitado a 10 meses</td>
                                <td class="custos">Limitado a 15 meses</td>
                                <td class="custos">Limitado a 20 meses</td>
                            </tr>
                            <tr>
                                <th class="recursos">Desconto</th>
                                <td>&nbsp;</td>
                                <?php if(isset($planos) && !isset($planos['error'])): ?>
                                    <?php foreach($planos as $plano): ?>
                                        <td><span class="descontoSEP"><?php echo $plano->getDesconto(); ?>%</span></td>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tr>


                        </tbody>
                        <tfoot>

                            <tr>
                                <th class="branco"></th>

                                <td>

                                </td>

                                <?php if(isset($planos) && !isset($planos['error'])): ?>
                                    <?php foreach($planos as $plano): ?>
                                        <td class="valor">R$ <?php echo number_format($plano->getPreco(),2,',','.'); ?><span>/mês</span></td>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tr>
                            <tr>
                                <th class="branco"></th>
                                <td class="branco">

                                </td>
                                <?php if(isset($planos) && !isset($planos['error'])): ?>
                                    <?php foreach($planos as $plano): ?>
                                        <td>
                                            <?php if($plano->getId() > $planoAtual): ?>
                                                <a href="#" class="btn btn-primary btn-block text-uppercase modal-pagamento" data-modal="modal-name" data-id="<?php echo $plano->getId(); ?>">Assinar</a>
                                            <?php endif; ?>
                                        </td>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tr>
                        </tfoot>
                    </table>
                </div>


                <div class="col-xs-12 div-relacionados assine">
                    <div class="col-xs-12 col-md-4 content-relacionados">
                        <p class="post-content">
                            * Visualização Site Grau 1
                        </p>
                        <div class="post-excerpt">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean egestas leo dolor, vitae imperdiet felis condime
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4 content-relacionados">
                        <p class="post-content">
                            * Visualização Site Grau 2
                        </p>
                        <div class="post-excerpt">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean egestas leo dolor, vitae imperdiet felis condime
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4 content-relacionados">
                        <p class="post-content">
                            * Visualização Site Grau 3
                        </p>
                        <div class="post-excerpt">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean egestas leo dolor, vitae imperdiet felis condime
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>
</div>

<link rel="stylesheet" type="text/css" href="<?php echo URL_SITE ?>/assets/css/modal-pagamento.css">

<!-- Modal -->
<div class="modal" id="modal-name">
    <!-- <div class="modal-sandbox"></div> -->
    
    <div class="modal-box">

        <div id="all"><div id="fechar" class="close-modal">     Fechar X </div>
        <?php if(isset($_SESSION['cliente_site']) || isset($_COOKIE['clienteCookie'])): ?>
            <?php  
            if(isset($_SESSION['cliente_site'])){
                $clienteUsuario       = $_SESSION['cliente_site']['email'];
            }else{
                $clienteCookie   = unserialize($_COOKIE['clienteCookie']);
                $clienteUsuario  = $clienteCookie['usuario'];
            }
            ?>
            <div id="header">
                <div id="name03"><img src="<?php echo URL_SITE ?>/assets/img/profile.png" style="width:100%;"></div>
                <div id="name04"><?php echo $clienteUsuario; ?><br>
                    <label style="font-size:12px;"><a href="<?php echo URL_SITE ?>/clientes/logout">logout</a></label>
                </div>  
            </div>
        <?php endif; ?>
        <div id="body">
            <div class="container">
                <div class="sessao-assinatura">
                    <div class="row form-group">
                        <div class="col-xs-9">
                            <ul class="nav nav-pills nav-justified thumbnail setup-panel">
                                <li class="active"><a href="#step-1">
                                    <h4 class="list-group-item-heading">Passo 1</h4>
                                    <p class="list-group-item-text">Detalhes Assinatura</p>
                                </a></li>
                                <li class="disabled"><a href="#step-2">
                                    <h4 class="list-group-item-heading">Passo 2</h4>
                                    <p class="list-group-item-text">Confirme seus dados</p>
                                </a></li>
                                <li class="disabled"><a href="#step-3">
                                    <h4 class="list-group-item-heading">Passo 3</h4>
                                    <p class="list-group-item-text">Dados para efetuar o pagamento</p>
                                </a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row setup-content" id="step-1">
                        <div class="col-xs-9">
                            <div class="col-md-12 well text-center">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="<?php echo URL_SITE ?>/assets/img/perfil.png" style="width:100%">
                                    </div>
                                    <div class="col-md-8">
                                        <h2 class="plano-nome"></h2>
                                        <h3 class="plano-preco-titulo"></h3>
                                        <table class="tabela-pagamento-plano">
                                            <tr>
                                                <th>Início assinatura</th>
                                                <th>Período</th>
                                                <th>Desconto</th>
                                                <th>Valor</th>
                                            </tr>
                                            <tr>
                                                <td>Hoje <?php echo date('d/m/Y'); ?></td>
                                                <td>Cobrado mensalmente todo dia <?php echo date('d'); ?> durante 12 meses</td>
                                                <td class="plano-desconto"></td>
                                                <td class="plano-preco"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="passo1-rodape" style="margin-top: 80px;">
                                        <div class="col-md-6">
                                            <h4>Selecione o país de cobrança</h4>
                                            <select class="form-control">
                                                <option>Brasil</option>
                                            </select>
                                            <div class="termos-uso-modal" id="termos-uso">
                                                <input style="margin-top: 20px;" type="checkbox" name="cobranca-termos-uso" checked>
                                                Concordo com os <a href="#">termos de uso</a>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <h4>Selecione Método de cobrança</h4>
                                            <img style="margin-top: -5px; margin-bottom: 100px; width: 65%;" src="<?php echo URL_SITE ?>/assets/img/pagseguro-logo.png">
                                        </div>  
                                    </div>
                                </div>
                                <div style="text-align: right;">
                                    <button style="height: 40px;" id="passo-2" class="btn btn-primary btn-lg">Continuar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row setup-content" id="step-2">
                        <div class="col-xs-9">
                            <div class="col-md-12 well">
                                <h3> Confirme seus dados</h3>
                                
                                <div class="formulario-dados-cliente" style="margin: 0 auto;">

                                    <input type="hidden" name="acao" id="acao">

                                    <div class="alert-danger mensagem-erro" style="padding:5px; border: 1px solid; display: none;">
                                        <p style="margin-top:10px; font-size: 15px;">Preencha todos os campos!</p>
                                    </div>

                                    <div class="row" style="margin-top: 15px;">
                                        <div class="col-md-6">
                                            <input type="text" name="cpf" id="cpf" class="form-control verificador" placeholder="CPF" value="<?php echo isset($cliente) ? $cliente->getCpf() : ''; ?>" />
                                        </div>        
                                    </div>                        

                                    <div class="row" style="margin-top: 15px;">
                                        <h4><center>Endereço</center></h4>

                                        <div class="col-md-6">
                                            <input type="text" name="cep" id="cep" class="form-control cep verificador" placeholder="CEP" value="<?php echo isset($cliente) ? $cliente->getEndereco()->getCep() : '' ;?>" />
                                        </div>        
                                    </div>
                                    <div class="row" style="margin-top: 15px;">
                                        <div class="col-md-6">
                                            <input type="text" name="endereco" id="endereco" class="form-control logradouro verificador"  placeholder="Endereço" value="<?php echo isset($cliente) ? $cliente->getEndereco()->getEndereco() : ''; ?>" readonly />            
                                        </div>               
                                        <div class="col-md-4">
                                            <input type="text" name="numero" id="numero" class="form-control numero" placeholder="N°" value="<?php echo isset($cliente) ? $cliente->getEndereco()->getNumero() : '' ?>"/>  
                                        </div>                              
                                    </div>
                                    <div class="row" style="margin-top: 15px;">
                                        <div class="col-md-4">
                                            <input type="text" name="cidade" id="cidade" class="form-control cidade verificador" placeholder="Cidade" value="<?php echo isset($cliente) ? $cliente->getEndereco()->getCidade() : '' ?>" readonly/>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" name="bairro" id="bairro" class="form-control bairro" placeholder="Bairro" value="<?php echo isset($cliente) ? $cliente->getEndereco()->getBairro() : '' ?>" readonly/>
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" name="uf" id="uf" class="form-control uf verificador" placeholder="UF" value="<?php echo isset($cliente) ? $cliente->getEndereco()->getUf() : '' ?>" readonly/>          
                                        </div>  
                                    </div>
                                    <div class="row" style="margin-top: 15px;">
                                        <div class="col-md-10">
                                            <input type="text" name="complemento" id="complemento" class="form-control verificador" placeholder="Complemento" value="<?php echo isset($cliente) ? $cliente->getEndereco()->getComplemento() : ''; ?>" />                                            
                                        </div>
                                    </div>  
                                </div>
                                <div style="text-align: right; margin-top: 15px;">
                                    <button style="height: 40px;" id="passo-3" class="btn btn-primary btn-lg">Continuar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row setup-content" id="step-3">
                        <div class="col-xs-9">
                            <div class="col-md-12 well">

                                <h3>Dados do seu cartão de crédito</h3>

                                <div class="formulario-dados-cliente" style="text-align: center;">

                                <input type="hidden" name="cartaoBrand" id="cartaoBrand">
                                <input type="hidden" name="planoId" id="planoId">

                                <div class="mensagem-pagamento" style="padding:5px; border: 1px solid; display: none;">
                                    <p style="margin-top:10px; font-size: 15px;" class="mensagem-pagamento-texto"></p>
                                </div>      

                                <div class="background-loader" style="display: none;">
                                    <div class="loader"></div>  
                                    <p>Processando...</p>                
                                </div>                                

                                 <div class="row" style="margin-top: 15px;">

                                    <div class="col-md-6">
                                        <input type="text" name="cartaoNumero" id="cartaoNumero" class="form-control numero verificadorCartao" placeholder="Número" />
                                    </div>        
                                </div>
                                <div class="row" style="margin-top: 15px;">
                                    <div class="col-md-3">
                                        <label>Data de Vencimento (99/9999)</label>
                                        <input type="text" name="cartaoMes" id="cartaoMes" class="form-control mes verificadorCartao" placeholder="Mês">
                                    </div>
                                    <div class="col-md-3">
                                        <label>&nbsp;</label>
                                        <input type="text" name="cartaoAno" id="cartaoAno" class="form-control ano verificadorCartao" placeholder="Ano">          
                                    </div>  
                                </div>
                                <div class="row" style="margin-top: 15px;">
                                    <div class="col-md-6">
                                        <input type="text" name="cartaoCodigoSeguranca" id="cartaoCodigoSeguranca" class="form-control verificadorCartao" placeholder="Código de Segurança"/>                                            
                                    </div>
                                </div>  
                            </div>
                            <div style="text-align: right; margin-top: 15px;">
                                <button style="height: 40px;" id="finalizar-pagamento" class="btn btn-primary btn-lg">Finalizar assinatura</button>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
    </div>  
</div>
</div>  

</div>
</div>

<script type="text/javascript" src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js">
</script>
<script type="text/javascript">
    $(function(){

        $.ajax({
            url: URL+"/library/Requests.php",
            type: 'GET',
            data : {
                modulo  : 'assinatura',
                acao    : 'iniciarSessaoPagamento'
            },
            dataType: 'json',
            cache: false,
            success: function (resp) {

                if(typeof(resp) != "undefined" && resp != null){    
                    
                    PagSeguroDirectPayment.setSessionId(resp.id[0]);

                    // PagSeguroDirectPayment.getPaymentMethods({

                    //     success: function(response) {
                    //         //meios de pagamento disponíveis
                    //         console.log(response);
                    //     },
                    //     error: function(response) {
                    //         //tratamento do erro
                    //         console.log(response);
                    //     },
                    //     complete: function(response) {
                    //         //tratamento comum para todas chamadas
                    //         console.log('completou a requisição');
                    //     }
                    //  });

                }else{
                    console.log('Ocorreu um erro na requisição');
                }
            },
            error: function(resp) {
                console.log('Ocorreu um erro na resposta');
            }

        });

        //REALIZAR BUSCA
        $("#termos-uso").click(function(){

           $(".modalTermos").show();

        });
        
        $(".modalTermos").click(function(){
            
           $(this).fadeOut();

        });
        
        $(".closeModal").click(function(){
            
           $(this).fadeOut();

        });
    });
    
</script>
