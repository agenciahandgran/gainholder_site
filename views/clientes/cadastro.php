<div class="container">
	<div class="col-xs-12 breadcrumbs hidden-xs" typeof="BreadcrumbList" vocab="http://schema.org/">
		<!-- Breadcrumb NavXT 5.7.1 -->
		<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to Gainholder Português." href="<?php echo URL_SITE ?>" class="home"><span property="name">Home</span></a><meta property="position" content="1"></span> | <span property="itemListElement" typeof="ListItem"><span property="name">Login</span><meta property="position" content="2"></span>						
	</div>
</div>

<div class="row">
	<div role="main" class="container contentMaster ContentLogin p0">
		<!-- section -->
		<section>
			<div class="col-xs-12 divInternaTitulo login">
				<h3>Faça seu Cadastro</h3>
			</div>
			<!-- lisatgem do corpo -->
			<div class="col-xs-12 contentInterno p0">

				<!-- article -->
				<article id="post-45" class="post-45 page type-page status-publish hentry">
					<div class="loginMaster">
						<div class="tml tml-login col-xs-12 col-sm-6 div-faca-login" id="theme-my-login">
							<p class="tem-conta">
								Já tem uma conta ?
							</p>
							<div class="btn-faca-login col-x-12">
								<input type="button" class="btn-default btn btn-login" onclick="window.location.href='<?php echo URL_SITE ?>/clientes/login'" name="btn-login-register" value="Faça seu login">
							</div>
							<div class="assinante-premium">
								<p class="tit-assinante-premium">Já é assinante e ainda não é <span>Premium</span>?</p>
								<p class="text-assinante-premium">
									Assine agora a Gainholder Premium, tenha acesso a todo conteúdo exclusivo do portal, ganhe descontos e vantagens na contratação de serviços personalizados Gainholder.
								</p>
							</div>
							<div class="btn-assine-agora col-x-12">
								<input type="button" class="btn-default btn btn-assine-agora btn-verm" name="btn-login-register" onclick="window.location.href='<?php echo URL_SITE ?>/assinantes'" value="Assine Agora">
							</div>
						</div>
						<div class="tml tml-register col-xs-12 col-sm-6" id="theme-my-login">

							<form name="formulario-cadastro" id="formulario-cadastro" action="<?php echo URL_SITE ?>/clientes/registrar" method="post">
								<div class="col-xs-12 form-group p0">
									<h5 class="label">Preencha com seus dados</h5>
								</div>
								<?php if(isset($status) && !empty($status)): ?>
									<?php if($status == '1'): ?>
										<div class="col-xs-12 form-group p0" style="text-align: center;">
											<p class="btnSucess" style="margin-top: 15px; font-size: 17px; line-height: 23px;">Para validar a conta, acesse seu e-mail e clique no link indicado.</p>
										</div>
									<?php else: ?>
										<div class="col-xs-12 form-group p0" style="text-align: center;">
											<p style="margin-top: 15px; font-size: 17px; line-height: 23px;color:red;"><?php echo $mensagem; ?></p>
										</div>
									<?php endif; ?>
								<?php endif; ?>
								<div class="col-xs-12 form-group p0">
									<input type="text" name="nome_completo" id="nome_completo" class="form-control" value="" placeholder="Nome Completo" required="required" />
								</div>	
								<div class="col-xs-12 form-group p0">
									<input type="text" name="data_nascimento" id="data_nascimento" class="form-control" value="" placeholder="Data de Nascimento" />
								</div>	
								<div class="col-xs-12 form-group p0">
									<input type="text" name="telefone" id="telefone" class="form-control" value="" placeholder="Telefone" />
								</div>																
								<div class="col-xs-12 form-group p0">
									<input type="text" name="email" id="email" class="form-control" value="" placeholder="E-mail" />
								</div>
								<div class="col-xs-12 p0">
									<input type="password" name="senha" placeholder="Senha" id="senha" class="form-control" value="" autocomplete="off" />
								</div>

								<div class="col-xs-12 form-group avisoTML" style="font-size: 14px; cursor: pointer;">
									<input type="checkbox" name="termos_uso" id="termos_uso" value="1"/> Concordo com os <u>termos de uso</u>								
								</div>

								<div class="col-xs-12 form-group p0">
									<input type="hidden" name="modulo" value="cadastro"/> 
									<button type="submit" name="wp-submit" id="wp-submit" class="btn-default btn btn-login btn-azul azul-login" title="Entrar">Criar Conta
									</button>
								</div>
							</form>
						</div>
					</div>
				</article>
				<!-- /article -->

				<div class="col-xs-12 p0 divAjudaLogin">
					<div class="col-xs-12 col-md-6 ajudaFale">
						<p>Precisa de ajuda? <b><a href="<?php echo URL_SITE ?>/contatos">Fale Conosco</a></b></p>
					</div>
					<div class="col-xs-12 col-md-6 ajudaCentral">
						<p>Central de Ajuda <b>0800 941 8020</b></p>
					</div>
				</div>
			</div>
		</section>
		<!-- /section -->
	</div>
</div>


