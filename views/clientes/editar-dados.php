	<link rel="stylesheet" type="text/css" href="<?php echo URL_SITE ?>/assets/css/editar-dados.css">

	<div class="container">

		<!-- conteúdo -->	 	
		<div id="container">

			<?php //var_dump($cliente); ?>

			<!-- PROCESSO 1 -->
			<div id="process1">

				<div id="content">

					<div id="container2">

						<div id="title1">
							<h3 style="">Seus dados</h3>
							<div class="divid">
								<?php if(isset($mensagem)): ?>
									<p style="font-size: 16px; padding-top: 10px;"><?php echo $mensagem; ?></p>
									<?php if($status): ?>
										<script type="text/javascript">
											setTimeout(function(){
												window.location='<?php echo URL_SITE."/clientes/editarDados" ?> ';
											}, 1000);	
										</script>
									<?php endif; ?>
								<?php endif; ?>
							</div>
						</div>
						<form name="formulario-edicao" id="formulario-edicao" method="post" action="<?php echo URL_SITE ?>/clientes/salvarEdicao/<?php echo isset($cliente) ? $cliente->getId() : $id; ?>">

							<input type="hidden" name="modulo" value="editar-usuario">
							<input type="hidden" name="id_endereco" value="<?php echo isset($cliente) ? $cliente->getEndereco()->getId() : '' ?>">
							
							<div id="conteudo01">

								<div id="align022">

									<div id="sel022">
										<h4><center>Tipo de Cadastro</center></h4>
										<br>
										<select name="tipo_pessoa" id='tipo_pessoa'>
											<option value="">Selecione...</option>
											<option value='1' <?php echo isset($cliente) ? ($cliente->getTipoLegislacao() == '1' ? 'selected' : '' ) : ''; ?>>Pessoa Física </option>
											<option value='2' <?php echo isset($cliente) ? ($cliente->getTipoLegislacao() == '2' ? 'selected' : '' ) : ''; ?>>Pessoa Jurídica </option>
										</select>
									</div>	

									<div id="pessoafisica">

										<div id="sel01">
											<h4><center>Dados Pessoais</center></h4><br>
											<input type="text" name="nome_completo" id="nome_completo" class="form-controlproduto" placeholder="Nome Completo" value="<?php echo isset($cliente) ? $cliente->getNome() : ''; ?>" />	
										</div>								

										<div id="sel01">
											<input type="text" name="cpf" id="cpf" class="form-controlproduto"  placeholder="CPF" value="<?php echo isset($cliente) ? $cliente->getCpf() : ''; ?>" <?php echo isset($cliente) ? ($cliente->getCpf() == 0 ? "style='display:none;'" : "") : ""; ?>/>
										</div>								
										
										<div id="sel01">
											<input type="text" name="cnpj" id="cnpj" class="form-controlproduto"  placeholder="CNPJ" value="<?php echo isset($cliente) ? $cliente->getCnpj() : ''; ?>" <?php echo isset($cliente) ? ($cliente->getCnpj() == 0 ? "style='display:none;'" : "") : ""; ?> />
										</div>	
										
										<div id="sel01">
											<input type="text" name="rg" id="rg" class="form-controlproduto"  placeholder="RG" value="<?php echo isset($cliente) ? $cliente->getRg() : ''; ?>" />
										</div>	

										<div id="sel01">
											<input type="text" name="telefone" id="telefone" class="form-controlproduto"  placeholder="Telefone" value="<?php echo isset($cliente) ? $cliente->getTelefone() : ''; ?>" />
										</div>	

										<div id="sel01">
											<input type="text" name="celular" id="celular" class="form-controlproduto"  placeholder="Celular" value="<?php echo isset($cliente) ? $cliente->getCelular() : ''; ?>" />	
										</div>									

										<div id="sel022">
											<select name="genero" id="genero">	
												<option value='1' <?php echo isset($cliente) ? ($cliente->getSexo() == '1' ? 'selected' : '' ) : ''; ?>>Masculino </option>
												<option value='2' <?php echo isset($cliente) ? ($cliente->getSexo() == '2' ? 'selected' : '' ) : ''; ?>>Feminino </option>
											</select>
										</div>	

										<br>

										<h4><center>Endereço para contato</center></h4>
										<br><br>

										<div id="sel01">
											<input type="text" name="cep" id="cep" class="form-controlproduto cep" placeholder="CEP" value="<?php echo isset($cliente) ? $cliente->getEndereco()->getCep() : '' ;?>" />
										</div>

										<div id="sel002">

											<div style="width:80%; float:left;">
												<input type="text" name="endereco" id="endereco" class="form-controlproduto logradouro"  placeholder="Endereço" value="<?php echo isset($cliente) ? $cliente->getEndereco()->getEndereco() : ''; ?>" readonly />			
											</div>

											<div style="width:20%; float:left; padding-left:15px; padding-right:15px; ">
												<input type="text" name="numero" id="numero" class="form-controlproduto numero" placeholder="N°" value="<?php echo isset($cliente) ? $cliente->getEndereco()->getNumero() : '' ?>"/>
											</div>
										</div>	

										<div id="sel002">

											<div style="width:40%; float:left;">
												<input type="text" name="cidade" id="cidade" class="form-controlproduto cidade" placeholder="Cidade" value="<?php echo isset($cliente) ? $cliente->getEndereco()->getCidade() : '' ?>" readonly/>
											</div>

											<div style="width:40%; float:left; padding-left:15px; padding-right:15px; ">
												<input type="text" name="bairro" id="bairro" class="form-controlproduto bairro" placeholder="Bairro" value="<?php echo isset($cliente) ? $cliente->getEndereco()->getBairro() : '' ?>" readonly/>
											</div>	

											<div style="width:20%; float:left; padding-left:15px; padding-right:15px; ">
												<input type="text" name="uf" id="uf" class="form-controlproduto uf" placeholder="UF" value="<?php echo isset($cliente) ? $cliente->getEndereco()->getUf() : '' ?>" readonly/>
											</div>										
										</div>	

										<div id="sel01">
											<input type="text" name="complemento" id="complemento" class="form-controlproduto" placeholder="Complemento" value="<?php echo isset($cliente) ? $cliente->getEndereco()->getComplemento() : ''; ?>" />
										</div>	

										<h4><center>Informações de acesso</center></h4>

										<div id="sel01">
											<h4 style="margin-left:10px; color:#bfbfbf;">E-mail</h4>
											<input type="text" name="email" id="email" class="form-controlproduto" value="<?php echo isset($cliente) ? $cliente->getLogin()->getEmail() : ''; ?>" />

											<input type="hidden" name="emailAtual" id="emailAtual" value="<?php echo isset($cliente) ? $cliente->getLogin()->getEmail() : ''; ?>">	
										</div>									
									</div>

									<!--//// conteúdo processo 1 -->
									<div class="divid88">
									</div>

									<div class="span2">
										<div id="continuar">
											<input type="submit" value="Salvar" class="continue0" id="salvar1">
										</div>	
									</div>	
								</div>
							</div>
						</div>
					</div>
				</form>

				<div class="banner">
					<img src="<?php echo URL_SITE ?>/assets/img/banner03.png" style="width:100%; margin-top:0px; ">
				</div>	
			</div>		
			<!-- // process 2 -->
		</div>
	</div>

