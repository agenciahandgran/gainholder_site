<div class="container">
	<div class="col-xs-12 breadcrumbs hidden-xs" typeof="BreadcrumbList" vocab="http://schema.org/">
		<!-- Breadcrumb NavXT 5.7.1 -->
		<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to Gainholder Português." href="<?php echo URL_SITE ?>" class="home"><span property="name">Home</span></a><meta property="position" content="1"></span> | <span property="itemListElement" typeof="ListItem"><span property="name">Login</span><meta property="position" content="2"></span>						
	</div>
</div>

<div class="row">
	<div role="main" class="container contentMaster ContentLogin p0">
		<!-- section -->
		<section>
			<div class="col-xs-12 divInternaTitulo login">
				<h3>Faça seu Login ou cadastre-se</h3>
			</div>
			<!-- lisatgem do corpo -->
			<div class="col-xs-12 contentInterno p0">

				<!-- article -->
				<article id="post-45" class="post-45 page type-page status-publish hentry">
					<div class="loginMaster">
						<div class="tml tml-login col-xs-12 col-sm-6" id="theme-my-login">

							<form name="formulario-login" id="formulario-login" action="<?php echo URL_SITE ?>/clientes/login/<?php echo $modulo."/".$url; ?>" method="post">
								<div class="col-xs-12 form-group p0">
									<h5 class="label text-center" style="color:#838383">Digite seus dados de cadastro</h5>
								</div>
								<?php if(isset($mensagem) && !empty($mensagem)): ?>
									<div class="col-xs-12 form-group avisoTML">
										<p style="color: red; background: #efd9d9; padding: 10px; border: 1px solid;"><?php echo $mensagem; ?></p>
									</div>
								<?php endif; ?>
								<div class="col-xs-12 form-group p0">
									<input type="text" name="email" id="email" class="form-control" value="" placeholder="Email" />
								</div>
								<div class="col-xs-12 p0">
									<input type="password" name="senha" placeholder="Senha" id="senha" class="form-control" value="" autocomplete="off" />
								</div>

								<div class="col-xs-12 form-group avisoTML">
									<input type="checkbox" name="continuar_conectado" id="continuar_conectado" value="1"/>  Continuar conectado
								</div>
								<div class="col-xs-12 form-group p0">
									<button type="submit" name="wp-submit" id="wp-submit" class="btn-default btn btn-login btn-azul azul-login" title="Entrar">ENTRAR
									</button>
									<div class="col-xs-12 form-group p0" style="text-align: center;">
										<ul class="tml-action-links">
											<li>
												<a href="<?php echo URL_SITE ?>/clientes/recuperarSenha" rel="nofollow">Esqueceu sua senha?</a>
											</li>
										</ul>
									</div>
									<p>				
										<input type="hidden" name="modulo" value="login" />
									</p>
								</div>
							</form>
							<div class="col-xs-12 form-group p0 divTitulo" style="text-align: center;">
								<h5 class="divLoginTitulo">Ou utilize as redes sociais</h5>
							</div>
							<div class="col-xs-12 p0 divLoginIcons">
								<div class='apsl-login-networks theme-4 clearfix'>
									<div class='social-networks'>
										<a href="https://www.facebook.com/gainholder/" title='Login with facebook' target="blank">
											<div class="apsl-icon-block icon-facebook"></div>
										</a>
										<a href="https://www.linkedin.com/company/9266038/" title='Login with linkedin' target="blank">
											<div class="apsl-icon-block icon-linkedin"></div>
										</a>
										<a href="#" title='Login with twitter'>
											<div class="apsl-icon-block icon-twitter"></div>
										</a>
										<a href="#" title='Login with google'>
											<div class="apsl-icon-block icon-google"></div>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="tml tml-register col-xs-12 col-sm-6 div-faca-login" id="theme-my-login">
							<div class="col-xs-12 form-group"></div>
							<p class="tem-conta">
								Não tem conta? Cadastre-se Gratuitamente!
							</p>
							<div class="btn-faca-login col-x-12">
								<input type="button" class="btn-default btn btn-login" onclick="window.location.href='<?php echo URL_SITE ?>/clientes/cadastro'" name="btn-login-register" value="Criar Conta">
							</div>
							<div class="assinante-premium">
								<p class="tit-assinante-premium">Já é assinante e ainda não é <span>Premium</span>?</p>
								<p class="text-assinante-premium">
									Assine agora a Gainholder Premium, tenha acesso a todo conteúdo exclusivo do portal, ganhe descontos e vantagens na contratação de serviços personalizados Gainholder.
								</p>
							</div>
							<div class="btn-assine-agora col-x-12">
								<input type="button" class="btn-default btn btn-assine-agora btn-verm" name="btn-login-register" onclick="window.location.href='<?php echo URL_SITE ?>/assinantes'" value="Assine Agora">
							</div>
						</div>
					</div>
				</article>
				<!-- /article -->

				<div class="col-xs-12 p0 divAjudaLogin">
					<div class="col-xs-12 col-md-6 ajudaFale">
						<p>Precisa de ajuda? <b><a href="<?php echo URL_SITE ?>/contatos">Fale Conosco</a></b></p>
					</div>
					<div class="col-xs-12 col-md-6 ajudaCentral">
						<p>Central de Ajuda <b>0800 941 8020</b></p>
					</div>
				</div>
			</div>
		</section>
		<!-- /section -->
	</div>
</div>

