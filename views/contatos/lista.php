<div class="container">
	<div class="col-xs-12 breadcrumbs hidden-xs" typeof="BreadcrumbList" vocab="http://schema.org/">
		<!-- Breadcrumb NavXT 5.7.1 -->
		<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to Gainholder Inglês." href="<?php echo URL_SITE ?>" class="home"><span property="name">Home</span></a><meta property="position" content="1"></span> | <span property="itemListElement" typeof="ListItem"><span property="name">Contato</span><meta property="position" content="2"></span>						</div>
	</div>


	<div class="row conteudo-home">
		<div class="col-xs-12 p0 boxBanner bannerCapa contato">
			<div class="col-xs-12 p0 container-banner">
				<div class="img" style="background-image: url(<?php echo URL_SITE ?>/assets/img/bg-contato.jpg) "></div>
				<div class="mapa">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3602.840212722304!2d-49.29057868449978!3d-25.44360508378218!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce389270f0b11%3A0xe96954cee71637d3!2sR.+Francisco+Rocha%2C+222+-+Batel%2C+Curitiba+-+PR%2C+80730-390!5e0!3m2!1spt-BR!2sbr!4v1499032981307" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
		</div>

		<div role="main" class="container conteudo-home textalignCenter internas categorias conteudo p0">
			<section>

				<div class="col-xs-12 p0 info">
					
					<div class="col-xs-12 col-sm-9 form p0">
						<h3 class="tituloContato">Formulário de Contato</h3>
						<p class="info">A Gainholder é especializada em importação e exportação, estamos focados em fornecer soluções completas para projetos exigentes através de modelos de execução confiáveis.
						</p>
						<?php if(isset($mensagem)): ?>
							<p class="info" style="font-size: 16px; padding-top: 10px; color:green;"><?php echo $mensagem; ?></p>
						<?php endif; ?>
						<div role="form" class="wpcf7 formularioContato" id="wpcf7-f182-p60-o1" lang="pt-BR" dir="ltr">
							<div class="screen-reader-response"></div>
							<form name="formulario-contato" id="formulario-contato" action="<?php echo URL_SITE ?>/contatos/enviarEmail" method="post">
								<input type="hidden" name="modulo" value="contato">
								<div class="col-xs-12 form-group p0 ">
									<span class="wpcf7-form-control-wrap nome">
										<input type="text" name="nome" id="nome" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" aria-required="true" aria-invalid="false" placeholder="Nome Completo" />
									</span>
								</div>
								<div class="col-xs-12 form-group p0">
									<span class="wpcf7-form-control-wrap telefone">
										<input type="text" name="telefone" id="telefone" size="40" class="wpcf7-form-control wpcf7-text form-control" aria-invalid="false" placeholder="Telefone" />
									</span>
								</div>
								<div class="col-xs-12 form-group p0">
									<span class="wpcf7-form-control-wrap celular">
										<input type="text" name="celular" id="celular" size="40" class="wpcf7-form-control wpcf7-text form-control" aria-invalid="false" placeholder="Celular" />
									</span>
								</div>
								<div class="col-xs-12 form-group p0">
									<span class="wpcf7-form-control-wrap empresa">
										<input type="text" name="empresa" id="empresa" size="40" class="wpcf7-form-control wpcf7-text form-control" aria-invalid="false" placeholder="Empresa" />
									</span>
								</div>
								<div class="col-xs-12 form-group p0">
									<span class="wpcf7-form-control-wrap cargo">
										<input type="text" name="cargo" id="cargo" size="40" class="wpcf7-form-control wpcf7-text form-control" aria-invalid="false" placeholder="Cargo" />
									</span>
								</div>
								<div class="col-xs-12 form-group p0">
									<span class="wpcf7-form-control-wrap email_corporativo">
										<input type="text" name="email_corporativo" id="email_corporativo" size="40" class="wpcf7-form-control wpcf7-text form-control" aria-invalid="false" placeholder="E-mail Corporativo" />
									</span>
								</div>
								<div class="col-xs-12 form-group p0">
									<span class="wpcf7-form-control-wrap cnpj">
										<input type="text" name="cnpj" id="cnpj" size="40" class="wpcf7-form-control wpcf7-text form-control" aria-invalid="false" placeholder="CNPJ" />
									</span>
								</div>
								<div class="col-xs-12 col-sm-6 form-group p0">
									<span class="wpcf7-form-control-wrap cidade_estado">
										<input type="text" name="cidade_estado" id="cidade_estado" size="40" class="wpcf7-form-control wpcf7-text form-control" aria-invalid="false" placeholder="Cidade/Estado" />
									</span>
								</div>
								<div class="col-xs-12 col-sm-6 form-group" style="padding-right:0px">
									<span class="wpcf7-form-control-wrap pais">
										<input type="text" name="pais" id="pais" size="40" class="wpcf7-form-control wpcf7-text form-control" aria-invalid="false" placeholder="País" />
									</span>
								</div>
								<div class="col-xs-12 form-group p0">
									<span class="wpcf7-form-control-wrap mensagem">
										<textarea name="mensagem" id="name" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required form-control" aria-required="true" aria-invalid="false" placeholder="Mensagem"></textarea>
									</span>
								</div>
								<div class="col-xs-12 botao p0">
									<input type="submit" class="wpcf7-form-control wpcf7-submit btn-default btn btn-banner btn-contato" value="Enviar" />
								</div>
								<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>		    	</div>
								<div class="col-xs-12 col-sm-3 endereco p0">
									<div style="text-align: center;">
										<img src="<?php echo URL_SITE ?>/assets/img/logo-contato.png" />
									</div>
									<p class="titulo-contato">Contato</p>
									<p class="texto-contato">
										A Gainholder é especializada em importação e exportação, estamos focados em fornecer soluções completas para projetos exigentes através de modelos de execução confiáveis.

									</p>
								</div>
								<div class="col-xs-12 col-sm-3 endereco2 p0">
									<p class="titulo-endereco">Endereço</p>
									<p class="cidade">Curitiba - Brasil</p>
									<p class="end">R. Francisco Rocha, 222 - Batel, Curitiba - PR, 80730-390</p>

									<p class="cidade">Fort Lauderdale - EUA</p>
									<p class="end">R. Francisco Rocha, 222 - Batel, Curitiba - PR, 80730-390</p>

									<p class="cidade">Shanghai - China</p>
									<p class="end">R. Francisco Rocha, 222 - Batel, Curitiba - PR, 80730-390</p>
								</div>
								<div class="col-xs-12 col-sm-3 endereco2 p0">
									<p class="titulo-endereco">E-mail</p>
									<p class="cidade">central@gainholder.com</p>
								</div>
								<div class="col-xs-12 col-sm-3 endereco2 p0">
									<p class="titulo-endereco">Telefone</p>
									<p class="cidade">[41] 3062.7000</p>
									<p class="cidade">[41]9941-3286</p>
								</div>
								<div class="col-xs-12 col-sm-3 endereco2 p0">
									<p class="titulo-endereco">SAC</p>
									<p class="cidade">0800 941 8020</p>
								</div>
							</div>

						</section>


					</div>
				</div>

