<div class="container">
	<div class="col-xs-12 breadcrumbs hidden-xs" typeof="BreadcrumbList" vocab="http://schema.org/">
		<!-- Breadcrumb NavXT 5.7.1 -->
		<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to Gainholder Inglês." href="<?php echo URL_SITE ?>" class="home"><span property="name">Home</span></a><meta property="position" content="1"></span> | <span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to the Outdoor category archives." href="<?php echo URL_SITE ?>/home/pesquisa/<?php echo $conteudo->getCategoria()->getNome(); ?>" class="taxonomy category"><span property="name"><?php echo $conteudo->getCategoria()->getNome(); ?></span></a><meta property="position" content="2"></span> | <span property="itemListElement" typeof="ListItem"><span property="name" style="color: #e30613"><?php echo $conteudo->getTitulo(); ?></span><meta property="position" content="3"></span>	

		<input type="hidden" name="redirecionamento" id="redirecionamento" value="<?php echo $conteudo->getSlug(); ?>">	
		<input type="hidden" name="nivelAcesso" id="nivelAcesso" value="<?php echo $conteudo->getNivel(); ?>">				
	</div>
</div>
<div class="row conteudo-home">
	<div role="main" class="container conteudo-home internas categorias conteudo p0">
		<section>
			<div class="col-xs-12 col-sm-12 p0">
				<div class="col-xs-12 col-sm-8 p0">
					<div class="col-xs-12 texto">
						<div class="col-xs-12 destaque">

							<div class="img-destaque col-xs-12 p0">

								<img width="750" height="360" src="<?php echo URL_SITE ?>/painel/assets/img/upload/conteudos/banners/<?php echo $conteudo->getBanner(); ?>" class="img-responsive wp-post-image" alt="" />
							</div>
								<div class="col-xs-12 col-md-12 conteudo-list p0">

									<div class="titulo-destaque p0">
										<div class="col-xs-12 p0">
											<div class="div-categoria-internas p0">
												<?php echo $conteudo->getCategoria()->getNome(); ?>	
											</div>
										</div>
										<a href="#">
											<p>
												<?php echo $conteudo->getTitulo(); ?>
											</p>
										</a>
									</div>
									<div class="col-xs-12 p0">
										<p class="date-conteudo-home"><span>Por <span>Redação Gainholder</span><?php echo formatarDataExtenso(date('d/m/Y',strtotime($conteudo->getDataPublicacao())),'d'); ?></span> | Tempo de leitura: <span class="tempo_leiura">  <?php echo estimarTempoLeitura($conteudo->getTexto()); ?>
										</span></p>
										<hr>
									</div>
									<div class="excerpt-content-home disable-select">
										<div class="textoSingle paragrafoSingle">
											<?php echo $conteudo->getTexto(); ?>
										</div>
									</div>

									<div class="bg-bco"></div>
								</div>

							</a>
						</div>

					</div>
					<div class="col-xs-12 btn-load-more-content">
						<input type="button" name="load-conteudo" value="Continuar Lendo" class="btn-banner btn-default btn-continuar-lendo" />
					</div>
				</div>

				<div class="col-xs-12 col-sm-4">
					<div class="col-xs-12 p0">
						<p class="tituloDestaques col-xs-12">Conteúdo Relacionado</p>

						<?php if(!isset($conteudosRelacionados['error'])): ?>
							<?php foreach($conteudosRelacionados as $conteudoRelacionado): ?>
								<?php if($conteudoRelacionado->getId() != $conteudo->getId()): ?>
									<div class="col-xs-12 p0 div-artigo-home">

										<div class="categoria col-xs-12"><?php echo $conteudoRelacionado->getCategoria()->getNome(); ?></div>
										<div class="col-xs-12 artigo-list">
											<div class="titulo-artigo">
												<a href="<?php echo URL_SITE ?>/conteudos/noticia/<?php echo $conteudoRelacionado->getSlug(); ?>.html"><p style="height: auto !important;"><?php echo $conteudoRelacionado->getTitulo(); ?></p>
												</a>
												<p class="date-conteudo-home single" style="font-weight: normal!important;">
													<?php echo formatarDataExtenso(date('d/m/Y', strtotime($conteudoRelacionado->getDataPublicacao())));?>
												</p>
											</div>
										</div>
									</div>
								<?php endif; ?>
							<?php endforeach; ?>
						<?php endif; ?>

					</div>
					<div class="col-xs-12 p0 banner-conteudo-2">
						<img src="<?php echo URL_SITE; ?>/assets/img/banner-conteudo-2.jpg">
					</div>
				</div>
				<div class="col-xs-12 col-md-9 div-relacionados" style="display: none;">
					<h3>Soluções Relacionadas</h3>
					<div class="col-xs-12 col-md-6 content-relacionados">
						<p class="post-content">
						Commodities						</p>
						<div class="post-excerpt">
							<div class="textoOutros">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vitae porttitor sem, sed ornare dolor. Nullam semper elit risus, .</p>
							</div>
						</div>
						<div class="col-xs-12 btn-saiba-mais p0">
							<input type="button" name="load-conteudo" onclick="window.location.href='../../../../solucoes/commodities/index.html';" value="Saiba Mais" class="btn-banner btn-default btn-saiba-mais" />
						</div>
					</div>

					<div class="col-xs-12 col-md-6 content-relacionados">
						<p class="post-content">
						Inspeção de Embarque						</p>
						<div class="post-excerpt">
							<div class="textoOutros">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vitae porttitor sem, sed ornare dolor. Nullam semper elit risus, .</p>
							</div>
						</div>
						<div class="col-xs-12 btn-saiba-mais p0">
							<input type="button" name="load-conteudo" onclick="window.location.href='../../../../solucoes/inspecao-de-embarque/index.html';" value="Saiba Mais" class="btn-banner btn-default btn-saiba-mais" />
						</div>
					</div>

				</div>
			</div>			


		</section>


	</div>
</div>

<script type="text/javascript">
	$(function(){

		$('.btn-continuar-lendo').on('click',function(){

			var URL           = "<?php echo URL_SITE; ?>";
			var redirecionar  = $("#redirecionamento").val();
			var grau          = $("#nivelAcesso").val();
			var botaoLeiaMais = $(this);

			$.ajax({
				url: URL+"/library/Requests.php",
				type: 'GET',
				data : {
					modulo  : 'conteudo',
					acao    : 'detalhe',
					grau    : grau
				},
				dataType: 'json',
				cache: false,
				success: function (resp) {

					if(typeof(resp) != "undefined" && resp != null){	
						
						if(resp.status){
							
							$('.conteudo-list').addClass('expand');
							$('.bg-bco').remove();
							//botaoLeiaMais.slideUp();
						 	botaoLeiaMais.remove();
							

						}else{

							var botaoTexto = "";

							if(resp.nivel == 1){
								botaoTexto = 'Login';
							}

							if(resp.nivel == 3){
								botaoTexto = 'Assinar';
							}

							if(resp.nivel == 4){
								botaoTexto = 'Verificar plano';
							}

							swal({

								title: 'Informação',
								text: resp.mensagem,
								type: 'warning',
								showCancelButton: true,
								confirmButtonColor: '#3085d6',
								cancelButtonColor: '#d33',
								confirmButtonText: botaoTexto,
								cancelButtonText: 'Cancelar',
								confirmButtonClass: 'btn btn-success',
								cancelButtonClass: 'btn btn-danger',
								buttonsStyling: false

							}).then(function () {

								if(resp.nivel == 1){
									window.location = URL+"/clientes/login/conteudos/"+redirecionar;
								}

								if(resp.nivel == 3 || resp.nivel == 4){
									window.location = URL+"/assinantes";
								}

							}, function (dismiss) {

								if (dismiss === 'cancel') {
									swal(
										'Cancelado',
										'Operação cancelada pelo usuário.',
										'error'
										);
								}
							});
						}

					}else{
						console.log('Ocorreu um erro na requisição');
					}

				},

				error: function(resp) {
					console.log('Ocorreu um erro na resposta');
				}

			});
		});
	});

</script>