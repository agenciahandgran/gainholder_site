		<div class="container">
			<div class="col-xs-12 breadcrumbs hidden-xs" typeof="BreadcrumbList" vocab="http://schema.org/">
				<!-- Breadcrumb NavXT 5.7.1 -->
				<span>
					<a title="Página principal" href="<?php echo URL_SITE; ?>" class="home">
						<span >Home</span>
					 </a>
				</span> | 
				<span>
					<span>Conteúdos</span>
				</span>						
			</div>
		</div>


		<div class="row conteudo-home">
			<div role="main" class="container conteudo-home internas categorias p0">
				<section>

					<?php if(isset($destaques[0])): ?>
						<div class="col-xs-12 col-sm-9 p0">
							<div class="col-xs-12 texto">
								<div class="col-xs-12 destaque">

									<div class="img-destaque col-xs-12 p0">
										<div class="div-categoria-home">
											<?php echo $destaques[0]->getCategoria()->getNome(); ?>
										</div>
										<img width="750" height="360" src="<?php echo URL_SITE ?>/painel/assets/img/upload/conteudos/banners/<?php echo $destaques[0]->getBanner(); ?>" class="img-responsive" alt="" />
									</div>
									
									<div class="col-xs-12 col-md-12 conteudo-list p0">
										<div class="col-xs-12 p0">
											<p class="date-conteudo-home"><span>Por <span>Redação Gainholder</span><?php echo formatarDataExtenso(date('d/m/Y',strtotime($destaques[0]->getDataPublicacao())), "d"); ?></span> | Tempo de leitura: <span class="tempo_leiura">  <?php echo estimarTempoLeitura($destaques[0]->getTexto()); ?>
											</span></p/>
										</div>
										<div class="titulo-destaque p0">
											<a href="<?php echo URL_SITE; ?>/conteudos/noticia/<?php echo $destaques[0]->getSlug(); ?>.html"><p>
												<?php echo $destaques[0]->getTitulo(); ?></p>
											</a>
											<p class="excerpt-conteudo-home">
												<?php echo $destaques[0]->getResumo(); ?>
											</p>

										</div>
									</div>
								</div>
						</div>
					<?php endif; ?>

					<div class="col-xs-12 col-md-12 p0 content-categorias-sub">
						<div class="col-xs-12 texto p0" id="listagem-conteudos">
							<?php if(!empty($destaques) && !isset($destaques['error'])): ?>
								<?php $cont = 0; foreach($destaques as $destaque): ?>
									<?php if($cont > 0): ?>
										<div class="col-xs-12 col-md-6 acessados">
											<div class="img-destaque col-xs-12 p0">
												<div class="div-categoria-home">
													<?php echo $destaque->getCategoria()->getNome(); ?>
												</div>
												<img width="265" height="158" src="<?php echo URL_SITE ?>/painel/assets/img/upload/conteudos/thumbs/<?php echo $destaque->getThumbnail(); ?>" class="img-responsive wp-post-image" alt="" />				            		
											</div>
											<div class="col-xs-12 conteudo-list p0">
												<div class="titulo-destaque">
													<p><a href="<?php echo URL_SITE; ?>/conteudos/noticia/<?php echo $destaque->getSlug(); ?>.html"><?php echo $destaque->getTitulo(); ?></a></p>
													<p class="excerpt-conteudo-home">
														<?php echo $destaque->getResumo(); ?>
													</p>
													<p class="date-conteudo-home dataConteudoLista"><?php echo formatarDataExtenso(date('d/m/Y',strtotime($destaque->getDataPublicacao()))); ?></p>
												</div>
											</div>
										</div>
									<?php endif; ?>
								<?php $cont++; endforeach; ?>
							<?php endif; ?>
						</div>
						<div class="col-xs-12 btn-load-more-content">
							<input type="button" name="load-conteudo" value="Carregar mais" class="btn-banner btn-default btn-load-content categoria" id="carregar-mais-pesquisa" data-paginas-destaques="<?php echo $totalPgDestaques; ?>" data-paginas-normais="<?php echo $totalPgNormais; ?>" data-pg-atual-destaque="2" data-pg-atual-normal="2" />
						</div>
					</div>

							</div>

							<div class="col-xs-12 col-sm-3">
								<div class="col-xs-12 p0">
									<p class="tituloDestaques col-xs-12">Últimos publicados</p>
									
									<?php if(!empty($conteudos) && !isset($conteudos['error'])): ?>
										<?php foreach($conteudos as $conteudo): ?>
												<div class="col-xs-12 p0 div-artigo-home">
													<div class="categoria col-xs-12">
														<?php echo $conteudo->getCategoria()->getNome(); ?>
													</div>
													<div class="col-xs-12 artigo-list">
														<div class="titulo-artigo">
															<a href="<?php echo URL_SITE; ?>/conteudos/noticia/<?php echo $conteudo->getSlug(); ?>.html">	
																<p><?php echo $conteudo->getTitulo(); ?></p>
															</a>
															<p class="excerpt-artigo-home">
																<?php echo $conteudo->getResumo(); ?>
															</p>
															<p class="date-artigo-home" style=" display: none; height: auto !important;">2 jul 2017 ÀS 1:26</p>
														</div>
													</div>
												</div>
										<?php endforeach; ?>
									<?php endif; ?>

								</div>
								<div class="col-xs-12 p0 banner-conteudo-2">
									<img src="<?php echo URL_SITE ?>/assets/img/conteudos/banner-conteudo-1.jpg">
								</div>
								<div class="col-xs-12 p0 banner-conteudo-1">
									<img src="<?php echo URL_SITE ?>/assets/img/conteudos/banner-conteudo-2.jpg">
								</div>

							</div>			


						</section>


					</div>
				</div>
				<script type="text/javascript">
					$(function(){

						var URL = "<?php echo URL_SITE; ?>";

						$("#carregar-mais-pesquisa").click(function(){
							
							var botao            = $(this);
							var pgAtualDestaque  = $(this).attr('data-pg-atual-destaque');
							var pgAtualNormal    = $(this).attr('data-pg-atual-normal');
							var totalPgDestaques = $(this).attr('data-paginas-destaques');
							var totalPgNormais   = $(this).attr('data-paginas-normais');

							$.ajax({
								url: URL+"/library/Requests.php",
								type: 'GET',
								data : {
									modulo          : 'conteudo',
									acao            : 'carregarNoticias',
									totalDestaque   : totalPgDestaques,
									totalNormais    : totalPgNormais,
									pgAtualDestaque : pgAtualDestaque,
									pgAtualNormal   : pgAtualNormal
								},
								dataType: 'json',
								cache: false,
								success: function (resp) {

									if(typeof(resp) != "undefined" && resp != null){

										if(resp.resultado){
											
											$("#listagem-conteudos").append(resp.html);
										
											botao.attr('data-pg-atual-destaque', resp.pgAtualDestaque);
											botao.attr('data-pg-atual-normal', resp.pgAtualNormais);

											if(resp.pgAtualDestaque > totalPgDestaques && resp.pgAtualNormais > totalPgNormais){
												botao.remove();
											}

										}else{
											botao.remove();
										}

									}else{
										console.log('Ocorreu um erro na requisição');
									}

								},

								error: function(resp) {
									console.log('Ocorreu um erro na resposta');
								}

							});
						});

					});
				</script>	