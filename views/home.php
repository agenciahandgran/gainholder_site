       
<?php print_r($conteudos); ?>

        <div class="row divHeader">
            <div class="row">
                <div class="col-xs-12 p0 boxBanner bannerCapa hidden-xs">
                    <div class="col-xs-12 p0 container-banner">

                        <aside id="banner_capa" style="width:100%" class="carousel slide" data-ride="carousel">

                            <!-- Indicators -->
                            <div class="carousel-control"></div>
                            <a class="left carousel-control" href="#banner_capa" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                            <a class="right carousel-control" href="#banner_capa" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
                            
                            <div class="carousel-inner">

                                <div class="item active" style="background-image:url('<?php echo URL_SITE ?>/assets/img/banner-carro.jpg'); background-repeat:no-repeat; background-position:center top; background-size:cover; height:620px;width: 100%; margin: 0 auto; border: none;">

                                    <div class="container container-banners">
                                        <div class="banner-info container">
                                            <div class="col-xs-12 col-sm-8">
                                                <h4>Exportação de Commodities</h4>
                                                <p><p>Transações internacionais de produtos e grande escala, com segurança e competência, são a marca da Gainhoder</p>
                                            </p>
                                            <button type="button" class="btn-default btn btn-banner" onclick="window.open('http://gainholder.com/');" title="Exportação de Commodities" title="conheça mais">
                                                <span>
                                                Saiba Mais </span>
                                            </button>
                                        </div>                      
                                    </div>
                                </div>
                            </div>

                            <div class="item" style="background-image:url('<?php echo URL_SITE ?>/assets/img/banner-energia.jpg'); background-repeat:no-repeat; background-position:center top; background-size:cover; height:620px;width: 100%; margin: 0 auto; border: none;">

                                <div class="container container-banners">

                                    <div class="banner-info container">
                                        <div class="col-xs-12 col-sm-8">
                                            <h4>Energia de Alta Rentabilidade</h4>
                                            <p><p>A Gainholder tem experiência no planejamento e operações de importação no segmento de Energia</p>
                                        </p>
                                        <button type="button" class="btn-default btn btn-banner" onclick="window.open('http://gainholder.com/');" title="Energia de Alta Rentabilidade" title="conheça mais">
                                            <span>Saiba Mais</span>
                                        </button>
                                    </div>                      
                                </div>

                            </div>

                        </div>

                        <div class="item" style="background-image:url('<?php echo URL_SITE ?>/assets/img/banner-graos.jpg'); background-repeat:no-repeat; background-position:center top; background-size:cover; height:620px;width: 100%; margin: 0 auto; border: none;">

                            <div class="container container-banners">

                                <div class="banner-info container">
                                    <div class="col-xs-12 col-sm-8">
                                        <h4>Importamos Carros de Luxo</h4>
                                        <p><p>Sonha em ter um importado na garagem? A Gainholder tem experiência no setor de importação de veículo de alto padrão.</p>
                                    </p>
                                    <button type="button" class="btn-default btn btn-banner" onclick="window.open('http://www.gainholder.com/');" title="Importamos Carros de Luxo" title="conheça mais">
                                        <span>Saiba Mais </span>
                                    </button>
                                </div>                      
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Controls -->
                <ol class="carousel-indicators">
                    <li data-target="#banner_capa" data-slide-to="0" class='active'></li>
                    <li data-target="#banner_capa" data-slide-to="1" ></li>
                    <li data-target="#banner_capa" data-slide-to="2" ></li>
                </ol>
            </aside>
        </div>
    </div>
</div>
</div>
<div class="container">
    <div class="row intro-home" style="display: none;">
        <div role="main" class="container intro-home p0">
            <section>
                <div class="col-xs-12">
                    <div class="col-xs-12 col-md-6 texto p0">
                        <h3>Oferecemos soluções completas para importação e exportação</h3>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 texto">
                    <p>A Gainholder é especializada em importação e exportação, estamos focados em fornecer soluções completas para projetos exigentes através de modelos de execução confiáveis, eficazes e flexíveis.<br />
                        <br />
                    Somos reconhecidos por completar desafios dentro de quadros orçamentários rigorosos e horários exigentes, dentro do prazo e com qualidade. Nossas entregas abrangem desde estudos conceituais e de viabilidade, engenharia operacional, aquisição, gerenciamento de plataformas e produtos.</p>
                </div>
                <div class="col-xs-12 col-md-6 lista">
                    <ul>
                        <li class="col-xs-12 col-sm-6 p0">Habilitação</li>
                        <li class="col-xs-12 col-sm-6 p0">Homologação</li>
                        <li class="col-xs-12 col-sm-6 p0">Index</li>
                        <li class="col-xs-12 col-sm-6 p0">Excore</li>
                        <li class="col-xs-12 col-sm-6 p0">Report</li>
                        <li class="col-xs-12 col-sm-6 p0">Host</li>
                        <li class="col-xs-12 col-sm-6 p0">Supply Chain 4PL</li>
                        <li class="col-xs-12 col-sm-6 p0">End-2-End</li>
                    </ul>
                </div>
            </section>
        </div>
    </div>

    <div class="row destaques-home">
        <div role="main" class="container destaques-home p0">
            <section>
                <div class="col-xs-12 col-sm-9 p0">
                    <p class="tituloDestaques col-xs-12" style="width: 96%;">Destaques</p>
                    <div class="col-xs-12 col-md-8 texto">

                        <a href="blog/artigos/emissao-de-vistos-brasileiros-para-os-eua-sofre-mudancas/index.html">
                            <div class="img-destaque col-xs-12 p0">
                                <img width="555" height="345" src="<?php echo URL_SITE ?>/assets/img/destaque-1.jpg" class="img-responsive wp-post-image" alt=""  />                            
                                <div class="titulo-destaque">
                                    <p>
                                    Emissão de vistos brasileiros para os EUA sofre mudanças                                </p>
                                </div>
                            </div>
                        </a>

                    </div>
                    <div class="col-xs-12 col-md-4 texto p0">
                        <div class="col-xs-12">
                            <a href="blog/artigos/emissao-de-vistos-brasileiros-para-os-eua-sofre-mudancas/index.html">
                                <div class="img-destaque col-xs-12 p0">
                                    <img width="265" height="158" src="<?php echo URL_SITE ?>/assets/img/destaque-1-265x158.jpg" class="img-responsive wp-post-image" alt="" />                                <div class="titulo-destaque2">
                                        <p>
                                        Emissão de vistos brasileiros para os EUA sofre mudanças                                    </p>
                                    </div>
                                </div>
                            </a>

                        </div>
                        <div class="col-xs-12 destaqueLast">
                            <a href="blog/artigos/contribuintes-com-debito-tem-4-meses-para-renegociacao/index.html">
                                <div class="img-destaque col-xs-12 p0">
                                    <img width="265" height="158" src="<?php echo URL_SITE ?>/assets/img/banner-graos-265x158.jpg" class="img-responsive wp-post-image" alt="" />                <div class="titulo-destaque2">
                                        <p>Contribuintes com débito tem 4 meses para renegociação</p>
                                    </div>
                                </div>
                            </a>                    
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <p class="tituloDestaques col-xs-12">Revista</p>
                    <div class="col-xs-12 texto">
                        <img width="264" height="346" src="<?php echo URL_SITE ?>/assets/img/revista.jpg" class="attachment-revista size-revista" alt=""  />
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div class="row conteudo-home">

        <div role="main" class="container conteudo-home p0">
            <section>
                <div class="col-xs-12 col-sm-6 p0">
                    <p class="tituloDestaques col-xs-12" style="width: 96%;">Conteúdo Gainholder</p>
                    <div class="col-xs-12 texto">

                        <div class="col-xs-12 p0">
                            <a href="blog/2017/07/02/trf4-reverte-decisao-de-moro-e-absolve-vaccari/index.html">
                                <div class="img-destaque col-xs-12 col-sm-6 p0">
                                    <div class="div-categoria-home">Main</div>
                                    <img width="265" height="158" src="<?php echo URL_SITE ?>/assets/img/mcjoao-vaccari-neto-depoimento-cpi-petrobras02090420151-265x158.jpg" class="img-responsive wp-post-image" alt="" />
                                </div>
                                <div class="col-xs-12 col-sm-6 conteudo-list">
                                    <div class="titulo-destaque">
                                        <p>TRF4 reverses Moro&#8217;s decision and acquits Vaccari</p>
                                        <p class="excerpt-conteudo-home">
                                        He had been convicted by federal judge Sérgio Moro 15 years and four months in prison for passive corruption and money laundering</p>
                                        <p class="date-conteudo-home">2 julho 2017</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-12 p0">
                            <a href="blog/2017/07/02/moro-pode-anunciar-sentenca-de-lula-a-qualquer-momento/index.html">
                                <div class="img-destaque col-xs-12 col-sm-6 p0">
                                    <div class="div-categoria-home">Outdoor</div>
                                    <img width="265" height="158" src="<?php echo URL_SITE ?>/assets/img/moro-265x158.jpg" class="img-responsive wp-post-image" alt="" />
                                </div>
                                <div class="col-xs-12 col-sm-6 conteudo-list">
                                    <div class="titulo-destaque">
                                        <p>Moro can announce Lula&#8217;s sentence at any moment</p>
                                        <p class="excerpt-conteudo-home">
                                        Judge Sergio Moro, who heads Operation Lava Jato, must turn his attention to the process involving former President Luiz Inacio Lula da Silva.</p>
                                        <p class="date-conteudo-home">2 julho 2017</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-12 p0">
                            <a href="blog/2017/07/02/juiz-aceita-denuncia-contra-cunha-e-henrique-eduardo-alves/index.html">
                                <div class="img-destaque col-xs-12 col-sm-6 p0">
                                    <div class="div-categoria-home">Outdoor</div>
                                    <img width="265" height="158" src="<?php echo URL_SITE ?>/assets/img/cunha-265x158.jpg" class="img-responsive wp-post-image" alt="" />
                                </div>
                                <div class="col-xs-12 col-sm-6 conteudo-list">
                                    <div class="titulo-destaque">
                                        <p>Juiz aceita denúncia contra Cunha e Henrique Eduardo Alves</p>
                                        <p class="excerpt-conteudo-home">
                                        ). Segundo nota da Justiça Federal do Rio Grande do Norte, divulgada neste sábado (1º), os dois estão supostamente envolvidos na investigação conhecida como Operação Manus </p>
                                        <p class="date-conteudo-home">2 julho 2017</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-12 p0">
                            <a href="blog/2017/07/02/como-um-bispo-medieval-adiantou-a-teoria-do-big-bang-no-seculo-13/index.html">
                                <div class="img-destaque col-xs-12 col-sm-6 p0">
                                    <div class="div-categoria-home">Outdoor</div>
                                    <img width="265" height="158" src="<?php echo URL_SITE ?>/assets/img/96588681gettyimages-590183480-265x158.jpg" class="img-responsive wp-post-image" alt="" />
                                </div>
                                <div class="col-xs-12 col-sm-6 conteudo-list">
                                    <div class="titulo-destaque">
                                        <p>Como um bispo medieval adiantou a teoria do Big Bang no século 13</p>
                                        <p class="excerpt-conteudo-home">
                                        Estudioso obcecado com arco-íris, cores e luz descreveu o nascimento do cosmos de forma semelhante aos cientistas modernos.</p>
                                        <p class="date-conteudo-home">2 julho 2017</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-12 p0">
                            <a href="blog/2017/07/02/explosoes-de-tres-carros-bomba-deixam-21-mortos-em-damasco/index.html">
                                <div class="img-destaque col-xs-12 col-sm-6 p0">
                                    <div class="div-categoria-home">Resources</div>
                                    <img width="265" height="158" src="<?php echo URL_SITE ?>/assets/img/1-265x158.jpg" class="img-responsive wp-post-image" alt="" /> 
                                </div>
                                <div class="col-xs-12 col-sm-6 conteudo-list">
                                    <div class="titulo-destaque">
                                        <p>Explosões de três carros-bomba deixam 21 mortos em Damasco</p>
                                        <p class="excerpt-conteudo-home">
                                        Um policial presente no local de uma das explosões informou que o total de mortos é de pelo menos sete pessoas, com 13 feridos</p>
                                        <p class="date-conteudo-home">2 julho 2017</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-3">
                    <p class="tituloDestaques col-xs-12">Artigos</p>
                    <div class="col-xs-12 p0 div-artigo-home">
                        <a href="blog/artigos/governo-investiga-supostas-irregularidades-em-vendas-de-spinner/index.html">
                            <div class="categoria col-xs-12">Exterior</div>
                            <div class="col-xs-12 artigo-list">
                                <div class="titulo-artigo">
                                    <p>Governo investiga supostas irregularidades em vendas de spinner</p>
                                    <p class="excerpt-artigo-home">Segundo o ministério, a investigação foi aberta após relatos no exterior que narram acidentes</p>
                                    <p class="date-artigo-home">27 junho 2017</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 p0 div-artigo-home">
                        <a href="blog/artigos/socio-investidor-entenda-o-seu-papel/index.html">
                            <div class="categoria col-xs-12">Exterior</div>
                            <div class="col-xs-12 artigo-list">
                                <div class="titulo-artigo">
                                    <p>Investor partner: Understand your role</p>
                                    <p class="excerpt-artigo-home">
                                    You may have heard of the term investor partner, but do you know exactly what that means and what is the role of this professional? </p>
                                    <p class="date-artigo-home">21 junho 2017</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 p0 div-artigo-home">
                        <a href="blog/artigos/privatizacao-nao-esta-na-agenda-da-petrobras/index.html">
                            <div class="categoria col-xs-12">Exterior</div>
                            <div class="col-xs-12 artigo-list">
                                <div class="titulo-artigo">
                                    <p>Privatização não esta na agenda da Petrobras</p>
                                    <p class="excerpt-artigo-home">
                                    Nono nonono non ono nono nonon ono nono nono nnnonononono nonoonon on ononono nononon ononono no non.</p>
                                    <p class="date-artigo-home">17 junho 2017</p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-3">
                    <p class="tituloDestaques col-xs-12">Colunistas</p>
                    <div class="col-xs-12 texto">
                        <div class="colunista">
                            <div class="imagem">
                                <span><img width="70" height="70" src="<?php echo URL_SITE ?>/assets/img/colaborador-2-70x70.jpg" class="attachment-colunista_capa size-colunista_capa wp-post-image" alt=""  /></span>
                            </div>
                            <div class="post">
                                <p class="titulo">Malévola da Silva</p>
                                <p class="post-content">
                                    <a href="blog/artigos/governo-investiga-supostas-irregularidades-em-vendas-de-spinner/index.html">Governo investiga supostas irregularidades em vendas de spinner</a>
                                </p>
                                <p class="date-artigo-home">27 junho 2017</p>
                                <p></p>
                            </div>
                        </div>
                        <div class="colunista">
                            <div class="imagem">
                                <span><img width="70" height="70" src="<?php echo URL_SITE ?>/assets/img/colaborador-3-70x70.jpg" class="attachment-colunista_capa size-colunista_capa wp-post-image" alt="" /></span>
                            </div>
                            <div class="post">
                                <p class="titulo">Paulo Falcão Brasil</p>
                                <p class="post-content">
                                    <a href="blog/artigos/socio-investidor-entenda-o-seu-papel/index.html">Investor partner: Understand your role</a>
                                </p>
                                <p class="date-artigo-home">21 junho 2017</p>
                                <p></p>
                            </div>
                        </div>
                        <div class="colunista">
                            <div class="imagem">
                                <span><img width="70" height="70" src="<?php echo URL_SITE ?>/assets/img/colaborador-4-70x70.jpg" class="attachment-colunista_capa size-colunista_capa wp-post-image" alt=""/></span>
                            </div>
                            <div class="post">
                                <p class="titulo">Nego James</p>
                                <p class="post-content">
                                    <a href="blog/artigos/contribuintes-com-debito-tem-4-meses-para-renegociacao/index.html">Contribuintes com débito tem 4 meses para renegociação</a>
                                </p>
                                <p class="date-artigo-home">17 junho 2017</p>
                                <p></p>
                            </div>
                        </div>
                        <div class="colunista">
                            <div class="imagem">
                                <span><img width="70" height="70" src="<?php echo URL_SITE ?>/assets/img/er-mota-70x70.jpg" class="attachment-colunista_capa size-colunista_capa wp-post-image" alt="" /></span>
                            </div>
                            <div class="post">
                                <p class="titulo">Marcos Fechado</p>
                                <p class="post-content">
                                    <a href="blog/artigos/privatizacao-nao-esta-na-agenda-da-petrobras/index.html">Privatização não esta na agenda da Petrobras</a>
                                </p>
                                <p class="date-artigo-home">17 junho 2017</p>
                                <p></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 p0">
                        <img src="<?php echo URL_SITE ?>/assets/img/banner-lateral-capa.jpg">
                    </div>
                </div>
            </section>

            <div class="col-xs-12 banner-meio-capa">
                <a href="#" >
                    <img src="<?php echo URL_SITE ?>/assets/img/banner-assinantes-capa.jpg">
                </a>
            </div>
        </div>
    </div>

    <div class="row conteudo-home">
        <div role="main" class="container conteudo-home p0">
            <section>
                <div class="col-xs-12 p0">
                    <p class="tituloDestaques col-xs-12" style="width: 96%;">Artigos mais acessados</p>
                    <div class="col-xs-12 texto p0">
                        <div class="col-xs-12 col-md-3 acessados">
                            <a href="blog/2017/06/21/juiz-auxiliar-de-teori-pede-desligamento-do-stf/index.html">
                                <div class="img-destaque col-xs-12 p0">
                                    <div class="div-categoria-home">Resources</div>
                                    <img width="265" height="158" src="<?php echo URL_SITE ?>/assets/img/Juiz-auxuliar-de-Teori-divulg-265x158.jpg" class="img-responsive wp-post-image" alt="" />                             
                                </div>
                                <div class="col-xs-12 conteudo-list p0">
                                    <div class="titulo-destaque">
                                        <p>Juiz auxiliar de Teori pede desligamento do STF</p>
                                        <p class="excerpt-conteudo-home">
                                        A presidente do Supremo Tribunal Federal (STF), Cármen Lúcia, aceitou o pedido de desligamento</p>
                                        <p class="date-conteudo-home">21 junho 2017</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-12 col-md-3 acessados">
                            <a href="blog/2017/06/21/novo-relator-da-lava-jato-devera-ser-definido-h/index.html">
                                <div class="img-destaque col-xs-12 p0">
                                    <div class="div-categoria-home">Marketplace</div>
                                    <img width="265" height="158" src="<?php echo URL_SITE ?>/assets/img/1496888136786-265x158.jpg" class="img-responsive wp-post-image" alt="" /> 
                                </div>
                                <div class="col-xs-12 conteudo-list p0">
                                    <div class="titulo-destaque">
                                        <p>Novo relator da Lava Jato deverá ser definido hoje pelo STF</p>
                                        <p class="excerpt-conteudo-home">
                                        Expectativa é de que a presidente do Supremo determine o sorteio eletrônico da relatoria</p>
                                        <p class="date-conteudo-home">21 junho 2017</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-12 col-md-3 acessados">
                            <a href="blog/2017/06/21/juiz-auxiliar-de-teori-pede-desligamento-do-stf/index.html">
                                <div class="img-destaque col-xs-12 p0">
                                    <div class="div-categoria-home">Resources</div>
                                    <img width="265" height="158" src="<?php echo URL_SITE ?>/assets/img/Juiz-auxuliar-de-Teori-divulg-265x158.jpg" class="img-responsive wp-post-image" alt="" />                             
                                </div>
                                <div class="col-xs-12 conteudo-list p0">
                                    <div class="titulo-destaque">
                                        <p>Juiz auxiliar de Teori pede desligamento do STF</p>
                                        <p class="excerpt-conteudo-home">
                                        A presidente do Supremo Tribunal Federal (STF), Cármen Lúcia, aceitou o pedido de desligamento</p>
                                        <p class="date-conteudo-home">21 junho 2017</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-12 col-md-3 acessados">
                            <a href="blog/2017/06/21/novo-relator-da-lava-jato-devera-ser-definido-h/index.html">
                                <div class="img-destaque col-xs-12 p0">
                                    <div class="div-categoria-home">Marketplace</div>
                                    <img width="265" height="158" src="<?php echo URL_SITE ?>/assets/img/1496888136786-265x158.jpg" class="img-responsive wp-post-image" alt="" /> 
                                </div>
                                <div class="col-xs-12 conteudo-list p0">
                                    <div class="titulo-destaque">
                                        <p>Novo relator da Lava Jato deverá ser definido hoje pelo STF</p>
                                        <p class="excerpt-conteudo-home">
                                        Expectativa é de que a presidente do Supremo determine o sorteio eletrônico da relatoria</p>
                                        <p class="date-conteudo-home">21 junho 2017</p>
                                    </div>
                                </div>
                            </a>
                        </div>                                
                    </div>
                </div>
            </section>
        </div>
    </div>

</div>