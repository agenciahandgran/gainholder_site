        <div class="row divHeader">
            <div class="row">
                <?php if(!isset($bannersTopo['error'])): ?>

                    <div class="col-xs-12 p0 boxBanner bannerCapa hidden-xs">
                        <div class="col-xs-12 p0 container-banner">

                            <aside id="banner_capa" style="width:100%" class="carousel slide" data-ride="carousel">

                                <!-- Indicators -->
                                <div class="carousel-control"></div>
                                <a class="left carousel-control" href="#banner_capa" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                                <a class="right carousel-control" href="#banner_capa" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>


                                <div class="carousel-inner">
                                    <?php foreach($bannersTopo as $banner): ?>
                                    <?php $ativo = $banner->getOrdem() == '1' ? 'active' : ''; ?>
                                    <div class="item <?php echo $ativo; ?>" style="background-image:url('<?php echo URL_SITE ?>/painel/assets/img/upload/banners/<?php echo $banner->getImagem(); ?>'); background-repeat:no-repeat; background-position:center top; background-size:cover; height:385px;width: 100%; margin: 0 auto; border: none;">
                                        <div class="container container-banners">
                                            <div class="banner-info container">
                                                <div class="col-xs-12 col-sm-8">
                                                    <h4><?php echo $banner->getTitulo(); ?></h4>
                                                    <p><?php echo $banner->getDescricao(); ?></p>
                                                    <button type="button" onclick="window.location('<?php echo URL_SITE."/".$banner->getLink(); ?>')" class="btn-default btn btn-banner" title="<?php echo $banner->getTitulo(); ?>" title="conheça mais">
                                                        <span>Saiba Mais</span>
                                                    </button>
                                                </div>                      
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach; ?>
                                </div>

                                <!-- Controls -->
                                <ol class="carousel-indicators">
                                    <?php for($i=0; $i<count($bannersTopo); $i++): ?>
                                        <?php $ativoIndicator = ($i == 0 ? "class='active'" : ''); ?>
                                        <li data-target="#banner_capa" data-slide-to="<?php echo $i; ?>"    <?php echo $ativoIndicator; ?>>
                                        </li>
                                    <?php endfor; ?>
                                </ol>
                            </aside>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="container">
            <div class="row intro-home" style="display: none;">
                <div role="main" class="container intro-home p0">
                    <section>
                        <div class="col-xs-12">
                            <div class="col-xs-12 col-md-6 texto p0">
                                <h3>Oferecemos soluções completas para importação e exportação</h3>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 texto">
                            <p>A Gainholder é especializada em importação e exportação, estamos focados em fornecer soluções completas para projetos exigentes através de modelos de execução confiáveis, eficazes e flexíveis.<br />
                                <br />
                            Somos reconhecidos por completar desafios dentro de quadros orçamentários rigorosos e horários exigentes, dentro do prazo e com qualidade. Nossas entregas abrangem desde estudos conceituais e de viabilidade, engenharia operacional, aquisição, gerenciamento de plataformas e produtos.</p>
                        </div>
                        <div class="col-xs-12 col-md-6 lista">
                            <ul>
                                <li class="col-xs-12 col-sm-6 p0">Habilitação</li>
                                <li class="col-xs-12 col-sm-6 p0">Homologação</li>
                                <li class="col-xs-12 col-sm-6 p0">Index</li>
                                <li class="col-xs-12 col-sm-6 p0">Excore</li>
                                <li class="col-xs-12 col-sm-6 p0">Report</li>
                                <li class="col-xs-12 col-sm-6 p0">Host</li>
                                <li class="col-xs-12 col-sm-6 p0">Supply Chain 4PL</li>
                                <li class="col-xs-12 col-sm-6 p0">End-2-End</li>
                            </ul>
                        </div>
                    </section>
                </div>
            </div>
            <div class="row destaques-home">
                <div role="main" class="container destaques-home p0">
                    <section>
                        <div class="col-xs-12  p0">
                            <p class="tituloDestaques col-xs-12" style="width: 96%;">Destaques</p>

                            <?php if(isset($destaquesConteudos[0])): ?>
                                <div class="col-xs-12 col-md-6 texto">
                                    <a href="<?php echo URL_SITE ?>/conteudos/noticia/<?php echo $destaquesConteudos[0]->getSlug(); ?>.html">
                                        <div class="img-destaque col-xs-12 p0">
                                            <img width="555" height="345" src="<?php echo URL_SITE ?>/painel/assets/img/upload/conteudos/banners/<?php echo $destaquesConteudos[0]->getBanner(); ?>" class="img-responsive wp-post-image" alt=""  />                            
                                            <div class="titulo-destaque">
                                                <p><?php echo $destaquesConteudos[0]->getResumo(); ?></p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php endif; ?>

                            <div class="col-xs-12 col-md-3 texto p0">

                                <?php if(isset($destaquesConteudos[1])): ?>
                                    <div class="col-xs-12">
                                        <a href="<?php echo URL_SITE ?>/conteudos/noticia/<?php echo $destaquesConteudos[1]->getSlug(); ?>.html">
                                            <div class="img-destaque col-xs-12 p0">
                                                <img width="265" height="158" src="<?php echo URL_SITE ?>/painel/assets/img/upload/conteudos/thumbs/<?php echo $destaquesConteudos[1]->getThumbnail(); ?>" class="img-responsive wp-post-image" alt="" /> 
                                                <div class="titulo-destaque2">
                                                    <p><?php echo $destaquesConteudos[1]->getResumo(); ?></p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                <?php endif; ?>

                                <?php if(isset($destaquesConteudos[2])): ?>
                                    <div class="col-xs-12 destaqueLast">
                                        <a href="<?php echo URL_SITE ?>/conteudos/noticia/<?php echo $destaquesConteudos[2]->getSlug(); ?>.html">
                                            <div class="img-destaque col-xs-12 p0">
                                                <img width="265" height="158" src="<?php echo URL_SITE ?>/painel/assets/img/upload/conteudos/thumbs/<?php echo $destaquesConteudos[2]->getThumbnail(); ?>" class="img-responsive wp-post-image" alt="" />
                                                <div class="titulo-destaque2">
                                                 <p><?php echo $destaquesConteudos[2]->getResumo(); ?></p>
                                             </div>
                                         </div>
                                     </a>                    
                                 </div>
                             <?php endif; ?>

                         </div>

                         <div class="col-xs-12 col-md-3 texto p0">

                            <?php if(isset($destaquesArtigos[0])): ?>
                                <div class="col-xs-12">
                                    <a href="<?php echo URL_SITE ?>/artigos/artigo/<?php echo $destaquesArtigos[0]->getSlug(); ?>.html">
                                        <div class="img-destaque col-xs-12 p0">
                                            <img width="265" height="158" src="<?php echo URL_SITE ?>/painel/assets/img/upload/artigos/thumbs/<?php echo $destaquesArtigos[0]->getThumbnail(); ?>" class="img-responsive wp-post-image" alt="" /> 
                                            <div class="titulo-destaque2">
                                                <p><?php echo $destaquesArtigos[0]->getResumo(); ?></p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php endif; ?>

                            <?php if(isset($destaquesArtigos[1])): ?>
                                <div class="col-xs-12 destaqueLast">
                                    <a href="<?php echo URL_SITE ?>/artigos/artigo/<?php echo $destaquesArtigos[1]->getSlug(); ?>.html">
                                        <div class="img-destaque col-xs-12 p0">
                                            <img width="265" height="158" src="<?php echo URL_SITE ?>/painel/assets/img/upload/artigos/thumbs/<?php echo $destaquesArtigos[1]->getThumbnail(); ?>" class="img-responsive wp-post-image" alt="" />
                                            <div class="titulo-destaque2">
                                             <p><?php echo $destaquesArtigos[1]->getResumo(); ?></p>
                                         </div>
                                     </div>
                                 </a>                    
                             </div>
                         <?php endif; ?>

                     </div>
                 </div>

            <!--  <div class="col-xs-12 col-sm-3">
                <p class="tituloDestaques col-xs-12">Revista</p>
                <div class="col-xs-12 texto">
                    <img width="264" height="346" src="<?php echo URL_SITE ?>/assets/img/revista.jpg" class="attachment-revista size-revista" alt=""  />
                </div>
            </div> -->
        </section>
    </div>
</div>

<div class="row conteudo-home">

    <div role="main" class="container conteudo-home p0">
        <section>
            <div class="col-xs-12 col-sm-6 p0">
                <p class="tituloDestaques col-xs-12" style="width: 96%;">Conteúdo Gainholder</p>
                <div class="col-xs-12 texto">

                    <?php if(!empty($conteudos) && !isset($conteudos['error'])): ?>

                        <?php foreach($conteudos as $conteudo): ?>
                            <div class="col-xs-12 p0">
                                <a href="<?php echo URL_SITE ?>/conteudos/noticia/<?php echo $conteudo->getSlug(); ?>.html">
                                    <div class="img-destaque col-xs-12 col-sm-5 p0">
                                        <div class="div-categoria-home">
                                            <?php echo $conteudo->getCategoria()->getNome(); ?>
                                        </div>
                                        <img width="265" height="158" src="<?php echo URL_SITE ?>/painel/assets/img/upload/conteudos/thumbs/<?php echo $conteudo->getThumbnail(); ?>" class="img-responsive wp-post-image" alt="" />
                                    </div>
                                    <div class="col-xs-12 col-sm-7 conteudo-list">
                                        <div class="titulo-destaque">
                                            <h2><?php echo trim_text($conteudo->getTitulo(),50); ?></h2>
                                            <p class="excerpt-conteudo-home">
                                                <?php echo $conteudo->getResumo(); ?>
                                            </p>
                                            <span><?php echo formaTarDataExtenso(date('d/m/Y',strtotime($conteudo->getDataPublicacao()))); ?></span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>

                </div>
            </div>

            <div class="col-xs-12 col-sm-3">
                <p class="tituloDestaques col-xs-12">Artigos</p>

                <?php if(!empty($artigos) && !isset($artigos['error'])): ?>

                    <?php foreach($artigos as $artigo): ?>
                        <div class="col-xs-12 p0 div-artigo-home">
                            <a href="<?php echo URL_SITE ?>/artigos/artigo/<?php echo $artigo->getSlug(); ?>.html">
                                <div class="categoria col-xs-12"><?php echo $artigo->getCategoria()->getNome(); ?></div>
                                <div class="col-xs-12 artigo-list">
                                    <div class="titulo-artigo">
                                        <h2><?php echo trim_text($artigo->getTitulo(),50); ?></h2>
                                        <p class="excerpt-artigo-home"><?php echo $artigo->getResumo(); ?></p>
                                        <p class="date-artigo-home" style="display: none;">27 junho 2017</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
                
            </div>

            <div class="col-xs-12 col-sm-3">
                <p class="tituloDestaques col-xs-12">Colunistas</p>
                <div class="col-xs-12 texto">
                    <?php if(!empty($colunistas) && !isset($colunistas['error'])): ?>
                        <?php foreach($colunistas as $colunista): ?>
                            <div class="colunista">
                                <div class="imagem">
                                    <span><img width="70" height="70" src="<?php echo URL_SITE ?>/painel/assets/img/upload/colunista_perfil/<?php echo $colunista->getUsuario()->getFotoPerfil(); ?>" class="attachment-colunista_capa size-colunista_capa wp-post-image" alt=""  /></span>
                                </div>
                                <div class="post">
                                    <p class="titulo"><?php echo $colunista->getUsuario()->getNomeCompleto(); ?></p>
                                    <p class="post-content">
                                        <a href="<?php echo URL_SITE ?>/artigos/artigo/<?php echo $colunista->getUsuario()->getArtigo()->getSlug(); ?>.html"><?php echo trim_text($colunista->getUsuario()->getArtigo()->getResumo(),50); ?></a>
                                    </p>
                                    <p class="date-artigo-home"><?php echo formatarDataExtenso(date('d/m/Y',strtotime($colunista->getUsuario()->getArtigo()->getDataPublicacao()))); ?></p>
                                    <p></p>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                <div class="col-xs-12 p0">
                    <img src="<?php echo URL_SITE ?>/assets/img/banner-lateral-capa.jpg">
                </div>
            </div>
        </section>

        <div class="col-xs-12 banner-meio-capa">
            <a href="#" >
                <img src="<?php echo URL_SITE ?>/assets/img/banner-assinantes-capa.jpg">
            </a>
        </div>
    </div>
</div>

<div class="row conteudo-home">
    <div role="main" class="container conteudo-home p0">
        <section>
            <div class="col-xs-12 p0">
                <p class="tituloDestaques col-xs-12" style="width: 96%;">Artigos mais acessados</p>
                <div class="col-xs-12 texto p0">
                    <?php if(!empty($artigosAcessados) && !isset($artigosAcessados['error'])): ?>

                        <?php foreach($artigosAcessados as $artigoAcessado): ?>
                            <div class="col-xs-12 col-md-3 acessados">
                                <a href="<?php echo URL_SITE ?>/artigos/artigo/<?php echo $artigoAcessado->getSlug(); ?>.html">
                                    <div class="img-destaque col-xs-12 p0">
                                        <div class="div-categoria-home"><?php echo $artigoAcessado->getCategoria()->getNome(); ?></div>
                                        <img width="265" height="158" src="<?php echo URL_SITE ?>/painel/assets/img/upload/artigos/thumbs/<?php echo $artigoAcessado->getThumbnail(); ?>" class="img-responsive wp-post-image" alt="" />                             
                                    </div>
                                    <div class="col-xs-12 conteudo-list p0">
                                        <div class="titulo-destaque">
                                            <p><?php echo trim_text($artigoAcessado->getTitulo(),90); ?></p>
                                            <p class="excerpt-conteudo-home">
                                                <?php echo $artigoAcessado->getResumo(); ?></p>
                                                <p class="date-conteudo-home"><?php echo formaTarDataExtenso(date('d/m/Y',strtotime($artigoAcessado->getDataPublicacao()))); ?></p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>                             
                    </div>
                </div>
            </section>
        </div>
    </div>

</div>