<div class="row conteudo-home">
	<div role="main" class="container conteudo-home internas categorias p0">
		<section>
			<div class="col-xs-12 col-sm-10 p0">
				<div class="col-xs-12 texto">
					<div class="col-xs-12 col-md-10 conteudo-list p0">
						<?php if(isset($contador)): ?>
							<p style="font-size: 20px; margin-bottom: -10px;"><?php echo $contador[0]->getTotal(); ?> resultados para <strong style=""><?php echo $palavraChave; ?></strong></p>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div id="listagem-resultados">
				<?php if(!empty($resultados) && !isset($resultados['error'])): ?>
					<?php foreach($resultados as $resultado): ?>		
						<div class="col-xs-12 col-sm-10 p0">
							<div class="col-xs-12 texto">
								<div class="col-xs-12 col-md-10 conteudo-list p0">
									<div class="titulo-destaque p0">
										<?php $modulo = ($resultado->getTipo() == 1 ? "conteudos/noticia/" : "artigos/artigo/"); ?>
										<a href="<?php echo URL_SITE ?>/<?php echo $modulo; ?><?php echo $resultado->getSlug(); ?>.html">
											<p><?php echo $resultado->getTitulo(); ?></p>
										</a>
									</div>
									<p class="excerpt-conteudo-home">
										<?php echo $resultado->getResumo(); ?>
									</p>
									<p class="date-conteudo-home dataConteudoLista"><?php echo formatarDataExtenso(date('d/m/Y', strtotime($resultado->getDataPublicacao()))); ?></p>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
				<div class="col-md-8 content-categorias-sub" style="float: left; margin-left: 165px; border-bottom: 0px;">
					<div class="col-xs-12 btn-load-more-content">
						<input type="button" name="load-conteudo" value="Carregar mais" class="btn-banner btn-default btn-load-content categoria" id="carregar-mais-pesquisa" data-paginas="<?php echo $totalPaginas; ?>" data-pesquisa="<?php echo $palavraChave; ?>" data-pg-atual="2" />
					</div>
				</div>
			<?php else: ?>
				<div class="col-xs-12 col-sm-10 p0">
					<div class="col-xs-12 texto">
						<div class="col-xs-12 col-md-10 conteudo-list p0">
							<p style="font-size: 15px;">Nenhum resultado encontrado.</p>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</section>
	</div>
</div>
<script type="text/javascript">
	$(function(){

		var URL = "<?php echo URL_SITE; ?>";

		$("#carregar-mais-pesquisa").click(function(){
			
			var botao        = $(this);
			var paginaAtual  = $(this).attr('data-pg-atual');
			var totalPaginas = $(this).attr('data-paginas');
			var pesquisa     = $(this).attr('data-pesquisa');

			$.ajax({
				url: URL+"/library/Requests.php",
				type: 'GET',
				data : {
					modulo    : 'home',
					acao      : 'pesquisa',
					pesquisa  : pesquisa,
					totalPg   : totalPaginas,
					pgAtual   : paginaAtual
				},
				dataType: 'json',
				cache: false,
				success: function (resp) {

					if(typeof(resp) != "undefined" && resp != null){

						if(resp.resultado){
							
							$("#listagem-resultados").append(resp.html);
							botao.attr('data-pg-atual', resp.pgAtual);

							if(resp.pgAtual > totalPaginas){
								botao.remove();
							}

						}else{
							botao.remove();
						}

					}else{
						console.log('Ocorreu um erro na requisição');
					}

				},

				error: function(resp) {
					console.log('Ocorreu um erro na resposta');
				}

			});
		});

	});
</script>