	<svg xmlns="http://www.w3.org/2000/svg" style="display: none">
		<symbol id="checkmark" viewBox="0 0 24 24">
			<path stroke-linecap="round" stroke-miterlimit="10" fill="none"  d="M22.9 3.7l-15.2 16.6-6.6-7.1">
			</path>
		</symbol>
	</svg>
	<!-- HEADER -->

	<div id="header">
		<div id="containerheader">
		</div>
	</div>
	
	<!-- // HEADER -->

	<div id="bannerprincipal">	
		<div id="imgbanner">
			<div id="titlebann">
				Importamos seu carro de luxo
			</div>			
			
			<div id="subtitlebann">
				A sua chance de adquirir carros, iates e aviões privativos está aqui.
			</div>			
			
			<div id="bt">
				<input type="submit" value="Saiba Mais" class="saibamais" id="continuar04">
				
			</div>
			
		</div>
	</div>

	<div id="all">
		<div id="content">
			<div id="container">
				<div id="title01">
					Soluções completas em Importação e Exportação<br>
					para qualquer tipo de empresa
				</div>
				
				<div id="subtitle01">
					Nossa função é garantir aos nossos clientes desde o início a certeza de um projeto bem-sucedido sem desperdício de investimentos, sendo assim, contamos com profissionais especializados para realizar as transações com responsabilidade e competência.
				</div>
				
				<div id="boxes">
					<div id="box1">
						<div id="imgbox1">
							<img src="<?php echo URL_SITE ?>/assets/img/institucional/imgb1.png" style="width:100%;">
						</div>
						<div id="titleb">
							Small Business
						</div>
						
						<div id="subtitleb">
							Empresas pequeno porte
						</div>				
						
						<div id="textb">
							Começando no mercado internacional, conte com a nossa orientação e auxílio sob medida para o amadurecimento do seu negócio.
						</div>
						
					</div>	
					
					<div id="box2">
						
						<div id="imgbox1">
							<img src="<?php echo URL_SITE ?>/assets/img/institucional/imgb2.png" style="width:100%;">
						</div>
						<div id="titleb">
							Small Business
						</div>
						
						<div id="subtitleb">
							Empresas pequeno porte
						</div>				
						
						<div id="textb">
							Começando no mercado internacional, conte com a nossa orientação e auxílio sob medida para o amadurecimento do seu negócio.
						</div>
						

					</div>
					
					<div id="box3">
						
						<div id="imgbox1">
							<img src="<?php echo URL_SITE ?>/assets/img/institucional/imgb3.png" style="width:100%;">
						</div>
						<div id="titleb">
							Small Business
						</div>
						
						<div id="subtitleb">
							Empresas pequeno porte
						</div>				
						
						<div id="textb">
							Começando no mercado internacional, conte com a nossa orientação e auxílio sob medida para o amadurecimento do seu negócio.
						</div>
												
					</div>
					
				</div>
				

			</div>
			
			<!-- SOLUCAO -->
			
			<div id="boxsolucao"> 
				<div id="titleseg00">
					Soluções Gainholder
				</div>
				<div id="solucao01">
					<div id="imgsegmento">
						<img src="<?php echo URL_SITE ?>/assets/img/institucional/solucao01.png" style="width:100%;">
					</div>
					<a>
						<div id="leftarrowb1" class="leftarrowb1"><img src="<?php echo URL_SITE ?>/assets/img/institucional/left.png" style="width:100%;">
						</div>	</a>
						
						<a>
							<div id="rightarrowb1" class="rightarrowb1"><img src="<?php echo URL_SITE ?>/assets/img/institucional/right.png" style="width:100%;"></a>
							</div>
							
							<div id="activeb">
								<div id="titleb1">
									Habitação
								</div>					
								<div id="textb1">
									A Gainholder é especializada em importação e exportação, estamos focados em execução confiáveis, eficazes e flexíveis.
								</div>
								<div id="bts">
									<input type="submit" value="Saiba Mais" class="saibamais2" id="continuar04">
								</div>
							</div>
							
							<div id="titleseg">
								Soluções Gainholder
							</div>
							
							<div id="alignbox">
								
								
								<div id="b11" >
									<div id="titleb1">
										Homologação
									</div>					
									<div id="textb1">
										A Gainholder é especializada em importação e exportação, estamos focados em execução confiáveis, eficazes e flexíveis.
									</div>
									<div id="bts">
										<input type="submit" value="Saiba Mais" class="saibamais2off" id="continuar04">
									</div>

								</div>
								
								<div id="b11">
									<div id="titleb1">
										Exportação Direta
									</div>					
									<div id="textb1">
										A Gainholder é especializada em importação e exportação, estamos focados em execução confiáveis, eficazes e flexíveis.

									</div>
									<div id="bts">
										<input type="submit" value="Saiba Mais" class="saibamais2off" id="continuar04">
									</div>

								</div>




							</div>

							
							
						</div>
						
						<div id="solucao02">
							<div id="imgsegmento">
								<img src="<?php echo URL_SITE ?>/assets/img/institucional/solucao02.png" style="width:100%;">
							</div>

							<div id="leftarrowb2" class="leftarrowb1"><img src="<?php echo URL_SITE ?>/assets/img/institucional/left.png" style="width:100%;">
							</div>	
							
							<div id="rightarrowb2" class="rightarrowb1"><img src="<?php echo URL_SITE ?>/assets/img/institucional/right.png" style="width:100%;">
							</div>
							
							<div id="activeb">
								<div id="titleb1">
									Homologação
								</div>					
								<div id="textb1">
									A Gainholder é especializada em importação e exportação, estamos focados em execução confiáveis, eficazes e flexíveis.
								</div>
								<div id="bts">
									<input type="submit" value="Saiba Mais" class="saibamais2" id="continuar04">
								</div>
							</div>
							
							<div id="titleseg">
								Soluções Gainholder
							</div>
							
							<div id="alignbox">
								
								<div id="b11">
									<div id="titleb1">
										Habitação
									</div>					
									<div id="textb1">
										A Gainholder é especializada em importação e exportação, estamos focados em execução confiáveis, eficazes e flexíveis.

									</div>
									<div id="bts">
										<input type="submit" value="Saiba Mais" class="saibamais2off" id="continuar04">
									</div>

								</div>
								
								<div id="b11" >
									<div id="titleb1">
										Exportação Direta
									</div>					
									<div id="textb1">
										A Gainholder é especializada em importação e exportação, estamos focados em execução confiáveis, eficazes e flexíveis.

									</div>
									<div id="bts">
										<input type="submit" value="Saiba Mais" class="saibamais2off" id="continuar04">
									</div>

								</div>

							</div>

							
							
						</div>
						<div id="solucao03">
							<div id="imgsegmento">
								<img src="<?php echo URL_SITE ?>/assets/img/institucional/solucao03.png" style="width:100%;">
							</div>
							
							<div id="leftarrowb3" class="leftarrowb1"><img src="img/left.png" style="width:100%;">
							</div>	
							
							<div id="rightarrowb3" class="rightarrowb1"><img src="img/right.png" style="width:100%;">
							</div>
							
							<div id="activeb">
								<div id="titleb1">
									Exportação Direta
								</div>					
								<div id="textb1">
									A Gainholder é especializada em importação e exportação, estamos focados em execução confiáveis, eficazes e flexíveis.

								</div>
								<div id="bts">
									<input type="submit" value="Saiba Mais" class="saibamais2" id="continuar04">
								</div>
							</div>
							
							<div id="titleseg">
								Soluções Gainholder
							</div>
							
							<div id="alignbox">
								
								<div id="b11" >
									<div id="titleb1">
										Habitação
									</div>					
									<div id="textb1">
										A Gainholder é especializada em importação e exportação, estamos focados em execução confiáveis, eficazes e flexíveis.
									</div>
									<div id="bts">
										<input type="submit" value="Saiba Mais" class="saibamais2off" id="continuar04">
									</div>
								</div>
								
								<div id="b11" style="">
									<div id="titleb1">
										Homologação
									</div>					
									<div id="textb1">
										A Gainholder é especializada em importação e exportação, estamos focados em execução confiáveis, eficazes e flexíveis.
									</div>
									<div id="bts">
										<input type="submit" value="Saiba Mais" class="saibamais2off" id="continuar04">
									</div>
								</div>

							</div>

						</div>
						
					</div>
					
					<!-- // SOLUCAO -->	
					
					
					<!-- UNINDO EMPRESAS -->
					
					<div id="unindo"> 
						<div id="container">
							<div id="titleunindo">
								Gainholder, unindo empresas ao redor<Br>
								do mundo.
							</div>
							<div id="textunindo">
								A Gainholder é especializada em importação e exportação, estamos focados em fornecer soluções completas para projetos exigentes através de modelos de execução confiáveis, eficazes e flexíveis. 

								Somos reconhecidos por completar desafios dentro de quadros orçamentários rigorosos e horários exigentes, dentro do prazo e com qualidade. Nossas entregas abrangem desde estudos conceituais e de viabilidade, engenharia operacion.
							</div>
							<div id="textunindo2">
								<div id="left">
									<li>
										Agilidade e excelência
									</li>				
									<br>
									<li>
										Profissionalismo
									</li>
									<br>
									<li>
										Responsabilidade Social
									</li>
								</div>
								
								<div id="right">
									<li>
										Disciplina
									</li>				
									<br>
									<li>
										Relacionamento
									</li>
									<br>
									<li>
										Transparência
									</li>
								</div>
								
							</div>
						</div>
						
					</div>

				</div>
				
				<!-- // UNINDO EMPRESAS -->

				<!-- SEGMENTOS -->

				
				<div id="segmentomain">
					<div id="titleseg00">
						Segmentos
					</div>
					<div id="segmento01">
						<div id="imgsegmento">
							<img src="<?php echo URL_SITE ?>/assets/img/institucional/segmento01.png" style="width:100%;">
						</div>
						<div id="leftarrow1" class="leftarrow"><img src="<?php echo URL_SITE ?>/assets/img/institucional/left.png" style="width:100%;"></div>					
						<div id="rightarrow1" class="rightarrow"><img src="<?php echo URL_SITE ?>/assets/img/institucional/right.png" style="width:100%;"></div>
						<div id="active">
							<img src="<?php echo URL_SITE ?>/assets/img/institucional/car.png" style="width:100%">
						</div>
						<div id="titleseg">
							Segmentos de Atuação
						</div>
						<div id="alignicons">
							<div id="icon1">
								<img src="<?php echo URL_SITE ?>/assets/img/institucional/02off.png" style="width:100%;">
							</div>
							
							<div id="icon1">
								<img src="<?php echo URL_SITE ?>/assets/img/institucional/03off.png" style="width:100%;">
								
							</div>

							<div id="icon1">
								<img src="<?php echo URL_SITE ?>/assets/img/institucional/04off.png" style="width:100%;">
							</div>
							
							<div id="icon1">
								<img src="<?php echo URL_SITE ?>/assets/img/institucional/05off.png" style="width:100%;">
							</div>
							
							<div id="icon1">
								<img src="<?php echo URL_SITE ?>/assets/img/institucional/06off.png" style="width:100%;">
							</div>
							

						</div>
						
						<div id="titleseg1">
							Automotivo
						</div>
						<div id="titleseg2">
							stie. Morbi eleifend tellus ac dolor vulputate vehicula ut venenatis ipsum. Nam condimentum, lorem vitae egestas gravida, enim turpis. Nullam vehicula tortor interdum euismod commodo. Suspendisse vitae odio in neque pharetra tempor ac id turpis. Etiam ultricies lacinia tempus. Morbi non orci vel libero eleifend eleifend sit amet condimentum mauris.
						</div>

						
						
					</div>
					
					<div id="segmento02">
						<div id="imgsegmento">
							<img src="<?php echo URL_SITE ?>/assets/img/institucional/segmento02.png" style="width:100%;">
						</div>
						<div id="leftarrow2" class="leftarrow"><img src="<?php echo URL_SITE ?>/assets/img/institucional/left.png" style="width:100%;"></div>					
						<div id="rightarrow2" class="rightarrow">
							<img src="<?php echo URL_SITE ?>/assets/img/institucional/right.png" style="width:100%;"></div>
							<div id="active">
								<img src="<?php echo URL_SITE ?>/assets/img/institucional/02active.png" style="width:100%">
							</div>
							<div id="titleseg">
								Segmentos de Atuação
							</div>
							<div id="alignicons">
								<div id="icon1">
									<img src="<?php echo URL_SITE ?>/assets/img/institucional/01off.png" style="width:100%;">
								</div>
								
								<div id="icon1">
									<img src="<?php echo URL_SITE ?>/assets/img/institucional/03off.png" style="width:100%;">
									
								</div>

								<div id="icon1">
									<img src="<?php echo URL_SITE ?>/assets/img/institucional/04off.png" style="width:100%;">
								</div>
								
								<div id="icon1">
									<img src="<?php echo URL_SITE ?>/assets/img/institucional/05off.png" style="width:100%;">
								</div>
								
								<div id="icon1">
									<img src="<?php echo URL_SITE ?>/assets/img/institucional/06off.png" style="width:100%;">
								</div>
								

							</div>
							
							<div id="titleseg1">
								Exemplo 2
							</div>
							<div id="titleseg2">
								stie. Morbi eleifend tellus ac dolor vulputate vehicula ut venenatis ipsum. Nam condimentum, lorem vitae egestas gravida, enim turpis. Nullam vehicula tortor interdum euismod commodo. Suspendisse vitae odio in neque pharetra tempor ac id turpis. Etiam ultricies lacinia tempus. Morbi non orci vel libero eleifend eleifend sit amet condimentum mauris.
							</div>

							
							
						</div>
						
						<div id="segmento03">
							<div id="imgsegmento">
								<img src="<?php echo URL_SITE ?>/assets/img/institucional/segmento03.png" style="width:100%;">
							</div>
							<div id="leftarrow3" class="leftarrow"><img src="<?php echo URL_SITE ?>/assets/img/institucional/left.png" style="width:100%;"></div>					
							<div id="rightarrow3" class="rightarrow"><img src="<?php echo URL_SITE ?>/assets/img/institucional/right.png" style="width:100%;"></div>
							<div id="active">
								<img src="<?php echo URL_SITE ?>/assets/img/institucional/03active.png" style="width:100%">
							</div>
							<div id="titleseg">
								Segmentos de Atuação
							</div>
							<div id="alignicons">
								<div id="icon1">
									<img src="<?php echo URL_SITE ?>/assets/img/institucional/01off.png" style="width:100%;">
								</div>
								
								<div id="icon1">
									<img src="<?php echo URL_SITE ?>/assets/img/institucional/02off.png" style="width:100%;">
									
								</div>

								<div id="icon1">
									<img src="<?php echo URL_SITE ?>/assets/img/institucional/04off.png" style="width:100%;">
								</div>
								
								<div id="icon1">
									<img src="<?php echo URL_SITE ?>/assets/img/institucional/05off.png" style="width:100%;">
								</div>
								
								<div id="icon1">
									<img src="<?php echo URL_SITE ?>/assets/img/institucional/06off.png" style="width:100%;">
								</div>
								

							</div>
							
							<div id="titleseg1">
								Exemplo 3
							</div>
							<div id="titleseg2">
								stie. Morbi eleifend tellus ac dolor vulputate vehicula ut venenatis ipsum. Nam condimentum, lorem vitae egestas gravida, enim turpis. Nullam vehicula tortor interdum euismod commodo. Suspendisse vitae odio in neque pharetra tempor ac id turpis. Etiam ultricies lacinia tempus. Morbi non orci vel libero eleifend eleifend sit amet condimentum mauris.
							</div>

							
							
						</div>
						
						<div id="segmento04">
							<div id="imgsegmento">
								<img src="<?php echo URL_SITE ?>/assets/img/institucional/segmento04.png" style="width:100%;">
							</div>
							<div id="leftarrow4" class="leftarrow"><img src="img/left.png" style="width:100%;"></div>					
							<div id="rightarrow4" class="rightarrow"><img src="img/right.png" style="width:100%;"></div>
							<div id="active">
								<img src="<?php echo URL_SITE ?>/assets/img/institucional/04active.png" style="width:100%">
							</div>
							<div id="titleseg">
								Segmentos de Atuação
							</div>
							<div id="alignicons">
								<div id="icon1">
									<img src="<?php echo URL_SITE ?>/assets/img/institucional/01off.png" style="width:100%;">
								</div>
								
								<div id="icon1">
									<img src="<?php echo URL_SITE ?>/assets/img/institucional/02off.png" style="width:100%;">
									
								</div>

								<div id="icon1">
									<img src="<?php echo URL_SITE ?>/assets/img/institucional/03off.png" style="width:100%;">
								</div>
								
								<div id="icon1">
									<img src="<?php echo URL_SITE ?>/assets/img/institucional/05off.png" style="width:100%;">
								</div>
								
								<div id="icon1">
									<img src="<?php echo URL_SITE ?>/assets/img/institucional/06off.png" style="width:100%;">
								</div>
								

							</div>
							
							<div id="titleseg1">
								Exemplo 4
							</div>
							<div id="titleseg2">
								stie. Morbi eleifend tellus ac dolor vulputate vehicula ut venenatis ipsum. Nam condimentum, lorem vitae egestas gravida, enim turpis. Nullam vehicula tortor interdum euismod commodo. Suspendisse vitae odio in neque pharetra tempor ac id turpis. Etiam ultricies lacinia tempus. Morbi non orci vel libero eleifend eleifend sit amet condimentum mauris.
							</div>

							
							
						</div>
						<div id="segmento05">
							<div id="imgsegmento">
								<img src="<?php echo URL_SITE ?>/assets/img/institucional/segmento05.png" style="width:100%;">
							</div>
							<div id="leftarrow5" class="leftarrow"><img src="<?php echo URL_SITE ?>/assets/img/institucional/left.png" style="width:100%;"></div>					
							<div id="rightarrow5" class="rightarrow"><img src="<?php echo URL_SITE ?>/assets/img/institucional/right.png" style="width:100%;"></div>
							<div id="active">
								<img src="<?php echo URL_SITE ?>/assets/img/institucional/05active.png" style="width:100%">
							</div>
							<div id="titleseg">
								Segmentos de Atuação
							</div>
							<div id="alignicons">
								<div id="icon1">
									<img src="<?php echo URL_SITE ?>/assets/img/institucional/01off.png" style="width:100%;">
								</div>
								
								<div id="icon1">
									<img src="<?php echo URL_SITE ?>/assets/img/institucional/02off.png" style="width:100%;">
									
								</div>

								<div id="icon1">
									<img src="<?php echo URL_SITE ?>/assets/img/institucional/03off.png" style="width:100%;">
								</div>
								
								<div id="icon1">
									<img src="<?php echo URL_SITE ?>/assets/img/institucional/04off.png" style="width:100%;">
								</div>
								
								<div id="icon1">
									<img src="<?php echo URL_SITE ?>/assets/img/institucional/06off.png" style="width:100%;">
								</div>
								

							</div>
							
							<div id="titleseg1">
								Exemplo 5
							</div>
							<div id="titleseg2">
								stie. Morbi eleifend tellus ac dolor vulputate vehicula ut venenatis ipsum. Nam condimentum, lorem vitae egestas gravida, enim turpis. Nullam vehicula tortor interdum euismod commodo. Suspendisse vitae odio in neque pharetra tempor ac id turpis. Etiam ultricies lacinia tempus. Morbi non orci vel libero eleifend eleifend sit amet condimentum mauris.
							</div>

							
							
						</div>
						
						
						<div id="segmento06">
							<div id="imgsegmento">
								<img src="<?php echo URL_SITE ?>/assets/img/institucional/segmento06.png" style="width:100%;">
							</div>
							<div id="leftarrow6" class="leftarrow"><img src="<?php echo URL_SITE ?>/assets/img/institucional/left.png" style="width:100%;"></div>					
							<div id="rightarrow6" class="rightarrow"><img src="<?php echo URL_SITE ?>/assets/img/institucional/right.png" style="width:100%;"></div>
							<div id="active">
								<img src="<?php echo URL_SITE ?>/assets/img/institucional/06active.png" style="width:100%">
							</div>
							<div id="titleseg">
								Segmentos de Atuação
							</div>
							<div id="alignicons">
								<div id="icon1">
									<img src="<?php echo URL_SITE ?>/assets/img/institucional/01off.png" style="width:100%;">
								</div>
								
								<div id="icon1">
									<img src="<?php echo URL_SITE ?>/assets/img/institucional/02off.png" style="width:100%;">
									
								</div>

								<div id="icon1">
									<img src="<?php echo URL_SITE ?>/assets/img/institucional/03off.png" style="width:100%;">
								</div>
								
								<div id="icon1">
									<img src="<?php echo URL_SITE ?>/assets/img/institucional/04off.png" style="width:100%;">
								</div>
								
								<div id="icon1">
									<img src="<?php echo URL_SITE ?>/assets/img/institucional/05off.png" style="width:100%;">
								</div>
								

							</div>
							
							<div id="titleseg1">
								Exemplo 6
							</div>
							<div id="titleseg2">
								stie. Morbi eleifend tellus ac dolor vulputate vehicula ut venenatis ipsum. Nam condimentum, lorem vitae egestas gravida, enim turpis. Nullam vehicula tortor interdum euismod commodo. Suspendisse vitae odio in neque pharetra tempor ac id turpis. Etiam ultricies lacinia tempus. Morbi non orci vel libero eleifend eleifend sit amet condimentum mauris.
							</div>

							
							
						</div>
						<!-- SEGMENTOS DE ATUAÇÃO-->

						
						<!-- HISTORIA -->

						<div id="historia">
							<div id="container">
								<div id="titlehistoria">
									História Gainholder
								</div>
								<div id="texthistoria">
									<div id="left1">
										Fundada em 2003 por Leandro Braga, a Gainholder é um sonho realizado com muita força de vontade, carregando coragem, inovação, e fiel à demanda do mercado global que conquistou seu espaço entre as companhias de importação e exportação. Em meados do ano 2000 entre trocas de governo e privatizações, abrimos caminho para nossa expansão, ganhamos a confiança do mercado antes tomado pelas companhias de domínio público. 

										A Gainholder é regida pela Lei 6.404 das Sociedades Anônimas. Seu capital social é dividido em ações de igual valor entre os sócios e pode ser transacionado livremente. Forjada em um difícil momento da crise nacional, a Gainholder criou laços estreitos com seus clientes solucionando cada projeto proposto com eficácia e responsabilidade somando conhecimento e experiência à necessidade de quem a busca. 
									</div>				
									
									<div id="right2">
										Fundada em 2003 por Leandro Braga, a Gainholder é um sonho realizado com muita força de vontade, carregando coragem, inovação, e fiel à demanda do mercado global que conquistou seu espaço entre as companhias de importação e exportação. Em meados do ano 2000 entre trocas de governo e privatizações, abrimos caminho para nossa expansão, ganhamos a confiança do mercado antes tomado pelas companhias de domínio público. 

										A Gainholder é regida pela Lei 6.404 das Sociedades Anônimas. Seu capital social é dividido em ações de igual valor entre os sócios e pode ser transacionado livremente. Forjada em um difícil momento da crise nacional, a Gainholder criou laços estreitos com seus clientes solucionando cada projeto proposto com eficácia e responsabilidade somando conhecimento e experiência à necessidade de quem a busca. 
									</div>
								</div>
								
							</div>
							
							
							
							
						</div>	</div>

						<!--  /// HISTORIA -->
						
					</div>

				</div>