<link href="<?php echo URL_SITE ?>/assets/css/index01mobile.css" rel="stylesheet" type="text/css" media="screen and (max-width:1000px)">
<link href="<?php echo URL_SITE ?>/assets/css/index01pc.css" rel="stylesheet" type="text/css" media="screen and (min-width:1001px)">


<style type="text/css">

.background-loader{
    display: block;
    z-index: 10000;
    background: #00000070;
    position: fixed;
    width: 100%;
    height: 100%;
    left: 0;
    top: 0;
    overflow: auto;
}

.background-loader p{
    font-size: 20px;
    font-family: 'Robot', sans-serif;
    text-align: center;
    color: #fff;
    font-weight: bolder;
    text-transform: uppercase;
}

.loader {
    border: 16px solid #f3f3f3; /* Light grey */
    border-top: 16px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 120px;
    height: 120px;
    animation: spin 2s linear infinite;
    margin: auto;
    margin-top: 25%;    
}

@keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
}	
</style>

<!-- <script type="text/javascript" src="main.js"></script>
	<script src="http://code.jquery.com/jquery-latest.js"></script>  -->
	
	<div class="index" style="min-height: 1500px;">

		<div id="container">
			<!-- URL -->
			<div id="traject">
				<div id="links">Home | Soluções | <label style="color:red; 	font-weight: 500;">Index</label></div>
			</div>

			<!-- BANNER -->
			<div id="banner">
				<img src="<?php echo URL_SITE ?>/assets/img/banner02.png" style="width:100%; ">
			</div>

			<!-- PROCESSO 1 -->
			<div id="process1">

				<div id="process">
					<div id="alignicon">

						<div id="p1">
							<div id="lineactive"></div>
							<div id="circleactive">
								<label style="">1</label>
							</div>
							<div class="textactive">Validação</div>
						</div>

						<div id="p2">
							<div id="lineoff"></div>
							<div id="circleoff">
								<label style="">2</label>
							</div>
							<div class="textoff">Tipificação</div>
						</div>

						<div id="p3">
							<div id="lineoff"></div>
							<div id="circleoff">
								<label style="">3</label>
							</div>
							<div class="textoff">Produto</div>
						</div>					

						<div id="p4">
							<div id="lineoff"></div>
							<div id="circleoff">
								<label style="">4</label>
							</div>
							<div class="textoff">Incoterm</div>
						</div>					

						<div id="p5">
							<div id="lineoff"></div>
							<div id="circleoff">
								<label style="">5</label>
							</div>
							<div class="textoff">Revisão</div>
						</div>

					</div>
				</div>

				<div id="content">
					<div id="container2">
						<div id="title1">Primeiros passos para Index</div>
						<div class="divid"></div>

						<div class="checkbox">
							<div class="perguntas">Quantos NCM'S você precisa para sua cotação?</div>
							<div id="align">
								<div class="opcao1">
									<input name="opcaoNcm" id="tmp1" class="opcaoNcm" type="checkbox" value="1"/>De 1 a 2
								</div>
								<div class="opcao2">
									<input name="opcaoNcm" id="tmp2" class="opcaoNcm" type="checkbox" value="2">De 2 a 5
								</div>
								<div class="opcao3">
									<input name="opcaoNcm" id="tmp3" class="opcaoNcm" type="checkbox" value="3" >Mais que 5
								</div>
							</div>
						</div>

						<div class="span">Não sabe o que é <b>NCM?</b> Clique aqui</div>

						<div class="checkbox">
							<div class="perguntas">E quantas casas decimais?</div>
							<div id="align">
								<div class="opcao1">
									<input id="tmp" class="casasDecimais" value="1" type="checkbox" style="margin-right:8px; "/> Até 2 casas <br>
									<label class="correcaoLabel" style="margin-left:22px; font-style:italic; color:#cfcfcf"> (Ex. 1.000,<label class="correcaoLabel" style="color:red">00</label>)</label>
								</div>
								
								<div class="opcao2">
									<input id="tmp" class="casasDecimais" value="2"  type="checkbox" style="margin-right:8px; margin-left:20px;" />Mais de 5 casas <br>
									<label class="correcaoLabel" style="margin-left:42px; font-style:italic; color:#cfcfcf">(Ex. 1.000,<label class="correcaoLabel" style="color:red">00000</label>)</label>
								</div>
							</div>
						</div>

						<div class="divid"></div>

						<div class="span2">
							<div id="voltar"></div>	
							<div id="checkconcordar1"></div>
							<div id="continuar">
								<input type="submit" value="Continuar" class="continue" id="continuar01">
							</div>						
						</div>	
						
					</div>
				</div>

			</div>

		</div>		

		<!-- PROCESSO 2 -->
		<div id="process2">
			<div id="CONTAINER" style="background: #fff">

				<div id="process">
					<div id="alignicon">

						<div id="p1">
							<div id="lineoff"></div>
							<div id="circleoff">
								<label style="">1</label>
							</div>
							<div class="textoff">Validação</div>
						</div>

						<div id="p2">
							<div id="lineactive"></div>
							<div id="circleactive">
								<label style="">2</label>
							</div>
							<div class="textactive">Tipificação</div>
						</div>

						<div id="p2">
							<div id="lineoff"></div>
							<div id="circleoff">
								<label style="">3</label>
							</div>
							<div class="textoff">Produto</div>
						</div>					

						<div id="p2">
							<div id="lineoff"></div>
							<div id="circleoff">
								<label style="">4</label>
							</div>
							<div class="textoff">Incoterm</div>
						</div>					

						<div id="p2">
							<div id="lineoff"></div>
							<div id="circleoff">
								<label style="">5</label>
							</div>
							<div class="textoff">Revisão</div>
						</div>

					</div>
				</div>

				<div id="content">
					<div id="container2">

						<div id="title8">Tipificação do embarque</div>
						<div class="divid"></div>

						<div id="align">
							<select name="tiposEmbarques" class="tiposEmbarques">
								<option value="">Selecione...</option>
								<option value="Imobilizado">Imobilizado</option>
								<option value="Revenda">Revenda</option>
								<option value="Materia-prima">Matéria Prima</option>
								<option value="Insumos-produtivos">Insumos Produtivos</option>
							</select>
						</div>			

						<div class="divid"></div>

						<div class="span2">
							<div id="voltar"><input type="submit" value="Voltar" class="continue" id="voltar01"></div>	
							<div id="checkconcordar1"></div>
							<div id="continuar">
								<input type="submit" value="Continuar" class="continue" id="continuar02">
							</div>						
						</div>	

					</div>
				</div>

			</div>
		</div>

		<!-- PROCESSO 3 -->
		<div id="process3">

			<div id="container" style="background: #fff">

				<div id="process">
					<div id="alignicon">

						<div id="p1">
							<div id="lineoff"></div>
							<div id="circleoff">
								<label style="">1</label>
							</div>
							<div class="textoff">Validação</div>
						</div>

						<div id="p2">
							<div id="lineoff"></div>
							<div id="circleoff">
								<label style="">2</label>
							</div>
							<div class="textoff">Tipificação</div>
						</div>

						<div id="p2">
							<div id="lineactive"></div>
							<div id="circleactive">
								<label style="">3</label>
							</div>
							<div class="textactive">Produto</div>
						</div>					

						<div id="p2">
							<div id="lineoff"></div>
							<div id="circleoff">
								<label style="">4</label>
							</div>
							<div class="textoff">Incoterm</div>
						</div>					

						<div id="p2">
							<div id="lineoff"></div>
							<div id="circleoff">
								<label style="">5</label>
							</div>
							<div class="textoff">Revisão</div>
						</div>

					</div>
				</div>

				<div id="content">
					<div id="alinhar">
						<div id="container2">
							<div id="title8">Incoterms</div>
							<div class="divid8"></div>
							<div id="boxes">

								<div class="formulariobox formulario-produtos">

									<div class="perguntas4">Item 1</div>	

									<div id="alignform">

										<div id="produtoNome" style="margin-bottom: 10px;">
											<input name="produtoNome" type="text" class="form-controlproduto produtoNome validador" placeholder="Produto"/>
										</div>

										<div id="valorunitario">
											<input type="text" name="produtoValor" class="form-controlproduto produtoValor validador" placeholder="Valor Unitário"/>
										</div>

										<div id="moeda" style="margin-top: 0px;">
											<select name="produtoMoeda" class="produtoMoeda" >
												<option value="" selected>Moeda</option>
												<option value="usd">USD</option>
											</select>
										</div>

										<div id="quantidade">
											<input type="text" class="form-controlproduto produtoQuantidade validador" placeholder="Quantidade"/>
										</div>

										<div id="boxncm">
											<div id="nomeproduto">
												<input type="text" class="form-controlproduto produtoCodigoNcm validador" placeholder="NCM Código"/>
											</div>
											<input type="submit" class="botaocontratar" value="Contratar Classificação"/>
											<input type="submit" class="botaosaibamais" value="Saiba Mais"/>
										</div>

									</div>

								</div>

								<div id="campo-adiciona-formulariobox"></div>

								<div class="span3">			
									<input type="submit" class="botaoadd" value="Adicionar Item"/>
								</div>

								<div class="divid5"></div>

								<div class="span2">

									<div id="voltar">
										<input type="submit" value="Voltar" class="continue" id="voltar02">
									</div>	

									<div id="checkconcordar1"></div>

									<div id="continuar">
										<input type="submit" value="Continuar" class="continue" id="continuar03">
									</div>	

								</div>	

							</div>
						</div>
					</div>
				</div>

			</div>

		</div>

		<!-- PROCESSO 4 -->
		<div id="process4">

			<div id="CONTAINER" style="background: #fff">

				<div id="process">
					<div id="alignicon">

						<div id="p1">
							<div id="lineoff"></div>
							<div id="circleoff">
								<label style="">1</label>
							</div>
							<div class="textoff">Validação</div>
						</div>

						<div id="p2">
							<div id="lineoff"></div>
							<div id="circleoff">
								<label style="">2</label>
							</div>
							<div class="textoff">Tipificação</div>
						</div>

						<div id="p2">
							<div id="lineoff"></div>
							<div id="circleoff">
								<label style="">3</label>
							</div>
							<div class="textoff">Produto</div>
						</div>					

						<div id="p2">
							<div id="lineactive"></div>
							<div id="circleactive">
								<label style="">4</label>
							</div>
							<div class="textactive">Incoterm</div>
						</div>					

						<div id="p2">
							<div id="lineoff"></div>
							<div id="circleoff">
								<label style="">5</label>
							</div>
							<div class="textoff">Revisão</div>
						</div>

					</div>
				</div>

				<div id="content">
					<div id="alinhar">
						<div id="container2">
							<div id="title8">Incoterms</div>
							<div class="divid9"></div>

							<div id="boxes">

								<div class="checkbox4">
									<div class="opcao">
										<div class="perguntas4">MARÍTMO</div>	

										<div class="op1">
											<input id="marcadorMaritimo" class="marcadorMaritimo validadorFreteMaritmo" type="checkbox"  />
											<label style="font-weight: 500; ;">EXW/FOB/FAS</label>
										</div>

										<input type="text" class="form-control valorFrete validadorFreteMaritmo" id="freteUsd"  placeholder="Valor do frete internacional USD">
										<br>

										<div class="op1">
											<input id="marcadorMaritimo2" class="marcadorMaritimo2 validadorFreteMaritmo" type="checkbox"  />
											<label style="font-weight: 500;">CIF/CFR/CPT</label>	
										</div>

										<br>
										<div id="subtext" >Assinale essa opção se o valor do frete internacional <br> está incluso no preço da mercadoria.</div>
									</div>
								</div>

								<div class="checkbox5">
									<div class="opcao">

										<div class="perguntas4">AÉREO</div>	

										<div class="op1">
											<input id="marcadorAereo" class="marcadorAereo validadorFreteAereo" type="checkbox"  />
											<label style="	font-weight: 500; ;">EXW/FCA</label>
										</div>

										<input type="text" class="form-control validadorFreteAereo valorFrete" id="freteUsd" placeholder="Valor do frete internacional USD">
										<br>

										<div class="op1">
											<input id="marcadorAereo2" class="marcadorAereo2 validadorFreteAereo" type="checkbox"  />
											<label style="	font-weight: 500;">CIF/CFR/CPT</label>	
										</div>

										<br>
										<div id="subtext" >Assinale essa opção se o valor do frete internacional <br> está incluso no preço da mercadoria.</div>
									</div>
								</div>

								<div class="span">Não sabe o que é <b>Incoterms</b> e suas derivações? Clique aqui</div>

								<div class="divid4"></div>

								<div class="span2">
									<div id="voltar">
										<input type="submit" value="Voltar" class="continue" id="voltar03">
									</div>	
									<div id="checkconcordar1"></div>
									<div id="continuar">
										<input type="submit" value="Continuar" class="continue" id="continuar04">
									</div>						
								</div>

							</div>

						</div>
					</div>
				</div>
			</div>

		</div>		

		<!-- PROCESSO 5 -->
		<div id="process5">

			<div id="CONTAINER" style="background: #fff">

				<div id="process">

					<div id="alignicon">

						<div id="p1">
							<div id="lineoff"></div>
							<div id="circleoff">
								<label style="">1</label>
							</div>
							<div class="textoff">Validação</div>
						</div>

						<div id="p2">
							<div id="lineoff"></div>
							<div id="circleoff">
								<label style="">2</label>
							</div>
							<div class="textoff">Tipificação</div>
						</div>

						<div id="p2">
							<div id="lineoff"></div>
							<div id="circleoff">
								<label style="">3</label>
							</div>
							<div class="textoff">Produto</div>
						</div>					

						<div id="p2">
							<div id="lineoff"></div>
							<div id="circleoff">
								<label style="">4</label>
							</div>
							<div class="textoff">Incoterm</div>
						</div>					

						<div id="p2">
							<div id="lineactive"></div>
							<div id="circleactive">
								<label style="">5</label>
							</div>
							<div class="textactive">Revisão</div>
						</div>

					</div>
				</div>

				<input type="hidden" name="qtdNcm" class="qtdNcm">
				<input type="hidden" name="qtdProdutoGerado" class="qtdProdutoGerado" value="0">
				<input type="hidden" name="qtdCasasDecimais" class="qtdCasasDecimais">
				<input type="hidden" name="tipoEscolhidoEmbarque" class="tipoEscolhidoEmbarque">
				<input type="hidden" name="produtosNome" class="produtosNome">
				<input type="hidden" name="produtosPreco" class="produtosPreco">
				<input type="hidden" name="produtosQuantidade" class="produtosQuantidade">
				<input type="hidden" name="produtosCodigosNcm" class="produtosCodigosNcm">
				<input type="hidden" name="tipoFreteSelecionado" class="tipoFreteSelecionado">
				<input type="hidden" name="valorFreteSelecionado" class="valorFreteSelecionado">

				<div id="content">
					<div id="alinhar">
						<div id="container2">
							<div id="title8">Incoterms</div>
							<div class="divid8"></div>
							<div id="boxes">
								<div class="resumo">
									<div class="row">
										<div class="col-md-6">
											<div class="opcao">
												<div class="titleresumo">Resumo dados Index</div>	

												<div class="titleitens">
													Natureza
													<div class="natureza"></div>	
													<div class="painel-resumo">
													</div>
													<div class="titleitens">Incoterm</div>		
													<div class="incoterm"></div>
													<br>
													<br>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="informacoesgerais">
												<div class="opcao">
													<div class="titleinformacoesgerais">Informações gerais</div>	
													<div id="textinformacoesgerais" > a) O requerente está ciente de que a apresentação de informações ou documentos falsos caracteriza crime.
														<br><br> b) Assumo integral responsabilidade pela fidelidade das informações aqui contidas neste formulário. 
														<br><br> c) Ausência ou divergência das informações prestadas pelo cliente, dependendo a sua natureza e grau de relevância para a operação, pode impedir o cumprimentos dos termos dessa proposta. 
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="divid4"></div>

									<div class="span2">

										<div id="voltar">
											<input type="submit" value="Voltar" class="continue" id="voltar04">
										</div>	

										<div id="checkconcordar">
											<div id="alignconcordar">
												<input id="tmp" type="checkbox" style="margin-right:8px; margin-left:20px;" /> Concordo com os termos
											</div>
										</div>

										<div id="continuar">
											<input type="submit" value="Enviar" class="continue" id="enviar">
										</div>	

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>		

            <div class="background-loader" style="display: none;">
                <div class="loader"></div>  
                <p>Processando...</p>                
            </div> 

			<div class="banner">
				<img src="<?php echo URL_SITE ?>/assets/img/banner03.png" style="width:100%; margin-top:0px; ">
			</div>
		</div>

		<!-- PROCESSO final -->
		<div id="final">
			<div id="CONTAINER" style="background: #fff">
				<div id="content">
					<div id="alinhar">
						
						<div id="container2">
							<div id="title1" style="text-align:center; color:#181f3d">Obrigado <?php echo $_SESSION['cliente_site']['nome']; ?></div>
							<div class="divid"></div>
							
							<div id="boxesfinale">
								
								<div class="formulariobox">
									<div class="perguntas4 mensagemFinal" style="color:#373737; 	font-weight: 300;">Em 24 horas a análise Index Basic será entregue.</div>	
								</div>

								<div class="span">			
									<input type="button" onclick="window.location='<?php echo URL_SITE ?>/solucoes'" class="botaoinicio" value="Página Inicial"/>
								</div>

							</div>

						</div>
					</div>

				</div>
			</div>
		</div>

	</div>
