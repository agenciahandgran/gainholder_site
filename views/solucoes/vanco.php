

<link href="<?php echo URL_SITE ?>/assets/css/VAMCOMOBILE.css" rel="stylesheet" type="text/css" media="screen and (max-width:1000px)">
<link href="<?php echo URL_SITE ?>/assets/css/VAMCOpc.css" rel="stylesheet" type="text/css" media="screen and (min-width:1001px)">
	
<div id="fb-root"></div>
<div class="container">

	<!-- conteúdo -->	 	
	<div id="container">
		
		<div id="traject">
			<div id="links">Home | Soluções | <label style="color:red; 	font-weight: 500;">Index</label></div>
		</div>
				
		<div id="banner">
			<div id="imgbanner">
				<div id="titlebann">Importamos seu carro de luxo</div>			
				<div id="subtitlebann">A vamco é fundamento para a construção de uma estratégia<br> de venda internacional. O planejamento de venda realizado por uma <br> equipe despreparada e com ações dispersas do obvjetivo final <br> da empresa pode tornar o processo de exportar impraticável.</div>			
			</div>
		</div>
						
		<!-- PROCESSO 1 -->
		<div id="process1">
			<div id="process">
				<div id="alignicon">
					
					<div id="p1">
						<div id="lineactive"></div>
						<div id="circleactive">1</div>
						<div class="textactive">Validação</div>
					</div>
				
					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">2</div>
						<div class="textoff">Tipificação</div>
					</div>
				
					<div id="p3">
						<div id="lineoff"></div>
						<div id="circleoff">3</div>
						<div class="textoff">Logística</div>
					</div>					
					
					<div id="p4">
						<div id="lineoff"></div>
						<div id="circleoff">4</div>
						<div class="textoff">Incoterm</div>
					</div>					
				
				</div>
			</div>
				
			<div id="content">
				<div id="container2">
					<div id="title1">Tipificação de Embarque</div>
					<div class="divid"></div>

					<!-- conteúdo processo 1 -->					
					<div id="conteudo01">
						
						<div id="align01">
							
							<div id="sel01">
								<select onChange='setText()' id='option1'>
									<option value='0'>Modal</option>
									<option value='1'>Aéreo</option>
									<option value='2'>Marítmo</option>
									<option value='3'>Rodoviário</option>
								</select>
							</div>	

							<div id="sel02">
								<select>
									<option>Modalidade</option>
									<option>01</option>						   
									<option>02</option>
								</select>
							</div>
														
							<div id="sel03">
								<select>
									<option>Natureza</option>
									<option>01</option>						   
									<option>02</option>
								</select>
							</div>
										
						</div>			
								
						<!--//// conteúdo processo 1 -->
						<div class="divid"></div>
						<div class="span2">
							<div id="voltar"></div>	

							<div id="checkconcordar1"></div>

							<div id="continuar">
								<div id='newInput'>
									<!-- lugar onde surgirá o botão -->
									<div id="botaoinicial">
										<input type="submit" value="Continuar" class="continue0" id="continuarnulo">
									</div>	

									<div id="btaereo">
										<input type="submit" value="Continuar" class="continue0" id="btaereoclick">
									</div>	

									<div id="btnavio">
										<input type="submit" value="Continuar" class="continue0" id="btnavioclick">
									</div>	

									<div id="btrodoviario">
										<input type="submit" value="Continuar" class="continue0" id="btrodoviarioclick">
									</div>

								</div>
							</div>

						</div>	
									
					</div>
				</div>
			</div>
		</div>	

	</div>
		
	<!-- PROCESSO 2 aereo -->	
	<div id="process2aereo">
	
		<div id="CONTAINER">
			<div id="process">
				<div id="alignicon">
					
					<div id="p1">
						<div id="lineoff"></div>
						<div id="circleoff">1</div>
						<div class="textoff">Validação</div>
					</div>
				
					<div id="p2">
						<div id="lineactive"></div>
						<div id="circleactive">2</div>
						<div class="textactive">Tipificação</div>
					</div>
				
					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">3</div>
						<div class="textoff">Logística</div>
					</div>					
					
					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">4</div>
						<div class="textoff">Incoterm</div>
					</div>					
					
				</div>
			</div>
			
			<!--conteúdo processo 2 -->		
			<div id="alinhar">
				<div id="container2">
					<div id="title8">Incoterms</div>

					<div id="imgaereo">
						<img src="<?php echo URL_SITE ?>/assets/img/aereo.png" style="width:250px; float:right;    margin-top: 75px;">
					</div>

					<div class="divid8"></div>

					<div id="boxes">
						<div class="formulariobox">

							<div class="perguntas4">Item 1 aereo</div>	

							<div id="alignform">

								<div id="nomeproduto">
									<input type="email" class="form-controlproduto" id="email" placeholder="Nome do Produto"/>
								</div>

								<div id="valorunitario">
									<input type="email" class="form-controlproduto" id="email" placeholder="Valor Unitário"/>
								</div>						

								<div id="moeda">
									<select>
										<option>Moeda</option>
										<option>01</option>
									</select>
								</div>

								<div id="quanti">
									<input type="email" class="form-controlproduto" id="email" placeholder="Valor Unitário"/>
								</div>

								<div id="quantidade">
									<input type="email" class="form-controlproduto" id="email" placeholder="Quantidade"/>
								</div>

								<div id="boxncm">
									<div id="nomeproduto">
										<input type="email" class="form-controlproduto" id="email" placeholder="Nome do Produto"/>
									</div>
									<input type="submit" class="botaocontratar" value="Contratar Classificação"/>
									<input type="submit" class="botaosaibamais" value="Saiba Mais"/>
								</div>

								<div id="boxpeso">
									<div id="pesoproduto">
										<input type="email" class="form-controlproduto" id="email" placeholder="Peso Líquido (kg)"/>
									</div>
									<div id="naosabecheck">
										<div id="alignnaosabe">
											<input id="tmp" type="checkbox" style="margin-right:8px; " /> Não Sabe
											<div class="what0">
												<img src="<?php echo URL_SITE ?>/assets/img/what.png" style=" width:30px; float:right;">
											</div>
										</div>
									</div>
								</div>

								<div id="message">Peso líquido do produto livre de<br> embalagem. Caso não saiba o peso <br> líquido selecione a opção "Não sabe".</div>
							</div>

						</div>

						<div class="span3">			
							<input type="submit" class="botaoadd" value="Adicionar Item"/>
						</div>

						<div class="divid5"></div>
						
						<div class="span2">
							<div id="voltar">
								<input type="submit" value="Voltar" class="continue" id="voltaraereo2">
							</div>	

							<div id="checkconcordar1"></div>

							<div id="continuar">
								<input type="submit" value="Continuar" class="continue" id="btaereo2">
							</div>						
						</div>	
					</div>

				</div>
			</div>

		</div>

	</div>
		
	<!-- PROCESSO 2 maritimo -->
	<div id="process2maritmo">
		<div id="CONTAINER">
			
			<div id="process">
				<div id="alignicon">
					
					<div id="p1">
						<div id="lineoff"></div>
						<div id="circleoff">1</div>
						<div class="textoff">Validação</div>
					</div>
				
					<div id="p2">
						<div id="lineactive"></div>
						<div id="circleactive">2</div>
						<div class="textactive">Tipificação</div>
					</div>
				
					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">3</div>
						<div class="textoff">Logística</div>
					</div>					
					
					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">4</div>
						<div class="textoff">Incoterm</div>
					</div>					
				
				</div>
			</div>
			
			<!--conteúdo processo 2 -->		
			<div id="alinhar">
				
				<div id="container2">
					<div id="title8">Incoterms</div>	
					
					<div id="imgmaritmo">
						<img src="<?php echo URL_SITE ?>/assets/img/mar.png" style="width:250px; float:right;    margin-top: 75px;">
					</div>
					
					<div class="divid8"></div>
	
					<div id="boxes">
						
						<div class="formulariobox">
								
							<div class="perguntas4">Item 1 maritmo</div>	
						
							<div id="alignform">

								<div id="nomeproduto">
									<input type="email" class="form-controlproduto" id="email" placeholder="Nome do Produto"/>
								</div>

								<div id="valorunitario">
									<input type="email" class="form-controlproduto" id="email" placeholder="Valor Unitário"/>
								</div>						

								<div id="moeda">
									<select>
										<option>Moeda</option>
										<option>01</option>
									</select>
								</div>

								<div id="quanti">
									<input type="email" class="form-controlproduto" id="email" placeholder="Valor Unitário"/>
								</div>

								<div id="quantidade">
									<input type="email" class="form-controlproduto" id="email" placeholder="Quantidade"/>
								</div>

								<div id="boxncm">
									<div id="nomeproduto">
										<input type="email" class="form-controlproduto" id="email" placeholder="Nome do Produto"/>
									</div>
									<input type="submit" class="botaocontratar" value="Contratar Classificação"/>
									<input type="submit" class="botaosaibamais" value="Saiba Mais"/>
								</div>

								<div id="boxpeso">
									
									<div id="pesoproduto">
										<input type="email" class="form-controlproduto" id="email" placeholder="Peso Líquido (kg)"/>
									</div>
									
									<div id="naosabecheck">
										<div id="alignnaosabe">
											<input id="tmp" type="checkbox" style="margin-right:8px; " /> Não Sabe
											<div id="what01">
												<img src="<?php echo URL_SITE ?>/assets/img/what.png" style=" width:30px; float:right;">
											</div>
										</div>
									</div>

								</div>

								<div id="message1">Peso líquido do produto livre de<br> embalagem. Caso não saiba o peso <br> líquido selecione a opção "Não sabe".</div>
							</div>
									
						</div>

						<div class="span3">			
							<input type="submit" class="botaoadd" value="Adicionar Item"/>
						</div>
				
						<div class="divid5"></div>
	
						<div class="span2">
							<div id="voltar">
								<input type="submit" value="Voltar" class="continue" id="voltarmaritmo2">
							</div>	

							<div id="checkconcordar1"></div>

							<div id="continuar">
								<input type="submit" value="Continuar" class="continue" id="btmaritmo2">
							</div>						
						</div>	
					
					</div>
				</div>
			</div>
		</div>								
	</div>		
			
	<div id="process2rodoviario">
	
		<div id="CONTAINER">
			<div id="process">
				<div id="alignicon">
					
					<div id="p1">
						<div id="lineoff"></div>
						<div id="circleoff">1</div>
						<div class="textoff">Validação</div>
					</div>
				
					<div id="p2">
						<div id="lineactive"></div>
						<div id="circleactive">2</div>
						<div class="textactive">Tipificação</div>
					</div>
				
					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">3</div>
						<div class="textoff">Logística</div>
					</div>					
					
					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">4</div>
						<div class="textoff">Incoterm</div>
					</div>					
					
				</div>
			</div>
			
			<!--conteúdo processo 2 -->		
			<div id="alinhar">
				<div id="container2">
					
					<div id="title8">Incoterms</div>
					<div id="imgrodovia">
						<img src="<?php echo URL_SITE ?>/assets/img/rodovia.png" style="width:210px; float:right;">
					</div>
					<div class="divid8"></div>
							
					<div id="boxes">
						<div class="formulariobox">
							
							<div class="perguntas4">Item 1 rodoviario</div>	
							<div id="alignform">
								
								<div id="nomeproduto">
									<input type="email" class="form-controlproduto" id="email" placeholder="Nome do Produto"/>
								</div>

								<div id="valorunitario">
									<input type="email" class="form-controlproduto" id="email" placeholder="Valor Unitário"/>
								</div>						

								<div id="moeda">
									<select>
										<option>Moeda</option>
										<option>01</option>
									</select>
								</div>

								<div id="quanti">
									<input type="email" class="form-controlproduto" id="email" placeholder="Valor Unitário"/>
								</div>

								<div id="quantidade">
									<input type="email" class="form-controlproduto" id="email" placeholder="Quantidade"/>
								</div>
								
								<div id="boxncm">
									<div id="nomeproduto">
										<input type="email" class="form-controlproduto" id="email" placeholder="Nome do Produto"/>
									</div>
									<input type="submit" class="botaocontratar" value="Contratar Classificação"/>
									<input type="submit" class="botaosaibamais" value="Saiba Mais"/>
								</div>

								<div id="boxpeso">
									
									<div id="pesoproduto">
										<input type="email" class="form-controlproduto" id="email" placeholder="Peso Líquido (kg)"/>
									</div>

									<div id="naosabecheck">
										<div id="alignnaosabe">
											<input id="tmp" type="checkbox" style="margin-right:8px; " /> Não Sabe
											<div id="what02">
												<img src="<?php echo URL_SITE ?>/assets/img/what.png" style=" width:30px; float:right;">
											</div>
										</div>
									</div>

								</div>

								<div id="message02"> Peso líquido do produto livre de<br> embalagem. Caso não saiba o peso <br> líquido selecione a opção "Não sabe".</div>
							
							</div>
						</div>
								
						<div class="span3">			
							<input type="submit" class="botaoadd" value="Adicionar Item"/>
						</div>

						<div class="divid5"></div>
						
						<div class="span2">

							<div id="voltar">
								<input type="submit" value="Voltar" class="continue" id="voltarrodoviario2">
							</div>	

							<div id="checkconcordar1"></div>

							<div id="continuar">
								<input type="submit" value="Continuar" class="continue" id="btrodoviario2">
							</div>

						</div>	
						
					</div>

				</div>
			</div>
		</div>
	</div>
								
	<!-- //PROCESSO 2 -->
	<div id="process6maritmo">
	
		<div id="CONTAINER">
			<div id="process">
				<div id="alignicon">
					
					<div id="p1">
						<div id="lineoff"></div>
						<div id="circleoff">1</div>
						<div class="textoff">Validação</div>
					</div>
				
					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">2</div>
						<div class="textoff">Tipificação</div>
					</div>
				
					<div id="p2">
						<div id="lineactive"></div>
						<div id="circleactive">3</div>
						<div class="textactive">Logística</div>
					</div>					
					
					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">4</div>
						<div class="textoff">Incoterm</div>
					</div>					

				</div>
			</div>
			
			<!--conteúdo processo 2 -->		
			<div id="alinhar">
				<div id="container2">
					<div id="title8">Incoterms</div>
					<div id="imgmaritmo">
						<img src="<?php echo URL_SITE ?>/assets/img/mar.png" style="width:250px; float:right;    margin-top: 75px;">
					</div>
					<div class="divid8"></div>
				
					<div id="boxes">
						
						<div class="formulariobox">

							<div class="perguntas4">Container</div>	
							
							<div id="alignform">
								
								<div id="nomeproduto">
									<input type="email" class="form-controlproduto" id="email" placeholder="Quantidade de Containers"/>
								</div>

								<div id="selcontainer">
									<select>
										<option>Tipo do Container</option>
										<option>01</option>
									</select>
								</div>

								<div id="boxpeso1">
									
									<div id="pesoproduto">
										<input type="email" class="form-controlproduto" id="email" placeholder="Peso Líquido (kg)"/>
									</div>

									<div id="pesoliq">
										<div id="alignnaosabe">
											<input id="tmp" type="checkbox" style="margin-right:8px; " /> Não Sabe
											<div class="what0">
												<img src="<?php echo URL_SITE ?>/assets/img/what.png" style=" width:30px;float:right;">
											</div>
										</div>
									</div>

								</div>

								<div class="perguntas4">Observações Gerais</div>	
								
								<div id="nomeproduto">
									<textarea name="" id="" placeholder="Mensagem" class="form-controlproduto"></textarea>
								</div>

								<div id="message"> Peso líquido do produto livre de<br> embalagem. Caso não saiba o peso <br> líquido selecione a opção "Não sabe".</div>
							
							</div>

						</div>

						<div class="span3">			
							<input type="submit" class="botaoadd" value="Adicionar Item"/>
						</div>

						<div class="divid5"></div>
						
						<div class="span2">
							<div id="voltar">
								<input type="submit" value="Voltar" class="continue" id="voltarmaritm6">
							</div>	

							<div id="checkconcordar1"></div>

							<div id="continuar">
								<input type="submit" value="Continuar" class="continue" id="btmaritmo6">
							</div>						
						</div>	
						
					</div>
				</div>
			</div>

		</div>								
	</div>
	
	<div id="process6rodoviario">
	
		<div id="CONTAINER">
			
			<div id="process">
				
				<div id="alignicon">
					
					<div id="p1">
						<div id="lineoff"></div>
						<div id="circleoff">1</div>
						<div class="textoff">Validação</div>
					</div>
				
					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">2</div>
						<div class="textoff">Tipificação</div>
					</div>
				
					<div id="p2">
						<div id="lineactive"></div>
						<div id="circleactive">3</div>
						<div class="textactive">Logística</div>
					</div>					
					
					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">4</div>
						<div class="textoff">Incoterm</div>
					</div>					
				
				</div>
			</div>
			
			<!--conteúdo processo 2 -->		
			<div id="alinhar">
				<div id="container2">
					<div id="title8">Incoterms</div>
					
					<div id="imgrodovia">
						<img src="<?php echo URL_SITE ?>/assets/img/rodovia.png" style="width:210px; float:right;">
					</div>

					<div class="divid8"></div>

					<div id="boxes">
						<div class="formulariobox">

							<div class="perguntas4">Veículos</div>	

							<div id="alignform">

								<div id="nomeproduto">
									<input type="email" class="form-controlproduto" id="email" placeholder="Número de Veículos"/>
								</div>

								<div id="selcontainer">
									<select>
										<option>Tipo de Veículo</option>
										<option>01</option>
									</select>
								</div>

								<div id="boxpeso1">

									<div id="pesoproduto">
										<input type="email" class="form-controlproduto" id="email" placeholder="Peso Líquido (kg)"/>
									</div>

									<div id="pesoliq">
										<div id="alignnaosabe">
											<input id="tmp" type="checkbox" style="margin-right:8px; " /> Não Sabe
											<div class="what0">
												<img src="<?php echo URL_SITE ?>/assets/img/what.png" style=" width:30px;float:right;">
											</div>
										</div>
									</div>

								</div>

								<div class="perguntas4">Observações Gerais</div>

								<div id="nomeproduto">
									
									<textarea name="" id="" placeholder="Mensagem" class="form-controlproduto"></textarea>
								</div>
								<div id="message"> Peso líquido do produto livre de<br> embalagem. Caso não saiba o peso <br> líquido selecione a opção "Não sabe".</div>
							</div>
						</div>

						<div class="span3">			
							<input type="submit" class="botaoadd" value="Adicionar Item"/>
						</div>

						<div class="divid5"></div>

						<div class="span2">
							<div id="voltar">
								<input type="submit" value="Voltar" class="continue" id="voltarrodoviario6">
							</div>	

							<div id="checkconcordar1"></div>

							<div id="continuar">
								<input type="submit" value="Continuar" class="continue" id="btrodoviario6">
							</div>						
						</div>	
					</div>
				</div>
			</div>

		</div>
	</div>		
	
	<!-- PROCESSO 3 -->
	<div id="process3aereo">
	
		<div id="CONTAINER">
			<div id="process">
				<div id="alignicon">
					
					<div id="p1">
						<div id="lineoff"></div>
						<div id="circleoff">1</div>
						<div class="textoff">Validação</div>
					</div>
				
					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">2</div>
						<div class="textoff">Tipificação</div>
					</div>
				
					<div id="p2">
						<div id="lineactive"></div>
						<div id="circleactive">3</div>
						<div class="textactive">Logística</div>

					</div>					
					
					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">4</div>
						<div class="textoff">Incoterm</div>
					</div>					
				</div>
			</div>
			
			<!-- /// conteúdo processo 3 -->
			<div id="alinhar">
				<div id="container2">
					<div id="title8">LOGÍSTICA <span> AÉREO</span></div>
					<div id="imgaereo">
						<img src="<?php echo URL_SITE ?>/assets/img/aereo.png" style="width:250px; float:right;    margin-top: 75px;">
					</div>
					<div class="divid8"></div>
					<div id="boxes">

						<div class="formulariobox">

							<div id="alignform">

								<div id="selaereo">
									<select>
										<option>EXW</option>
										<option>FOB</option>						  
										<option>CIF</option>						  
										<option>CFR</option>
									</select>
								</div>

								<div class="span">Não sabe o que é <b>Iconterms</b> e suas derivações? <small class="linkTermos">Clique aqui</small></div>					
								<div class="span8">Endereço de Coleta (Exportador)</a></div>

								<div id="nomeproduto">
									<input type="email" class="form-controlproduto" id="email" placeholder="Cidade"/>
								</div>

								<div id="city">
									<input type="email" class="form-controlproduto" id="email" placeholder="P"/>
								</div>

								<div id="pais">
									<input type="email" class="form-controlproduto" id="email" placeholder="ZIP code"/>
								</div>

								<div id="quantidade">
									<input type="email" class="form-controlproduto" id="email" placeholder="ZIP code"/>
								</div>

							</div>

						</div>

						<div class="divid5"></div>

						<div class="span2">

							<div id="voltar">
								<input type="submit" value="Voltar" class="continue" id="voltaraereo3">
							</div>	

							<div id="checkconcordar1"></div>
							<div id="continuar">
								<input type="submit" value="Continuar" class="continue" id="btaereo3">
							</div>	

						</div>	

					</div>	
				</div>
			</div>
	
		</div>
	</div>
	
	<!-- PROCESSO 3 -->
	<div id="process3maritmo">
	
		<div id="CONTAINER">
			
			<div id="process">
				<div id="alignicon">
					
					<div id="p1">
						<div id="lineoff"></div>
						<div id="circleoff">1</div>
						<div class="textoff">Validação</div>
					</div>

					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">2</div>
						<div class="textoff">Tipificação</div>
					</div>

					<div id="p2">
						<div id="lineactive"></div>
						<div id="circleactive">3</div>
						<div class="textactive">Logística</div>
					</div>					
					
					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">4</div>
						<div class="textoff">Incoterm</div>
					</div>					

				</div>
			</div>
			
			<div id="alinhar">
				<div id="container2">
					<div id="title8">LOGÍSTICA CARGA MARÍTIMO</div>
					
					<div id="imgmaritmo">
						<img src="<?php echo URL_SITE ?>/assets/img/mar.png" style="width:250px; float:right;    margin-top: 75px;">
					</div>

					<div class="divid8"></div>

					<div id="boxes">
						<div class="formulariobox">

							<div id="alignform">

								<div id="selaereo">
									<select>
										<option>EXW</option>
										<option>FOB</option>						  
										<option>CIF</option>						  
										<option>CFR</option>
									</select>
								</div>

								<div class="span">Não sabe o que é <b>Iconterms</b> e suas derivações? <small class="linkTermos">Clique aqui</small></div>					
								<div class="span">Endereço de Coleta <a>(Exportador)</a></div>

								<div id="nomeproduto">
									<input type="email" class="form-controlproduto" id="email" placeholder="Cidade"/>
								</div>

								<div id="city">
									<input type="email" class="form-controlproduto" id="email" placeholder="P"/>
								</div>

								<div id="pais">
									<input type="email" class="form-controlproduto" id="email" placeholder="ZIP code"/>
								</div>

								<div id="quantidade">
									<input type="email" class="form-controlproduto" id="email" placeholder="ZIP code"/>
								</div>

							</div>

						</div>

						<div class="divid5"></div>

						<div class="span2">
							<div id="voltar">
								<input type="submit" value="Voltar" class="continue" id="voltarmaritmo3">
							</div>	

							<div id="checkconcordar1"></div>

							<div id="continuar">
								<input type="submit" value="Continuar" class="continue" id="btmaritmo3">
							</div>						
						</div>	

					</div>
				</div>
			</div>
		
		</div>
	</div>
	
	<!-- PROCESSO 5 aereo -->
	<div id="process3rodoviario">
		<div id="CONTAINER">
			
			<div id="process">
				<div id="alignicon">
					
					<div id="p1">
						<div id="lineoff"></div>
						<div id="circleoff">1</div>
						<div class="textoff">Validação</div>
					</div>
				
					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">2</div>
						<div class="textoff">Tipificação</div>
					</div>
				
					<div id="p2">
						<div id="lineactive"></div>
						<div id="circleactive">3</div>
						<div class="textactive">Logística</div>
					</div>					
					
					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">4</div>
						<div class="textoff">Incoterm</div>
					</div>					
				</div>
			</div>
			
			<div id="alinhar">
				<div id="container2">
					<div id="title8">LOGÍSTICA RODOVIÁRIA</div>
					
					<div id="imgrodovia">
						<img src="<?php echo URL_SITE ?>/assets/img/rodovia.png" style="width:210px; float:right;">
					</div>

					<div class="divid8"></div>

					<div id="boxes">
						<div class="formulariobox">

							<div id="alignform">
								<div id="selaereo">
									<select>
										<option>EXW</option>
										<option>FOB</option>						  
										<option>CIF</option>						  
										<option>CFR</option>
									</select>
								</div>

								<div class="span">Não sabe o que é <b>Iconterms</b> e suas derivações? <small class="linkTermos">Clique aqui</small></div>					
								<div class="span8">Endereço de Coleta <a>(Exportador)</a></div>

								<div id="nomeproduto">
									<input type="email" class="form-controlproduto" id="email" placeholder="Cidade"/>
								</div>

								<div id="city">
									<input type="email" class="form-controlproduto" id="email" placeholder="P"/>
								</div>

								<div id="pais">
									<input type="email" class="form-controlproduto" id="email" placeholder="ZIP code"/>
								</div>

								<div id="quantidade">
									<input type="email" class="form-controlproduto" id="email" placeholder="ZIP code"/>
								</div>
							</div>

						</div>

						<div class="divid5"></div>

						<div class="span2">
							<div id="voltar">
								<input type="submit" value="Voltar" class="continue" id="voltarrodoviario3">
							</div>	

							<div id="checkconcordar1"></div>

							<div id="continuar">
								<input type="submit" value="Continuar" class="continue" id="btrodoviario3">
							</div>						
						</div>	

					</div>
				</div>
			</div>
		</div>
	</div>
			
	<div id="process5aereo">
		<div id="CONTAINER">
			
			<div id="process">
				<div id="alignicon">
					
					<div id="p1">
						<div id="lineoff"></div>
						<div id="circleoff">1</div>
						<div class="textoff">Validação</div>
					</div>

					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">2</div>
						<div class="textoff">Tipificação</div>
					</div>

					<div id="p2">
						<div id="lineactive"></div>
						<div id="circleactive">3
						</div>
						<div class="textactive">Logística</div>
					</div>					
					
					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">4</div>
						<div class="textoff">Incoterm</div>
					</div>					
				</div>			
			</div>

			<!-- CONTEÚDO  4.1 AVIAO -->	
			<div id="alinhar">
				<div id="container2">
					
					<div id="title8">LOGÍSTICA TRANSPORTE AÉREO</div>
					<div id="imgaereo">
						<img src="<?php echo URL_SITE ?>/assets/img/aereo.png" style="width:250px; float:right;    margin-top: 75px;">
					</div>
					<div class="divid8"></div>

					<div id="boxes">
						<div class="formulariobox">

							<div id="alignform">

								<div class="span8"><a>Volume 1</a></div>

								<div id="line001">

									<div id="comp">
										<input type="email" class="form-controlproduto" id="email" placeholder="Comp. (cm)"/>
									</div>

									<div id="larg">
										<input type="email" class="form-controlproduto" id="email" placeholder="Larg. (cm)"/>
									</div>								

									<div id="alt">
										<input type="email" class="form-controlproduto" id="email" placeholder="Alt. (cm)"/>
									</div>

									<div id="help1">
										<img src="<?php echo URL_SITE ?>/assets/img/what.png" style=" width:100%; float:right;">
									</div>

								</div>

								<div id="line001">
									<div id="peso00">
										<input type="email" class="form-controlproduto" id="email" placeholder="Aeroporto de origem"/>
									</div>
									<div id="help1">
										<img src="<?php echo URL_SITE ?>/assets/img/what.png" style=" width:100%; float:right;">
									</div>
								</div>						

								<div id="line001">
									<div id="textdetalhes">
										<input type="email" class="form-controlproduto" id="email" placeholder="Observações"/>
									</div>
								</div>

							</div>

							<div class="divid5"></div>

							<div class="span2">
								<div id="voltar">
									<input type="submit" value="Voltar" class="continue" id="voltaraereo5">
								</div>	

								<div id="checkconcordar1"></div>

								<div id="continuar">
									<input type="submit" value="Continuar" class="continue" id="btaereo5">
								</div>						
							</div>	

						</div>
					</div>
				</div>
			</div>
	
		</div>
	</div>
					
	<div id="process5maritmo">
	
		<div id="CONTAINER">
			
			<div id="process">
				<div id="alignicon">
					
					<div id="p1">
						<div id="lineoff"></div>
						<div id="circleoff">1</div>
						<div class="textoff">Validação</div>
					</div>

					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">2</div>
						<div class="textoff">Tipificação</div>
					</div>

					<div id="p2">
						<div id="lineactive"></div>
						<div id="circleactive">3</div>
						<div class="textactive">Logística</div>
					</div>					
					
					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">4</div>
						<div class="textoff">Incoterm</div>
					</div>					
				</div>			
			</div>
			
			<!-- CONTEÚDO  4.1 AVIAO -->	
			<div id="alinhar">
				<div id="container2">
					<div id="title8">LOGÍSTICA TRANSPORTE MARÍTMO</div>

					<div id="imgmaritmo">
						<img src="<?php echo URL_SITE ?>/assets/img/mar.png" style="width:250px; float:right;    margin-top: 75px;">
					</div>

					<div class="divid8"></div>

					<div id="boxes">
						<div class="formulariobox">

							<div id="alignform">

								<div class="span8"><a>Volume 1</a></div>

								<div id="line001">
									<div id="comp">
										<input type="email" class="form-controlproduto" id="email" placeholder="Comp. (cm)"/>
									</div>

									<div id="larg">
										<input type="email" class="form-controlproduto" id="email" placeholder="Larg. (cm)"/>
									</div>								

									<div id="alt">
										<input type="email" class="form-controlproduto" id="email" placeholder="Alt. (cm)"/>
									</div>

									<div id="help1">
										<img src="<?php echo URL_SITE ?>/assets/img/what.png" style=" width:100%; float:right;">
									</div>

								</div>

								<div id="line001">
									<div id="peso00">
										<input type="email" class="form-controlproduto" id="email" placeholder="Porto de origem"/>
									</div>

									<div id="help1">
										<img src="<?php echo URL_SITE ?>/assets/img/what.png" style=" width:100%; float:right;">
									</div>
								</div>						

								<div id="line001">
									<div id="textdetalhes">
										<input type="email" class="form-controlproduto" id="email" placeholder="Observações"/>
									</div>
								</div>

							</div>

							<div class="divid5"></div>

							<div class="span2">
								<div id="voltar">
									<input type="submit" value="Voltar" class="continue" id="voltarmaritmo5">
								</div>	

								<div id="checkconcordar1"></div>

								<div id="continuar">
									<input type="submit" value="Continuar" class="continue" id="btmaritmo5">
								</div>						
							</div>	

						</div>
					</div>
				</div>
			</div>
				
		</div>
	</div>
								
	<div id="process5rodoviario">
	
		<div id="CONTAINER">
			<div id="process">
				<div id="alignicon">
					
					<div id="p1">
						<div id="lineoff"></div>
						<div id="circleoff">1</div>
						<div class="textoff">Validação</div>
					</div>

					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">2</div>
						<div class="textoff">Tipificação</div>
					</div>

					<div id="p2">
						<div id="lineactive"></div>
						<div id="circleactive">3</div>
						<div class="textactive">Logística</div>
					</div>					
					
					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">4</div>
						<div class="textoff">Incoterm</div>
					</div>					

				</div>			
			</div>

			<!-- CONTEÚDO  4.1 AVIAO -->	
			<div id="alinhar">
				<div id="container2">
					
					<div id="title8">LOGÍSTICA TRANSPORTE RODOVIÁRIO</div>
					<div id="imgrodovia">
						<img src="<?php echo URL_SITE ?>/assets/img/rodovia.png" style="width:210px; float:right;">
					</div>
					<div class="divid8"></div>

					<div id="boxes">
						<div class="formulariobox">

							<div id="alignform">
								<div class="span8"><a>Volume 1</a></div>

								<div id="line001">
									<div id="comp">
										<input type="email" class="form-controlproduto" id="email" placeholder="Comp. (cm)"/>
									</div>

									<div id="larg">
										<input type="email" class="form-controlproduto" id="email" placeholder="Larg. (cm)"/>
									</div>								

									<div id="alt">
										<input type="email" class="form-controlproduto" id="email" placeholder="Alt. (cm)"/>
									</div>

									<div id="help1">
										<img src="<?php echo URL_SITE ?>/assets/img/what.png" style=" width:100%; float:right;">
									</div>
								</div>

								<div id="line001">
									<div id="peso00">
										<input type="email" class="form-controlproduto" id="email" placeholder="Porto de origem"/>
									</div>
									<div id="help1">
										<img src="<?php echo URL_SITE ?>/assets/img/what.png" style=" width:100%; float:right;">
									</div>
								</div>						

								<div id="line001">
									<div id="textdetalhes">
										<input type="email" class="form-controlproduto" id="email" placeholder="Observações"/>
									</div>
								</div>
							</div>

							<div class="divid5"></div>

							<div class="span2">
								<div id="voltar">
									<input type="submit" value="Voltar" class="continue" id="voltarrodoviario5">
								</div>	

								<div id="checkconcordar1"></div>

								<div id="continuar">
									<input type="submit" value="Continuar" class="continue" id="btrodoviario5">
								</div>						
							</div>	
						</div>
					</div>
				</div>
			</div>
				
		</div>
	
	</div>					
					
	<!-- PROCESSO 4 -->
	<div id="process4aereo">
	
		<div id="CONTAINER">
			
			<div id="process">
				
				<div id="alignicon">
					
					<div id="p1">
						<div id="lineoff"></div>
						<div id="circleoff">1</div>
						<div class="textoff">Validação</div>
					</div>
				
					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">2</div>
						<div class="textoff">Tipificação</div>
					</div>
				
					<div id="p2">
						<div id="lineactive"></div>
						<div id="circleactive">3</div>
						<div class="textactive">Logística</div>
					</div>					
					
					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">4</div>
						<div class="textoff">Incoterm</div>
					</div>	

				</div>			
			</div>
	
			<!-- CONTEÚDO  4.1 AVIAO -->	
			<div id="alinhar">
				<div id="container2">
					<div id="title8">LOGÍSTICA TRANSPORTE AÉREO</div>

					<div id="imgaereo">
						<img src="<?php echo URL_SITE ?>/assets/img/aereo.png" style="width:250px; float:right;    margin-top: 75px;">
					</div>

					<div class="divid8"></div>

					<div id="boxes">
						<div class="formulariobox">

							<div id="alignform">

								<div class="span89"><a>Aeroporto</a></div>

								<div id="nomeproduto">
									<input type="email" class="form-controlproduto" id="email" placeholder="Aeroporto de origem"/>
								</div>

								<div id="nomeproduto">
									<input type="email" class="form-controlproduto" id="email" placeholder="Aeroporto de destino"/>
								</div>

								<div class="span89">Destino Final</div>

								<div id="what">
									<img src="<?php echo URL_SITE ?>/assets/img/what.png" style=" width:100%; float:right;">
								</div>

								<div class="boxdestino">

									<div id="seluf">
										<select>
											<option>UF</option>
										</select>
									</div>

									<div id="cidadeproduto">
										<input type="email" class="form-controlproduto" id="email" placeholder="Cidade"/>
									</div>

									<div id="naoincluir">
										<div id="alignnaoincluir">
											<input id="tmp" type="checkbox" style="margin-right:8px; " /> Não Incluir
										</div>
									</div>

								</div>	

								<div class="span9">Carga</div>

								<div id="what1">
									<img src="<?php echo URL_SITE ?>/assets/img/what.png" style=" width:100%; float:right;">
								</div>

								<div class="boxdestino">
									<div id="selcarga1">
										<div id="carga01">
											<div id="alinharcheck">
												<input id="tmp" type="checkbox" style="margin:auto; " />
											</div>
										</div>										
										<div id="carga02">LTL</div>										
										<div id="carga03">LESS THAN TRUCKLOAD</div>
									</div>									

									<div id="selcarga2">
										<div id="carga01">
											<div id="alinharcheck">
												<input id="tmp" type="checkbox" style="margin:auto; " />
											</div>
										</div>										
										<div id="carga02">FTL</div>										
										<div id="carga03">LESS THAN TRUCKLOAD</div>
									</div>
								</div>

							</div>
							
							<div class="divid5"></div>
							
							<div class="span2">

								<div id="voltar">
									<input type="submit" value="Voltar" class="continue" id="voltaraereo4">
								</div>	

								<div id="checkconcordar1"></div>

								<div id="continuar">
									<input type="submit" value="Continuar" class="continue" id="btaereo4">
								</div>	

							</div>	
							
						</div>
					</div>
				</div>
			</div>
	
		</div>
	</div>

	<!-- PROCESSO 4 -->
	<div id="process4maritmo">
	
		<div id="CONTAINER">
			
			<div id="process">
				<div id="alignicon">
					<div id="p1">
						<div id="lineoff"></div>
						<div id="circleoff">1</div>
						<div class="textoff">Validação</div>						
					</div>
				
					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">2</div>
						<div class="textoff">Tipificação</div>
					</div>
				
					<div id="p2">
						<div id="lineactive"></div>
						<div id="circleactive">3</div>
						<div class="textactive">Logística</div>
					</div>					
					
					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">4</div>
						<div class="textoff">Incoterm</div>
					</div>					
				</div>			
			</div>

			<!-- CONTEÚDO  4.1 AVIAO -->	
			<div id="alinhar">
				<div id="container2">
					
					<div id="title8">LOGÍSTICA CARGA MARÍTMO</div>
					
					<div id="imgmaritmo">
						<img src="<?php echo URL_SITE ?>/assets/img/mar.png" style="width:250px; float:right;    margin-top: 75px;">
					</div>

					<div class="divid8"></div>
				
					<div id="boxes">
						<div class="formulariobox">

							<div id="alignform">

								<div class="span8"><a>Porto</a></div>

								<div id="nomeproduto">
									<input type="email" class="form-controlproduto" id="email" placeholder="Porto de origem"/>
								</div>

								<div id="nomeproduto">
									<input type="email" class="form-controlproduto" id="email" placeholder="Porto de destino"/>
								</div>

								<div class="span9">Destino Final</div>
								<div id="what">
									<img src="<?php echo URL_SITE ?>/assets/img/what.png" style=" width:100%;float:right;">
								</div>

								<div class="boxdestino">

									<div id="seluf">
										<select>
											<option>UF</option>
										</select>
									</div>

									<div id="cidadeproduto">
										<input type="email" class="form-controlproduto" id="email" placeholder="Cidade"/>
									</div>

									<div id="naoincluir">
										<div id="alignnaoincluir">
											<input id="tmp" type="checkbox" style="margin-right:8px; " /> Não Incluir
										</div>
									</div>

								</div>	

								<div class="span9">Carga</div>

								<div id="what1">
									<img src="<?php echo URL_SITE ?>/assets/img/what.png" style=" width:100%; float:right;">
								</div>

								<div class="boxdestino">

									<div id="selcarga1">
										<div id="carga01">
											<div id="alinharcheck">
												<input id="tmp" type="checkbox" style="margin:auto;"/>
											</div>
										</div>										
										<div id="carga02">LCL</div>										
										<div id="carga03">FULL TRUCK LOAD</div>
									</div>									

									<div id="selcarga2">
										<div id="carga01">
											<div id="alinharcheck">
												<input id="tmp" type="checkbox" style="margin:auto;"/>
											</div>
										</div>										
										<div id="carga02">FCL</div>										
										<div id="carga03">LESS THAN TRUCKLOAD</div>
									</div>
								</div>

							</div>

							<div class="divid5"></div>

							<div class="span2">
								<div id="voltar">
									<input type="submit" value="Voltar" class="continue" id="voltarmaritmo4">
								</div>	
								<div id="checkconcordar1"></div>
								<div id="continuar">
									<input type="submit" value="Continuar" class="continue" id="btmaritmo4">
								</div>						
							</div>	

						</div>
					</div>
				</div>
			</div>
	
		</div>

	</div>
					
	<div id="process4rodoviario">
	
		<div id="CONTAINER">
			
			<div id="process">
				<div id="alignicon">
					
					<div id="p1">
						<div id="lineoff"></div>
						<div id="circleoff">1</div>
						<div class="textoff">Validação</div>
					</div>

					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">2</div>
						<div class="textoff">Tipificação</div>
					</div>

					<div id="p2">
						<div id="lineactive"></div>
						<div id="circleactive">3</div>
						<div class="textactive">Logística</div>
					</div>					
					
					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">4</div>
						<div class="textoff">Incoterm</div>
					</div>					
				</div>			
			</div>

			<!-- CONTEÚDO  4.1 AVIAO -->	
			<div id="alinhar">
				<div id="container2">
					<div id="title8">LOGÍSTICA TRANSPORTE rodoviario</div>
						<div id="imgrodovia">
							<img src="<?php echo URL_SITE ?>/assets/img/rodovia.png" style="width:210px; float:right;">
						</div>
						<div class="divid8"></div>
							
					<div id="boxes">
						<div class="formulariobox">

							<div id="alignform">

								<div class="span9">Destino Final</div>
								<div id="what">
									<img src="<?php echo URL_SITE ?>/assets/img/what.png" style=" width:100%;float:right;">
								</div>
								
								<div class="boxdestino">
									<div id="seluf">
										<select>
											<option>UF</option>
										</select>
									</div>
									<div id="cidadeproduto">
										<input type="email" class="form-controlproduto" id="email" placeholder="Cidade"/>
									</div>
								</div>	
								
								<div class="span9">Carga</div>
								
								<div id="what1">
									<img src="<?php echo URL_SITE ?>/assets/img/what.png" style=" width:100%; float:right;">
								</div>
								
								<div class="boxdestino">

									<div id="selcarga1">
										<div id="carga01">
											<div id="alinharcheck">
												<input id="tmp" type="checkbox" style="margin:auto;"/>
											</div>
										</div>										
										<div id="carga02">FTL</div>										
										<div id="carga03">FULL TRUCK LOAD</div>
									</div>									
									
									<div id="selcarga2">
										<div id="carga01">
											<div id="alinharcheck">
												<input id="tmp" type="checkbox" style="margin:auto; " />
											</div>
										</div>										
										<div id="carga02">LTL</div>										
										<div id="carga03">LESS THAN TRUCKLOAD</div>
									</div>

								</div>

							</div>
							
							<div class="divid5"></div>

							<div class="span2">
								<div id="voltar">
									<input type="submit" value="Voltar" class="continue" id="voltarrodoviario4">
								</div>	
								<div id="checkconcordar1"></div>
								<div id="continuar">
									<input type="submit" value="Continuar" class="continue" id="btrodoviario4">
								</div>						
							</div>	

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
					
	<!-- //PROCESSO 5 aereo -->
	<div id="processfinalmaritmo">
		<div id="CONTAINER">
			<div id="process">
				<div id="alignicon">
					
					<div id="p1">
						<div id="lineoff"></div>
						<div id="circleoff">1</div>
						<div class="textoff">Validação</div>
					</div>

					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">2</div>
						<div class="textoff">Tipificação</div>
					</div>


					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">3</div>
						<div class="textoff">Logística</div>
					</div>					
					
					<div id="p2">
						<div id="lineactive"></div>
						<div id="circleactive">4</div>
						<div class="textoff">Incoterm</div>
					</div>					
					
				</div>
			</div>
			
			<div id="content">
				<div id="alinhar">
					<div id="container2">
						<div id="title8">Incoterms</div>
						<div class="divid8"></div>

						<div id="boxes">
							<div class="resumo">
								
								<div class="opcao">
									<div class="titleresumo">Resumo dados Index</div>	
									<div class="titleitens">Natureza</div>	
									<div class="natureza">Imobilizado</div>	
									<div class="titleitens">Item 1</div>	
									<div class="detalhesitem"> Produto: Cadeira <br> Preço: 125,98 USD <br> Quantidade: 450 <br> NCM: 79876589 </div>	
									<div class="titleitens">Incoterm</div>	
									<div class="incoterm">Marítimo: EXW/FOB/FAS <br> Valor do Frete: 25.050,00 USD</div>

									<div class="titleitens">Aeroporto</div>	
									<div class="incoterm"> Aeroporto de Origem: EXW/FOB/FAS <br> Aeroporto de Destino: EXW/FOB/FAS <br> Destino Final: Curitiba/PR <br> Carga: LTL - Less Than Truckload <br> País: Brasil <br> ZIP Code: 80420-250</div>

									<div class="titleitens">Volume</div>	
									<div class="incoterm"> Dimensões C X L X A:45cm X 45cm X 45cm <br> Peso Bruto: 1000kg <br> Observações Gerais: Lorem ipsum dolor sit amet <br> Valor do Frete: 25.050,00 USD</div>
									<br>
									<br>
									<br>
								</div>

							</div>

							<div class="informacoesgerais">
								<div class="opcao">
									<div class="titleinformacoesgerais">Informações gerais</div>	
									<div id="textinformacoesgerais" >a) O requerente está ciente de que a apresentação de informações ou documentos falsos caracteriza crime.	 <br><br> b) Assumo integral responsabilidade pela fidelidade das informações aqui contidas neste formulário. 						 <br><br> c) Ausência ou divergência das informações prestadas pelo cliente, dependendo a sua natureza e grau de relevância para a operação, pode impedir o cumprimentos dos termos dessa proposta. </div>
								</div>
							</div>

							<div class="divid4"></div>

							<div class="span2">
								<div id="voltar">
									<input type="submit" value="okVoltar" class="continue" id="voltarmaritmo6">
								</div>	
								<div id="checkconcordar">
									<div id="alignconcordar" class="linkTermos">
										 Concordo com os termos
									</div>
								</div>
								<div id="continuar">
									<input type="submit" value="Enviar" class="continue" id="enviarmaritmo">
								</div>						
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>	
	
	<!-- PROCESSO 5 -->
	<div id="processfinalrodoviario">
		<div id="CONTAINER">

			<div id="process">
				<div id="alignicon">

					<div id="p1">
						<div id="lineoff"></div>
						<div id="circleoff">1</div>
						<div class="textoff">Validação</div>
					</div>

					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">2</div>
						<div class="textoff">Tipificação</div>
					</div>

					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">3</div>
						<div class="textoff">Logística</div>
					</div>					

					<div id="p2">
						<div id="lineactive"></div>
						<div id="circleactive">4</div>
						<div class="textoff">Incoterm</div>
					</div>					

				</div>
			</div>
			
			<div id="content">
				<div id="alinhar">
					<div id="container2">
						<div id="title8">Incoterms</div>
					
						<div class="divid8"></div>
							
						<div id="boxes">
							
							<div class="resumo">
								<div class="opcao">
									<div class="titleresumo">Resumo dados Index</div>	
									<div class="titleitens">Natureza</div>	
									<div class="natureza">Imobilizado</div>	
									<div class="titleitens">Item 1</div>	
									<div class="detalhesitem"> Produto: Cadeira <br> Preço: 125,98 USD <br> Quantidade: 450 <br> NCM: 79876589</div>	
									<div class="titleitens">Incoterm</div>	
									<div class="incoterm">Marítimo: EXW/FOB/FAS <br> Valor do Frete: 25.050,00 USD</div>
									<div class="titleitens">Aeroporto</div>	
									<div class="incoterm">Aeroporto de Origem: EXW/FOB/FAS <br> Aeroporto de Destino: EXW/FOB/FAS <br> Destino Final: Curitiba/PR <br> Carga: LTL - Less Than Truckload	</div>
									<div class="titleitens">Volume</div>	
									<div class="incoterm"> Dimensões C X L X A:45cm X 45cm X 45cm <br> Peso Bruto: 1000kg <br> Observações Gerais: Lorem ipsum dolor sit amet <br> Valor do Frete: 25.050,00 USD</div>
									<br>
									<br>
									<br>
								</div>
							</div>

							<div class="informacoesgerais">
								<div class="opcao">
									<div class="titleinformacoesgerais">Informações gerais</div>	
									<div id="textinformacoesgerais" >a) O requerente está ciente de que a apresentação de informações ou documentos falsos caracteriza crime.	 <br><br> b) Assumo integral responsabilidade pela fidelidade das informações aqui contidas neste formulário. 						 <br><br> c) Ausência ou divergência das informações prestadas pelo cliente, dependendo a sua natureza e grau de relevância para a operação, pode impedir o cumprimentos dos termos dessa proposta. </div>
								</div>
							</div>

							<div class="divid4"></div>
								
							<div class="span2">
								<div id="voltar">
									<input type="submit" value="Voltar" class="continue" id="voltarrodoviario7">
								</div>	

								<div id="checkconcordar">
									<div id="alignconcordar" class="linkTermos">
										 Concordo com os termos
									</div>
								</div>

								<div id="continuar">
									<input type="submit" value="Enviar" class="continue" id="enviarrodoviario">
								</div>						
							</div>
				
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>	
			
	<div id="processfinalaereo">
		<div id="CONTAINER">
			<div id="process">
				<div id="alignicon">

					<div id="p1">
						<div id="lineoff"></div>
						<div id="circleoff">1</div>
						<div class="textoff">Validação</div>
					</div>

					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">2</div>
						<div class="textoff">Tipificação</div>
					</div>

					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">3</div>
						<div class="textoff">Logística</div>
					</div>					

					<div id="p2">
						<div id="lineactive"></div>
						<div id="circleactive">4</div>
						<div class="textoff">Incoterm</div>
					</div>

				</div>
			</div>
			
			<div id="content">

				<div id="alinhar">
					<div id="container2">
						<div id="title8">Incoterms</div>

						<div class="divid8"></div>

						<div id="boxes">

							<div class="resumo">
								<div class="opcao">
									<div class="titleresumo">Resumo dados Index</div>	
									<div class="titleitens">Natureza</div>	
									<div class="natureza">Imobilizado</div>	
									<div class="titleitens">Item 1</div>	
									<div class="detalhesitem"> Produto: Cadeira<br>Preço: 125,98 USD<br>Quantidade: 450<br>NCM: 79876589 </div>	
									<div class="titleitens">Incoterm</div>	
									<div class="incoterm"> Marítimo: EXW/FOB/FAS<br>Valor do Frete: 25.050,00 USD </div>
									<div class="titleitens">Aeroporto</div>	
									<div class="incoterm"> Aeroporto de Origem: EXW/FOB/FAS <br> Aeroporto de Destino: EXW/FOB/FAS <br> Destino Final: Curitiba/PR <br> Carga: LTL - Less Than Truckload											 <br> País: Brasil												 <br> ZIP Code: 80420-250									 </div>
									<div class="titleitens">Volume</div>	
									<div class="incoterm"> Dimensões C X L X A:45cm X 45cm X 45cm <br> Peso Bruto: 1000kg <br> Observações Gerais: Lorem ipsum dolor sit amet <br> Valor do Frete: 25.050,00 USD											 </div>
									<br>
									<br>
									<br>
								</div>
							</div>

							<div class="informacoesgerais">
								<div class="opcao">
									<div class="titleinformacoesgerais">Informações gerais</div>	
									<div id="textinformacoesgerais" >a) O requerente está ciente de que a apresentação de informações ou documentos falsos caracteriza crime.	 <br><br> b) Assumo integral responsabilidade pela fidelidade das informações aqui contidas neste formulário. 						 <br><br> c) Ausência ou divergência das informações prestadas pelo cliente, dependendo a sua natureza e grau de relevância para a operação, pode impedir o cumprimentos dos termos dessa proposta.  </div>
								</div>
							</div>

							<div class="divid4"></div>

							<div class="span2">
								<div id="voltar">
									<input type="submit" value="Voltar" class="continue" id="voltaraereo6">
								</div>	

								<div id="checkconcordar">
									<div id="alignconcordar" class="linkTermos">
										 Concordo com os termos
									</div>
								</div>

								<div id="continuar">
									<input type="submit" value="Enviar" class="continue" id="enviaraero">
								</div>						
							</div>

						</div>
					</div>
				</div>

			</div>
		</div>
	</div>		
		
	<!-- PROCESSO 5.1 -->
	<div id="process51">

		<div id="CONTAINER">

			<div id="process">
				<div id="alignicon">
					
					<div id="p1">
						<div id="lineoff"></div>
						<div id="circleoff">1</div>
						<div class="textoff">Validação</div>
					</div>

					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">2</div>
						<div class="textoff">Tipificação</div>
					</div>


					<div id="p2">
						<div id="lineoff"></div>
						<div id="circleoff">3</div>
						<div class="textoff">Logística</div>
					</div>					
					
					<div id="p2">
						<div id="lineactive"></div>
						<div id="circleactive">4</div>
						<div class="textoff">Incoterm</div>
					</div>					

				</div>
			</div>
			
			<div id="content">
				<div id="alinhar">
					<div id="container2">
						<div id="title8">Incoterms</div>

						<div class="divid8"></div>

						<div id="boxes">
							
							<div class="resumo">
								<div class="opcao">
									<div class="titleresumo">Resumo dados Index</div>	
									<div class="titleitens">Natureza</div>	
									<div class="natureza">Imobilizado</div>	
									<div class="titleitens">Item 1</div>	
									<div class="detalhesitem"> Produto: Cadeira <br> Preço: 125,98 USD <br> Quantidade: 450 <br> NCM: 79876589</div>	
									<div class="titleitens">Incoterm</div>	
									<div class="incoterm"> Marítimo: EXW/FOB/FAS <br> Valor do Frete: 25.050,00 USD</div>								
									<div class="titleitens">Incoterm</div>	
									<div class="incoterm">Marítimo: EXW/FOB/FAS <br> Valor do Frete: 25.050,00 USD</div>
									<br>
									<br>
								</div>
							</div>

							<div class="informacoesgerais">
								<div class="opcao">
									<div class="titleinformacoesgerais">Informações gerais</div>	
									<div id="textinformacoesgerais" >a) O requerente está ciente de que a apresentação de informações ou documentos falsos caracteriza crime.	 <br><br> b) Assumo integral responsabilidade pela fidelidade das informações aqui contidas neste formulário. 						 <br><br> c) Ausência ou divergência das informações prestadas pelo cliente, dependendo a sua natureza e grau de relevância para a operação, pode impedir o cumprimentos dos termos dessa proposta. </div>
								</div>
							</div>

							<div class="divid4"></div>

							<div class="span2">
								<div id="voltar">
									<input type="submit" value="Voltar" class="continue" id="voltar04">
								</div>	
								<div id="checkconcordar">
									<div id="alignconcordar" class="linkTermos">
										 Concordo com os termos
									</div>
								</div>
								<div id="continuar">
									<input type="submit" value="Enviar" class="continue" id="enviar">
								</div>	
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>		
	
	<!-- PROCESSO final -->
	<div id="final">
		<div id="CONTAINER">
			<div id="content">
				<div id="alinhar">
					<div id="container2">
						<div id="title1" style="text-align:center; color:#181f3d">Obrigado "Nome do Logado"</div>
						<div class="divid"></div>
						<div id="boxesfinale">
							<div class="formulariobox">
								<div class="perguntas4" style="color:#373737; 	font-weight: 300;">Em 24 horas a análise Vamco Basic será entregue.</div>	
							</div>
							<div class="span">			
								<input type="submit" class="botaoinicio" value="Página Inicial"/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="banner">
		<img src="<?php echo URL_SITE ?>/assets/img/banner03.png" style="width:100%; margin-top:0px; ">
	</div>
</div>

	<div class="modalTermos">
	    <div class="areaTexto">
	        <button class="closeModal">Sair <i>X</i></button>
	        <div class="texto">
	            <h6>Termos de uso</h6>
	         <p> O que é Lorem Ipsum?
	         Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.</p>

	         <p>Porque nós o usamos?
	         É um fato conhecido de todos que um leitor se distrairá com o conteúdo de texto legível de uma página quando estiver examinando sua diagramação. A vantagem de usar Lorem Ipsum é que ele tem uma distribuição normal de letras, ao contrário de "Conteúdo aqui, conteúdo aqui", fazendo com que ele tenha uma aparência similar a de um texto legível. Muitos softwares de publicação e editores de páginas na internet agora usam Lorem Ipsum como texto-modelo padrão, e uma rápida busca por 'lorem ipsum' mostra vários websites ainda em sua fase de construção. Várias versões novas surgiram ao longo dos anos, eventualmente por acidente, e às vezes de propósito (injetando humor, e coisas do gênero).</p>


	         <p> De onde ele vem?
	         Ao contrário do que se acredita, Lorem Ipsum não é simplesmente um texto randômico. Com mais de 2000 anos, suas raízes podem ser encontradas em uma obra de literatura latina clássica datada de 45 AC. Richard McClintock, um professor de latim do Hampden-Sydney College na Virginia, pesquisou uma das mais obscuras palavras em latim, consectetur, oriunda de uma passagem de Lorem Ipsum, e, procurando por entre citações da palavra na literatura clássica, descobriu a sua indubitável origem. Lorem Ipsum vem das seções 1.10.32 e 1.10.33 do "de Finibus Bonorum et Malorum" (Os Extremos do Bem e do Mal), de Cícero, escrito em 45 AC. Este livro é um tratado de teoria da ética muito popular na época da Renascença. A primeira linha de Lorem Ipsum, "Lorem Ipsum dolor sit amet..." vem de uma linha na seção 1.10.32.</p>

	         <p> O trecho padrão original de Lorem Ipsum, usado desde o século XVI, está reproduzido abaixo para os interessados. Seções 1.10.32 e 1.10.33 de "de Finibus Bonorum et Malorum" de Cicero também foram reproduzidas abaixo em sua forma exata original, acompanhada das versões para o inglês da tradução feita por H. Rackham em 1914.</p>
	          <p> O que é Lorem Ipsum?
	         Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.</p>

	         <p>Porque nós o usamos?
	         É um fato conhecido de todos que um leitor se distrairá com o conteúdo de texto legível de uma página quando estiver examinando sua diagramação. A vantagem de usar Lorem Ipsum é que ele tem uma distribuição normal de letras, ao contrário de "Conteúdo aqui, conteúdo aqui", fazendo com que ele tenha uma aparência similar a de um texto legível. Muitos softwares de publicação e editores de páginas na internet agora usam Lorem Ipsum como texto-modelo padrão, e uma rápida busca por 'lorem ipsum' mostra vários websites ainda em sua fase de construção. Várias versões novas surgiram ao longo dos anos, eventualmente por acidente, e às vezes de propósito (injetando humor, e coisas do gênero).</p>


	         <p> De onde ele vem?
	         Ao contrário do que se acredita, Lorem Ipsum não é simplesmente um texto randômico. Com mais de 2000 anos, suas raízes podem ser encontradas em uma obra de literatura latina clássica datada de 45 AC. Richard McClintock, um professor de latim do Hampden-Sydney College na Virginia, pesquisou uma das mais obscuras palavras em latim, consectetur, oriunda de uma passagem de Lorem Ipsum, e, procurando por entre citações da palavra na literatura clássica, descobriu a sua indubitável origem. Lorem Ipsum vem das seções 1.10.32 e 1.10.33 do "de Finibus Bonorum et Malorum" (Os Extremos do Bem e do Mal), de Cícero, escrito em 45 AC. Este livro é um tratado de teoria da ética muito popular na época da Renascença. A primeira linha de Lorem Ipsum, "Lorem Ipsum dolor sit amet..." vem de uma linha na seção 1.10.32.</p>

	         <p> O trecho padrão original de Lorem Ipsum, usado desde o século XVI, está reproduzido abaixo para os interessados. Seções 1.10.32 e 1.10.33 de "de Finibus Bonorum et Malorum" de Cicero também foram reproduzidas abaixo em sua forma exata original, acompanhada das versões para o inglês da tradução feita por H. Rackham em 1914.</p>

			<div class="termosAceitar">
				<input type="checkbox" name="vehicle" value="Bike"> Concordo com os termos
			</div>
	     </div>
	 </div>
	</div>
	<script type="text/javascript">
		window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/72x72\/", "ext":".png", "svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/svg\/", "svgExt":".svg", "source":{"concatemoji":"http:\/\/localhost\/clientes\/gainholer\/html\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.8"}};
		!function(a, b, c){function d(a){var b, c, d, e, f = String.fromCharCode; if (!k || !k.fillText)return!1; switch (k.clearRect(0, 0, j.width, j.height), k.textBaseline = "top", k.font = "600 32px Arial", a){case"flag":return k.fillText(f(55356, 56826, 55356, 56819), 0, 0), b = j.toDataURL(), k.clearRect(0, 0, j.width, j.height), k.fillText(f(55356, 56826, 8203, 55356, 56819), 0, 0), c = j.toDataURL(), b === c && (k.clearRect(0, 0, j.width, j.height), k.fillText(f(55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447), 0, 0), b = j.toDataURL(), k.clearRect(0, 0, j.width, j.height), k.fillText(f(55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447), 0, 0), c = j.toDataURL(), b !== c); case"emoji4":return k.fillText(f(55358, 56794, 8205, 9794, 65039), 0, 0), d = j.toDataURL(), k.clearRect(0, 0, j.width, j.height), k.fillText(f(55358, 56794, 8203, 9794, 65039), 0, 0), e = j.toDataURL(), d !== e}return!1}function e(a){var c = b.createElement("script"); c.src = a, c.defer = c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)}var f, g, h, i, j = b.createElement("canvas"), k = j.getContext && j.getContext("2d"); for (i = Array("flag", "emoji4"), c.supports = {everything:!0, everythingExceptFlag:!0}, h = 0; h < i.length; h++)c.supports[i[h]] = d(i[h]), c.supports.everything = c.supports.everything && c.supports[i[h]], "flag" !== i[h] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[i[h]]); c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function(){c.DOMReady = !0}, c.supports.everything || (g = function(){c.readyCallback()}, b.addEventListener?(b.addEventListener("DOMContentLoaded", g, !1), a.addEventListener("load", g, !1)):(a.attachEvent("onload", g), b.attachEvent("onreadystatechange", function(){"complete" === b.readyState && c.readyCallback()})), f = c.source || {}, f.concatemoji?e(f.concatemoji):f.wpemoji && f.twemoji && (e(f.twemoji), e(f.wpemoji)))}(window, document, window._wpemojiSettings);
	</script>
	<style type="text/css">
	    img.wp-smiley,
	    img.emoji {
	        display: inline !important;
	        border: none !important;
	        box-shadow: none !important;
	        height: 1em !important;
	        width: 1em !important;
	        margin: 0 .07em !important;
	        vertical-align: -0.1em !important;
	        background: none !important;
	        padding: 0 !important;
	    }
	</style>
	<script>
		$(document).ready(function(){

			$("#process1").show();
			$("#process2").hide();
			$("#process2maritmo").hide();
			$("#process2rodoviario").hide();
			$("#process2aereo").hide();
			$("#process3aereo").hide();
			$("#process3maritmo").hide();
			$("#process3rodoviario").hide();
			$("#process4aereo").hide();
			$("#process5").hide();
			$("#process51").hide();
			$("#final").hide();

			$("#message").hide();
			$("#btaereo").hide();
			$("#btnavio").hide();
			$("#btrodoviario").hide();
			$("#process5aereo").hide();
			$("#process4maritmo").hide();
			$("#process4rodoviario").hide();
			$("#process5maritmo").hide();
			$("#process5rodoviario").hide();
			$("#process6maritmo").hide();
			$("#process6rodoviario").hide();
			$("#processfinalaereo").hide();
			$("#processfinalmaritmo").hide();
			$("#processfinalrodoviario").hide();

			$("#continuar001").click(function(){
				$("#process1").hide();
				$("#process2").show();
				$("#process3").hide();
				$("#process4").hide();
				$("#process5").hide();
				$("#final").hide();
			});


			$("#continuar02").click(function(){
				$("#process1").hide();
				$("#process2").hide();
				$("#process3").show();
				$("#process4").hide();
				$("#process5").hide();
				$("#final").hide();
			});


			$("#continuar03").click(function(){
				$("#process1").hide();
				$("#process2").hide();
				$("#process3").hide();
				$("#process4").show();
				$("#process5").hide();
				$("#final").hide();
			});

			$("#continuar04").click(function(){
				$("#process1").hide();
				$("#process2").hide();
				$("#process3").hide();
				$("#process4").hide();
				$("#process5").show();
				$("#final").hide();
			});

			$("#voltar01").click(function(){
				$("#process1").show();
				$("#process2").hide();
				$("#process3").hide();
				$("#process4").hide();
				$("#process5").hide();
				$("#final").hide();
			});


			$("#voltaraereo2").click(function(){
				$("#process1").show();
				$("#process2aereo").hide();

			});
			$("#voltarmaritmo2").click(function(){
				$("#process1").show();
				$("#process2maritmo").hide();

			});
			$("#voltarrodoviario2").click(function(){
				$("#process1").show();
				$("#process2rodoviario").hide();

			});
			$("#voltaraereo3").click(function(){
				$("#process2aereo").show();
				$("#process3aereo").hide();

			});
			$("#voltarmaritmo3").click(function(){
				$("#process2maritmo").show();
				$("#process3maritmo").hide();

			});
			$("#voltarrodoviario3").click(function(){
				$("#process2rodoviario").show();
				$("#process3rodoviario").hide();

			});
			$("#voltaraereo4").click(function(){
				$("#process3aereo").show();
				$("#process4aereo").hide();

			});
			$("#voltarmaritmo4").click(function(){
				$("#process3maritmo").show();
				$("#process4maritmo").hide();

			});
			$("#voltarrodoviario4").click(function(){
				$("#process3rodoviario").show();
				$("#process4rodoviario").hide();

			});
			$("#voltaraereo5").click(function(){
				$("#process4aereo").show();
				$("#process5aereo").hide();

			});
			$("#voltarmaritmo5").click(function(){
				$("#process4maritmo").show();
				$("#process5maritmo").hide();

			});
			$("#voltarrodoviario5").click(function(){
				$("#process4rodoviario").show();
				$("#process5rodoviario").hide();

			});
			$("#voltaraereo6").click(function(){
				$("#process5aereo").show();
				$("#processfinalaereo").hide();

			});
			$("#voltarmaritmo6").click(function(){
				$("#process5maritmo").show();
				$("#processfinalmaritmo").hide();

			});
			$("#voltarrodoviario6").click(function(){
				$("#process5rodoviario").show();
				$("#process6rodoviario").hide();

			});
			$("#voltarrodoviario7").click(function(){
				$("#process6rodoviario").show();
				$("#processfinalrodoviario").hide();

			});

			$("#voltar03").click(function(){
				$("#process1").hide();
				$("#process2").hide();
				$("#process3").show();
				$("#process4").hide();
				$("#process5").hide();
				$("#final").hide();
			});

			$("#voltar04").click(function(){
				$("#process1").hide();
				$("#process2").hide();
				$("#process3").hide();
				$("#process4").show();
				$("#process5").hide();
				$("#final").hide();
			});

			$("#enviaraero").click(function(){
				$("#processfinalaero").hide();
				$("#process1").hide();
				$("#process2").hide();
				$("#final").show();
				$("#processfinalaereo").hide();
			});
			$("#enviarmaritmo").click(function(){
				$("#processfinalmaritmo").hide();

				$("#final").show();
				$("#processfinalaereo").hide();
			});

			$("#enviarrodoviario").click(function(){
				$("#processfinalrodoviario").hide();

				$("#final").show();

			});


			$(".what0").click(function(){
				$("#message").show();
			});
			$("#what01").click(function(){
				$("#message1").show();
			});
			$("#what02").click(function(){
				$("#message").show();
			});


			$("#continuarnulo").click(function(){
				alert("Selecione alguma opção");

			});

			$("#btaereo").click(function(){

				$("#process2aereo").show();
				$("#process1").hide();

			});
			$("#btaereo2").click(function(){

				$("#process3aereo").show();
				$("#process2aereo").hide();

			});
			$("#btaereo3").click(function(){
				$("#process4aereo").show();
				$("#process3aereo").hide();
				$("#process2aereo").hide();

			});

			$("#btaereo4").click(function(){
				$("#process5aereo").show();
				$("#process4aereo").hide();
				$("#process3aereo").hide();
				$("#process2aereo").hide();

			});


			$("#btaereo5").click(function(){
				$("#processfinalaereo").show();
				$("#process4aereo").hide();
				$("#process5aereo").hide();
				$("#process3aereo").hide();
				$("#process2aereo").hide();

			});

			$("#btmaritmo2").click(function(){
				$("#process3maritmo").show();
				$("#process2maritmo").hide();
				$("#process5aereo").hide();
				$("#process3aereo").hide();
				$("#process2aereo").hide();

			});

			$("#btmaritmo3").click(function(){
				$("#process4maritmo").show();
				$("#process3maritmo").hide();
			});


			$("#btmaritmo4").click(function(){
				$("#process5maritmo").show();
				$("#process4maritmo").hide();
			});
			$("#btmaritmo5").click(function(){
				$("#process6maritmo").show();
				$("#process5maritmo").hide();
			});
			$("#btmaritmo6").click(function(){
				$("#processfinalmaritmo").show();
				$("#process6maritmo").hide();
			});





			$("#btnavio").click(function(){
				$("#process2maritmo").show();
				$("#process1").hide();


			});
			$("#btrodoviario").click(function(){
				$("#process2rodoviario").show();
				$("#process1").hide();
			});

			$("#btrodoviario2").click(function(){
				$("#process3rodoviario").show();
				$("#process2rodoviario").hide();
			});


			$("#btrodoviario3").click(function(){
				$("#process4rodoviario").show();
				$("#process3rodoviario").hide();
			});

			$("#btrodoviario4").click(function(){
				$("#process5rodoviario").show();
				$("#process4rodoviario").hide();
			});

			$("#btrodoviario5").click(function(){
				$("#process6rodoviario").show();
				$("#process5rodoviario").hide();
			});
			$("#btrodoviario6").click(function(){
				$("#processfinalrodoviario").show();
				$("#process6rodoviario").hide();
			});



		});
	</script>	
	<script>
		function setText(){

		var x=document.getElementById('option1')
		value = x.options[x.selectedIndex].value

		if (value == '0'){
		   document.getElementById('botaoinicial').style.display = 'block';  
		   document.getElementById('btaereo').style.display = 'none';
		   document.getElementById('btnavio').style.display = 'none';     
		   document.getElementById('btrodoviario').style.display = 'none';      
		}

		if (value == '1'){
		   document.getElementById('btaereo').style.display = 'block';
		   document.getElementById('botaoinicial').style.display = 'none';  
		   document.getElementById('btnavio').style.display = 'none';     
		   document.getElementById('btrodoviario').style.display = 'none';      
		}

		   
		if (value == '2'){
		   document.getElementById('btnavio').style.display = 'inline';
		   document.getElementById('botaoinicial').style.display = 'none';     
		   document.getElementById('btaereo').style.display = 'none';      
		   document.getElementById('btrodoviario').style.display = 'none';   
		}

		   
		   if (value == '3'){
		   document.getElementById('btrodoviario').style.display = 'inline';
		   document.getElementById('botaoinicial').style.display = 'none';
		      document.getElementById('btaereo').style.display = 'none';      
		   document.getElementById('btnavio').style.display = 'none'; 
		}
		}
	</script>
	<script type='text/javascript'>
	    /* <![CDATA[ */
	    var wpcf7 = {"apiSettings":{"root":"http:\/\/localhost\/clientes\/gainholer\/html\/wp-json\/", "namespace":"contact-form-7\/v1"}, "recaptcha":{"messages":{"empty":"Verifique se voc\u00ea n\u00e3o \u00e9 um rob\u00f4."}}};
	    /* ]]> */
	</script>
	<script>
	    //REALIZAR BUSCA
	    $(".linkTermos").click(function(e){

	        $(".modalTermos").show();

	    });
	    $(".modalTermos").click(function(e){

	        $(this).fadeOut();

	    });
	    $(".closeModal").click(function(e){

	        $(".modalTermos").fadeOut();

	    });
	</script>